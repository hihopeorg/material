# Material

本项目是基于开源项目Material进行ohos化的移植和开发的，可以通过项目标签以及github地址( https://github.com/rey5137/material )追踪到原项目版本

### 项目介绍

- 项目名称：Material风格的UI控件库
- 所属系列：ohos的第三方组件适配移植
- 功能：支持自定义的ProgressView、Button、Switch、Slider、SnackBar等UI控件
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/rey5137/material
- 原项目基线版本：V1.3.0
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/Material.gif)

### 安装教程

方法1.
1. 下载har包Material.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用
```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'Material', ext: 'har')
	……
}
```
方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.github.rey5137.ohos:material:1.0.1'
}
```

### 使用说明

#### Progress
你可以按下面这样在布局文件里添加com.rey.material.widget.ProgressView：
```xml
<com.rey.material.widget.ProgressView
    ohos:id="$+id:progress_pv_circular"
    ohos:height="32vp"
    ohos:width="32vp"
    ohos:layout_alignment="center"
    app:cpd_strokeColor="#FF03A9F4"
    app:pv_autostart="false"
    app:pv_circular="true"
    app:pv_progressMode="1"/>
```
自定义属性：
* `pv_progressMode` - The mode of progress indicator. There are 4 modes: indeterminate（0）, determinate（1）, query（2） and buffer（3）
* `pv_circular` -  Set true to show circular progress, false to show linear progress.
* `pv_autostart` - If true, the progress will auto start when it becomes visible and stop when it's hided.
* `pv_progress` - The progress value in [0..1]
* `pv_secondaryProgress` - The secondary progress value in [0..1] (use in buffer mode).
* `pv_progressStyle` - The style of progress drawable. Depend on "pv_circular" attribute, it'll create a CircularProgressDrawable or LinearProgressDrawable.
  
##### CircularProgressDrawable

CircularProgressDrawable 仅支持2中模式: indeterminate and determinate.

自定义属性:
* `cpd_padding` - The padding of progress with the boundary.
* `cpd_initialAngle` - The start angle of progress [0..360]
* `cpd_maxSweepAngle` - The maximum sweep angle of progress.
* `cpd_minSweepAngle` - The minimum sweep angle of progress.
* `cpd_strokeSize` - The stroke's size of progress.
* `cpd_strokeColor` - The stroke's color of progress.
* `cpd_strokeSecondaryColor` - The stroke's color of secondary progress.
* `cpd_strokeColors` - The array of colors will be used as stroke's color (for indeterminate mode).
* `cpd_reverse` - If true, progress will rotate counter-clockwise.
* `cpd_inStepColors` - The array of colors will be shown in start animation.
* `cpd_inStepPercent` - the percent each step color will take.

##### LinearProgressDrawable

自定义属性:
* `lpd_maxLineWidth` - The maximum width of progress (can be dimension or fraction).
* `lpd_minLineWidth` - The minimum width of progress (can be dimension or fraction).
* `lpd_strokeSize` - The stroke's size of progress.
* `lpd_strokeColor` - The stroke's color of progress.
* `lpd_strokeSecondaryColor` - The stroke's color of secondary progress.
* `lpd_strokeColors` - The list of stroke's color (for indeterminate mode).
* `lpd_reverse` - If true, progress will run from right to left.
* `lpd_keepDuration` - the time it keep the progress's width at minLineWidth and maxLineWidth.
* `lpd_verticalAlign` - the vertical align of progress with boundary. There're 3 values: top, center and bottom.

#### Button

com.rey.material.widget.Button是继承自原生Button的自定义控件，并且支持点击时水波纹效果：
```xml
<com.rey.material.widget.Button
    ohos:id="$+id:button_bt_flat"
    ohos:width="150vp"
    ohos:height="36vp"
    ohos:text="BUTTON"
    ohos:text_size="15fp"
    ohos:text_color="#de000000"
    ohos:theme="$pattern:button"
    app:rd_cornerRadius="4vp"
    app:rd_enable="true"/>
```
自定义属性：
* `rd_rippleType` - The type of ripple. There are 2 types: touch(0) and wave(1).
* `rd_delayClick` - There are 3 types: none (0), untilRelease (1) and afterRelease (2).
* `rd_delayRipple` - Delay starting ripple animation. Default is 0.
* `rd_backgroundColor` - The background color of ripple's layer (used in touch ripple tyle).
* `rd_maxRippleRadius` - The maximum ripple radius (used in touch ripple tyle).
* `rd_rippleColor` - The color of ripple.
* `rd_maskType` - The mask's type of ripple.
* `rd_cornerRadius` - The corner's radius of mask.
* `rd_topLeftCornerRadius` - The top left corner's radius of mask.
* `rd_topRightCornerRadius` - The top right corner's radius of mask.
* `rd_bottomLeftCornerRadius` - The bottom left corner's radius of mask.
* `rd_bottomRightCornerRadius` - The bottom right corner's radius of mask.
* `rd_padding` - The padding of mask.
* `rd_leftPadding` - The left padding of mask.
* `rd_topPadding` - The top padding of mask.
* `rd_rightPadding` - The right padding of mask.
* `rd_bottomPadding` - The bottom padding of mask.

#### FloatingActionButton
自定义属性:
* `fab_backgroundColor` - The background color of this view.
* `fab_radius` - The radius of this view.
* `fab_elevation` - The elevation of this view.
* `fab_iconSize` - The size of icon drawable.
* `fab_iconSrc` - The drawable used as icon.
* `fab_iconLineMorphing` - The style of LineMorphingDrawable will be used as icon.

#### CheckBox

```xml
<com.rey.material.widget.CheckBox
    ohos:height="30vp"
    ohos:width="140vp"
    ohos:theme="$pattern:checkbox"
    ohos:text="CheckBox_1"
    ohos:text_size="17fp"
    ohos:layout_alignment="horizontal_center"
    ohos:top_margin="15vp"
    app:cbd_checked="true"/>
```
自定义属性:
* `cbd_width` - The width of the CheckBoxDrawable.
* `cbd_height` - The height of the CheckBoxDrawable.
* `cbd_boxSize` - The size of square box.
* `cbd_cornerRadius` - The corner radius of box.
* `cbd_strokeSize` - The size of stroke.
* `cbd_strokeColor` - The color of stroke. Accept Color value or ColorStateList resource.
* `cbd_tickColor` - The color of tick mark.
* `cbd_checked` - checked state.

#### RadioButton

```xml
<com.rey.material.widget.RadioButton
    ohos:height="30vp"
    ohos:width="160vp"
    ohos:text="RadioButton_1"
    ohos:text_size="17fp"
    ohos:layout_alignment="horizontal_center"
    ohos:top_margin="10vp"
    app:rbd_checked="true"/>
```
自定义属性:
* `rbd_width` - The width of the RadioButtonDrawable.
* `rbd_height` - The height of the RadioButtonDrawable.
* `rbd_radius` - The radius of the box.
* `rbd_innerRadius` - The radius of tick box.
* `rbd_strokeSize` - The size of stroke.
* `rbd_strokeColor` - The color of stroke. Accept Color value or ColorStateList resource.
* `rbd_checked` - checked state.

#### Switch

```xml
<com.rey.material.widget.Switch
    ohos:height="50vp"
    ohos:width="50vp"
    ohos:top_margin="10vp"
    ohos:layout_alignment="horizontal_center"
    ohos:theme="$pattern:switch_box"
    app:sw_checked="true"
/>
```
自定义属性:
* `sw_trackSize` - The stroke's width of track bar.
* `sw_trackColor` - The color of track bar. Accept Color value or ColorStateList resource.
* `sw_trackCap` - The cap setting of track bar. There're 3 types: butt, round and square.
* `sw_thumbColor` - The color of thumb. Accept Color value or ColorStateList resource.
* `sw_thumbRadius` - The radius of thumb.
* `sw_thumbElevation` - The elevation of thumb.
* `sw_checked` - checked state.

#### Slider

```xml
<com.rey.material.widget.Slider
    ohos:id="$+id:slider_sl_continuous"
    ohos:height="20vp"
    ohos:width="match_parent"
    ohos:padding="16vp"
    ohos:top_margin="35vp"
    ohos:theme="$pattern:Slider"
    app:sl_primaryColor="#FF03A9F4"
    app:sl_value="50"/>
```
自定义属性:    
* `sl_trackSize` - The stroke's width of the track bar.
* `sl_trackCap` - The cap setting of track bar.
* `sl_primaryColor` - The foreground color of track bar and thumb.
* `sl_secondaryColor` - The background color of track bar.
* `sl_thumbBorderSize` - The border's width of thumb.
* `sl_thumbRadius` - The radius of thumb.
* `sl_thumbFocusRadius` - The radius of thumb when focused.
* `sl_thumbTouchRadius` - The radius of touch region. if not set will use sl_thumbRadius.
* `sl_travelAnimDuration` - The max time the thumb takes to move from left to right side.
* `sl_transformAnimDuration` - The duration of animation when thumb switch to focused state.
* `sl_minValue` - The min value of slider.
* `sl_maxValue` - The max value of slider.
* `sl_stepValue` - The step value of slider (used in discrete mode).
* `sl_value` - The current value of slider.
* `sl_discreteMode` - If true, slider is in discrete mode, false and slider is in continuous mode.
* `sl_textSize` - The size of text (used in discrete mode).
* `sl_textColor` - The color of text (used in discrete mode).
* `sl_alwaysFillThumb` - Indicate that should always fill thumb at min value or not.

#### SnackBar

你可以在布局里添加SnackBar：
```xml   
<com.rey.material.widget.SnackBar
    ohos:id="$+id:main_sn"
    ohos:height="48vp"
    ohos:width="match_parent"
    ohos:layout_alignment="bottom"
    ohos:text_alignment="horizontal_center|bottom"
    ohos:visibility="invisible"/>
```
或者在代码里初始化:
```java
    mSnackBar.applyStyle(ResourceTable.Pattern_SnackBarSingleLine);
    mSnackBar.text("This is a SnackBar")
        .singleLine(true)
        .actionText("CLOSE")
        .actionClickListener(new SnackBar.OnActionClickListener(){...})
        .duration(1000)
        .show();
```
自定义属性: 
* `sb_backgroundColor` - The background color of SnackBar.
* `sb_backgroundCornerRadius` - The corner's radius of SnackBar.
* `sb_horizontalPadding` - The horizontal padding between background and text.
* `sb_verticalPadding` - The vertical padding between background and text.
* `sb_width` - The width of SnackBar. Can be dimension value or match_parent or wrap_content enum.
* `sb_minWidth` - The minimum width of SnackBar.
* `sb_maxWidth` - The maximum width of SnackBar.
* `sb_height` - The height of SnackBar. Can be dimension value or match_parent or wrap_content enum.
* `sb_minHeight` - The minimum height of SnackBar.
* `sb_maxHeight` - The maximum height of SnackBar.
* `sb_marginStart` - The start margin of SnackBar with parent view.
* `sb_marginBottom` - The bottom margin of SnackBar with parent view.
* `sb_textSize` - The size of message text.
* `sb_textColor` - The color of message text.
* `sb_text` - The message text.
* `sb_singleLine` - Indicate that message text should be show as single-line or not.
* `sb_maxLines` - The maximum lines of message text.
* `sb_lines` - The line number of message text.
* `sb_ellipsize` - The ellipsize setting of message text.
* `sb_actionTextSize` - The size of action text.
* `sb_actionTextColor` - The color of action text.
* `sb_actionText` - The action text. Empty text mean hide action button.
* `sb_actionRipple` - The ripple style for action button.
* `sb_duration` - The duration the SnackBar will be shown.

#### TimePickerLayout

```xml    
<com.rey.material.widget.TimePickerLayout
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:theme="$pattern:timePick"/>
```
自定义属性: 
* `tp_backgroundColor` - The color of background circle.
* `tp_selectionColor` - The color of selection circle.
* `tp_selectionRadius` - The radius of selection circle.
* `tp_tickSize` - The size of tick's stroke.
* `tp_textSize` - The size of text.
* `tp_textColor` - The color of text.
* `tp_textHighlightColor` - The color of text when is highlighted.
* `tp_animDuration` - The duration of animations.
* `tp_mode` - The current select mode of time picker. There're 2 modes: hour and minute.
* `tp_24Hour` - Indicate should show hour in 24 hours format or not.
* `tp_hour` - The current hour (in 24 hours format).
* `tp_minute` - The current minute.
* `tp_headerHeight` - The height of header view that shows the time text.
* `tp_textTimeColor` - The color of time text.
* `tp_textTimeSize` - The size of time text.
* `tp_am` - The custom label for AM button.
* `tp_pm` - The custom label for PM button.
* `tp_leadingZero` - Indicate that should show hour value in leading zero format.

#### 版本迭代

- v1.0.1

#### 版权和许可信息

- Apache Licence
