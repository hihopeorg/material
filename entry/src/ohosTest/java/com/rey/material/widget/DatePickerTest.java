package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;

public class DatePickerTest extends TestCase {

    public void testSetContentPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setContentPadding(25,25,25,25);
        Field paddingLeft = datePicker.getClass().getDeclaredField("mPaddingLeft");
        paddingLeft.setAccessible(true);
        assertEquals(25, paddingLeft.get(datePicker));
        Field paddingRight = datePicker.getClass().getDeclaredField("mPaddingRight");
        paddingRight.setAccessible(true);
        assertEquals(25, paddingRight.get(datePicker));
        Field paddingTop = datePicker.getClass().getDeclaredField("mPaddingTop");
        paddingTop.setAccessible(true);
        assertEquals(25, paddingTop.get(datePicker));
        Field paddingBottom = datePicker.getClass().getDeclaredField("mPaddingBottom");
        paddingBottom.setAccessible(true);
        assertEquals(25, paddingBottom.get(datePicker));
    }

    public void testSetDateRange() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setDateRange(25,1,2021,30, 12, 2049);
        Field adapter = datePicker.getClass().getDeclaredField("mAdapter");
        adapter.setAccessible(true);
        Object adapterValue = adapter.get(datePicker);
        Field minDay = adapterValue.getClass().getDeclaredField("mMinDay");
        minDay.setAccessible(true);
        assertEquals(25, minDay.get(adapterValue));
        Field minMonth = adapterValue.getClass().getDeclaredField("mMinMonth");
        minMonth.setAccessible(true);
        assertEquals(1, minMonth.get(adapterValue));
        Field minYear = adapterValue.getClass().getDeclaredField("mMinYear");
        minYear.setAccessible(true);
        assertEquals(2021, minYear.get(adapterValue));
        Field maxDay = adapterValue.getClass().getDeclaredField("mMaxDay");
        maxDay.setAccessible(true);
        assertEquals(30, maxDay.get(adapterValue));
        Field maxMonth = adapterValue.getClass().getDeclaredField("mMaxMonth");
        maxMonth.setAccessible(true);
        assertEquals(12, maxMonth.get(adapterValue));
        Field maxYear = adapterValue.getClass().getDeclaredField("mMaxYear");
        maxYear.setAccessible(true);
        assertEquals(2049, maxYear.get(adapterValue));

    }

    public void testGoTo() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.goTo(6,2021);
        Field handler = datePicker.getClass().getDeclaredField("mHandler");
        handler.setAccessible(true);
        assertNotNull(handler.get(datePicker));
    }

    public void testPostSetSelectionFromTop() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.postSetSelectionFromTop(6,0);
        Field handler = datePicker.getClass().getDeclaredField("mHandler");
        handler.setAccessible(true);
        assertNotNull(handler.get(datePicker));
    }

    public void testSetDate() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setDate(6,6, 2020);
        Field adapter = datePicker.getClass().getDeclaredField("mAdapter");
        adapter.setAccessible(true);
        Object adapterValue = adapter.get(datePicker);
        Field mDay = adapterValue.getClass().getDeclaredField("mDay");
        mDay.setAccessible(true);
        assertEquals(6, mDay.get(adapterValue));
        Field mMonth = adapterValue.getClass().getDeclaredField("mMonth");
        mMonth.setAccessible(true);
        assertEquals(6, mMonth.get(adapterValue));
        Field mYear = adapterValue.getClass().getDeclaredField("mYear");
        mYear.setAccessible(true);
        assertEquals(2020, mYear.get(adapterValue));
    }

    public void testGetDay() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setDate(6,6, 2020);
        assertEquals(6, datePicker.getDay());
    }

    public void testGetMonth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setDate(6,6, 2020);
        assertEquals(6, datePicker.getMonth());
    }

    public void testGetYear() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        datePicker.setDate(6,6, 2020);
        assertEquals(2020, datePicker.getYear());
    }

    public void testGetFormattedDate() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        String date = datePicker.getFormattedDate(SimpleDateFormat.getDateInstance());
        assertNotNull(date);
    }

    public void testGetSelectionColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(0xFF000000, datePicker.getSelectionColor());
    }

    public void testGetTextSize() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(36, datePicker.getTextSize());
    }

    public void testGetTextColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(0xFF000000, datePicker.getTextColor());
    }

    public void testGetTextLabelColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(0xFF767676, datePicker.getTextLabelColor());
    }

    public void testGetTextHighlightColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(0xFFFFFFFF, datePicker.getTextHighlightColor());
    }

    public void testGetTextDisableColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertEquals(0, datePicker.getTextDisableColor());
    }

    public void testGetCalendar() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePicker datePicker = new DatePicker(context);
        assertNotNull(datePicker.getCalendar());
    }
}