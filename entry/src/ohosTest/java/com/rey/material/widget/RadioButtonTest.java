package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;

public class RadioButtonTest extends TestCase {

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        RadioButton radioButton = new RadioButton(context, null);
        radioButton.start();
        Field field = radioButton.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(true, field.get(radioButton));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        RadioButton radioButton = new RadioButton(context, null);
        radioButton.stop();
        Field field = radioButton.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(false, field.get(radioButton));
    }

    public void testIsRunning() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        RadioButton radioButton = new RadioButton(context, null);
        radioButton.start();
        assertEquals(true, radioButton.isRunning());
        radioButton.stop();
        assertEquals(false, radioButton.isRunning());
    }
}