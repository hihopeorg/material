package com.rey.material.widget;

import com.rey.material.ResourceTable;
import com.rey.material.utils.AttrUtils;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FloatingActionButtonTest extends TestCase {

    public void testGetRadius() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatingActionButton floatingActionButton = new FloatingActionButton(context, null);
        floatingActionButton.setRadius(10);
        assertEquals(10, floatingActionButton.getRadius());
    }

    public void testGetIcon() throws IOException, NotExistException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        FloatingActionButton floatingActionButton = new FloatingActionButton(context, null);
        Element element = new PixelMapElement(context.getResourceManager().getResource(ResourceTable.Media_ic_autorenew_white_24dp));
        floatingActionButton.setIcon(element, false);
        assertEquals(element, floatingActionButton.getIcon());
    }
}