package com.rey.material.widget;

import com.rey.material.utils.AttrUtils;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SliderTest extends TestCase {

    public void testGetMinValue() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setValueRange(10,80, true);
        assertEquals(10, slider.getMinValue());
    }

    public void testGetMaxValue() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setValueRange(10,80, true);
        assertEquals(80, slider.getMaxValue());
    }

    public void testGetStepValue() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int stepValue = 2;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(AttrUtils.createAttr("sl_stepValue", "2", stepValue, AttrUtils.AttrType.INT, context));
        Slider slider = new Slider(context, AttrUtils.createAttrSet(attrs));
        assertEquals(2, slider.getStepValue());
    }

    public void testGetValue() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setPosition(.5f, true);
        assertEquals(50, slider.getValue());
    }

    public void testGetExactValue() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setPosition(.5f, true);
        assertEquals(50.0f, slider.getExactValue());
    }

    public void testGetPosition() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setPosition(.5f, true);
        assertEquals(.5f, slider.getPosition());
    }

    public void testGetMinWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int thumbRadius = 5;
        int thumbFocusRadius = 5;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(AttrUtils.createAttr("sl_thumbRadius", "5vp", thumbRadius, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(AttrUtils.createAttr("sl_thumbFocusRadius", "5vp", thumbFocusRadius, AttrUtils.AttrType.DIMENSION, context));
        Slider slider = new Slider(context, AttrUtils.createAttrSet(attrs));
        assertEquals(20, slider.getMinWidth());
    }

    public void testGetMinHeight() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int thumbRadius = 5;
        int thumbFocusRadius = 5;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(AttrUtils.createAttr("sl_thumbRadius", "5vp", thumbRadius, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(AttrUtils.createAttr("sl_thumbFocusRadius", "5vp", thumbFocusRadius, AttrUtils.AttrType.DIMENSION, context));
        Slider slider = new Slider(context, AttrUtils.createAttrSet(attrs));
        assertEquals(10, slider.getMinHeight());
    }

    public void testSetPrimaryColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setPrimaryColor(Color.BLUE.getValue());
        Field field = slider.getClass().getDeclaredField("mPrimaryColor");
        field.setAccessible(true);
        assertEquals(Color.BLUE.getValue(), field.get(slider));
    }

    public void testSetSecondaryColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setSecondaryColor(Color.BLUE.getValue());
        Field field = slider.getClass().getDeclaredField("mSecondaryColor");
        field.setAccessible(true);
        assertEquals(Color.BLUE.getValue(), field.get(slider));
    }

    public void testSetAlwaysFillThumb() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Slider slider = new Slider(context);
        slider.setAlwaysFillThumb(true);
        Field field = slider.getClass().getDeclaredField("mAlwaysFillThumb");
        field.setAccessible(true);
        assertEquals(true, field.get(slider));
    }
}