package com.rey.material.widget;

import com.rey.material.widget.ButtonBox;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;

public class ButtonBoxTest extends TestCase {

    public void testSetDelayClickType() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.setDelayClickType(1);
        Field field = button.getClass().getDeclaredField("mDelayClickType");
        field.setAccessible(true);
        assertEquals(1, field.get(button));
    }

    public void testCancel() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.cancel();
        Field field = button.getClass().getDeclaredField("mState");
        field.setAccessible(true);
        assertEquals(0, field.get(button));
    }

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.start();
        Field field = button.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(true, field.get(button));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.stop();
        Field field = button.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(false, field.get(button));
    }

    public void testPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.padding(10);
        Field field = button.getClass().getDeclaredField("mMaskLeft");
        field.setAccessible(true);
        assertEquals(10, field.get(button));
    }

    public void testCornerRadius() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ButtonBox button = new ButtonBox(context, null);
        button.cornerRadius(10);
        Field field = button.getClass().getDeclaredField("mMaskTopLeftCornerRadius");
        field.setAccessible(true);
        assertEquals(10, field.get(button));
    }
}