package com.rey.material.widget;

import com.rey.material.utils.AttrUtils;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class SwitchTest extends TestCase {

    public void testSetChecked() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Switch button = new Switch(context);
        button.setChecked(false);
        Field field = button.getClass().getDeclaredField("mChecked");
        field.setAccessible(true);
        assertEquals(false, field.get(button));
    }

    public void testSetCheckedImmediately() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Switch button = new Switch(context);
        button.setCheckedImmediately(false);
        Field field = button.getClass().getDeclaredField("mChecked");
        field.setAccessible(true);
        assertEquals(false, field.get(button));
    }

    public void testIsChecked() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Switch button = new Switch(context);
        button.setChecked(false);
        assertEquals(false, button.isChecked());
    }

    public void testToggle() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        Switch button = new Switch(context);
        button.toggle();
        assertEquals(true, button.isChecked());
        button.toggle();
        assertEquals(false, button.isChecked());
    }

    public void testGetMinWidth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int thumbRadius = 30;
        int shadowSize = 6;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(AttrUtils.createAttr("sw_thumbRadius", "10vp", thumbRadius, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(AttrUtils.createAttr("sw_thumbElevation", "2vp", shadowSize, AttrUtils.AttrType.DIMENSION, context));
        Switch button = new Switch(context, AttrUtils.createAttrSet(attrs));
        assertEquals(132, button.getMinWidth());
    }

    public void testGetMinHeight() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        int thumbRadius = 30;
        int shadowSize = 6;
        List<Attr> attrs = new ArrayList<>();
        attrs.add(AttrUtils.createAttr("sw_thumbRadius", "10vp", thumbRadius, AttrUtils.AttrType.DIMENSION, context));
        attrs.add(AttrUtils.createAttr("sw_thumbElevation", "2vp", shadowSize, AttrUtils.AttrType.DIMENSION, context));
        Switch button = new Switch(context, AttrUtils.createAttrSet(attrs));
        assertEquals(72, button.getMinHeight());
    }
}