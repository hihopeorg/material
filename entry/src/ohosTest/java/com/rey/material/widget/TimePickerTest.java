package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

public class TimePickerTest extends TestCase {

    public void testGetBackgroundColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(1073741824, timePicker.getBackgroundColor());
    }

    public void testGetSelectionColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(0xFF000000, timePicker.getSelectionColor());
    }

    public void testGetTextSize() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(12, timePicker.getTextSize());
    }

    public void testGetTextColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(0xFF000000, timePicker.getTextColor());
    }

    public void testGetTextHighlightColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(0xFFFFFFFF, timePicker.getTextHighlightColor());
    }

    public void testGetAnimDuration() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(400, timePicker.getAnimDuration());
    }

    public void testGetInInterpolator() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertNotNull(timePicker.getInInterpolator());
    }

    public void testGetOutInterpolator() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertNotNull(timePicker.getOutInterpolator());
    }

    public void testGetMode() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        assertEquals(TimePicker.MODE_HOUR, timePicker.getMode());
    }

    public void testGetHour() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        timePicker.setHour(6);
        assertEquals(6, timePicker.getHour());
    }

    public void testGetMinute() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        timePicker.setMinute(16);
        assertEquals(16, timePicker.getMinute());
    }

    public void testIs24Hour() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePicker timePicker = new TimePicker(context);
        timePicker.set24Hour(true);
        assertEquals(true, timePicker.is24Hour());
    }
}