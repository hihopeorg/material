package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.text.SimpleDateFormat;

public class TimePickerLayoutTest extends TestCase {

    public void testGetHour() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePickerLayout timePickerLayout = new TimePickerLayout(context, null);
        timePickerLayout.setHour(6);
        assertEquals(6, timePickerLayout.getHour());
    }

    public void testGetMinute() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePickerLayout timePickerLayout = new TimePickerLayout(context, null);
        timePickerLayout.setMinute(6);
        assertEquals(6, timePickerLayout.getMinute());
    }

    public void testGetFormattedTime() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        TimePickerLayout timePickerLayout = new TimePickerLayout(context, null);
        assertNotNull(timePickerLayout.getFormattedTime(SimpleDateFormat.getDateInstance()));
    }
}