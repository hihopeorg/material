package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import com.rey.material.widget.CheckBox;
import ohos.app.Context;

import java.lang.reflect.Field;

public class CheckBoxTest extends TestCase {

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CheckBox checkbox = new CheckBox(context, null);
        checkbox.start();
        Field field = checkbox.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(true, field.get(checkbox));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CheckBox checkbox = new CheckBox(context, null);
        checkbox.stop();
        Field field = checkbox.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(false, field.get(checkbox));
    }

    public void testIsRunning() throws NoSuchFieldException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CheckBox checkbox = new CheckBox(context, null);
        checkbox.stop();
        assertEquals(false, checkbox.isRunning());
    }
}