package com.rey.material.widget;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;

public class DatePickerLayoutTest extends TestCase {

    public void testGetDay() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePickerLayout datePickerLayout = new DatePickerLayout(context, null);
        datePickerLayout.setDate(1, 6, 2021);
        assertEquals(1, datePickerLayout.getDay());
    }

    public void testGetMonth() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePickerLayout datePickerLayout = new DatePickerLayout(context, null);
        datePickerLayout.setDate(1, 6, 2021);
        assertEquals(6, datePickerLayout.getMonth());
    }

    public void testGetYear() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePickerLayout datePickerLayout = new DatePickerLayout(context, null);
        datePickerLayout.setDate(1, 6, 2021);
        assertEquals(2021, datePickerLayout.getYear());
    }

    public void testGetFormattedDate() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePickerLayout datePickerLayout = new DatePickerLayout(context, null);
        assertNotNull(datePickerLayout.getFormattedDate(SimpleDateFormat.getDateInstance()));
    }

    public void testGetCalendar() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        DatePickerLayout datePickerLayout = new DatePickerLayout(context, null);
        assertNotNull(datePickerLayout.getCalendar());
    }
}