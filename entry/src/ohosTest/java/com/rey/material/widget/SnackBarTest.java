package com.rey.material.widget;

import com.rey.material.ResourceTable;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.lang.reflect.Field;

public class SnackBarTest extends TestCase {

    public void testMake() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        assertNotNull("create SnackBar failed", snackBar);
    }

    public void testText() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.text("test");
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        String text = ((Text)field.get(snackBar)).getText();
        assertEquals("test", text);
    }

    public void testTestText() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.text(ResourceTable.String_HelloWorld);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        String text = ((Text)field.get(snackBar)).getText();
        assertEquals(context.getString(ResourceTable.String_HelloWorld), text);
    }

    public void testTextColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.textColor(Color.BLUE.getValue());
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int color = ((Text)field.get(snackBar)).getTextColor().getValue();
        assertEquals(Color.BLUE.getValue(), color);
    }

    public void testTextSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.textSize(25);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int size = ((Text)field.get(snackBar)).getTextSize();
        assertEquals(25, size);
    }

    public void testEllipsize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.ellipsize(Text.TruncationMode.ELLIPSIS_AT_END);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        Text.TruncationMode size = ((Text)field.get(snackBar)).getTruncationMode();
        assertEquals(Text.TruncationMode.ELLIPSIS_AT_END, size);
    }

    public void testSingleLine() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.singleLine(true);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        boolean singleLine = ((Text)field.get(snackBar)).isMultipleLine();
        assertEquals(false, singleLine);
    }

    public void testMaxLines() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.maxLines(2);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int lines = ((Text)field.get(snackBar)).getMaxTextLines();
        assertEquals(2, lines);
    }

    public void testLines() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.lines(2);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int lines = ((Text)field.get(snackBar)).getMaxTextLines();
        assertEquals(2, lines);
    }

    public void testActionText() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.actionText("test");
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        String text = ((Button)field.get(snackBar)).getText();
        assertEquals("test", text);
    }

    public void testTestActionText() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.actionText(ResourceTable.String_HelloWorld);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        String text = ((Button)field.get(snackBar)).getText();
        assertEquals(context.getString(ResourceTable.String_HelloWorld), text);
    }

    public void testActionTextColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.actionTextColor(Color.BLUE.getValue());
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        int color = ((Button)field.get(snackBar)).getTextColor().getValue();
        assertEquals(Color.BLUE.getValue(), color);
    }

    public void testTestActionTextColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.actionTextColor(Color.BLUE);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        Color color = ((Button)field.get(snackBar)).getTextColor();
        assertEquals(Color.BLUE, color);
    }

    public void testActionTextSize() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.actionTextSize(25);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        int size = ((Button)field.get(snackBar)).getTextSize();
        assertEquals(25, size);
    }

    public void testDuration() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.duration(25);
        Field field = snackBar.getClass().getDeclaredField("mDuration");
        field.setAccessible(true);
        assertEquals(25L, field.get(snackBar));
    }

    public void testBackgroundColor() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.backgroundColor(Color.BLUE.getValue());
        Field field = snackBar.getClass().getDeclaredField("mBackground");
        field.setAccessible(true);
        RgbColor color = ((ShapeElement)field.get(snackBar)).getRgbColors()[0];
        assertEquals(RgbColor.fromArgbInt(Color.BLUE.getValue()), color);
    }

    public void testBackgroundRadius() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.backgroundRadius(25);
        Field field = snackBar.getClass().getDeclaredField("mBackground");
        field.setAccessible(true);
        float radius = ((ShapeElement)field.get(snackBar)).getCornerRadius();
        assertEquals(25F, radius);
    }

    public void testHorizontalPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.horizontalPadding(25);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        int paddingLeft = ((Button)field.get(snackBar)).getPaddingLeft();
        assertEquals(25, paddingLeft);
    }

    public void testVerticalPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.verticalPadding(25);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        int paddingTop = ((Button)field.get(snackBar)).getPaddingTop();
        assertEquals(25, paddingTop);
    }

    public void testPadding() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.padding(25, 30);
        Field field = snackBar.getClass().getDeclaredField("mAction");
        field.setAccessible(true);
        int paddingTop = ((Button)field.get(snackBar)).getPaddingTop();
        int paddingLeft = ((Button)field.get(snackBar)).getPaddingLeft();
        assertEquals(30, paddingTop);
        assertEquals(25, paddingLeft);
    }

    public void testWidth() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.width(30);
        Field field = snackBar.getClass().getDeclaredField("mWidth");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testMinWidth() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.minWidth(30);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int width = ((Text)field.get(snackBar)).getMinWidth();
        assertEquals(30, width);

    }

    public void testMaxWidth() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.maxWidth(30);
        Field field = snackBar.getClass().getDeclaredField("mText");
        field.setAccessible(true);
        int width = ((Text)field.get(snackBar)).getMaxTextWidth();
        assertEquals(30, width);
    }

    public void testHeight() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.height(30);
        Field field = snackBar.getClass().getDeclaredField("mHeight");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testMaxHeight() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.maxHeight(30);
        Field field = snackBar.getClass().getDeclaredField("mMaxHeight");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testMinHeight() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.minHeight(30);
        Field field = snackBar.getClass().getDeclaredField("mMinHeight");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testMarginStart() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.marginStart(30);
        Field field = snackBar.getClass().getDeclaredField("mMarginStart");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testMarginBottom() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.marginBottom(30);
        Field field = snackBar.getClass().getDeclaredField("mMarginBottom");
        field.setAccessible(true);
        assertEquals(30, field.get(snackBar));
    }

    public void testRemoveOnDismiss() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        snackBar.removeOnDismiss(false);
        Field field = snackBar.getClass().getDeclaredField("mRemoveOnDismiss");
        field.setAccessible(true);
        assertEquals(false, field.get(snackBar));
    }

    public void testGetState() throws NoSuchFieldException, IllegalAccessException {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SnackBar snackBar = SnackBar.make(context);
        Field field = snackBar.getClass().getDeclaredField("mState");
        field.setAccessible(true);
        field.set(snackBar, SnackBar.STATE_SHOWN);
        assertEquals(SnackBar.STATE_SHOWN, snackBar.getState());
    }
}