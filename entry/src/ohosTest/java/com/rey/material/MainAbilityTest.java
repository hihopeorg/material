package com.rey.material;

import com.rey.material.utils.EventHelper;
import com.rey.material.widget.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.lang.reflect.Field;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainAbilityTest {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01207, "materialTest");
    private static Ability ability = EventHelper.startAbility(MainAbility.class);

    public void stopThread(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String s) {
        try {
            Runtime.getRuntime().exec(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clickOne(Component buttonBox) {
        int width = buttonBox.getLocationOnScreen()[0] + buttonBox.getWidth() / 2;
        int height = buttonBox.getLocationOnScreen()[1] + buttonBox.getHeight() / 2;
        exec("input tap " + width + " " + height);
        stopThread(1000);
    }

    @Test
    public void test01Process() {
        Button process = (Button) ability.findComponentById(ResourceTable.Id_process);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, process);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        stopThread(500);
        Assert.assertTrue("页面显示失败", ability != currentTopAbility);
        ProgressView progress = (ProgressView) currentTopAbility.findComponentById(ResourceTable.Id_progress_pv_linear_buffer);
        stopThread(1000);
        float secondaryProgress1 = progress.getSecondaryProgress();
        float progress1 = progress.getProgress();
        stopThread(666);
        float secondaryProgress2 = progress.getSecondaryProgress();
        float progress2 = progress.getProgress();
        Assert.assertTrue("进度条未发生改变", secondaryProgress1 != secondaryProgress2 || progress1 != progress2);
        stopThread(3000);
    }

    @Test
    public void test02Button1() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ButtonBox buttonBoxA1 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_flat);
        ButtonBox buttonBoxA2 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_flat_color);
        ButtonBox buttonBoxC1 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_raise);
        ButtonBox buttonBoxC2 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_raise_color);
        stopThread(1000);
        Field f = null;
        try {
            f = ButtonBox.class.getDeclaredField("mRippleType");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        int o = 0;
        int anInt = 0;
        try {
            Field field = ButtonBox.class.getDeclaredField("TYPE_TOUCH");
            field.setAccessible(true);
            anInt = field.getInt(ButtonBox.class);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        stopThread(1000);
        clickOne(buttonBoxA1);


        try {
            o = (int) f.get(buttonBoxA1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("一行一列按钮错误", anInt == o);
        clickOne(buttonBoxA2);
        try {
            o = (int) f.get(buttonBoxA2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("一行二列按钮错误", anInt == o);
        clickOne(buttonBoxC1);
        try {
            o = (int) f.get(buttonBoxC1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("三行一列按钮错误", anInt == o);
        clickOne(buttonBoxC2);
        try {
            o = (int) f.get(buttonBoxC2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("三行二列按钮错误", anInt == o);
        stopThread(3000);
    }

    @Test
    public void test03Button2() {
        Button button = (Button) ability.findComponentById(ResourceTable.Id_button);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, button);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ButtonBox buttonBoxB1 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_flat_wave);
        ButtonBox buttonBoxB2 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_flat_wave_color);
        ButtonBox buttonBoxD1 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_raise_wave);
        ButtonBox buttonBoxD2 = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_button_bt_raise_wave_color);
        stopThread(1000);
        Field f = null;
        try {
            f = ButtonBox.class.getDeclaredField("mRippleType");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        int o = 0;
        int anInt = 0;
        try {
            Field field = ButtonBox.class.getDeclaredField("TYPE_WAVE");
            field.setAccessible(true);
            anInt = field.getInt(ButtonBox.class);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        stopThread(1000);
        clickOne(buttonBoxB1);
        try {
            o = (int) f.get(buttonBoxB1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("二行一列按钮错误", anInt == o);
        clickOne(buttonBoxB2);
        try {
            o = (int) f.get(buttonBoxB2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("二行二列按钮错误", anInt == o);
        clickOne(buttonBoxD1);
        try {
            o = (int) f.get(buttonBoxD1);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("四行一列按钮错误", anInt == o);
        clickOne(buttonBoxD2);
        try {
            o = (int) f.get(buttonBoxD2);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("四行二列按钮错误", anInt == o);
        stopThread(3000);
    }

    @Test
    public void test04FabsImage() {
        Button fabs = (Button) ability.findComponentById(ResourceTable.Id_fabs);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, fabs);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        FloatingActionButton fabImage = (FloatingActionButton) currentTopAbility.findComponentById(ResourceTable.Id_fab_image);
        Element icon1 = fabImage.getIcon();
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, fabImage);
        stopThread(1000);
        Element icon2 = fabImage.getIcon();
        Assert.assertTrue("图标未发生变化", !icon1.equals(icon2));

        stopThread(3000);
    }

    @Test
    public void test05FabsLine() {
        Button fabs = (Button) ability.findComponentById(ResourceTable.Id_fabs);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, fabs);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        FloatingActionButton fabLine = (FloatingActionButton) currentTopAbility.findComponentById(ResourceTable.Id_fab_line);
        int iconState = fabLine.getLineMorphingState();
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, fabLine);
        stopThread(3000);
        int iconState2 = fabLine.getLineMorphingState();
        Assert.assertTrue("图标未发生变化", iconState != iconState2);

        stopThread(3000);
    }

    @Test
    public void test06SwitchsCheckBox() {
        Button switchs = (Button) ability.findComponentById(ResourceTable.Id_switchs);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, switchs);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
            directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }
        Field f = null;
        try {
            f = CheckBox.class.getDeclaredField("mChecked");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        for (int i = 1; i < 4; i++) {
            CheckBox checkBox1 = (CheckBox) directionalLayout.getComponentAt(i);
            boolean checked = false;
            try {
                checked = (boolean) f.get(checkBox1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            stopThread(500);
            EventHelper.inputSwipe(currentTopAbility, checkBox1, 0, 0, 10, 10, 200);
            boolean checked2 = false;
            try {
                checked2 = (boolean) f.get(checkBox1);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            stopThread(1000);
            Assert.assertTrue("checkBox选中效果改变失败", checked2 != checked);
            stopThread(2000);
        }

    }

    @Test
    public void test07SwitchsRadioButton() {
        Button switchs = (Button) ability.findComponentById(ResourceTable.Id_switchs);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, switchs);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
            directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }

        Field f = null;
        try {
            f = RadioButton.class.getDeclaredField("mChecked");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        TextView textView = (TextView) directionalLayout.getComponentAt(4);
        textView.getText();
        for (int i = 5; i < 8; i++) {
            RadioButton radioButton = (RadioButton) directionalLayout.getComponentAt(i);
            boolean checked = false;
            try {
                checked = (boolean) f.get(radioButton);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            stopThread(500);
            EventHelper.inputSwipe(currentTopAbility, radioButton, 0, 0, 10, 10, 200);
            boolean checked2 = false;
            try {
                checked2 = (boolean) f.get(radioButton);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            stopThread(1000);
            Assert.assertTrue("radioButton选中效果改变失败", checked2 != checked);
            stopThread(2000);
        }
        TextView textView1 = (TextView) directionalLayout.getComponentAt(8);
        textView.getText();
    }

    @Test
    public void test08SwitchsSwitch() {
        Button switchs = (Button) ability.findComponentById(ResourceTable.Id_switchs);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, switchs);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
            directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }

        Field f = null;
        try {
            f = Switch.class.getDeclaredField("mChecked");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        TextView textView1 = (TextView) directionalLayout.getComponentAt(8);

        Switch aSwitch = (Switch) directionalLayout.getComponentAt(9);

        boolean checked = false;
        try {
            checked = (boolean) f.get(aSwitch);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        stopThread(500);
        EventHelper.inputSwipe(currentTopAbility, aSwitch, 10, 10, 10, 10, 200);
        boolean checked2 = false;
        try {
            checked2 = (boolean) f.get(aSwitch);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        stopThread(1000);
        Assert.assertTrue("Switch选中效果改变失败", checked2 != checked);
        stopThread(500);
        EventHelper.inputSwipe(currentTopAbility, aSwitch, 10, 10, 10, 10, 200);
        boolean checked3 = false;
        try {
            checked3 = (boolean) f.get(aSwitch);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        stopThread(1000);
        Assert.assertTrue("Switch选中效果改变失败", checked2 != checked3);
        stopThread(2000);
    }

    @Test
    public void test09Continuous() {
        Button sliders = (Button) ability.findComponentById(ResourceTable.Id_sliders);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, sliders);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Slider slider = (Slider) currentTopAbility.findComponentById(ResourceTable.Id_slider_sl_continuous);
        int width = slider.getLocationOnScreen()[0];
        int width1 = slider.getWidth();
        int height = slider.getLocationOnScreen()[1] + slider.getHeight() / 2;
        int paddingRight = slider.getPaddingRight();
        int paddingLeft = slider.getPaddingLeft();
        exec("input tap " + (width + width1 - paddingRight) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条结尾失败", slider.getPosition() == 1.0 && slider.getValue() == 100);
        exec("input tap " + (width + paddingLeft) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条开头失败", slider.getPosition() == 0 && slider.getValue() == 0);
        exec("input tap " + (width + width1 / 2) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条中间失败", slider.getPosition() == 0.5 && slider.getValue() == 50);

    }

    @Test
    public void test10Discrete() {
        Button sliders = (Button) ability.findComponentById(ResourceTable.Id_sliders);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, sliders);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        Slider slider = (Slider) currentTopAbility.findComponentById(ResourceTable.Id_slider_sl_discrete);
        int width = slider.getLocationOnScreen()[0];
        int width1 = slider.getWidth();
        int height = slider.getLocationOnScreen()[1] + slider.getHeight() / 2;
        int paddingRight = slider.getPaddingRight();
        int paddingLeft = slider.getPaddingLeft();
        exec("input tap " + (width + width1 - paddingRight) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条结尾失败", slider.getPosition() == 1.0 && slider.getValue() == 100);
        exec("input tap " + (width + paddingLeft) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条开头失败", slider.getPosition() == 0 && slider.getValue() == 0);
        exec("input tap " + (width + width1 / 2) + " " + height);
        stopThread(3000);
        Assert.assertTrue("拖动至进度条中间失败", slider.getPosition() == 0.5 && slider.getValue() == 50);

    }

    @Test
    public void test11SnackBarSin() {
        Button snackBar = (Button) ability.findComponentById(ResourceTable.Id_snackBar);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, snackBar);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ButtonBox mobileSingle = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_snackBar_bt_mobile_single);
        ButtonBox tabletSingle = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_snackBar_bt_tablet_single);
        SnackBar mainSn = (SnackBar) currentTopAbility.findComponentById(ResourceTable.Id_main_sn);

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, mobileSingle);
        stopThread(1000);
        int height1 = mainSn.getLocationOnScreen()[1];
        Assert.assertTrue("下方snackBar未弹出", mainSn.getVisibility() == 0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, mobileSingle);
        stopThread(1000);
        Assert.assertTrue("点击single下方snackBar未消失", mainSn.getVisibility() == 2);
        stopThread(2000);

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, tabletSingle);
        stopThread(1000);
        int height2 = mainSn.getLocationOnScreen()[1];
        Assert.assertTrue("两次snackBar位置一致", height1 != height2);
        Assert.assertTrue("下方snackBar未弹出", mainSn.getVisibility() == 0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, mainSn.getComponentAt(1));
        stopThread(1000);
        Assert.assertTrue("点击close按钮snackBar未消失", mainSn.getVisibility() == 2);
        stopThread(2000);

    }

    @Test
    public void test12SnackBarMul() {
        Button snackBar = (Button) ability.findComponentById(ResourceTable.Id_snackBar);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, snackBar);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        ButtonBox mobileMulti = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_snackBar_bt_mobile_multi);
        ButtonBox tabletMulti = (ButtonBox) currentTopAbility.findComponentById(ResourceTable.Id_snackBar_bt_tablet_multi);
        SnackBar mainSn = (SnackBar) currentTopAbility.findComponentById(ResourceTable.Id_main_sn);

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, mobileMulti);
        stopThread(1000);
        int height1 = mainSn.getLocationOnScreen()[1];
        Assert.assertTrue("snackBar未多行显示", ((TextView) mainSn.getComponentAt(0)).getText().contains("\n"));
        Assert.assertTrue("下方snackBar未弹出", mainSn.getVisibility() == 0);
        stopThread(2000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, mobileMulti);
        stopThread(1000);
        Assert.assertTrue("点击single下方snackBar未消失", mainSn.getVisibility() == 2);
        stopThread(2000);

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, tabletMulti);
        stopThread(1000);
        int height2 = mainSn.getLocationOnScreen()[1];
        Assert.assertTrue("两次snackBar位置一致", height1 != height2);
        Assert.assertTrue("下方snackBar未弹出", mainSn.getVisibility() == 0);
        stopThread(5000);
        Assert.assertTrue("5s后snackBar未消失", mainSn.getVisibility() == 2);
        stopThread(2000);

    }

    @Test
    public void test13TimePicker1() {
        Button timePicker = (Button) ability.findComponentById(ResourceTable.Id_timePicker);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, timePicker);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();

        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
            directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }
        TimePickerLayout timePickerLayout = (TimePickerLayout) directionalLayout.getComponentAt(0);
        stopThread(1000);
        Assert.assertTrue("页面未显示", timePickerLayout != null);
        for (int i = 0; i < timePickerLayout.getChildCount(); i++) {
            Assert.assertTrue("页面未正常显示", timePickerLayout.getComponentAt(i) != null);
        }
        stopThread(3000);
    }

    @Test
    public void test14TimePicker2() {
        Button timePickerButton = (Button) ability.findComponentById(ResourceTable.Id_timePicker);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, timePickerButton);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();

        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
            directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }

        TimePickerLayout timePickerLayout = (TimePickerLayout) directionalLayout.getComponentAt(0);
        stopThread(1000);
        Text pm = (Text) timePickerLayout.getComponentAt(2);
        Text am = (Text) timePickerLayout.getComponentAt(1);
        Field f = null;
        try {
            f = TimePickerLayout.class.getDeclaredField("mMidday");
            f.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        String mMidday1 = null;
        try {
            mMidday1 = (String) f.get(timePickerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Color textColor1 = pm.getTextColor();
        Element backgroundElement1 = pm.getBackgroundElement();
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, pm);

        stopThread(1000);
        String mMidday2 = null;
        try {
            mMidday2 = (String) f.get(timePickerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Color textColor2 = pm.getTextColor();
        Element backgroundElement2 = pm.getBackgroundElement();
        Assert.assertTrue("点击下午按键后效果未产生", mMidday1 != mMidday2 && textColor1 != textColor2 && backgroundElement1 != backgroundElement2);
        stopThread(1000);

        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(currentTopAbility, am);

        stopThread(1000);
        String mMidday3 = null;
        try {
            mMidday3 = (String) f.get(timePickerLayout);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Color textColor3 = pm.getTextColor();
        Element backgroundElement3 = pm.getBackgroundElement();
        Assert.assertTrue("点击下午按键后效果未产生", mMidday3 != mMidday2 && textColor3 != textColor2 && backgroundElement3 != backgroundElement2);

        stopThread(3000);
    }

    @Test
    public void test15TimePicker3() {
        Button timePickerButton = (Button) ability.findComponentById(ResourceTable.Id_timePicker);
        stopThread(1000);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability, timePickerButton);
        stopThread(3000);
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        DirectionalLayout directionalLayout=null;
        try {
            Field f  = Window.class.getDeclaredField("agpWindow");
            f.setAccessible(true);
            Object o = f.get(currentTopAbility.getWindow());

            Field mViewGroup = o.getClass().getDeclaredField("mViewGroup");
            mViewGroup.setAccessible(true);
             directionalLayout = (DirectionalLayout)mViewGroup.get(o);
        } catch (NoSuchFieldException |IllegalAccessException e) {
            e.printStackTrace();
        }


        TimePickerLayout timePickerLayout = (TimePickerLayout) directionalLayout.getComponentAt(0);
        TimePicker timePicker = (TimePicker) timePickerLayout.getComponentAt(0);
        stopThread(1000);
        Field fmMode = null;
        Field fmHour = null;
        Field fmMinute = null;
        try {
            fmMode = TimePicker.class.getDeclaredField("mMode");
            fmMode.setAccessible(true);
            fmHour = TimePicker.class.getDeclaredField("mHour");
            fmHour.setAccessible(true);
            fmMinute = TimePicker.class.getDeclaredField("mMinute");
            fmMinute.setAccessible(true);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        int mMode = 0;
        int mHour = 0;
        int mMinute = 0;
        try {
            mMode = (int) fmMode.get(timePicker);
            mHour = (int) fmHour.get(timePicker);
            mMinute = (int) fmMinute.get(timePicker);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("默认非显示小时选择", mMode == TimePicker.MODE_HOUR);
        stopThread(1000);
        exec("input tap " + (timePicker.getPaddingLeft() * 2 + timePicker.getLocationOnScreen()[0]) + " " + (timePicker.getLocationOnScreen()[1] + timePicker.getWidth() / 2));
        stopThread(2000);
        int mMode2 = 0;
        int mHour2 = 0;
        try {
            mMode2 = (int) fmMode.get(timePicker);
            mHour2 = (int) fmHour.get(timePicker);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("选择小时后未显示分钟选择", mMode2 == TimePicker.MODE_MINUTE);
        Assert.assertTrue("选择小时后未正确显示选择的时间", mHour2 == 9);

        stopThread(1000);
        exec("input tap " + (timePicker.getLocationOnScreen()[0] + timePicker.getWidth() - timePicker.getPaddingLeft() * 2) + " " + (timePicker.getLocationOnScreen()[1] + timePicker.getWidth() / 2));
        stopThread(2000);
        int mMinute3 = 0;
        try {
            mMinute3 = (int) fmMinute.get(timePicker);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Assert.assertTrue("选择分钟后未正确显示选择的时间", mMinute3 == 15);
        stopThread(3000);
    }
}