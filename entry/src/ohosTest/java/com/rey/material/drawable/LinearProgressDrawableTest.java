package com.rey.material.drawable;

import com.rey.material.widget.ProgressView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;

public class LinearProgressDrawableTest extends TestCase {

    private LinearProgressDrawable linearProgressDrawable;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        LinearProgressDrawable.Builder builder = new LinearProgressDrawable.Builder(context, 0);
        linearProgressDrawable = builder.build();
        assertNotNull(linearProgressDrawable);
    }

    public void testSetProgressMode() {
        linearProgressDrawable.setProgressMode(ProgressView.MODE_INDETERMINATE);
        assertEquals(ProgressView.MODE_INDETERMINATE, linearProgressDrawable.getProgressMode());
    }

    public void testGetProgress() {
        linearProgressDrawable.setProgress(.5f);
        assertEquals(0.5f, linearProgressDrawable.getProgress());
    }

    public void testGetSecondaryProgress() {
        linearProgressDrawable.setSecondaryProgress(0.5f);
        assertEquals(.5f, linearProgressDrawable.getSecondaryProgress());
    }

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        linearProgressDrawable.start();
        Field field = linearProgressDrawable.getClass().getDeclaredField("mRunState");
        field.setAccessible(true);
        assertEquals(1, field.get(linearProgressDrawable));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        linearProgressDrawable.stop();
        Field field = linearProgressDrawable.getClass().getDeclaredField("mRunState");
        field.setAccessible(true);
        assertEquals(0, field.get(linearProgressDrawable));
    }

    public void testIsRunning() {
        linearProgressDrawable.start();
        assertEquals(true, linearProgressDrawable.isRunning());
    }
}