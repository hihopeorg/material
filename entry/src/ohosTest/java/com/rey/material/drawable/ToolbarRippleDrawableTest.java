package com.rey.material.drawable;

import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Button;
import ohos.app.Context;

import java.lang.reflect.Field;

public class ToolbarRippleDrawableTest extends TestCase {

    private ToolbarRippleDrawable toolbarRippleDrawable;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        ToolbarRippleDrawable.Builder builder = new ToolbarRippleDrawable.Builder(context, 0);
        toolbarRippleDrawable = builder.build();
        assertNotNull(toolbarRippleDrawable);
    }

    public void testGetDelayClickType() {
        toolbarRippleDrawable.setDelayClickType(RippleDrawable.DELAY_CLICK_NONE);
        assertEquals(RippleDrawable.DELAY_CLICK_NONE, toolbarRippleDrawable.getDelayClickType());
    }

    public void testGetClickDelayTime() {

        assertEquals(-1, toolbarRippleDrawable.getClickDelayTime());
    }

    public void testIsStateful() {
        assertEquals(true, toolbarRippleDrawable.isStateful());
    }

    public void testCancel() throws NoSuchFieldException, IllegalAccessException {
        toolbarRippleDrawable.cancel();
        Field field = toolbarRippleDrawable.getClass().getDeclaredField("mState");
        field.setAccessible(true);
        assertEquals(0, field.get(toolbarRippleDrawable));
    }

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        toolbarRippleDrawable.start();
        Field field = toolbarRippleDrawable.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(true, field.get(toolbarRippleDrawable));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        toolbarRippleDrawable.stop();
        Field field = toolbarRippleDrawable.getClass().getDeclaredField("mRunning");
        field.setAccessible(true);
        assertEquals(false, field.get(toolbarRippleDrawable));
    }

    public void testIsRunning() {
        toolbarRippleDrawable.start();
        assertEquals(true, toolbarRippleDrawable.isRunning());
        toolbarRippleDrawable.stop();
        assertEquals(false, toolbarRippleDrawable.isRunning());
    }
}