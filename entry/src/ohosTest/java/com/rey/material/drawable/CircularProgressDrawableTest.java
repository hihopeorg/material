package com.rey.material.drawable;

import com.rey.material.widget.ProgressView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import java.lang.reflect.Field;

public class CircularProgressDrawableTest extends TestCase {

    private CircularProgressDrawable circularProgressDrawable;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CircularProgressDrawable.Builder builder = new CircularProgressDrawable.Builder(context, 0);
        circularProgressDrawable = builder.build();
        assertNotNull(circularProgressDrawable);
    }

    public void testGetProgressMode() {
        circularProgressDrawable.setProgressMode(ProgressView.MODE_INDETERMINATE);
        assertEquals(ProgressView.MODE_INDETERMINATE, circularProgressDrawable.getProgressMode());
    }

    public void testGetProgress() {
        circularProgressDrawable.setProgress(.5f);
        assertEquals(0.5f, circularProgressDrawable.getProgress());
    }

    public void testGetSecondaryProgress() {
        circularProgressDrawable.setSecondaryProgress(0.5f);
        assertEquals(.5f, circularProgressDrawable.getSecondaryProgress());
    }

    public void testStart() throws NoSuchFieldException, IllegalAccessException {
        circularProgressDrawable.start();
        Field field = circularProgressDrawable.getClass().getDeclaredField("mRunState");
        field.setAccessible(true);
        assertEquals(1, field.get(circularProgressDrawable));
    }

    public void testStop() throws NoSuchFieldException, IllegalAccessException {
        circularProgressDrawable.stop();
        Field field = circularProgressDrawable.getClass().getDeclaredField("mRunState");
        field.setAccessible(true);
        assertEquals(0, field.get(circularProgressDrawable));
    }

    public void testIsRunning() {
        circularProgressDrawable.start();
        assertEquals(true, circularProgressDrawable.isRunning());
    }
}