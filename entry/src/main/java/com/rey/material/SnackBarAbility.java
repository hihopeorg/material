package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.rey.material.widget.ButtonBox;
import com.rey.material.widget.SnackBar;

public class SnackBarAbility extends Ability implements Component.ClickedListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "SnackBarAbility");
    SnackBar mSnackBar;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_snack_bar);

        ButtonBox bt_mobile_single = (ButtonBox) findComponentById(ResourceTable.Id_snackBar_bt_mobile_single);
        ButtonBox bt_mobile_multi = (ButtonBox) findComponentById(ResourceTable.Id_snackBar_bt_mobile_multi);
        ButtonBox bt_tablet_single = (ButtonBox) findComponentById(ResourceTable.Id_snackBar_bt_tablet_single);
        ButtonBox bt_tablet_multi = (ButtonBox) findComponentById(ResourceTable.Id_snackBar_bt_tablet_multi);

        bt_mobile_single.setClickedListener(this);
        bt_mobile_multi.setClickedListener(this);
        bt_tablet_single.setClickedListener(this);
        bt_tablet_multi.setClickedListener(this);

        mSnackBar = (SnackBar) findComponentById(ResourceTable.Id_main_sn);
    }

    @Override
    public void onClick(Component component) {
        if (mSnackBar.getState() == SnackBar.STATE_SHOWN) {
            mSnackBar.dismiss();
        } else {
            switch (component.getId()) {
                case ResourceTable.Id_snackBar_bt_mobile_single:
                    mSnackBar.applyStyle(ResourceTable.Pattern_SnackBarSingleLine);
                    mSnackBar.show();
                    break;
                case ResourceTable.Id_snackBar_bt_mobile_multi:
                    mSnackBar.applyStyle(ResourceTable.Pattern_SnackBarMultiLine);
                    mSnackBar.show();
                    break;
                case ResourceTable.Id_snackBar_bt_tablet_single:
                    mSnackBar.applyStyle(com.rey.material.ResourceTable.Pattern_Material_Widget_SnackBar_Tablet);
                    mSnackBar.text("This is single-line snackbar.")
                            .actionText("CLOSE")
                            .duration(0)
                            .show();
                    break;
                case ResourceTable.Id_snackBar_bt_tablet_multi:
                    mSnackBar.applyStyle(com.rey.material.ResourceTable.Pattern_Material_Widget_SnackBar_Tablet_MultiLine);
                    mSnackBar.text("This is multi-line snackbar.\nIt will auto-close after 5s.")
                            .actionText(null)
                            .duration(5000)
                            .show();
                    break;
            }
        }
    }
}
