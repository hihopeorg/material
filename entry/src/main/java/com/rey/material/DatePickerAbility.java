package com.rey.material;

import com.rey.material.widget.ButtonBox;
import com.rey.material.widget.DatePickerLayout;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.text.SimpleDateFormat;

public class DatePickerAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_date_picker);
        DatePickerLayout datePickerLayout = (DatePickerLayout) findComponentById(ResourceTable.Id_date_picker);
        ButtonBox buttonBox = (ButtonBox) findComponentById(ResourceTable.Id_date_picker_ok);
        buttonBox.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                String date = datePickerLayout.getFormattedDate(SimpleDateFormat.getDateInstance());
                ToastDialog toastDialog = new ToastDialog(DatePickerAbility.this);
                toastDialog.setText(date)
                        .setAlignment(LayoutAlignment.BOTTOM)
                        .show();
            }
        });
    }
}
