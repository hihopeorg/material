package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TimePickerAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_time_picker);
    }
}
