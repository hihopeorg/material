package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;

import com.rey.material.widget.FloatingActionButton;

import java.io.IOException;

public class FabAbility extends Ability {

    private Element[] mDrawables = new Element[2];
    private int index = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fab);

        FloatingActionButton fab_image = (FloatingActionButton) findComponentById(ResourceTable.Id_fab_image);
        FloatingActionButton fab_line = (FloatingActionButton) findComponentById(ResourceTable.Id_fab_line);
        fab_line.setClickedListener(new Component.ClickedListener() {

            @Override
            public void onClick(Component v) {
                fab_line.setLineMorphingState((fab_line.getLineMorphingState() + 1) % 2, true);
            }
        });

        try {
            mDrawables[0] = new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_ic_autorenew_white_24dp));
            mDrawables[1] = new PixelMapElement(getResourceManager().getResource(ResourceTable.Media_ic_done_white_24dp));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
        fab_image.setIcon(mDrawables[index], false);
        fab_image.setClickedListener(new Component.ClickedListener() {

            @Override
            public void onClick(Component v) {
                index = (index + 1) % 2;
                fab_image.setIcon(mDrawables[index], true);
            }
        });
    }
}
