package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

import com.rey.material.widget.Slider;

public class SliderAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slider);

        Slider sl_continuous = (Slider) findComponentById(ResourceTable.Id_slider_sl_continuous);
        final Text tv_continuous = (Text) findComponentById(ResourceTable.Id_slider_tv_continuous);
        tv_continuous.setText(String.format("pos=%.1f value=%d", sl_continuous.getPosition(), sl_continuous.getValue()));
        sl_continuous.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {

            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                tv_continuous.setText(String.format("pos=%.1f value=%d", newPos, newValue));
            }
        });

        Slider sl_discrete = (Slider) findComponentById(ResourceTable.Id_slider_sl_discrete);
        final Text tv_discrete = (Text) findComponentById(ResourceTable.Id_slider_tv_discrete);
        tv_discrete.setText(String.format("pos=%.1f value=%d", sl_discrete.getPosition(), sl_discrete.getValue()));
        sl_discrete.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {

            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                tv_discrete.setText(String.format("pos=%.1f value=%d", newPos, newValue));
            }
        });
    }
}
