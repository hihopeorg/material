package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

public class MainAbility extends Ability implements Component.ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initComponents();
    }

    private void initComponents() {
        Button process = (Button) findComponentById(ResourceTable.Id_process);
        Button button = (Button) findComponentById(ResourceTable.Id_button);
        Button fabs = (Button) findComponentById(ResourceTable.Id_fabs);
        Button switchs = (Button) findComponentById(ResourceTable.Id_switchs);
        Button sliders = (Button) findComponentById(ResourceTable.Id_sliders);
        Button snackBar = (Button) findComponentById(ResourceTable.Id_snackBar);
        Button timePicker = (Button) findComponentById(ResourceTable.Id_timePicker);
        //Button datePicker = (Button) findComponentById(ResourceTable.Id_datePicker);
        process.setClickedListener(this);
        button.setClickedListener(this);
        fabs.setClickedListener(this);
        switchs.setClickedListener(this);
        sliders.setClickedListener(this);
        snackBar.setClickedListener(this);
        timePicker.setClickedListener(this);
        //datePicker.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_process:
                startAbility(ProgressAbility.class.getName());
                break;
            case ResourceTable.Id_button:
                startAbility(ButtonAbility.class.getName());
                break;
            case ResourceTable.Id_fabs:
                startAbility(FabAbility.class.getName());
                break;
            case ResourceTable.Id_switchs:
                startAbility(SwitchAbility.class.getName());
                break;
            case ResourceTable.Id_sliders:
                startAbility(SliderAbility.class.getName());
                break;
            case ResourceTable.Id_snackBar:
                startAbility(SnackBarAbility.class.getName());
                break;
            case ResourceTable.Id_timePicker:
                startAbility(TimePickerAbility.class.getName());
                break;
//            case ResourceTable.Id_datePicker:
//                startAbility(DatePickerAbility.class.getName());
//                break;
            default:
                break;

        }
    }

    private void startAbility(String abilityName) {
        Intent secondIntent = new Intent();
        // 指定待启动FA的bundleName和abilityName
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.rey.material")
                .withAbilityName(abilityName)
                .build();
        secondIntent.setOperation(operation);
        // 通过AbilitySlice的startAbility接口实现启动另一个页面
        startAbility(secondIntent);
    }
}
