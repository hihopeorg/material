package com.rey.material;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import com.rey.material.widget.ProgressView;

public class ProgressAbility extends Ability {

    private ProgressView pv_circular;
    private ProgressView pv_circular_colors;
    private ProgressView pv_circular_inout;
    private ProgressView pv_circular_inout_colors;
    private ProgressView pv_circular_determinate_in_out;
    private ProgressView pv_circular_determinate;
    private ProgressView pv_linear;
    private ProgressView pv_linear_colors;
    private ProgressView pv_linear_determinate;
    private ProgressView pv_linear_query;
    private ProgressView pv_linear_buffer;

    private MyHandler mHandler;

    private static final int MSG_START_PROGRESS = 1000;
    private static final int MSG_STOP_PROGRESS = 1001;
    private static final int MSG_UPDATE_PROGRESS = 1002;
    private static final int MSG_UPDATE_QUERY_PROGRESS = 1003;
    private static final int MSG_UPDATE_BUFFER_PROGRESS = 1004;

    private static final long PROGRESS_INTERVAL = 7000;
    private static final long START_DELAY = 500;
    private static final long PROGRESS_UPDATE_INTERVAL = PROGRESS_INTERVAL / 100;
    private static final long START_QUERY_DELAY = PROGRESS_INTERVAL / 2;
    private static final long QUERY_PROGRESS_UPDATE_INTERVAL = (PROGRESS_INTERVAL - START_QUERY_DELAY) / 100;
    private static final long BUFFER_PROGRESS_UPDATE_INTERVAL = (PROGRESS_INTERVAL - START_QUERY_DELAY) / 100;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_progress);

        pv_circular = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular);
        pv_circular_colors = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular_colors);
        pv_circular_inout = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular_inout);
        pv_circular_inout_colors = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular_inout_colors);
        pv_circular_determinate_in_out = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular_determinate_in_out);
        pv_circular_determinate = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_circular_determinate);
        pv_linear = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_linear);
        pv_linear_colors = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_linear_colors);
        pv_linear_determinate = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_linear_determinate);
        pv_linear_query = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_linear_query);
        pv_linear_buffer = (ProgressView) findComponentById(ResourceTable.Id_progress_pv_linear_buffer);

        mHandler = new MyHandler(EventRunner.getMainEventRunner());
    }

    @Override
    protected void onActive() {
        super.onActive();
        mHandler.sendEvent(MSG_START_PROGRESS, START_DELAY);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        mHandler.removeAllEvent();
        pv_circular_determinate_in_out.setVisibility(Component.HIDE);
    }

    private class MyHandler extends EventHandler {

        public MyHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case MSG_START_PROGRESS:
                    pv_circular.start();
                    pv_circular_colors.start();
                    pv_circular_inout.start();
                    pv_circular_inout_colors.start();
                    pv_circular_determinate_in_out.setProgress(0f);
                    pv_circular_determinate_in_out.start();
                    pv_circular_determinate.setProgress(0f);
                    pv_circular_determinate.start();
                    pv_linear.start();
                    pv_linear_colors.start();
                    pv_linear_determinate.setProgress(0f);
                    pv_linear_determinate.start();
                    pv_linear_query.setProgress(0f);
                    pv_linear_query.start();
                    pv_linear_buffer.setProgress(0f);
                    pv_linear_buffer.setSecondaryProgress(0f);
                    pv_linear_buffer.start();
                    mHandler.sendEvent(MSG_STOP_PROGRESS, PROGRESS_INTERVAL);
                    mHandler.sendEvent(MSG_UPDATE_PROGRESS, PROGRESS_UPDATE_INTERVAL);
                    mHandler.sendEvent(MSG_UPDATE_QUERY_PROGRESS, START_QUERY_DELAY);
                    mHandler.sendEvent(MSG_UPDATE_BUFFER_PROGRESS, BUFFER_PROGRESS_UPDATE_INTERVAL);
                    break;
                case MSG_UPDATE_QUERY_PROGRESS:
                    pv_linear_query.setProgress(pv_linear_query.getProgress() + 0.01f);

                    if (pv_linear_query.getProgress() < 1f)
                        mHandler.sendEvent(MSG_UPDATE_QUERY_PROGRESS, QUERY_PROGRESS_UPDATE_INTERVAL);
                    else
                        pv_linear_query.stop();
                    break;
                case MSG_UPDATE_BUFFER_PROGRESS:
                    pv_linear_buffer.setSecondaryProgress(pv_linear_buffer.getSecondaryProgress() + 0.01f);

                    if (pv_linear_buffer.getSecondaryProgress() < 1f)
                        mHandler.sendEvent(MSG_UPDATE_BUFFER_PROGRESS, BUFFER_PROGRESS_UPDATE_INTERVAL);
                    break;
                case MSG_UPDATE_PROGRESS:
                    pv_circular_determinate_in_out.setProgress(pv_circular_determinate_in_out.getProgress() + 0.01f);
                    pv_circular_determinate.setProgress(pv_circular_determinate.getProgress() + 0.01f);

                    pv_linear_determinate.setProgress(pv_linear_determinate.getProgress() + 0.01f);
                    pv_linear_buffer.setProgress(pv_linear_buffer.getProgress() + 0.01f);
                    if (pv_circular_determinate_in_out.getProgress() < 1f)
                        mHandler.sendEvent(MSG_UPDATE_PROGRESS, PROGRESS_UPDATE_INTERVAL);
                    else {
                        pv_circular_determinate_in_out.stop();
                        pv_circular_determinate.stop();
                        pv_linear_determinate.stop();
                        pv_linear_buffer.stop();
                    }
                    break;
                case MSG_STOP_PROGRESS:
                    pv_circular.stop();
                    pv_circular_colors.stop();
                    pv_circular_inout.stop();
                    pv_circular_inout_colors.stop();
                    pv_linear.stop();
                    pv_linear_colors.stop();
                    mHandler.sendEvent(MSG_START_PROGRESS, START_DELAY);
                    break;
            }
        }
    }
}
