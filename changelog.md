#### version 1.0.1

1. 添加JUnit单元测试和自动化测试

2. 适配新版本IDE

#### version 1.0.0

1. Circular和Linear样式的ProgressView
2. 带有水波纹效果的Button
3. FloatingActionButton
4. CheckBox、RadioButton、Switch选择控件
5. Slider控件
6. SnackBar控件
7. TimePickerLayout

