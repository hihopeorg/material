package com.rey.material.app;

import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.eventhandler.EventRunner;
import ohos.utils.PlainArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Rey on 5/25/2015.
 */
public class ThemeManager {

    private volatile static ThemeManager mInstance;

    private Context mContext;
    private PlainArray<int[]> mStyles = new PlainArray<>();
    private int mCurrentTheme;
    private int mThemeCount;
    private EventDispatcher mDispatcher;

    private static final String PREF = "theme.pref";
    private static final String KEY_THEME = "theme";

    public static final int THEME_UNDEFINED = Integer.MIN_VALUE;

    /**
     * Init ThemeManager. Should be call in {@link AbilityPackage#onInitialize()}.
     *
     * @param context      The context object. Should be {#link Application} object.
     * @param totalTheme   The total theme.
     * @param defaultTheme The default theme if current theme isn't set.
     * @param dispatcher   The {@link EventDispatcher} will be used to dispatch {@link OnThemeChangedEvent}. If null, then use {@link SimpleDispatcher}.
     */
    public static void init(Context context, int totalTheme, int defaultTheme, EventDispatcher dispatcher) {
        getInstance().setup(context, totalTheme, defaultTheme, dispatcher);
    }

    /**
     * Get the singleton instance of ThemeManager.
     *
     * @return The singleton instance of ThemeManager.
     */
    public static ThemeManager getInstance() {
        if (mInstance == null) {
            synchronized (ThemeManager.class) {
                if (mInstance == null)
                    mInstance = new ThemeManager();
            }
        }

        return mInstance;
    }

    /**
     * Get the styleId from attributes.
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     * @return The styleId.
     */
    public static int getStyleId(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ThemableView, defStyleAttr, defStyleRes);
//        int styleId = a.getResourceId(R.styleable.ThemableView_v_styleId, 0);
//        a.recycle();

        return 0;
    }

    protected void setup(Context context, int totalTheme, int defaultTheme, EventDispatcher dispatcher) {
        mContext = context;
        mDispatcher = dispatcher != null ? dispatcher : new SimpleDispatcher();
        mThemeCount = totalTheme;
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Preferences pref = getPreferences(context, databaseHelper);
        if (pref != null)
            mCurrentTheme = pref.getInt(KEY_THEME, defaultTheme);
        else
            mCurrentTheme = defaultTheme;
        if (mCurrentTheme >= mThemeCount)
            setCurrentTheme(defaultTheme);
    }

    private int[] loadStyleList(Context context, int resId) {
        if (context == null)
            return null;

//        TypedArray array = context.getResources().obtainTypedArray(resId);
//        int[] result = new int[array.length()];
//        for(int i = 0; i < result.length; i++)
//            result[i] = array.getResourceId(i, 0);
//        array.recycle();
//
//        return result;
        return new int[]{0};
    }

    private int[] getStyleList(int styleId) {
        if (mStyles == null)
            return null;

        int[] list = mStyles.get(styleId).get();
        if (list == null) {
            list = loadStyleList(mContext, styleId);
            mStyles.put(styleId, list);
        }

        return list;
    }

    private Preferences getPreferences(Context context, DatabaseHelper helper) {
        return context == null ? null : helper.getPreferences(PREF);
    }

    private void dispatchThemeChanged(int theme) {
        if (mDispatcher != null)
            mDispatcher.dispatchThemeChanged(theme);
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * Get the current theme.
     *
     * @return The current theme.
     */
    public int getCurrentTheme() {
        return mCurrentTheme;
    }

    /**
     * Set the current theme. Should be called in main thread (UI thread).
     *
     * @param theme The current theme.
     * @return True if set theme successfully, False if method's called on main thread or theme already set.
     */
    public boolean setCurrentTheme(int theme) {
        if (EventRunner.getMainEventRunner() != EventRunner.current())
            return false;

        if (mCurrentTheme != theme) {
            mCurrentTheme = theme;
            DatabaseHelper helper = new DatabaseHelper(mContext);
            Preferences pref = getPreferences(mContext, helper);
            if (pref != null)
                pref.putInt(KEY_THEME, mCurrentTheme).flush();
            dispatchThemeChanged(mCurrentTheme);
            return true;
        }

        return false;
    }

    /**
     * Get the total theme.
     *
     * @return The total theme.
     */
    public int getThemeCount() {
        return mThemeCount;
    }

    /**
     * Get current style of a styleId.
     *
     * @param styleId The styleId.
     * @return The current style.
     */
    public int getCurrentStyle(int styleId) {
        return getStyle(styleId, mCurrentTheme);
    }

    /**
     * Get a specific style of a styleId.
     *
     * @param styleId The styleId.
     * @param theme   The theme.
     * @return The specific style.
     */
    public int getStyle(int styleId, int theme) {
        int[] styles = getStyleList(styleId);
        return styles == null ? 0 : styles[theme];
    }

    /**
     * Register a listener will be called when current theme changed.
     *
     * @param listener A {@link OnThemeChangedListener} will be registered.
     */
    public void registerOnThemeChangedListener(OnThemeChangedListener listener) {
        if (mDispatcher != null)
            mDispatcher.registerListener(listener);
    }

    /**
     * Unregister a listener from be called when current theme changed.
     *
     * @param listener A {@link OnThemeChangedListener} will be unregistered.
     */
    public void unregisterOnThemeChangedListener(OnThemeChangedListener listener) {
        if (mDispatcher != null)
            mDispatcher.unregisterListener(listener);
    }

    public interface EventDispatcher {

        void registerListener(OnThemeChangedListener listener);

        void unregisterListener(OnThemeChangedListener listener);

        void dispatchThemeChanged(int theme);
    }

    public static class SimpleDispatcher implements EventDispatcher {

        ArrayList<WeakReference<OnThemeChangedListener>> mListeners = new ArrayList<>();

        @Override
        public void registerListener(OnThemeChangedListener listener) {
            boolean exist = false;
            for (int i = mListeners.size() - 1; i >= 0; i--) {
                WeakReference<OnThemeChangedListener> ref = mListeners.get(i);
                if (ref.get() == null)
                    mListeners.remove(i);
                else if (ref.get() == listener)
                    exist = true;
            }

            if (!exist)
                mListeners.add(new WeakReference<>(listener));
        }

        @Override
        public void unregisterListener(OnThemeChangedListener listener) {
            for (int i = mListeners.size() - 1; i >= 0; i--) {
                WeakReference<OnThemeChangedListener> ref = mListeners.get(i);
                if (ref.get() == null || ref.get() == listener)
                    mListeners.remove(i);
            }
        }

        @Override
        public void dispatchThemeChanged(int theme) {
            OnThemeChangedEvent event = new OnThemeChangedEvent(theme);

            for (int i = mListeners.size() - 1; i >= 0; i--) {
                WeakReference<OnThemeChangedListener> ref = mListeners.get(i);
                if (ref.get() == null)
                    mListeners.remove(i);
                else
                    ref.get().onThemeChanged(event);
            }
        }
    }

    public interface OnThemeChangedListener {

        void onThemeChanged(OnThemeChangedEvent event);

    }

    public static class OnThemeChangedEvent {
        public final int theme;

        public OnThemeChangedEvent(int theme) {
            this.theme = theme;
        }
    }


}
