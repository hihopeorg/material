package com.rey.material.util;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

public class ColorUtil {

    private static int getMiddleValue(int prev, int next, float factor) {
        return Math.round(prev + (next - prev) * factor);
    }

    public static int getMiddleColor(int prevColor, int curColor, float factor) {
        if (prevColor == curColor)
            return curColor;

        if (factor == 0f)
            return prevColor;
        else if (factor == 1f)
            return curColor;

        RgbColor prevRgbColor = new RgbColor(prevColor);
        RgbColor curRgbColor = new RgbColor(curColor);
        int a = getMiddleValue(prevRgbColor.getAlpha(), curRgbColor.getAlpha(), factor);
        int r = getMiddleValue(prevRgbColor.getRed(), curRgbColor.getRed(), factor);
        int g = getMiddleValue(prevRgbColor.getGreen(), curRgbColor.getGreen(), factor);
        int b = getMiddleValue(prevRgbColor.getBlue(), curRgbColor.getBlue(), factor);

        return Color.argb(a, r, g, b);
    }

    public static int getColor(int baseColor, float alphaPercent) {
        int alpha = Math.round(Color.alpha(baseColor) * alphaPercent);

        return (baseColor & 0x00FFFFFF) | (alpha << 24);
    }
}
