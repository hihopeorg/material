package com.rey.material.util;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.Pattern;
import ohos.global.resource.solidxml.Theme;
import ohos.global.resource.solidxml.TypedAttribute;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class ViewUtil {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ViewUtil");

    public static final long FRAME_DURATION = 1000 / 60;

    private static final AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    public static int generateViewId() {
        for (; ; ) {
            final int result = sNextGeneratedId.get();
            // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF)
                newValue = 1; // Roll over to 1, not 0.
            if (sNextGeneratedId.compareAndSet(result, newValue))
                return result;
        }
    }

    public static boolean hasState(int[] states, int state) {
        if (states == null)
            return false;

        for (int state1 : states)
            if (state1 == state)
                return true;

        return false;
    }

    public static void setBackground(Component v, Element drawable) {
        v.setBackground(drawable);
    }

    /**
     * Apply any View style attributes to a view.
     *
     * @param v     The view is applied.
     * @param resId The style resourceId.
     */
    public static void applyStyle(Component v, int resId) {
        applyStyle(v, null, 0, resId);
    }

    /**
     * Apply any View style attributes to a view.
     *
     * @param v            The view is applied.
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     */
    public static void applyStyle(Component v, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        if (attrs == null) {
            return;
        }

        int leftPadding = -1;
        int topPadding = -1;
        int rightPadding = -1;
        int bottomPadding = -1;
        int startPadding = Integer.MIN_VALUE;
        int endPadding = Integer.MIN_VALUE;
        int padding = -1;

        boolean startPaddingDefined = false;
        boolean endPaddingDefined = false;
        boolean leftPaddingDefined = false;
        boolean rightPaddingDefined = false;

        int count = attrs.getLength();
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("background_element")) {
                Element bg = Util.getElement(attrs, name);
                ViewUtil.setBackground(v, bg);
            } else if (name.equals("padding")) {
                padding = Util.getDimensionValue(attrs, name, -1);
                leftPaddingDefined = true;
                rightPaddingDefined = true;
            } else if (name.equals("left_padding")) {
                leftPadding = Util.getDimensionValue(attrs, name, -1);
                leftPaddingDefined = true;
            } else if (name.equals("top_padding")) {
                topPadding = Util.getDimensionValue(attrs, name, -1);
            } else if (name.equals("right_padding")) {
                rightPadding = Util.getDimensionValue(attrs, name, -1);
                rightPaddingDefined = true;
            } else if (name.equals("bottom_padding")) {
                bottomPadding = Util.getDimensionValue(attrs, name, -1);
            } else if (name.equals("start_padding")) {
                startPadding = Util.getDimensionValue(attrs, name, Integer.MIN_VALUE);
                startPaddingDefined = (startPadding != Integer.MIN_VALUE);
            } else if (name.equals("end_padding")) {
                endPadding = Util.getDimensionValue(attrs, name, Integer.MIN_VALUE);
                endPaddingDefined = (endPadding != Integer.MIN_VALUE);
            } else if (name.equals("scrollbar_fading_enabled")) {
                v.setScrollbarFadingEnabled(Util.getBoolValue(attrs, name, true));
            } else if (name.equals("min_height")) {
                v.setMinHeight(Util.getDimensionValue(attrs, name, 0));
            } else if (name.equals("min_width")) {
                v.setMinWidth(Util.getDimensionValue(attrs, name, 0));
            } else if (name.equals("scrollbar_fading_delay")) {
                v.setScrollbarFadingDelay(Util.getDimensionValue(attrs, name, 0));
            } else if (name.equals("scrollbar_fading_duration")) {
                v.setScrollbarFadingDuration(Util.getDimensionValue(attrs, name, 0));
            } else if (name.equals("visibility")) {
                int value = Util.getIntegerValue(attrs, name, 0);
                switch (value) {
                    case 0:
                        v.setVisibility(Component.VISIBLE);
                        break;
                    case 1:
                        v.setVisibility(Component.INVISIBLE);
                        break;
                    case 2:
                        v.setVisibility(Component.HIDE);
                        break;
                }
            } else if (name.equals("layout_direction")) {
                int value = Util.getIntegerValue(attrs, name, 0);
                switch (value) {
                    case 0:
                        v.setLayoutDirection(Component.LayoutDirection.LTR);
                        break;
                    case 1:
                        v.setLayoutDirection(Component.LayoutDirection.RTL);
                        break;
                    case 2:
                        v.setLayoutDirection(Component.LayoutDirection.INHERIT);
                        break;
                    case 3:
                        v.setLayoutDirection(Component.LayoutDirection.LOCALE);
                        break;
                }
            } else if (name.equals("image_src")) {
                if (v instanceof Image) {
                    int resId = Util.getIntegerValue(attrs, name, 0);
                    ((Image) v).setImageElement(null);
                }
            }
            if (padding >= 0) {
                v.setPadding(padding, padding, padding, padding);
            } else {
                if (leftPaddingDefined || rightPaddingDefined)
                    v.setPadding(leftPaddingDefined ? leftPadding : v.getPaddingLeft(),
                            topPadding >= 0 ? topPadding : v.getPaddingTop(),
                            rightPaddingDefined ? rightPadding : v.getPaddingRight(),
                            bottomPadding >= 0 ? bottomPadding : v.getPaddingBottom());

                if (startPaddingDefined || endPaddingDefined)
                    v.setPaddingRelative(startPaddingDefined ? startPadding : v.getPaddingStart(),
                            topPadding >= 0 ? topPadding : v.getPaddingTop(),
                            endPaddingDefined ? endPadding : v.getPaddingEnd(),
                            bottomPadding >= 0 ? bottomPadding : v.getPaddingBottom());
            }
            if (v instanceof Text)
                applyStyle(v, attrs, defStyleAttr, defStyleRes);
        }
    }

    public static class TypedArray {
        private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "TypedArray");

        @FunctionalInterface
        public interface EnumParser {
            default Optional<Integer> parseFullString(String fullString) {
                if (TextTool.isNullOrEmpty(fullString)) {
                    return Optional.empty();
                }
                String[] values =
                        Arrays.asList(fullString.split("\\|")).stream().map(String::trim).toArray(String[]::new);
                int result = 0;
                for (String value : values) {
                    Optional<Integer> flag = parseEmum(value);

                    // Illegal String
                    if (!flag.isPresent()) {
                        return Optional.empty();
                    }

                    result |= flag.get();
                }
                return Optional.of(result);
            }

            /**
             * A method to map String value to int value(bit-flag).
             * If "|" is legal, make sure your flags not conflict with bits for each other.
             * For example, if CENTER -> 3, RIGHT -> 1, then CENTER | RIGHT may cause problem for the final value(3)
             * will be considered as CENTER only.
             *
             * @param enumString String value
             * @return The int value in Optional, if String is illegal, return an empty Optional
             */
            Optional<Integer> parseEmum(String enumString);
        }

        /**
         * A simple implementation of EnumParser, use array pair to map values.
         */
        public static class ArrayPairEnumParser implements EnumParser {
            String[] values;
            int[] flags;

            /**
             * Constructor.
             *
             * @param values Legal String values. Can be null.
             * @param flags  Flags. If values not null, flags must non-null and as same size as values.
             */
            public ArrayPairEnumParser(String[] values, int[] flags) {
                if (values != null && (flags == null || values.length != flags.length)) {
                    throw new IllegalArgumentException("If values is no null, values.length must be same as flags"
                            + ".length!");
                }
                this.values = values;
                this.flags = flags;
            }

            @Override
            public Optional<Integer> parseEmum(String enumString) {
                if (values != null && values.length > 0) {
                    for (int i = 0; i < values.length; i++) {
                        String value = values[i];
                        if (Objects.equals(value, enumString)) {
                            return Optional.of(flags[i]);
                        }
                    }
                }
                return Optional.empty();
            }
        }

        private static Map<String, EnumParser> REGISTED_PARSERS = new HashMap<>();

        public static void registerParser(String attrName, EnumParser parser) {
            EnumParser oldParser = REGISTED_PARSERS.put(attrName, parser);
            if (oldParser != null) {
                HiLog.warn(LABEL, "duplicated parser of attr: " + attrName);
            }
        }

        static {
            registerParser("layout_alignment", new ArrayPairEnumParser(new String[]{"bottom", "center", "end",
                    "horizontal_center", "left", "right", "start", "top", "vertical_center"},
                    new int[]{LayoutAlignment.BOTTOM, LayoutAlignment.CENTER, LayoutAlignment.END,
                            LayoutAlignment.HORIZONTAL_CENTER, LayoutAlignment.LEFT, LayoutAlignment.RIGHT,
                            LayoutAlignment.START, LayoutAlignment.TOP, LayoutAlignment.VERTICAL_CENTER}));
            registerParser("visibility", new ArrayPairEnumParser(new String[]{"visible", "invisible", "hide"},
                    new int[]{Component.VISIBLE, Component.INVISIBLE, Component.HIDE}));
            registerParser("focusable", new ArrayPairEnumParser(new String[]{"focus_enable", "focus_disable",
                    "focus_adaptable"}, new int[]{Component.FOCUS_ENABLE, Component.FOCUS_DISABLE,
                    Component.FOCUS_ADAPTABLE}));
            registerParser("text_alignment", new ArrayPairEnumParser(new String[]{"bottom", "center", "end",
                    "horizontal_center", "left", "right", "start", "top", "vertical_center"},
                    new int[]{TextAlignment.BOTTOM, TextAlignment.CENTER, TextAlignment.END,
                            TextAlignment.HORIZONTAL_CENTER, TextAlignment.LEFT, TextAlignment.RIGHT,
                            TextAlignment.START, TextAlignment.TOP, TextAlignment.VERTICAL_CENTER}));

            registerParser("pv_progressMode", new ArrayPairEnumParser(new String[]{"determinate", "indeterminate",
                    "buffer", "query"}, new int[]{0x00000000, 0x00000001, 0x00000002, 0x00000003}));
            registerParser("lpd_verticalAlign", new ArrayPairEnumParser(new String[]{"top", "center", "bottom"},
                    new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("rd_maxRippleRadius", new ArrayPairEnumParser(new String[]{"match_view"},
                    new int[]{0x00000000}));//dimen also
            registerParser("rd_maskType", new ArrayPairEnumParser(new String[]{"rectangle", "oval"},
                    new int[]{0x00000000, 0x00000001}));
            registerParser("rd_rippleType", new ArrayPairEnumParser(new String[]{"touch", "wave"},
                    new int[]{0x00000000, 0x00000001}));
            registerParser("rd_delayClick", new ArrayPairEnumParser(new String[]{"none", "untilRelease",
                    "afterRelease"}, new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("lmd_strokeCap", new ArrayPairEnumParser(new String[]{"butt", "round", "square"},
                    new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("lmd_strokeJoin", new ArrayPairEnumParser(new String[]{"miter", "round", "bevel"},
                    new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("lmd_layoutDirection", new ArrayPairEnumParser(new String[]{"ltr", "rtl", "locale"},
                    new int[]{0x00000000, 0x00000001, 0x00000003}));
            registerParser("sw_trackCap", new ArrayPairEnumParser(new String[]{"butt", "round", "square"},
                    new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("sl_trackCap", new ArrayPairEnumParser(new String[]{"butt", "round", "square"},
                    new int[]{0x00000000, 0x00000001, 0x00000002}));
            registerParser("sl_textStyle", new ArrayPairEnumParser(new String[]{"normal", "bold", "italic",
                    "bold_italic"}, new int[]{0, 1, 2, 3}));
            registerParser("tpi_mode", new ArrayPairEnumParser(new String[]{"scroll", "fixed"},
                    new int[]{0x00000000,
                            0x00000001}));
            registerParser("et_labelEllipsize", new ArrayPairEnumParser(new String[]{"start", "middle", "end",
                    "marquee"}, new int[]{0x00000001, 0x00000002, 0x00000003, 0x00000004}));
            registerParser("et_supportMode", new ArrayPairEnumParser(new String[]{"none", "helper",
                    "helperWithError"
                    , "charCounter"}, new int[]{0x00000000, 0x00000001, 0x00000002, 0x00000003}));
            registerParser("et_supportEllipsize", new ArrayPairEnumParser(new String[]{"start", "middle", "end",
                    "marquee"}, new int[]{0x00000001, 0x00000002, 0x00000003, 0x00000004}));
            registerParser("et_autoCompleteMode", new ArrayPairEnumParser(new String[]{"none", "single", "multi"},
                    new int[]{0, 1, 2}));
            registerParser("sb_ellipsize", new ArrayPairEnumParser(new String[]{"start", "middle", "end",
                    "marquee"},
                    new int[]{0x00000001, 0x00000002, 0x00000003, 0x00000004}));
            registerParser("spn_labelEllipsize", new ArrayPairEnumParser(new String[]{"start", "middle", "end",
                    "marquee"}, new int[]{0x00000001, 0x00000002, 0x00000003, 0x00000004}));
            registerParser("di_layoutDirection", new ArrayPairEnumParser(new String[]{"ltr", "rtl", "locale"},
                    new int[]{0, 1, 3}));
            registerParser("tp_textStyle", new ArrayPairEnumParser(new String[]{"normal", "bold", "italic",
                    "bold_italic"}, new int[]{0, 1, 2, 3}));
            registerParser("dp_textStyle", new ArrayPairEnumParser(new String[]{"normal", "bold", "italic",
                    "bold_italic"}, new int[]{0, 1, 2, 3}));
        }

        public static TypedArray obtainStyledAttributes(Context context, AttrSet attrSet, int[] attrs,
                                                        String defStyleAttr, int defStyleRes) {
            return new TypedArray(context, attrSet, attrs, defStyleAttr, defStyleRes);
        }

        private Context context;
        private AttrSet attrSet;
        private Pattern patternFromTheme;
        private Pattern patternByRes;
        List<String> keys = new ArrayList<String>();

        public TypedArray(Context context, AttrSet attrSet, int[] attrs, String defStyleAttr, int defStyleRes) {
            if (context == null) {
                throw new IllegalArgumentException("context invalid!");
            }
            this.context = context;
            this.attrSet = attrSet;
            if (attrSet != null) {
                for (int i = 0; i < attrSet.getLength(); i++) {
                    keys.add(attrSet.getAttr(i).get().getName());
                }
            }
            Theme theme = context.getTheme();
            if (theme != null && defStyleAttr != null) {
                try {
                    patternFromTheme = theme.getPatternValue(defStyleAttr);
                    if (patternFromTheme != null) {
                        Set<String> keySet = patternFromTheme.getPatternHash().keySet();
                        for (String key : keySet) {
                            if (!keys.contains(key)) {
                                keys.add(key);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
            }
            try {
                patternByRes = context.getResourceManager().getElement(defStyleRes).getPattern();
                if (patternByRes != null) {
                    Set<String> keySet = patternByRes.getPatternHash().keySet();
                    for (String key : keySet) {
                        if (!keys.contains(key)) {
                            keys.add(key);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }

        public int getIndexCount() {
            return keys == null ? 0 : keys.size();
        }

        public String getIndex(int i) {
            return keys == null ? null : keys.get(i);
        }

        public int getIntegerValue(String name, int defaultValue) {
            EnumParser parser = REGISTED_PARSERS.get(name);
            if (parser != null) {
                Optional<Integer> parsedValue = parser.parseFullString(getStringValue(name, null));
                if (parsedValue.isPresent()) {
                    return parsedValue.get();
                }
            }
            int result = defaultValue;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                result = attrSet.getAttr(name).get().getIntegerValue();
                return result;
            }
            if (patternFromTheme != null) {
                TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                if (typedAttribute != null) {
                    try {
                        result = typedAttribute.getIntegerValue();
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (patternByRes != null) {
                TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                if (typedAttribute != null) {
                    try {
                        result = typedAttribute.getIntegerValue();
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

        public Element getElement(String name) {
            Element result = null;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                result = attrSet.getAttr(name).get().getElement();
            }
            if (result == null) {
                if (patternFromTheme != null) {
                    TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        try {
                            int resId = typedAttribute.getResId();
                            if (resId != 0) {
                                result = ElementScatter.getInstance(context).parse(typedAttribute.getResId());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        } catch (WrongTypeException e) {
                            e.printStackTrace();
                        }
                        if (result == null) {
                            result = AttrHelper.convertValueToElement(typedAttribute.getOriginalValue());
                        }
                    }
                }
            }
            if (result == null) {
                if (patternByRes != null) {
                    TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        try {
                            int resId = typedAttribute.getResId();
                            if (resId != 0) {
                                result = ElementScatter.getInstance(context).parse(typedAttribute.getResId());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        } catch (WrongTypeException e) {
                            e.printStackTrace();
                        }
                        if (result == null) {
                            result = AttrHelper.convertValueToElement(typedAttribute.getOriginalValue());
                        }
                    }
                }
            }
            return result;
        }

        public int getDimensionValue(String name, int defaultValue) {
            int result = defaultValue;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                String stringValue = attrSet.getAttr(name).get().getStringValue();
                if ("match_parent".equals(stringValue)) {
                    return ComponentContainer.LayoutConfig.MATCH_PARENT;
                } else if ("match_content".equals(stringValue)) {
                    return ComponentContainer.LayoutConfig.MATCH_CONTENT;
                }
                result = attrSet.getAttr(name).get().getDimensionValue();
                return result;
            }
            if (patternFromTheme != null) {
                TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                if (typedAttribute != null) {
                    String stringValue = typedAttribute.getOriginalValue();
                    if ("match_parent".equals(stringValue)) {
                        return ComponentContainer.LayoutConfig.MATCH_PARENT;
                    } else if ("match_content".equals(stringValue)) {
                        return ComponentContainer.LayoutConfig.MATCH_CONTENT;
                    }
                    try {
                        result = typedAttribute.getPixelValue(false);
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (patternByRes != null) {
                TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                if (typedAttribute != null) {
                    String stringValue = typedAttribute.getOriginalValue();
                    if ("match_parent".equals(stringValue)) {
                        return ComponentContainer.LayoutConfig.MATCH_PARENT;
                    } else if ("match_content".equals(stringValue)) {
                        return ComponentContainer.LayoutConfig.MATCH_CONTENT;
                    }
                    try {
                        result = typedAttribute.getPixelValue(false);
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

        public Color getColorValue(String name, Color defaultValue) {
            Color result = null;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                result = attrSet.getAttr(name).get().getColorValue();
            }
            if (result == null) {
                if (patternFromTheme != null) {
                    TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        result = AttrHelper.convertValueToColor(typedAttribute.getOriginalValue());
                    }
                }
            }
            if (result == null) {
                if (patternByRes != null) {
                    TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        result = AttrHelper.convertValueToColor(typedAttribute.getOriginalValue());
                    }
                }
            }
            if (result == null) {
                result = defaultValue;
            }
            return result;
        }

        public boolean getBoolValue(String name, boolean defaultValue) {
            boolean result = defaultValue;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                result = attrSet.getAttr(name).get().getBoolValue();
                return result;
            }

            if (patternFromTheme != null) {
                TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                if (typedAttribute != null) {
                    try {
                        result = typedAttribute.getBooleanValue();
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (patternByRes != null) {
                TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                if (typedAttribute != null) {
                    try {
                        result = typedAttribute.getBooleanValue();
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    } catch (WrongTypeException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }

        public String getStringValue(String name, String defaultValue) {
            String result = null;
            if (attrSet != null && attrSet.getAttr(name).isPresent()) {
                result = attrSet.getAttr(name).get().getStringValue();
            }
            if (result == null) {
                if (patternFromTheme != null) {
                    TypedAttribute typedAttribute = patternFromTheme.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        try {
                            result = typedAttribute.getStringValue();
                            return result;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        } catch (WrongTypeException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (result == null) {
                if (patternByRes != null) {
                    TypedAttribute typedAttribute = patternByRes.getPatternHash().get(name);
                    if (typedAttribute != null) {
                        try {
                            result = typedAttribute.getStringValue();
                            return result;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        } catch (WrongTypeException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (result == null) {
                result = defaultValue;
            }
            return result;
        }

        public void recycle() {
            attrSet = null;
            patternFromTheme = null;
            patternByRes = null;
            keys.clear();
            keys = null;
        }
    }
}
