package com.rey.material.drawable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;

import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

import java.util.ArrayList;
import java.util.List;

public class LineMorphingDrawable extends Element implements Animatable {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "LineMorphingDrawable");

    private boolean mRunning = false;

    private Paint mPaint;

    private int mWidth;
    private int mHeight;

    private int mPaddingLeft = 12;
    private int mPaddingTop = 12;
    private int mPaddingRight = 12;
    private int mPaddingBottom = 12;

    private RectFloat mDrawBound;

    private int mPrevState;
    private int mCurState;
    private long mStartTime;
    private float mAnimProgress;
    private int mAnimDuration;
    private Interpolator mInterpolator;
    private int mStrokeSize;
    private int mStrokeColor;
    private boolean mClockwise;
    private Paint.StrokeCap mStrokeCap;
    private Paint.Join mStrokeJoin;
    private boolean mIsRtl;

    private Path mPath;

    private State[] mStates;

    private Component mComponent;
    private EventHandler mHandler;

    private LineMorphingDrawable(Component component, State[] states, int curState, int width, int height, int paddingLeft, int paddingTop, int paddingRight, int paddingBottom, int animDuration, Interpolator interpolator, int strokeSize, int strokeColor, Paint.StrokeCap strokeCap, Paint.Join strokeJoin, boolean clockwise, boolean isRtl) {

        mComponent = component;
        mStates = states;
        mWidth = width;
        mHeight = height;

        mPaddingLeft = paddingLeft;
        mPaddingTop = paddingTop;
        mPaddingRight = paddingRight;
        mPaddingBottom = paddingBottom;

        mAnimDuration = animDuration;
        mInterpolator = interpolator;
        mStrokeSize = strokeSize;
        mStrokeColor = strokeColor;
        mStrokeCap = strokeCap;
        mStrokeJoin = strokeJoin;
        mClockwise = clockwise;
        mIsRtl = isRtl;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setStrokeCap(mStrokeCap);
        mPaint.setStrokeJoin(mStrokeJoin);
        mPaint.setColor(new Color(mStrokeColor));
        mPaint.setStrokeWidth(mStrokeSize);

        mDrawBound = new RectFloat();

        mPath = new Path();

        switchLineState(curState, false);
        mHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        boundChanges();
        int restoreCount = canvas.save();
        float degrees = (mClockwise ? 180 : -180) * ((mPrevState < mCurState ? 0f : 1f) + mAnimProgress);

        if (mIsRtl)
            canvas.scale(-1f, 1f, mDrawBound.getHorizontalCenter(), mDrawBound.getVerticalCenter());

        canvas.rotate(degrees, mDrawBound.getHorizontalCenter(), mDrawBound.getVerticalCenter());
        canvas.drawPath(mPath, mPaint);
        canvas.restoreToCount(restoreCount);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    private void boundChanges() {
        if (mWidth > 0 && mHeight > 0) {
            mDrawBound.left = (mComponent.getWidth() - mWidth) / 2f;
            mDrawBound.top = (mComponent.getHeight() - mHeight) / 2f;
            mDrawBound.right = mDrawBound.left + mWidth;
            mDrawBound.bottom = mDrawBound.top + mHeight;
        } else {
            mDrawBound.left = mPaddingLeft;
            mDrawBound.top = mPaddingTop;
            mDrawBound.right = mComponent.getWidth() - mPaddingRight;
            mDrawBound.bottom = mComponent.getHeight() - mPaddingBottom;
        }

        updatePath();
    }

    public void switchLineState(int state, boolean animation) {
        if (mCurState != state) {
            mPrevState = mCurState;
            mCurState = state;
            if (animation)
                start();
            else {
                mAnimProgress = 1f;
                updatePath();
            }
        } else if (!animation) {
            mAnimProgress = 1f;
            updatePath();
        }
    }

    public boolean setLineState(int state, float progress) {
        if (mCurState != state) {
            mPrevState = mCurState;
            mCurState = state;
            mAnimProgress = progress;
            updatePath();
            return true;
        } else if (mAnimProgress != progress) {
            mAnimProgress = progress;
            updatePath();
            return true;
        }

        return false;
    }

    public int getLineState() {
        return mCurState;
    }

    public int getLineStateCount() {
        return mStates == null ? 0 : mStates.length;
    }

    public float getAnimProgress() {
        return mAnimProgress;
    }

    private void updatePath() {
        mPath.reset();

        if (mStates == null)
            return;

        if (mAnimProgress == 0f || (mStates[mPrevState].links != null && mAnimProgress < 0.05f))
            updatePathWithState(mPath, mStates[mPrevState]);
        else if (mAnimProgress == 1f || (mStates[mCurState].links != null && mAnimProgress > 0.95f))
            updatePathWithState(mPath, mStates[mCurState]);
        else
            updatePathBetweenStates(mPath, mStates[mPrevState], mStates[mCurState], mInterpolator.getInterpolation(mAnimProgress));

        mComponent.invalidate();
    }

    private void updatePathWithState(Path path, State state) {
        if (state.links != null) {
            for (int i = 0; i < state.links.length; i += 2) {
                int index1 = state.links[i] * 4;
                int index2 = state.links[i + 1] * 4;

                float x1 = getX(state.points[index1]);
                float y1 = getY(state.points[index1 + 1]);
                float x2 = getX(state.points[index1 + 2]);
                float y2 = getY(state.points[index1 + 3]);

                float x3 = getX(state.points[index2]);
                float y3 = getY(state.points[index2 + 1]);
                float x4 = getX(state.points[index2 + 2]);
                float y4 = getY(state.points[index2 + 3]);

                if (x1 == x3 && y1 == y3) {
                    path.moveTo(x2, y2);
                    path.lineTo(x1, y1);
                    path.lineTo(x4, y4);
                } else if (x1 == x4 && y1 == y4) {
                    path.moveTo(x2, y2);
                    path.lineTo(x1, y1);
                    path.lineTo(x3, y3);
                } else if (x2 == x3 && y2 == y3) {
                    path.moveTo(x1, y1);
                    path.lineTo(x2, y2);
                    path.lineTo(x4, y4);
                } else {
                    path.moveTo(x1, y1);
                    path.lineTo(x2, y2);
                    path.lineTo(x3, y3);
                }
            }

            for (int i = 0, count = state.points.length / 4; i < count; i++) {
                boolean exist = false;
                for (int j = 0; j < state.links.length; j++)
                    if (state.links[j] == i) {
                        exist = true;
                        break;
                    }

                if (exist)
                    continue;

                int index = i * 4;

                path.moveTo(getX(state.points[index]), getY(state.points[index + 1]));
                path.lineTo(getX(state.points[index + 2]), getY(state.points[index + 3]));
            }
        } else {
            for (int i = 0, count = state.points.length / 4; i < count; i++) {
                int index = i * 4;

                path.moveTo(getX(state.points[index]), getY(state.points[index + 1]));
                path.lineTo(getX(state.points[index + 2]), getY(state.points[index + 3]));
            }
        }
    }

    private void updatePathBetweenStates(Path path, State prev, State cur, float progress) {
        int count = Math.max(prev.points.length, cur.points.length) / 4;

        for (int i = 0; i < count; i++) {
            int index = i * 4;

            float x1;
            float y1;
            float x2;
            float y2;
            if (index >= prev.points.length) {
                x1 = 0.5f;
                y1 = 0.5f;
                x2 = 0.5f;
                y2 = 0.5f;
            } else {
                x1 = prev.points[index];
                y1 = prev.points[index + 1];
                x2 = prev.points[index + 2];
                y2 = prev.points[index + 3];
            }

            float x3;
            float y3;
            float x4;
            float y4;
            if (index >= cur.points.length) {
                x3 = 0.5f;
                y3 = 0.5f;
                x4 = 0.5f;
                y4 = 0.5f;
            } else {
                x3 = cur.points[index];
                y3 = cur.points[index + 1];
                x4 = cur.points[index + 2];
                y4 = cur.points[index + 3];
            }

            mPath.moveTo(getX(x1 + (x3 - x1) * progress), getY(y1 + (y3 - y1) * progress));
            mPath.lineTo(getX(x2 + (x4 - x2) * progress), getY(y2 + (y4 - y2) * progress));
        }
    }

    private float getX(float value) {
        return mDrawBound.left + mDrawBound.getWidth() * value;
    }

    private float getY(float value) {
        return mDrawBound.top + mDrawBound.getHeight() * value;
    }

    //Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mAnimProgress = 0f;
    }

    public void cancel() {
        stop();
        setLineState(mCurState, 1f);
    }

    @Override
    public void start() {
        resetAnimation();

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        mComponent.invalidate();
    }

    @Override
    public void stop() {
        if (!isRunning())
            return;

        mRunning = false;
        mHandler.removeTask(mUpdater);
        mComponent.invalidate();
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        float value = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (value == 1f) {
            setLineState(mCurState, 1f);
            mRunning = false;
        } else
            setLineState(mCurState, mInterpolator.getInterpolation(value));

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
    }

    public static class State {
        float[] points;
        int[] links;

        public State() {
        }

        public State(float[] points, int[] links) {
            this.points = points;
            this.links = links;
        }
    }

    public void createNativePtr(Object obj) {

    }

    public static class Builder {
        private int mCurState;

        private int mWidth;
        private int mHeight;

        private int mPaddingLeft;
        private int mPaddingTop;
        private int mPaddingRight;
        private int mPaddingBottom;

        private int mAnimDuration;
        private Interpolator mInterpolator;
        private int mStrokeSize;
        private int mStrokeColor;
        private boolean mClockwise;
        private Paint.StrokeCap mStrokeCap;
        private Paint.Join mStrokeJoin;
        private boolean mIsRtl;
        private Component mComponent;

        private State[] mStates;

        private static final String TAG_STATE_LIST = "state-list";
        private static final String TAG_STATE = "state";
        private static final String TAG_POINTS = "points";
        private static final String TAG_LINKS = "links";
        private static final String TAG_ITEM = "item";

        public Builder() {
        }

        public Builder(Component component, Context context, int defStyleRes) {
            this(component, context, null, defStyleRes);
        }

        public Builder(Component component, Context context, AttrSet attrs, int defStyleRes) {

//			if((resId = a.getResourceId(R.styleable.LineMorphingDrawable_lmd_state, 0)) != 0)
            mComponent = component;
            states(readStates());
            curState(Util.getIntegerValue(attrs, "lmd_curState", 0));
            width(Util.getDimensionValue(attrs, "lmd_width", 0));
            height(Util.getDimensionValue(attrs, "lmd_height", 0));
            padding(Util.getDimensionValue(attrs, "lmd_padding", 0));
            paddingLeft(Util.getDimensionValue(attrs, "lmd_paddingLeft", mPaddingLeft));
            paddingTop(Util.getDimensionValue(attrs, "lmd_paddingTop", mPaddingTop));
            paddingRight(Util.getDimensionValue(attrs, "lmd_paddingRight", mPaddingRight));
            paddingBottom(Util.getDimensionValue(attrs, "lmd_paddingBottom", mPaddingBottom));
            animDuration(Util.getIntegerValue(attrs, "lmd_animDuration", 400));
            strokeSize(Util.getDimensionValue(attrs, "lmd_strokeSize", 9));
            strokeColor(Util.getColorValue(attrs, "lmd_strokeColor", new Color(0xFFFFFFFF)).getValue());
            int cap = Util.getIntegerValue(attrs, "lmd_strokeCap", 0);
            if (cap == 0)
                strokeCap(Paint.StrokeCap.BUTT_CAP);
            else if (cap == 1)
                strokeCap(Paint.StrokeCap.ROUND_CAP);
            else
                strokeCap(Paint.StrokeCap.SQUARE_CAP);
            int join = Util.getIntegerValue(attrs, "lmd_strokeJoin", 0);
            if (join == 0)
                strokeJoin(Paint.Join.MITER_JOIN);
            else if (join == 1)
                strokeJoin(Paint.Join.ROUND_JOIN);
            else
                strokeJoin(Paint.Join.BEVEL_JOIN);
            clockwise(Util.getBoolValue(attrs, "lmd_clockwise", true));
            int direction = Util.getIntegerValue(attrs, "lmd_layoutDirection", 0);
            if (direction == 3) {
                //rtl(TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == ViewCompat.LAYOUT_DIRECTION_RTL);
            } else
                rtl(direction == 1);
        }

        private State[] readStates() {
            List<State> states = new ArrayList<>();
            State stateOne = new State();
            stateOne.points = new float[8];
            stateOne.points[0] = 0.5f;
            stateOne.points[1] = 0;
            stateOne.points[2] = 0.5f;
            stateOne.points[3] = 1f;
            stateOne.points[4] = 0;
            stateOne.points[5] = 0.5f;
            stateOne.points[6] = 1f;
            stateOne.points[7] = 0.5f;
            states.add(stateOne);

            State stateTwo = new State();
            stateTwo.points = new float[8];
            stateTwo.points[0] = 0.633f;
            stateTwo.points[1] = 0.161f;
            stateTwo.points[2] = 0;
            stateTwo.points[3] = 0.793f;
            stateTwo.points[4] = 0.633f;
            stateTwo.points[5] = 0.161f;
            stateTwo.points[6] = 1f;
            stateTwo.points[7] = 0.527f;
            stateTwo.links = new int[2];
            stateTwo.links[0] = 0;
            stateTwo.links[1] = 1;
            states.add(stateTwo);

            return states.toArray(new State[states.size()]);
        }

        public LineMorphingDrawable build() {
            if (mStrokeCap == null)
                mStrokeCap = Paint.StrokeCap.BUTT_CAP;

            if (mStrokeJoin == null)
                mStrokeJoin = Paint.Join.MITER_JOIN;

            if (mInterpolator == null)
                mInterpolator = new AccelerateInterpolator();

            return new LineMorphingDrawable(mComponent, mStates, mCurState, mWidth, mHeight, mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom, mAnimDuration, mInterpolator, mStrokeSize, mStrokeColor, mStrokeCap, mStrokeJoin, mClockwise, mIsRtl);
        }

        public Builder states(State... states) {
            mStates = states;
            return this;
        }

        public Builder curState(int state) {
            mCurState = state;
            return this;
        }

        public Builder width(int width) {
            mWidth = width;
            return this;
        }

        public Builder height(int height) {
            mHeight = height;
            return this;
        }

        public Builder padding(int padding) {
            mPaddingLeft = padding;
            mPaddingTop = padding;
            mPaddingRight = padding;
            mPaddingBottom = padding;
            return this;
        }

        public Builder paddingLeft(int padding) {
            mPaddingLeft = padding;
            return this;
        }

        public Builder paddingTop(int padding) {
            mPaddingTop = padding;
            return this;
        }

        public Builder paddingRight(int padding) {
            mPaddingRight = padding;
            return this;
        }

        public Builder paddingBottom(int padding) {
            mPaddingBottom = padding;
            return this;
        }

        public Builder animDuration(int duration) {
            mAnimDuration = duration;
            return this;
        }

        public Builder interpolator(Interpolator interpolator) {
            mInterpolator = interpolator;
            return this;
        }

        public Builder strokeSize(int size) {
            mStrokeSize = size;
            return this;
        }

        public Builder strokeColor(int strokeColor) {
            mStrokeColor = strokeColor;
            return this;
        }

        public Builder strokeCap(Paint.StrokeCap cap) {
            mStrokeCap = cap;
            return this;
        }

        public Builder strokeJoin(Paint.Join join) {
            mStrokeJoin = join;
            return this;
        }

        public Builder clockwise(boolean clockwise) {
            mClockwise = clockwise;
            return this;
        }

        public Builder rtl(boolean rtl) {
            mIsRtl = rtl;
            return this;
        }
    }
}
