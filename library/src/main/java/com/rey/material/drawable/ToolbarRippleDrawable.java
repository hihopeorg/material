package com.rey.material.drawable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.miscservices.timeutility.Time;

import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class ToolbarRippleDrawable extends Element implements Animatable, Component.DrawTask {

    private boolean mRunning = false;

    private Paint mShaderPaint;
    private Paint mFillPaint;
    private RadialShader mInShader;
    private RadialShader mOutShader;
    private Matrix mMatrix;
    private int mAlpha = 255;

    private RectFloat mBackgroundBounds;
    private Path mBackground;
    private int mBackgroundAnimDuration;
    private int mBackgroundColor;
    private float mBackgroundAlphaPercent;

    private Point mRipplePoint;
    private float mRippleRadius;
    private int mRippleType;
    private int mMaxRippleRadius;
    private int mRippleAnimDuration;
    private int mRippleColor;
    private float mRippleAlphaPercent;
    private int mDelayClickType;

    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;

    private long mStartTime;

    private boolean mPressed = false;

    private int mState = STATE_OUT;

    private static final int STATE_OUT = 0;
    private static final int STATE_PRESS = 1;
    private static final int STATE_HOVER = 2;
    private static final int STATE_RELEASE_ON_HOLD = 3;
    private static final int STATE_RELEASE = 4;

    private static final int TYPE_TOUCH_MATCH_VIEW = -1;
    private static final int TYPE_TOUCH = 0;
    private static final int TYPE_WAVE = 1;

    private static final float[] GRADIENT_STOPS = new float[]{0f, 0.99f, 1f};
    private static final float GRADIENT_RADIUS = 16;

    private EventHandler mHandler;

    private ToolbarRippleDrawable(int backgroundAnimDuration, int backgroundColor, int rippleType, int delayClickType, int maxTouchRadius, int touchAnimDuration, int touchColor, Interpolator inInterpolator, Interpolator outInterpolator) {
        mBackgroundAnimDuration = backgroundAnimDuration;
        mBackgroundColor = backgroundColor;

        mRippleType = rippleType;
        mMaxRippleRadius = maxTouchRadius;
        mRippleAnimDuration = touchAnimDuration;
        mRippleColor = touchColor;
        mDelayClickType = delayClickType;

        if (mRippleType == TYPE_TOUCH && mMaxRippleRadius <= 0)
            mRippleType = TYPE_TOUCH_MATCH_VIEW;

        mInInterpolator = inInterpolator;
        mOutInterpolator = outInterpolator;

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL_STYLE);

        mShaderPaint = new Paint();
        mShaderPaint.setStyle(Paint.Style.FILL_STYLE);

        mBackground = new Path();
        mBackgroundBounds = new RectFloat();

        mRipplePoint = new Point();

        mMatrix = new Matrix();

        mInShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                new Color[]{new Color(mRippleColor), new Color(mRippleColor), new Color(0)}, Shader.TileMode.CLAMP_TILEMODE);
        if (mRippleType == TYPE_WAVE)
            mOutShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                    new Color[]{new Color(0), new Color(ColorUtil.getColor(mRippleColor, 0f)), new Color(mRippleColor)}, Shader.TileMode.CLAMP_TILEMODE);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
        setCallback(mOnChangeListener);
    }

    public int getDelayClickType() {
        return mDelayClickType;
    }

    public void setDelayClickType(int type) {
        mDelayClickType = type;
    }

    @Override
    public void setAlpha(int alpha) {
        mAlpha = alpha;
    }

    public long getClickDelayTime() {
        switch (mDelayClickType) {
            case RippleDrawable.DELAY_CLICK_NONE:
                return -1;
            case RippleDrawable.DELAY_CLICK_UNTIL_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
            case RippleDrawable.DELAY_CLICK_AFTER_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return 2 * Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                else if (mState == STATE_RELEASE)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
        }

        return -1;
    }

    private void setRippleState(int state) {
        if (mState != state) {
            mState = state;

            if (mState != STATE_OUT) {
                if (mState != STATE_HOVER)
                    start();
                else
                    stop();
            } else
                stop();
        }
    }

    private boolean setRippleEffect(float x, float y, float radius) {
        if (mRipplePoint.getPointX() != x || mRipplePoint.getPointY() != y || mRippleRadius != radius) {
            mRipplePoint.modify(x, y);
            mRippleRadius = radius;
            radius = mRippleRadius / GRADIENT_RADIUS;
            mMatrix.reset();
            mMatrix.postTranslate(x, y);
            mMatrix.postScale(radius, radius, x, y);
            mInShader.setShaderMatrix(mMatrix);
            if (mOutShader != null)
                mOutShader.setShaderMatrix(mMatrix);

            return true;
        }

        return false;
    }

    Element.OnChangeListener mOnChangeListener = new OnChangeListener() {
        @Override
        public void onChange(Element element) {
            Rect bounds = element.getBounds();
            mBackgroundBounds.modify(bounds.left, bounds.top, bounds.right, bounds.bottom);
            mBackground.reset();
            mBackground.addRect(mBackgroundBounds, Path.Direction.CLOCK_WISE);
        }
    };

    @Override
    public boolean isStateful() {
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        switch (mRippleType) {
            case TYPE_TOUCH:
            case TYPE_TOUCH_MATCH_VIEW:
                drawTouch(canvas);
                break;
            case TYPE_WAVE:
                drawWave(canvas);
                break;
        }
    }

    private void drawTouch(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mBackgroundAlphaPercent > 0) {
                mFillPaint.setColor(new Color(mBackgroundColor));
                mFillPaint.setAlpha(Math.round(mAlpha * mBackgroundAlphaPercent));
                canvas.drawPath(mBackground, mFillPaint);
            }

            if (mRippleRadius > 0 && mRippleAlphaPercent > 0) {
                mShaderPaint.setAlpha(Math.round(mAlpha * mRippleAlphaPercent));
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private void drawWave(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mState == STATE_RELEASE) {
                if (mRippleRadius == 0) {
                    mFillPaint.setColor(new Color(mRippleColor));
                    canvas.drawPath(mBackground, mFillPaint);
                } else {
                    mShaderPaint.setShader(mOutShader, Paint.ShaderType.RADIAL_SHADER);
                    canvas.drawPath(mBackground, mShaderPaint);
                }
            } else if (mRippleRadius > 0) {
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private int getMaxRippleRadius(float x, float y) {
        float x1 = x < mBackgroundBounds.getHorizontalCenter() ? mBackgroundBounds.right : mBackgroundBounds.left;
        float y1 = y < mBackgroundBounds.getVerticalCenter() ? mBackgroundBounds.bottom : mBackgroundBounds.top;

        return (int) Math.round(Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2)));
    }

    //Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/

    public void cancel() {
        setRippleState(STATE_OUT);
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
    }

    @Override
    public void start() {
        if (isRunning())
            return;

        resetAnimation();

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
    }

    @Override
    public void stop() {
        if (!isRunning())
            return;

        mRunning = false;
        mHandler.removeTask(mUpdater);
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            switch (mRippleType) {
                case TYPE_TOUCH:
                case TYPE_TOUCH_MATCH_VIEW:
                    updateTouch();
                    break;
                case TYPE_WAVE:
                    updateWave();
                    break;
            }

        }

    };

    private void updateTouch() {
        if (mState != STATE_RELEASE) {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = mInInterpolator.getInterpolation(backgroundProgress) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = mInInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(touchProgress));

            if (backgroundProgress == 1f && touchProgress == 1f) {
                mStartTime = Time.getRealActiveTime();
                setRippleState(mState == STATE_PRESS ? STATE_HOVER : STATE_RELEASE);
            }

        } else {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = (1f - mOutInterpolator.getInterpolation(backgroundProgress)) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = 1f - mOutInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * (1f + 0.5f * mOutInterpolator.getInterpolation(touchProgress)));

            if (backgroundProgress == 1f && touchProgress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

    }

    private void updateWave() {
        float progress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);

        if (mState != STATE_RELEASE) {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(progress));

            if (progress == 1f) {
                mStartTime = Time.getRealActiveTime();
                if (mState == STATE_PRESS)
                    setRippleState(STATE_HOVER);
                else {
                    setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), 0);
                    setRippleState(STATE_RELEASE);
                }
            }
        } else {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mOutInterpolator.getInterpolation(progress));

            if (progress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

    }

    public void createNativePtr(Object obj) {

    }

    public static class Builder {
        private int mBackgroundAnimDuration = 200;
        private int mBackgroundColor;

        private int mRippleType;
        private int mMaxRippleRadius;
        private int mRippleAnimDuration = 400;
        private int mRippleColor;
        private int mDelayClickType;

        private Interpolator mInInterpolator;
        private Interpolator mOutInterpolator;

        public Builder() {
        }

        public Builder(Context context, int defStyleRes) {
            this(context, null, 0, defStyleRes);
        }

        public Builder(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

            backgroundColor(Util.getColorValue(attrs, "rd_backgroundColor", new Color(0)).getValue());
            backgroundAnimDuration(Util.getIntegerValue(attrs, "rd_backgroundAnimDuration", 400));
            rippleType(Util.getIntegerValue(attrs, "rd_rippleType", ToolbarRippleDrawable.TYPE_TOUCH));
            delayClickType(Util.getIntegerValue(attrs, "rd_delayClick", RippleDrawable.DELAY_CLICK_NONE));
            maxRippleRadius(Util.getDimensionValue(attrs, "rd_maxRippleRadius", 48));
            rippleColor(Util.getColorValue(attrs, "rd_rippleColor", new Color(0)).getValue());
            rippleAnimDuration(Util.getIntegerValue(attrs, "rd_rippleAnimDuration", 400));
        }

        public ToolbarRippleDrawable build() {
            if (mInInterpolator == null)
                mInInterpolator = new AccelerateInterpolator();

            if (mOutInterpolator == null)
                mOutInterpolator = new DecelerateInterpolator();

            return new ToolbarRippleDrawable(mBackgroundAnimDuration, mBackgroundColor, mRippleType, mDelayClickType, mMaxRippleRadius, mRippleAnimDuration, mRippleColor, mInInterpolator, mOutInterpolator);
        }

        public Builder backgroundAnimDuration(int duration) {
            mBackgroundAnimDuration = duration;
            return this;
        }

        public Builder backgroundColor(int color) {
            mBackgroundColor = color;
            return this;
        }

        public Builder rippleType(int type) {
            mRippleType = type;
            return this;
        }

        public Builder delayClickType(int type) {
            mDelayClickType = type;
            return this;
        }

        public Builder maxRippleRadius(int radius) {
            mMaxRippleRadius = radius;
            return this;
        }

        public Builder rippleAnimDuration(int duration) {
            mRippleAnimDuration = duration;
            return this;
        }

        public Builder rippleColor(int color) {
            mRippleColor = color;
            return this;
        }

        public Builder inInterpolator(Interpolator interpolator) {
            mInInterpolator = interpolator;
            return this;
        }

        public Builder outInterpolator(Interpolator interpolator) {
            mOutInterpolator = interpolator;
            return this;
        }
    }
}
