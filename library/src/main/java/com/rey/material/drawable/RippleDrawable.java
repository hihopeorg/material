package com.rey.material.drawable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class RippleDrawable extends Element implements Animatable, Component.DrawTask, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "RippleDrawable");

    private boolean mRunning = false;

    private Paint mShaderPaint;
    private Paint mFillPaint;
    private Mask mMask;
    private RadialShader mInShader;
    private RadialShader mOutShader;
    private Matrix mMatrix;
    private int mAlpha = 255;

    private Element mBackgroundDrawable;
    private RectFloat mBackgroundBounds;
    private Path mBackground;
    private int mBackgroundAnimDuration;
    private int mBackgroundColor;
    private float mBackgroundAlphaPercent;

    private Point mRipplePoint;
    private float mRippleRadius;
    private int mRippleType;
    private int mMaxRippleRadius;
    private int mRippleAnimDuration;
    private int mRippleColor;
    private float mRippleAlphaPercent;
    private int mDelayClickType;

    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;

    private long mStartTime;

    private long mTouchTime;
    private int mDelayRippleTime;

    private int mState = STATE_OUT;

    public static final int DELAY_CLICK_NONE = 0;
    public static final int DELAY_CLICK_UNTIL_RELEASE = 1;
    public static final int DELAY_CLICK_AFTER_RELEASE = 2;

    private static final int STATE_OUT = 0;
    private static final int STATE_PRESS = 1;
    private static final int STATE_HOVER = 2;
    private static final int STATE_RELEASE_ON_HOLD = 3;
    private static final int STATE_RELEASE = 4;

    private static final int TYPE_TOUCH_MATCH_VIEW = -1;
    private static final int TYPE_TOUCH = 0;
    private static final int TYPE_WAVE = 1;

    private static final float[] GRADIENT_STOPS = new float[]{0f, 0.99f, 1f};
    private static final float GRADIENT_RADIUS = 16;

    private EventHandler mHandler;
    private Component mComponent;

    private RippleDrawable(Element backgroundDrawable, int backgroundAnimDuration, int backgroundColor, int rippleType, int delayClickType, int delayRippleTime, int maxRippleRadius, int rippleAnimDuration, int rippleColor, Interpolator inInterpolator, Interpolator outInterpolator, int type, int topLeftCornerRadius, int topRightCornerRadius, int bottomRightCornerRadius, int bottomLeftCornerRadius, int left, int top, int right, int bottom) {
        setBackgroundDrawable(backgroundDrawable);
        mBackgroundAnimDuration = backgroundAnimDuration;
        mBackgroundColor = backgroundColor;

        mRippleType = rippleType;
        setDelayClickType(delayClickType);
        mDelayRippleTime = delayRippleTime;
        mMaxRippleRadius = maxRippleRadius;
        mRippleAnimDuration = rippleAnimDuration;
        mRippleColor = rippleColor;

        if (mRippleType == TYPE_TOUCH && mMaxRippleRadius <= 0)
            mRippleType = TYPE_TOUCH_MATCH_VIEW;

        mInInterpolator = inInterpolator;
        mOutInterpolator = outInterpolator;

        setMask(type, topLeftCornerRadius, topRightCornerRadius, bottomRightCornerRadius, bottomLeftCornerRadius, left, top, right, bottom);

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL_STYLE);

        mShaderPaint = new Paint();
        mShaderPaint.setStyle(Paint.Style.FILL_STYLE);

        mBackground = new Path();
        mBackgroundBounds = new RectFloat();

        mRipplePoint = new Point();

        mMatrix = new Matrix();

        mInShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                new Color[]{new Color(mRippleColor), new Color(mRippleColor), new Color(0)}, Shader.TileMode.CLAMP_TILEMODE);
        if (mRippleType == TYPE_WAVE)
            mOutShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                    new Color[]{new Color(0), new Color(ColorUtil.getColor(mRippleColor, 0f)), new Color(mRippleColor)},
                    Shader.TileMode.CLAMP_TILEMODE);
        setCallback(mOnChangeListener);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public void setBackgroundDrawable(Element backgroundDrawable) {
        mBackgroundDrawable = backgroundDrawable;
        if (mBackgroundDrawable != null)
            mBackgroundDrawable.setBounds(getBounds());
    }

    public Element getBackgroundDrawable() {
        return mBackgroundDrawable;
    }

    public int getDelayClickType() {
        return mDelayClickType;
    }

    public void setDelayClickType(int type) {
        mDelayClickType = type;
    }

    public void setMask(int type, int topLeftCornerRadius, int topRightCornerRadius, int bottomRightCornerRadius, int bottomLeftCornerRadius, int left, int top, int right, int bottom) {
        mMask = new Mask(type, topLeftCornerRadius, topRightCornerRadius, bottomRightCornerRadius, bottomLeftCornerRadius, left, top, right, bottom);
    }

    @Override
    public void setAlpha(int alpha) {
        mAlpha = alpha;
        if (mBackgroundDrawable != null)
            mBackgroundDrawable.setAlpha(alpha);
    }

    public long getClickDelayTime() {
        switch (mDelayClickType) {
            case DELAY_CLICK_NONE:
                return -1;
            case DELAY_CLICK_UNTIL_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
            case DELAY_CLICK_AFTER_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return 2 * Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                else if (mState == STATE_RELEASE)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
        }

        return -1;
    }

    private void setRippleState(int state) {
        if (mState != state) {
            //fix bug incorrect state switch
            if (mState == STATE_OUT && state != STATE_PRESS)
                return;

            mState = state;

            if (mState == STATE_OUT || mState == STATE_HOVER)
                stop();
            else
                start();
        }
    }

    private boolean setRippleEffect(float x, float y, float radius) {

        if (mRipplePoint.getPointX() != x || mRipplePoint.getPointY() != y || mRippleRadius != radius) {
            mRipplePoint.modify(x, y);
            mRippleRadius = radius;
            mInShader = new RadialShader(new Point(x, y), mRippleRadius / 1.5f, GRADIENT_STOPS,
                    new Color[]{new Color(mRippleColor), new Color(mRippleColor), new Color(0)}, Shader.TileMode.CLAMP_TILEMODE);
            if (mOutShader != null)
                mOutShader = new RadialShader(new Point(x, y), mRippleRadius / 1.5f, GRADIENT_STOPS,
                        new Color[]{new Color(0), new Color(ColorUtil.getColor(mRippleColor, 0f)), new Color(mRippleColor)},
                        Shader.TileMode.CLAMP_TILEMODE);
            return true;
        }

        return false;
    }

    Element.OnChangeListener mOnChangeListener = new OnChangeListener() {
        @Override
        public void onChange(Element element) {
            Rect bounds = element.getBounds();
            if (mBackgroundDrawable != null)
                mBackgroundDrawable.setBounds(bounds);

            mBackgroundBounds.modify(bounds.left + mMask.left, bounds.top + mMask.top, bounds.right - mMask.right, bounds.bottom - mMask.bottom);
            mBackground.reset();

            switch (mMask.type) {
                case Mask.TYPE_OVAL:
                    mBackground.addOval(mBackgroundBounds, Path.Direction.CLOCK_WISE);
                    break;
                case Mask.TYPE_RECTANGLE:
                    mBackground.addRoundRect(mBackgroundBounds, mMask.cornerRadius, Path.Direction.CLOCK_WISE);
                    break;
            }
        }
    };

    private void addPath(Component component) {
        int width = component.getWidth();
        int height = component.getHeight();
        mBackgroundBounds.modify(mMask.left, mMask.top, width - mMask.right, height - mMask.bottom);
        mBackground.reset();

        switch (mMask.type) {
            case Mask.TYPE_OVAL:
                mBackground.addOval(mBackgroundBounds, Path.Direction.CLOCK_WISE);
                break;
            case Mask.TYPE_RECTANGLE:
                mBackground.addRoundRect(mBackgroundBounds, mMask.cornerRadius, Path.Direction.CLOCK_WISE);
                break;
        }
    }

    @Override
    public boolean isStateful() {
        return mBackgroundDrawable != null && mBackgroundDrawable.isStateful();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mComponent = component;
        addPath(component);
        if (mBackgroundDrawable != null)
            mBackgroundDrawable.drawToCanvas(canvas);

        switch (mRippleType) {
            case TYPE_TOUCH:
            case TYPE_TOUCH_MATCH_VIEW:
                drawTouch(canvas);
                break;
            case TYPE_WAVE:
                drawWave(canvas);
                break;
        }
    }

    private void drawTouch(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mBackgroundAlphaPercent > 0) {
                mFillPaint.setColor(new Color(mBackgroundColor));
                mFillPaint.setAlpha(mBackgroundAlphaPercent);
                canvas.drawPath(mBackground, mFillPaint);
            }
            if (mRippleRadius > 0 && mRippleAlphaPercent > 0) {
                mShaderPaint.setAlpha(mRippleAlphaPercent);
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private void drawWave(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mState == STATE_RELEASE) {
                if (mRippleRadius == 0) {
                    mFillPaint.setColor(new Color(mRippleColor));
                    canvas.drawPath(mBackground, mFillPaint);
                } else {
                    mShaderPaint.setShader(mOutShader, Paint.ShaderType.RADIAL_SHADER);
                    canvas.drawPath(mBackground, mShaderPaint);
                }
            } else if (mRippleRadius > 0) {
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private int getMaxRippleRadius(float x, float y) {
        float x1 = x < mBackgroundBounds.getHorizontalCenter() ? mBackgroundBounds.right : mBackgroundBounds.left;
        float y1 = y < mBackgroundBounds.getVerticalCenter() ? mBackgroundBounds.bottom : mBackgroundBounds.top;

        return (int) Math.round(Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2)));
    }

    @Override
    public boolean onTouchEvent(Component v, TouchEvent event) {

        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = v.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
            case TouchEvent.POINT_MOVE:
                if (mState == STATE_OUT || mState == STATE_RELEASE) {
                    long time = Time.getRealActiveTime();
                    if (mTouchTime == 0)
                        mTouchTime = time;


                    setRippleEffect(x, y, 0);

                    if (mTouchTime <= time - mDelayRippleTime) {
                        if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                            mMaxRippleRadius = getMaxRippleRadius(x, y);
                        setRippleState(STATE_PRESS);
                    }
                } else if (mRippleType == TYPE_TOUCH) {
                    if (setRippleEffect(x, y, mRippleRadius)) {
                        mComponent.invalidate();//invalidateSelf();
                    }
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mTouchTime > 0 && mState == STATE_OUT) {
                    if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                        mMaxRippleRadius = getMaxRippleRadius(x, y);
                    setRippleState(STATE_PRESS);
                }
            case TouchEvent.CANCEL:
                mTouchTime = 0;
                if (mState != STATE_OUT) {
                    if (mState == STATE_HOVER) {
                        if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), 0);

                        setRippleState(STATE_RELEASE);
                    } else
                        setRippleState(STATE_RELEASE_ON_HOLD);
                }
                break;
        }
        return true;
    }

    //Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/

    public void cancel() {
        setRippleState(STATE_OUT);
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
    }

    @Override
    public void start() {
        if (isRunning())
            return;

        resetAnimation();
        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        mComponent.invalidate();
    }

    @Override
    public void stop() {
        mRunning = false;
        mHandler.removeTask(mUpdater);
        mComponent.invalidate();
    }

    @Override
    public boolean isRunning() {
        return mState != STATE_OUT && mState != STATE_HOVER && mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            switch (mRippleType) {
                case TYPE_TOUCH:
                case TYPE_TOUCH_MATCH_VIEW:
                    updateTouch();
                    break;
                case TYPE_WAVE:
                    updateWave();
                    break;
            }
        }

    };

    private void updateTouch() {
        if (mState != STATE_RELEASE) {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = mInInterpolator.getInterpolation(backgroundProgress) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = mInInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(touchProgress));

            if (backgroundProgress == 1f && touchProgress == 1f) {
                mStartTime = Time.getRealActiveTime();
                setRippleState(mState == STATE_PRESS ? STATE_HOVER : STATE_RELEASE);
            }
        } else {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = (1f - mOutInterpolator.getInterpolation(backgroundProgress)) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = 1f - mOutInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * (1f + 0.5f * mOutInterpolator.getInterpolation(touchProgress)));

            if (backgroundProgress == 1f && touchProgress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();
    }

    private void updateWave() {
        float progress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);

        if (mState != STATE_RELEASE) {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(progress));

            if (progress == 1f) {
                mStartTime = Time.getRealActiveTime();
                if (mState == STATE_PRESS)
                    setRippleState(STATE_HOVER);
                else {
                    setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), 0);
                    setRippleState(STATE_RELEASE);
                }
            }
        } else {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mOutInterpolator.getInterpolation(progress));

            if (progress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();
    }

    public void createNativePtr(Object obj) {

    }

    public static class Mask {

        public static final int TYPE_RECTANGLE = 0;
        public static final int TYPE_OVAL = 1;

        final int type;

        final float[] cornerRadius = new float[8];

        final int left;
        final int top;
        final int right;
        final int bottom;

        public Mask(int type, int topLeftCornerRadius, int topRightCornerRadius, int bottomRightCornerRadius, int bottomLeftCornerRadius, int left, int top, int right, int bottom) {
            this.type = type;

            cornerRadius[0] = topLeftCornerRadius;
            cornerRadius[1] = topLeftCornerRadius;

            cornerRadius[2] = topRightCornerRadius;
            cornerRadius[3] = topRightCornerRadius;

            cornerRadius[4] = bottomRightCornerRadius;
            cornerRadius[5] = bottomRightCornerRadius;

            cornerRadius[6] = bottomLeftCornerRadius;
            cornerRadius[7] = bottomLeftCornerRadius;

            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }
    }

    public static class Builder {
        private Element mBackgroundDrawable;
        private int mBackgroundAnimDuration = 200;
        private int mBackgroundColor;

        private int mRippleType;
        private int mMaxRippleRadius;
        private int mRippleAnimDuration = 400;
        private int mRippleColor;
        private int mDelayClickType;
        private int mDelayRippleTime;

        private Interpolator mInInterpolator;
        private Interpolator mOutInterpolator;

        private int mMaskType;
        private int mMaskTopLeftCornerRadius;
        private int mMaskTopRightCornerRadius;
        private int mMaskBottomLeftCornerRadius;
        private int mMaskBottomRightCornerRadius;
        private int mMaskLeft;
        private int mMaskTop;
        private int mMaskRight;
        private int mMaskBottom;

        public Builder() {
        }

        public Builder(Context context, int defStyleRes) {
            this(context, null, 0, defStyleRes);
        }

        public Builder(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

            backgroundColor(Util.getColorValue(attrs, "rd_backgroundColor", new Color(0)).getValue());
            backgroundAnimDuration(Util.getIntegerValue(attrs, "rd_backgroundAnimDuration", 400));
            rippleType(Util.getIntegerValue(attrs, "rd_rippleType", RippleDrawable.TYPE_TOUCH));
            delayClickType(Util.getIntegerValue(attrs, "rd_delayClick", RippleDrawable.DELAY_CLICK_NONE));
            delayRippleTime(Util.getIntegerValue(attrs, "rd_delayRipple", 0));
            maxRippleRadius(Util.getDimensionValue(attrs, "rd_maxRippleRadius", 48));
            rippleColor(Util.getColorValue(attrs, "rd_rippleColor", new Color(0)).getValue());
            rippleAnimDuration(Util.getIntegerValue(attrs, "rd_rippleAnimDuration", 400));
            maskType(Util.getIntegerValue(attrs, "rd_maskType", Mask.TYPE_RECTANGLE));
            cornerRadius(Util.getDimensionValue(attrs, "rd_cornerRadius", 0));
            topLeftCornerRadius(Util.getDimensionValue(attrs, "rd_topLeftCornerRadius", mMaskTopLeftCornerRadius));
            topRightCornerRadius(Util.getDimensionValue(attrs, "rd_topRightCornerRadius", mMaskTopRightCornerRadius));
            bottomRightCornerRadius(Util.getDimensionValue(attrs, "rd_bottomRightCornerRadius", mMaskBottomRightCornerRadius));
            bottomLeftCornerRadius(Util.getDimensionValue(attrs, "rd_bottomLeftCornerRadius", mMaskBottomLeftCornerRadius));
            padding(Util.getDimensionValue(attrs, "rd_padding", 0));
            left(Util.getDimensionValue(attrs, "rd_leftPadding", mMaskLeft));
            right(Util.getDimensionValue(attrs, "rd_rightPadding", mMaskRight));
            top(Util.getDimensionValue(attrs, "rd_topPadding", mMaskTop));
            bottom(Util.getDimensionValue(attrs, "rd_bottomPadding", mMaskBottom));
        }

        public RippleDrawable build() {
            if (mInInterpolator == null)
                mInInterpolator = new AccelerateInterpolator();

            if (mOutInterpolator == null)
                mOutInterpolator = new DecelerateInterpolator();

            return new RippleDrawable(mBackgroundDrawable, mBackgroundAnimDuration, mBackgroundColor, mRippleType, mDelayClickType, mDelayRippleTime, mMaxRippleRadius, mRippleAnimDuration, mRippleColor, mInInterpolator, mOutInterpolator, mMaskType, mMaskTopLeftCornerRadius, mMaskTopRightCornerRadius, mMaskBottomRightCornerRadius, mMaskBottomLeftCornerRadius, mMaskLeft, mMaskTop, mMaskRight, mMaskBottom);
        }

        public Builder backgroundDrawable(Element drawable) {
            mBackgroundDrawable = drawable;
            return this;
        }

        public Builder backgroundAnimDuration(int duration) {
            mBackgroundAnimDuration = duration;
            return this;
        }

        public Builder backgroundColor(int color) {
            mBackgroundColor = color;
            return this;
        }

        public Builder rippleType(int type) {
            mRippleType = type;
            return this;
        }

        public Builder delayClickType(int type) {
            mDelayClickType = type;
            return this;
        }

        public Builder delayRippleTime(int time) {
            mDelayRippleTime = time;
            return this;
        }

        public Builder maxRippleRadius(int radius) {
            mMaxRippleRadius = radius;
            return this;
        }

        public Builder rippleAnimDuration(int duration) {
            mRippleAnimDuration = duration;
            return this;
        }

        public Builder rippleColor(int color) {
            mRippleColor = color;
            return this;
        }

        public Builder inInterpolator(Interpolator interpolator) {
            mInInterpolator = interpolator;
            return this;
        }

        public Builder outInterpolator(Interpolator interpolator) {
            mOutInterpolator = interpolator;
            return this;
        }

        public Builder maskType(int type) {
            mMaskType = type;
            return this;
        }

        public Builder cornerRadius(int radius) {
            mMaskTopLeftCornerRadius = radius;
            mMaskTopRightCornerRadius = radius;
            mMaskBottomLeftCornerRadius = radius;
            mMaskBottomRightCornerRadius = radius;
            return this;
        }

        public Builder topLeftCornerRadius(int radius) {
            mMaskTopLeftCornerRadius = radius;
            return this;
        }

        public Builder topRightCornerRadius(int radius) {
            mMaskTopRightCornerRadius = radius;
            return this;
        }

        public Builder bottomLeftCornerRadius(int radius) {
            mMaskBottomLeftCornerRadius = radius;
            return this;
        }

        public Builder bottomRightCornerRadius(int radius) {
            mMaskBottomRightCornerRadius = radius;
            return this;
        }

        public Builder padding(int padding) {
            mMaskLeft = padding;
            mMaskTop = padding;
            mMaskRight = padding;
            mMaskBottom = padding;
            return this;
        }

        public Builder left(int padding) {
            mMaskLeft = padding;
            return this;
        }

        public Builder top(int padding) {
            mMaskTop = padding;
            return this;
        }

        public Builder right(int padding) {
            mMaskRight = padding;
            return this;
        }

        public Builder bottom(int padding) {
            mMaskBottom = padding;
            return this;
        }
    }
}
