package com.rey.material.drawable;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;

import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;
import com.rey.material.widget.ProgressView;

public class CircularProgressDrawable extends Element implements Animatable, Component.DrawTask {
    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "CircularProgressDrawable");

    private Component mComponent;

    private long mLastUpdateTime;
    private long mLastProgressStateTime;
    private long mLastRunStateTime;

    private int mProgressState;

    private static final int PROGRESS_STATE_HIDE = -1;
    private static final int PROGRESS_STATE_STRETCH = 0;
    private static final int PROGRESS_STATE_KEEP_STRETCH = 1;
    private static final int PROGRESS_STATE_SHRINK = 2;
    private static final int PROGRESS_STATE_KEEP_SHRINK = 3;

    private int mRunState = RUN_STATE_STOPPED;

    private static final int RUN_STATE_STOPPED = 0;
    private static final int RUN_STATE_STARTING = 1;
    private static final int RUN_STATE_STARTED = 2;
    private static final int RUN_STATE_RUNNING = 3;
    private static final int RUN_STATE_STOPPING = 4;

    private Paint mPaint;
    private RectFloat mRect;
    private float mStartAngle;
    private float mSweepAngle;
    private int mStrokeColorIndex;

    private int mPadding;
    private float mInitialAngle;
    private float mProgressPercent;
    private float mSecondaryProgressPercent;
    private float mMaxSweepAngle;
    private float mMinSweepAngle;
    private int mStrokeSize;
    private int[] mStrokeColors;
    private int mStrokeSecondaryColor;
    private boolean mReverse;
    private int mRotateDuration;
    private int mTransformDuration;
    private int mKeepDuration;
    private float mInStepPercent;
    private int[] mInColors;
    private int mInAnimationDuration;
    private int mOutAnimationDuration;
    private int mProgressMode;
    private Interpolator mTransformInterpolator;

    private EventHandler mHandler;

    private CircularProgressDrawable(int padding, float initialAngle, float progressPercent, float secondaryProgressPercent, float maxSweepAngle, float minSweepAngle, int strokeSize, int[] strokeColors, int strokeSecondaryColor, boolean reverse, int rotateDuration, int transformDuration, int keepDuration, Interpolator transformInterpolator, int progressMode, int inAnimDuration, float inStepPercent, int[] inStepColors, int outAnimDuration) {

        mPadding = padding;
        mInitialAngle = initialAngle;
        setProgress(progressPercent);
        setSecondaryProgress(secondaryProgressPercent);
        mMaxSweepAngle = maxSweepAngle;
        mMinSweepAngle = minSweepAngle;
        mStrokeSize = strokeSize;
        mStrokeColors = strokeColors;
        mStrokeSecondaryColor = strokeSecondaryColor;
        mReverse = reverse;
        mRotateDuration = rotateDuration;
        mTransformDuration = transformDuration;
        mKeepDuration = keepDuration;
        mTransformInterpolator = transformInterpolator;
        mProgressMode = progressMode;
        mInAnimationDuration = inAnimDuration;
        mInStepPercent = inStepPercent;
        mInColors = inStepColors;
        mOutAnimationDuration = outAnimDuration;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);

        mRect = new RectFloat();

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public void applyStyle(Context context, AttrSet attrSet, int resId) {

        int strokeColor = 0;
        boolean strokeColorDefined = false;
        int[] strokeColors = null;

        int count = attrSet.getLength();
        for (int i = 0; i < count; i++) {
            Attr attr = attrSet.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("cpd_padding")) {
                mPadding = Util.getDimensionValue(attrSet, name, 0);
            } else if (name.equals("cpd_initialAngle")) {
                mInitialAngle = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("pv_progress")) {
                setProgress(Util.getFloatValue(attrSet, name, 0));
            } else if (name.equals("pv_secondaryProgress")) {
                setSecondaryProgress(Util.getFloatValue(attrSet, name, 0));
            } else if (name.equals("cpd_maxSweepAngle")) {
                mMaxSweepAngle = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_minSweepAngle")) {
                mMinSweepAngle = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_strokeSize")) {
                mStrokeSize = Util.getDimensionValue(attrSet, name, 0);
            } else if (name.equals("cpd_strokeColor")) {
                strokeColor = Util.getColorValue(attrSet, name, new Color(0)).getValue();
                strokeColorDefined = true;
            } else if (name.equals("cpd_strokeColors")) {
                strokeColors = new int[]{0xFF03A9F4, 0xFFD8433C, 0xFFF2AF3A, 0xFF279B5E};
            } else if (name.equals("cpd_strokeSecondaryColor")) {
                mStrokeSecondaryColor = Util.getColorValue(attrSet, name, new Color(0)).getValue();
            } else if (name.equals("cpd_reverse")) {
                mReverse = Util.getBoolValue(attrSet, name, false);
            } else if (name.equals("cpd_rotateDuration")) {
                mRotateDuration = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_transformDuration")) {
                mTransformDuration = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_keepDuration")) {
                mKeepDuration = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_transformInterpolator")) {
                //mTransformInterpolator = AnimationUtils.loadInterpolator(context, a.getResourceId(attr, 0));
            } else if (name.equals("pv_progressMode")) {
                mProgressMode = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_inAnimDuration")) {
                mInAnimationDuration = Util.getIntegerValue(attrSet, name, 0);
            } else if (name.equals("cpd_inStepColors")) {
                mInColors = new int[]{0x4B03A9F4, 0x3303A9F4, 0x1903A9F4};
            } else if (name.equals("cpd_inStepPercent")) {
                mInStepPercent = Util.getFloatValue(attrSet, name, 0);
            } else if (name.equals("cpd_outAnimDuration")) {
                mOutAnimationDuration = Util.getIntegerValue(attrSet, name, 0);
            }
        }

        if (strokeColors != null)
            mStrokeColors = strokeColors;
        else if (strokeColorDefined)
            mStrokeColors = new int[]{strokeColor};

        if (mStrokeColorIndex >= mStrokeColors.length)
            mStrokeColorIndex = 0;

        mComponent.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mComponent = component;
        switch (mProgressMode) {
            case ProgressView.MODE_DETERMINATE:
                drawDeterminate(component, canvas);
                break;
            case ProgressView.MODE_INDETERMINATE:
                drawIndeterminate(component, canvas);
                break;
        }
    }

    private void drawDeterminate(Component component, Canvas canvas) {
        int width = component.getWidth();
        int height = component.getHeight();
        float radius = 0f;
        float size = 0f;

        if (mRunState == RUN_STATE_STARTING) {
            size = (float) mStrokeSize * Math.min(mInAnimationDuration, (Time.getRealActiveTime() - mLastRunStateTime)) / mInAnimationDuration;
            if (size > 0)
                radius = (Math.min(width, height) - mPadding * 2 - mStrokeSize * 2 + size) / 2f;
        } else if (mRunState == RUN_STATE_STOPPING) {
            size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;
            if (size > 0)
                radius = (Math.min(width, height) - mPadding * 2 - mStrokeSize * 2 + size) / 2f;
        } else if (mRunState != RUN_STATE_STOPPED) {
            size = mStrokeSize;
            radius = (Math.min(width, height) - mPadding * 2 - mStrokeSize) / 2f;
        }

        if (radius > 0) {
            float x = width / 2f;
            float y = height / 2f;

            mPaint.setStrokeWidth(size);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            if (mProgressPercent == 1f) {
                mPaint.setColor(new Color(mStrokeColors[0]));
                canvas.drawCircle(x, y, radius, mPaint);
            } else if (mProgressPercent == 0f) {
                mPaint.setColor(new Color(mStrokeSecondaryColor));
                canvas.drawCircle(x, y, radius, mPaint);
            } else {
                float sweepAngle = (mReverse ? -360 : 360) * mProgressPercent;

                mRect.modify(x - radius, y - radius, x + radius, y + radius);
                mPaint.setColor(new Color(mStrokeSecondaryColor));
                canvas.drawArc(mRect, new Arc(mStartAngle + sweepAngle, (mReverse ? -360 : 360) - sweepAngle, false), mPaint);

                mPaint.setColor(new Color(mStrokeColors[0]));
                canvas.drawArc(mRect, new Arc(mStartAngle, sweepAngle, false), mPaint);
            }
        }
    }

    private int getIndeterminateStrokeColor() {
        if (mProgressState != PROGRESS_STATE_KEEP_SHRINK || mStrokeColors.length == 1)
            return mStrokeColors[mStrokeColorIndex];

        float value = Math.max(0f, Math.min(1f, (float) (Time.getRealActiveTime() - mLastProgressStateTime) / mKeepDuration));
        int prev_index = mStrokeColorIndex == 0 ? mStrokeColors.length - 1 : mStrokeColorIndex - 1;

        return ColorUtil.getMiddleColor(mStrokeColors[prev_index], mStrokeColors[mStrokeColorIndex], value);
    }

    private void drawIndeterminate(Component component, Canvas canvas) {
        if (mRunState == RUN_STATE_STARTING) {
            int width = component.getWidth();
            int height = component.getHeight();
            float x = width / 2f;
            float y = height / 2f;

            float maxRadius = (Math.min(width, height) - mPadding * 2) / 2f;

            float stepTime = 1f / (mInStepPercent * (mInColors.length + 2) + 1);
            float time = (float) (Time.getRealActiveTime() - mLastRunStateTime) / mInAnimationDuration;
            float steps = time / stepTime;

            float outerRadius = 0f;
            float innerRadius = 0f;

            for (int i = (int) Math.floor(steps); i >= 0; i--) {
                innerRadius = outerRadius;
                outerRadius = Math.min(1f, (steps - i) * mInStepPercent) * maxRadius;

                if (i >= mInColors.length)
                    continue;

                if (innerRadius == 0) {
                    mPaint.setColor(new Color(mInColors[i]));
                    mPaint.setStyle(Paint.Style.FILL_STYLE);
                    canvas.drawCircle(x, y, outerRadius, mPaint);
                } else if (outerRadius > innerRadius) {
                    float radius = (innerRadius + outerRadius) / 2;
                    mRect.modify(x - radius, y - radius, x + radius, y + radius);

                    mPaint.setStrokeWidth(outerRadius - innerRadius);
                    mPaint.setStyle(Paint.Style.STROKE_STYLE);
                    mPaint.setColor(new Color(mInColors[i]));

                    canvas.drawCircle(x, y, radius, mPaint);
                } else
                    break;
            }

            if (mProgressState == PROGRESS_STATE_HIDE) {
                if (steps >= 1 / mInStepPercent || time >= 1) {
                    resetAnimation();
                    mProgressState = PROGRESS_STATE_STRETCH;
                }
            } else {
                float radius = maxRadius - mStrokeSize / 2f;

                mRect.modify(x - radius, y - radius, x + radius, y + radius);
                mPaint.setStrokeWidth(mStrokeSize);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                mPaint.setColor(new Color(getIndeterminateStrokeColor()));
                canvas.drawArc(mRect, new Arc(mStartAngle, mSweepAngle, false), mPaint);
            }
        } else if (mRunState == RUN_STATE_STOPPING) {
            float size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;

            if (size > 0) {
                int width = component.getWidth();
                int height = component.getHeight();
                float x = width / 2f;
                float y = height / 2f;
                float radius = (Math.min(width, height) - mPadding * 2 - mStrokeSize * 2 + size) / 2f;

                mRect.modify(x - radius, y - radius, x + radius, y + radius);
                mPaint.setStrokeWidth(size);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                mPaint.setColor(new Color(getIndeterminateStrokeColor()));

                canvas.drawArc(mRect, new Arc(mStartAngle, mSweepAngle, false), mPaint);
            }
        } else if (mRunState != RUN_STATE_STOPPED) {
            int width = component.getWidth();
            int height = component.getHeight();
            float x = width / 2f;
            float y = height / 2f;

            float radius = (Math.min(width, height) - mPadding * 2 - mStrokeSize) / 2f;

            mRect.modify(x - radius, y - radius, x + radius, y + radius);
            mPaint.setStrokeWidth(mStrokeSize);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setColor(new Color(getIndeterminateStrokeColor()));
            canvas.drawArc(mRect, new Arc(mStartAngle, mSweepAngle, false), mPaint);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    public int getProgressMode() {
        return mProgressMode;
    }

    public void setProgressMode(int mode) {
        if (mProgressMode != mode) {
            mProgressMode = mode;
            mComponent.invalidate();
        }
    }

    public float getProgress() {
        return mProgressPercent;
    }

    public float getSecondaryProgress() {
        return mSecondaryProgressPercent;
    }

    public void setProgress(float percent) {
        percent = Math.min(1f, Math.max(0f, percent));
        if (mProgressPercent != percent) {
            mProgressPercent = percent;
            if (isRunning())
                mComponent.invalidate();
            else if (mProgressPercent != 0f)
                start();
        }
    }

    public void setSecondaryProgress(float percent) {
        percent = Math.min(1f, Math.max(0f, percent));
        if (mSecondaryProgressPercent != percent) {
            mSecondaryProgressPercent = percent;
            if (isRunning())
                mComponent.invalidate();
            else if (mSecondaryProgressPercent != 0f)
                start();
        }
    }

    //Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/

    private void resetAnimation() {
        mLastUpdateTime = Time.getRealActiveTime();
        mLastProgressStateTime = mLastUpdateTime;
        mStartAngle = mInitialAngle;
        mStrokeColorIndex = 0;
        mSweepAngle = mReverse ? -mMinSweepAngle : mMinSweepAngle;
    }

    @Override
    public void start() {
        start(mInAnimationDuration > 0);
    }

    @Override
    public void stop() {
        stop(mOutAnimationDuration > 0);
    }

    private void start(boolean withAnimation) {
        if (isRunning())
            return;

        resetAnimation();

        if (withAnimation) {
            mRunState = RUN_STATE_STARTING;
            mLastRunStateTime = Time.getRealActiveTime();
            mProgressState = PROGRESS_STATE_HIDE;
        }

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
    }

    private void stop(boolean withAnimation) {
        if (!isRunning())
            return;

        if (withAnimation) {
            mLastRunStateTime = Time.getRealActiveTime();
            if (mRunState == RUN_STATE_STARTED) {
                scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
                mComponent.invalidate();
            }
            mRunState = RUN_STATE_STOPPING;
        } else {
            mRunState = RUN_STATE_STOPPED;
            mHandler.removeTask(mUpdater);
            mComponent.invalidate();
        }
    }

    @Override
    public boolean isRunning() {
        return mRunState != RUN_STATE_STOPPED;
    }

    private void scheduleSelf(Runnable what, long delay) {
        if (mRunState == RUN_STATE_STOPPED)
            mRunState = mInAnimationDuration > 0 ? RUN_STATE_STARTING : RUN_STATE_RUNNING;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        switch (mProgressMode) {
            case ProgressView.MODE_DETERMINATE:
                updateDeterminate();
                break;
            case ProgressView.MODE_INDETERMINATE:
                updateIndeterminate();
                break;
        }
    }

    private void updateDeterminate() {
        long curTime = Time.getRealActiveTime();
        float rotateOffset = (curTime - mLastUpdateTime) * 360f / mRotateDuration;
        if (mReverse)
            rotateOffset = -rotateOffset;
        mLastUpdateTime = curTime;

        mStartAngle += rotateOffset;

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration) {
                mRunState = RUN_STATE_RUNNING;
            }
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        mComponent.invalidate();
    }

    private void updateIndeterminate() {
        //update animation
        long curTime = Time.getRealActiveTime();
        float rotateOffset = (curTime - mLastUpdateTime) * 360f / mRotateDuration;
        if (mReverse)
            rotateOffset = -rotateOffset;
        mLastUpdateTime = curTime;

        switch (mProgressState) {
            case PROGRESS_STATE_STRETCH:
                if (mTransformDuration <= 0) {
                    mSweepAngle = mReverse ? -mMinSweepAngle : mMinSweepAngle;
                    mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                    mStartAngle += rotateOffset;
                    mLastProgressStateTime = curTime;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    float maxAngle = mReverse ? -mMaxSweepAngle : mMaxSweepAngle;
                    float minAngle = mReverse ? -mMinSweepAngle : mMinSweepAngle;

                    mStartAngle += rotateOffset;
                    mSweepAngle = mTransformInterpolator.getInterpolation(value) * (maxAngle - minAngle) + minAngle;

                    if (value > 1f) {
                        mSweepAngle = maxAngle;
                        mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                        mLastProgressStateTime = curTime;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_STRETCH:
                mStartAngle += rotateOffset;

                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_SHRINK;
                    mLastProgressStateTime = curTime;
                }
                break;
            case PROGRESS_STATE_SHRINK:
                if (mTransformDuration <= 0) {
                    mSweepAngle = mReverse ? -mMinSweepAngle : mMinSweepAngle;
                    mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                    mStartAngle += rotateOffset;
                    mLastProgressStateTime = curTime;
                    mStrokeColorIndex = (mStrokeColorIndex + 1) % mStrokeColors.length;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    float maxAngle = mReverse ? -mMaxSweepAngle : mMaxSweepAngle;
                    float minAngle = mReverse ? -mMinSweepAngle : mMinSweepAngle;

                    float newSweepAngle = (1f - mTransformInterpolator.getInterpolation(value)) * (maxAngle - minAngle) + minAngle;
                    mStartAngle += rotateOffset + mSweepAngle - newSweepAngle;
                    mSweepAngle = newSweepAngle;

                    if (value > 1f) {
                        mSweepAngle = minAngle;
                        mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                        mLastProgressStateTime = curTime;
                        mStrokeColorIndex = (mStrokeColorIndex + 1) % mStrokeColors.length;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_SHRINK:
                mStartAngle += rotateOffset;

                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_STRETCH;
                    mLastProgressStateTime = curTime;
                }
                break;
        }

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration) {
                mRunState = RUN_STATE_RUNNING;
                if (mProgressState == PROGRESS_STATE_HIDE) {
                    resetAnimation();
                    mProgressState = PROGRESS_STATE_STRETCH;
                }
            }
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        mComponent.invalidate();
    }

    public void createNativePtr(Object obj) {

    }

    public static class Builder {
        private int mPadding;
        private float mInitialAngle;
        private float mProgressPercent;
        private float mSecondaryProgressPercent;
        private float mMaxSweepAngle;
        private float mMinSweepAngle;
        private int mStrokeSize;
        private int[] mStrokeColors;
        private int mStrokeSecondaryColor;
        private boolean mReverse;
        private int mRotateDuration;
        private int mTransformDuration;
        private int mKeepDuration;
        private Interpolator mTransformInterpolator;
        private int mProgressMode;
        private float mInStepPercent;
        private int[] mInColors;
        private int mInAnimationDuration;
        private int mOutAnimationDuration;

        public Builder() {
        }

        public Builder(Context context, int defStyleRes) {
            this(context, null, 0, defStyleRes);
        }

        public Builder(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
            int resId;
            padding(Util.getDimensionValue(attrs, "cpd_padding", 0));
            initialAngle(Util.getDimensionValue(attrs, "cpd_initialAngle", 0));
            progressPercent(Util.getFloatValue(attrs, "pv_progress", 0));
            secondaryProgressPercent(Util.getFloatValue(attrs, "pv_secondaryProgress", 0));
            maxSweepAngle(Util.getIntegerValue(attrs, "cpd_maxSweepAngle", 270));
            minSweepAngle(Util.getIntegerValue(attrs, "cpd_minSweepAngle", 1));
            strokeSize(Util.getDimensionValue(attrs, "cpd_strokeSize", 12));
            strokeColors(Util.getColorValue(attrs, "cpd_strokeColor", new Color(0xFF000000)).getValue());
            if (Util.getBoolValue(attrs, "cpd_strokeColors", false)) {
                int[] colors = new int[]{0xFF03A9F4, 0xFFD8433C, 0xFFF2AF3A, 0xFF279B5E};
                strokeColors(colors);
            }
            strokeSecondaryColor(Util.getColorValue(attrs, "cpd_strokeSecondaryColor", new Color(0)).getValue());
            reverse(Util.getBoolValue(attrs, "cpd_reverse", false));
            rotateDuration(Util.getIntegerValue(attrs, "cpd_rotateDuration", 500));
            transformDuration(Util.getIntegerValue(attrs, "cpd_transformDuration", 400));
            keepDuration(Util.getIntegerValue(attrs, "cpd_keepDuration", 200));
            progressMode(Util.getIntegerValue(attrs, "pv_progressMode", ProgressView.MODE_INDETERMINATE));
            inAnimDuration(Util.getIntegerValue(attrs, "cpd_inAnimDuration", 400));
            if (Util.getBoolValue(attrs, "cpd_strokeColors", false)) {
                int[] colors = new int[]{0x4B03A9F4, 0x3303A9F4, 0x1903A9F4};
                inStepColors(colors);
            }
            inStepPercent(Util.getFloatValue(attrs, "cpd_inStepPercent", 0.5f));
            outAnimDuration(Util.getIntegerValue(attrs, "cpd_outAnimDuration", 400));
        }

        public CircularProgressDrawable build() {
            if (mStrokeColors == null)
                mStrokeColors = new int[]{0xFF0099FF};

            if (mInColors == null && mInAnimationDuration > 0)
                mInColors = new int[]{0xFFB5D4FF, 0xFFDEEAFC, 0xFFFAFFFE};

            if (mTransformInterpolator == null)
                mTransformInterpolator = new DecelerateInterpolator();
            return new CircularProgressDrawable(mPadding, mInitialAngle, mProgressPercent, mSecondaryProgressPercent, mMaxSweepAngle, mMinSweepAngle, mStrokeSize, mStrokeColors, mStrokeSecondaryColor, mReverse, mRotateDuration, mTransformDuration, mKeepDuration, mTransformInterpolator, mProgressMode, mInAnimationDuration, mInStepPercent, mInColors, mOutAnimationDuration);
        }

        public Builder padding(int padding) {
            mPadding = padding;
            return this;
        }

        public Builder initialAngle(float angle) {
            mInitialAngle = angle;
            return this;
        }

        public Builder progressPercent(float percent) {
            mProgressPercent = percent;
            return this;
        }

        public Builder secondaryProgressPercent(float percent) {
            mSecondaryProgressPercent = percent;
            return this;
        }

        public Builder maxSweepAngle(float angle) {
            mMaxSweepAngle = angle;
            return this;
        }

        public Builder minSweepAngle(float angle) {
            mMinSweepAngle = angle;
            return this;
        }

        public Builder strokeSize(int strokeSize) {
            mStrokeSize = strokeSize;
            return this;
        }

        public Builder strokeColors(int... strokeColors) {
            mStrokeColors = strokeColors;
            return this;
        }

        public Builder strokeSecondaryColor(int color) {
            mStrokeSecondaryColor = color;
            return this;
        }

        public Builder reverse(boolean reverse) {
            mReverse = reverse;
            return this;
        }

        public Builder reverse() {
            return reverse(true);
        }

        public Builder rotateDuration(int duration) {
            mRotateDuration = duration;
            return this;
        }

        public Builder transformDuration(int duration) {
            mTransformDuration = duration;
            return this;
        }

        public Builder keepDuration(int duration) {
            mKeepDuration = duration;
            return this;
        }

        public Builder transformInterpolator(Interpolator interpolator) {
            mTransformInterpolator = interpolator;
            return this;
        }

        public Builder progressMode(int mode) {
            mProgressMode = mode;
            return this;
        }

        public Builder inAnimDuration(int duration) {
            mInAnimationDuration = duration;
            return this;
        }

        public Builder inStepPercent(float percent) {
            mInStepPercent = percent;
            return this;
        }

        public Builder inStepColors(int... colors) {
            mInColors = colors;
            return this;
        }

        public Builder outAnimDuration(int duration) {
            mOutAnimationDuration = duration;
            return this;
        }

    }
}
