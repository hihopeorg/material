package com.rey.material.drawable;

import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;

import com.rey.material.util.ViewUtil;

/**
 * Created by Rey on 5/19/2015.
 */
public class OvalShadowDrawable extends Element implements Animatable {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "OvalShadowDrawable");

    private boolean mRunning = false;
    private long mStartTime;
    private float mAnimProgress;
    private int mAnimDuration;

    private boolean mEnable = true;
    private boolean mInEditMode = false;
    private boolean mAnimEnable = true;

    private Paint mShadowPaint;
    private Paint mGlowPaint;
    private Paint mPaint;

    private int mRadius;
    private float mShadowSize;
    private float mShadowOffset;

    private Path mShadowPath;
    private Path mGlowPath;

    private RectFloat mTempRect = new RectFloat();

    private Color mColorStateList;

    private boolean mNeedBuildShadow = true;

    private static final int COLOR_SHADOW_START = 0x4C000000;
    private static final int COLOR_SHADOW_END = 0x00000000;

    private Component mComponent;
    private EventHandler mHandler;

    public OvalShadowDrawable(Component component, int radius, Color colorStateList, float shadowSize, float shadowOffset, int animDuration) {
        this.mComponent = component;
        mAnimDuration = animDuration;

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_STYLE);

        setColor(colorStateList);
        setRadius(radius);
        setShadow(shadowSize, shadowOffset);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public boolean setRadius(int radius) {
        if (mRadius != radius) {
            mRadius = radius;
            mNeedBuildShadow = true;
            mComponent.invalidate();

            return true;
        }

        return false;
    }

    public boolean setShadow(float size, float offset) {
        if (mShadowSize != size || mShadowOffset != offset) {
            mShadowSize = size;
            mShadowOffset = offset;
            mNeedBuildShadow = true;
            mComponent.invalidate();

            return true;
        }

        return false;
    }

    public boolean setAnimationDuration(int duration) {
        if (mAnimDuration != duration) {
            mAnimDuration = duration;
            return true;
        }

        return false;
    }

    public void setColor(Color colorStateList) {
        mColorStateList = colorStateList;

    }
    public Color getColor() {
        return mColorStateList;
    }

    public void setInEditMode(boolean b) {
        mInEditMode = b;
    }

    public void setAnimEnable(boolean b) {
        mAnimEnable = b;
    }

    public int getRadius() {
        return mRadius;
    }

    public float getShadowSize() {
        return mShadowSize;
    }

    public float getShadowOffset() {
        return mShadowOffset;
    }

    public float getPaddingLeft() {
        return mShadowSize;
    }

    public float getPaddingTop() {
        return mShadowSize;
    }

    public float getPaddingRight() {
        return mShadowSize;
    }

    public float getPaddingBottom() {
        return mShadowSize + mShadowOffset;
    }

    public float getCenterX() {
        return mRadius + mShadowSize;
    }

    public float getCenterY() {
        return mRadius + mShadowSize;
    }

    public boolean isPointerOver(float x, float y) {
        float distance = (float) Math.sqrt(Math.pow(x - getCenterX(), 2) + Math.pow(y - getCenterY(), 2));

        return distance < mRadius;
    }

    public int getIntrinsicWidth() {
        return (int) ((mRadius + mShadowSize) * 2 + 0.5f);
    }

    public int getIntrinsicHeight() {
        return (int) ((mRadius + mShadowSize) * 2 + mShadowOffset + 0.5f);
    }


    private void buildShadow() {
        if (mShadowSize <= 0)
            return;

        if (mShadowPaint == null) {
            mShadowPaint = new Paint();
            mShadowPaint.setStyle(Paint.Style.FILL_STYLE);
            mShadowPaint.setDither(true);
        }
        float startRatio = (float) mRadius / (mRadius + mShadowSize + mShadowOffset);
        mShadowPaint.setShader(new RadialShader(new Point(0, 0), mRadius + mShadowSize,
                new float[]{0f, startRatio, 1f},
                new Color[]{new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_END)},
                Shader.TileMode.CLAMP_TILEMODE), Paint.ShaderType.RADIAL_SHADER);

        if (mShadowPath == null) {
            mShadowPath = new Path();
            mShadowPath.setFillType(Path.FillType.EVEN_ODD);
        } else
            mShadowPath.reset();
        float radius = mRadius + mShadowSize;
        mTempRect.modify(-radius, -radius, radius, radius);
        mShadowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);
        radius = mRadius - 1;
        mTempRect.modify(-radius, -radius - mShadowOffset, radius, radius - mShadowOffset);
        mShadowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);

        if (mGlowPaint == null) {
            mGlowPaint = new Paint();
            mGlowPaint.setStyle(Paint.Style.FILL_STYLE);
            mGlowPaint.setDither(true);
        }
        startRatio = (mRadius - mShadowSize / 2f) / (mRadius + mShadowSize / 2f);
        mGlowPaint.setShader(new RadialShader(new Point(0, 0), mRadius + mShadowSize / 2f,
                new float[]{0f, startRatio, 1f},
                new Color[]{new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_END)},
                Shader.TileMode.CLAMP_TILEMODE), Paint.ShaderType.RADIAL_SHADER);

        if (mGlowPath == null) {
            mGlowPath = new Path();
            mGlowPath.setFillType(Path.FillType.EVEN_ODD);
        } else
            mGlowPath.reset();

        radius = mRadius + mShadowSize / 2f;
        mTempRect.modify(-radius, -radius, radius, radius);
        mGlowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);
        radius = mRadius - 1;
        mTempRect.modify(-radius, -radius, radius, radius);
        mGlowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
        if (mNeedBuildShadow) {
            buildShadow();
            mNeedBuildShadow = false;
        }
        int saveCount;

        if (mShadowSize > 0) {
            saveCount = canvas.save();
            canvas.translate(mComponent.getWidth() / 2, mComponent.getHeight() / 2 + mShadowOffset);
            canvas.drawPath(mShadowPath, mShadowPaint);


            canvas.translate(0, -mShadowOffset);
            if (mShadowSize > 0)
                canvas.drawPath(mGlowPath, mGlowPaint);
            mTempRect.modify(-mRadius, -mRadius, mRadius, mRadius);
            if (!isRunning())
                mPaint.setColor(mColorStateList);
            else
                mPaint.setColor(mColorStateList);
            canvas.drawOval(mTempRect, mPaint);

            canvas.restoreToCount(saveCount);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        mShadowPaint.setAlpha(alpha);
        mPaint.setAlpha(alpha);
    }

    @Override
    public boolean isStateful() {
        return true;
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mAnimProgress = 0f;
    }

    @Override
    public void start() {
        resetAnimation();
        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        mComponent.invalidate();
    }

    @Override
    public void stop() {
        mRunning = false;
        mHandler.removeTask(mUpdater);
        mComponent.invalidate();
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);

    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (mAnimProgress == 1f)
            mRunning = false;

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();
    }

    public void createNativePtr(Object obj) {

    }
}
