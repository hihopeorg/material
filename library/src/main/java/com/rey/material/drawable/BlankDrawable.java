package com.rey.material.drawable;

import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;

/**
 * A drawable that draw nothing.
 *
 * @author Rey
 */
public class BlankDrawable extends Element {

    private static BlankDrawable mInstance;

    public static BlankDrawable getInstance() {
        if (mInstance == null)
            synchronized (BlankDrawable.class) {
                if (mInstance == null)
                    mInstance = new BlankDrawable();
            }

        return mInstance;
    }

    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);
    }

    @Override
    public void setAlpha(int alpha) {
    }

}
