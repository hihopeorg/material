package com.rey.material.drawable;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;

import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;
import com.rey.material.widget.ProgressView;

public class LinearProgressDrawable extends Element implements Animatable, Component.DrawTask {

    public static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "LinearProgressDrawable");

    private Component mComponent;

    private long mLastUpdateTime;
    private long mLastProgressStateTime;
    private long mLastRunStateTime;

    private int mProgressState;

    private static final int PROGRESS_STATE_STRETCH = 0;
    private static final int PROGRESS_STATE_KEEP_STRETCH = 1;
    private static final int PROGRESS_STATE_SHRINK = 2;
    private static final int PROGRESS_STATE_KEEP_SHRINK = 3;

    private int mRunState = RUN_STATE_STOPPED;

    private static final int RUN_STATE_STOPPED = 0;
    private static final int RUN_STATE_STARTING = 1;
    private static final int RUN_STATE_STARTED = 2;
    private static final int RUN_STATE_RUNNING = 3;
    private static final int RUN_STATE_STOPPING = 4;

    public static final int ALIGN_TOP = 0;
    public static final int ALIGN_CENTER = 1;
    public static final int ALIGN_BOTTOM = 2;

    private Paint mPaint;
    private float mStartLine;
    private float mLineWidth;
    private int mStrokeColorIndex;
    private float mAnimTime;

    private Path mPath;
    private PathEffect mPathEffect;

    private float mProgressPercent;
    private float mSecondaryProgressPercent;
    private int mMaxLineWidth;
    private float mMaxLineWidthPercent;
    private int mMinLineWidth;
    private float mMinLineWidthPercent;
    private int mStrokeSize;
    private int mVerticalAlign;
    private int[] mStrokeColors;
    private int mStrokeSecondaryColor;
    private boolean mReverse;
    private int mTravelDuration;
    private int mTransformDuration;
    private int mKeepDuration;
    private int mInAnimationDuration;
    private int mOutAnimationDuration;
    private int mProgressMode;
    private Interpolator mTransformInterpolator;

    private EventHandler mHandler;

    private LinearProgressDrawable(float progressPercent, float secondaryProgressPercent, int maxLineWidth, float maxLineWidthPercent, int minLineWidth, float minLineWidthPercent, int strokeSize, int verticalAlign, int[] strokeColors, int strokeSecondaryColor, boolean reverse, int travelDuration, int transformDuration, int keepDuration, Interpolator transformInterpolator, int progressMode, int inAnimDuration, int outAnimDuration) {
        setProgress(progressPercent);
        setSecondaryProgress(secondaryProgressPercent);
        mMaxLineWidth = maxLineWidth;
        mMaxLineWidthPercent = maxLineWidthPercent;
        mMinLineWidth = minLineWidth;
        mMinLineWidthPercent = minLineWidthPercent;
        mStrokeSize = strokeSize;
        mVerticalAlign = verticalAlign;
        mStrokeColors = strokeColors;
        mStrokeSecondaryColor = strokeSecondaryColor;
        mReverse = reverse;
        mTravelDuration = travelDuration;
        mTransformDuration = transformDuration;
        mKeepDuration = keepDuration;
        mTransformInterpolator = transformInterpolator;
        mProgressMode = progressMode;
        mInAnimationDuration = inAnimDuration;
        mOutAnimationDuration = outAnimDuration;

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mPaint.setStrokeJoin(Paint.Join.ROUND_JOIN);

        mPath = new Path();

        mHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public void applyStyle(Context context, AttrSet attrs, int resId) {

        int strokeColor = 0;
        boolean strokeColorDefined = false;
        int[] strokeColors = null;

        int count = attrs.getLength();
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("pv_progress")) {
                setProgress(Util.getFloatValue(attrs, name, 0));
            } else if (name.equals("pv_secondaryProgress")) {
                setSecondaryProgress(Util.getFloatValue(attrs, name, 0));
            } else if (name.equals("lpd_maxLineWidth")) {
                mMaxLineWidth = Util.getDimensionValue(attrs, name, 0);
                mMaxLineWidthPercent = 0f;
            } else if (name.equals("lpd_minLineWidth")) {
                mMinLineWidth = Util.getDimensionValue(attrs, name, 0);
                mMinLineWidthPercent = 0f;
            } else if (name.equals("lpd_strokeSize")) {
                mStrokeSize = Util.getDimensionValue(attrs, name, 0);
            } else if (name.equals("lpd_verticalAlign")) {
                mVerticalAlign = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("lpd_strokeColor")) {
                strokeColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            } else if (name.equals("lpd_strokeColors")) {
                strokeColors = new int[]{0xFF03A9F4, 0xFFD8433C, 0xFFF2AF3A, 0xFF279B5E};
            } else if (name.equals("lpd_strokeSecondaryColor")) {
                mStrokeSecondaryColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            } else if (name.equals("lpd_reverse")) {
                mReverse = Util.getBoolValue(attrs, name, false);
            } else if (name.equals("lpd_travelDuration")) {
                mTravelDuration = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("lpd_transformDuration")) {
                mTransformDuration = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("lpd_keepDuration")) {
                mKeepDuration = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("pv_progressMode")) {
                mProgressMode = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("lpd_inAnimDuration")) {
                mInAnimationDuration = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("lpd_outAnimDuration")) {
                mOutAnimationDuration = Util.getIntegerValue(attrs, name, 0);
            }

        }

        if (strokeColors != null)
            mStrokeColors = strokeColors;
        else if (strokeColorDefined)
            mStrokeColors = new int[]{strokeColor};

        if (mStrokeColorIndex >= mStrokeColors.length)
            mStrokeColorIndex = 0;

        mComponent.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mComponent = component;
        switch (mProgressMode) {
            case ProgressView.MODE_DETERMINATE:
                drawDeterminate(component, canvas);
                break;
            case ProgressView.MODE_INDETERMINATE:
                drawIndeterminate(component, canvas);
                break;
            case ProgressView.MODE_BUFFER:
                drawBuffer(component, canvas);
                break;
            case ProgressView.MODE_QUERY:
                drawQuery(component, canvas);
                break;
        }
    }

    private void drawLinePath(Canvas canvas, float x1, float y1, float x2, float y2, Paint paint) {
        mPath.reset();
        mPath.moveTo(x1, y1);
        mPath.lineTo(x2, y2);
        canvas.drawPath(mPath, paint);
    }

    private void drawDeterminate(Component component, Canvas canvas) {
        int width = component.getWidth();
        int height = component.getHeight();
        float size = 0f;

        if (mRunState == RUN_STATE_STARTING)
            size = (float) mStrokeSize * Math.min(mInAnimationDuration, (Time.getRealActiveTime() - mLastRunStateTime)) / mInAnimationDuration;
        else if (mRunState == RUN_STATE_STOPPING)
            size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;
        else if (mRunState != RUN_STATE_STOPPED)
            size = mStrokeSize;

        if (size > 0) {
            float y = 0;
            float lineWidth = width * mProgressPercent;

            switch (mVerticalAlign) {
                case ALIGN_TOP:
                    y = size / 2;
                    break;
                case ALIGN_CENTER:
                    y = height / 2f;
                    break;
                case ALIGN_BOTTOM:
                    y = height - size / 2;
                    break;
            }

            mPaint.setStrokeWidth(size);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            if (mProgressPercent != 1f) {
                mPaint.setColor(new Color(mStrokeSecondaryColor));

                if (mReverse)
                    canvas.drawLine(new Point(0, y), new Point(width - lineWidth, y), mPaint);
                else
                    canvas.drawLine(new Point(lineWidth, y), new Point(width, y), mPaint);
            }

            if (mProgressPercent != 0f) {
                mPaint.setColor(new Color(mStrokeColors[0]));
                if (mReverse)
                    drawLinePath(canvas, width - lineWidth, y, width, y, mPaint);
                else
                    drawLinePath(canvas, 0, y, lineWidth, y, mPaint);
            }
        }
    }

    private int getIndeterminateStrokeColor() {
        if (mProgressState != PROGRESS_STATE_KEEP_SHRINK || mStrokeColors.length == 1)
            return mStrokeColors[mStrokeColorIndex];

        float value = Math.max(0f, Math.min(1f, (float) (Time.getRealActiveTime() - mLastProgressStateTime) / mKeepDuration));
        int prev_index = mStrokeColorIndex == 0 ? mStrokeColors.length - 1 : mStrokeColorIndex - 1;

        return ColorUtil.getMiddleColor(mStrokeColors[prev_index], mStrokeColors[mStrokeColorIndex], value);
    }

    private void drawIndeterminate(Component component, Canvas canvas) {
        int width = component.getWidth();
        int height = component.getHeight();
        float size = 0f;

        if (mRunState == RUN_STATE_STARTING)
            size = (float) mStrokeSize * Math.min(mInAnimationDuration, (Time.getRealActiveTime() - mLastRunStateTime)) / mInAnimationDuration;
        else if (mRunState == RUN_STATE_STOPPING)
            size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;
        else if (mRunState != RUN_STATE_STOPPED)
            size = mStrokeSize;

        if (size > 0) {
            float y = 0;

            switch (mVerticalAlign) {
                case ALIGN_TOP:
                    y = size / 2;
                    break;
                case ALIGN_CENTER:
                    y = height / 2f;
                    break;
                case ALIGN_BOTTOM:
                    y = height - size / 2;
                    break;
            }

            mPaint.setStrokeWidth(size);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);

            float endLine = offset(mStartLine, mLineWidth, width);
            if (mReverse) {
                if (endLine <= mStartLine) {
                    mPaint.setColor(new Color(mStrokeSecondaryColor));
                    if (endLine > 0)
                        canvas.drawLine(new Point(0, y), new Point(endLine, y), mPaint);
                    if (mStartLine < width)
                        canvas.drawLine(new Point(mStartLine, y), new Point(width, y), mPaint);

                    mPaint.setColor(new Color(getIndeterminateStrokeColor()));
                    drawLinePath(canvas, endLine, y, mStartLine, y, mPaint);
                } else {
                    mPaint.setColor(new Color(mStrokeSecondaryColor));
                    canvas.drawLine(new Point(mStartLine, y), new Point(endLine, y), mPaint);

                    mPaint.setColor(new Color(getIndeterminateStrokeColor()));
                    mPath.reset();

                    if (mStartLine > 0) {
                        mPath.moveTo(0, y);
                        mPath.lineTo(mStartLine, y);
                    }
                    if (endLine < width) {
                        mPath.moveTo(endLine, y);
                        mPath.lineTo(width, y);
                    }

                    canvas.drawPath(mPath, mPaint);
                }
            } else {
                if (endLine >= mStartLine) {
                    mPaint.setColor(new Color(mStrokeSecondaryColor));
                    if (mStartLine > 0)
                        canvas.drawLine(new Point(0, y), new Point(mStartLine, y), mPaint);
                    if (endLine < width)
                        canvas.drawLine(new Point(endLine, y), new Point(width, y), mPaint);

                    mPaint.setColor(new Color(getIndeterminateStrokeColor()));
                    drawLinePath(canvas, mStartLine, y, endLine, y, mPaint);
                } else {
                    mPaint.setColor(new Color(mStrokeSecondaryColor));
                    canvas.drawLine(new Point(endLine, y), new Point(mStartLine, y), mPaint);

                    mPaint.setColor(new Color(getIndeterminateStrokeColor()));
                    mPath.reset();

                    if (endLine > 0) {
                        mPath.moveTo(0, y);
                        mPath.lineTo(endLine, y);
                    }
                    if (mStartLine < width) {
                        mPath.moveTo(mStartLine, y);
                        mPath.lineTo(width, y);
                    }

                    canvas.drawPath(mPath, mPaint);
                }
            }
        }
    }

    private PathEffect getPathEffect() {
        if (mPathEffect == null)
            mPathEffect = new PathEffect(new float[]{0.1f, mStrokeSize * 2}, 0f);

        return mPathEffect;
    }

    private void drawBuffer(Component component, Canvas canvas) {
        int width = component.getWidth();
        int height = component.getHeight();
        float size = 0f;

        if (mRunState == RUN_STATE_STARTING)
            size = (float) mStrokeSize * Math.min(mInAnimationDuration, (Time.getRealActiveTime() - mLastRunStateTime)) / mInAnimationDuration;
        else if (mRunState == RUN_STATE_STOPPING)
            size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;
        else if (mRunState != RUN_STATE_STOPPED)
            size = mStrokeSize;

        if (size > 0) {
            float y = 0;
            float lineWidth = width * mProgressPercent;
            float secondaryLineWidth = width * mSecondaryProgressPercent;

            switch (mVerticalAlign) {
                case ALIGN_TOP:
                    y = size / 2;
                    break;
                case ALIGN_CENTER:
                    y = height / 2f;
                    break;
                case ALIGN_BOTTOM:
                    y = height - size / 2;
                    break;
            }

            mPaint.setStyle(Paint.Style.STROKE_STYLE);

            if (mProgressPercent != 1f) {
                mPaint.setStrokeWidth(size);
                mPaint.setColor(new Color(mStrokeSecondaryColor));
                mPaint.setPathEffect(null);

                if (mReverse)
                    drawLinePath(canvas, width - secondaryLineWidth, y, width - lineWidth, y, mPaint);
                else
                    drawLinePath(canvas, secondaryLineWidth, y, lineWidth, y, mPaint);

                mPaint.setStrokeWidth(mLineWidth);
                mPaint.setPathEffect(getPathEffect());
                float offset = mStrokeSize * 2 - mStartLine;

                if (mReverse)
                    drawLinePath(canvas, -offset, y, width - secondaryLineWidth, y, mPaint);
                else
                    drawLinePath(canvas, width + offset, y, secondaryLineWidth, y, mPaint);
            }

            if (mProgressPercent != 0f) {
                mPaint.setStrokeWidth(size);
                mPaint.setColor(new Color(mStrokeColors[0]));
                mPaint.setPathEffect(null);

                if (mReverse)
                    drawLinePath(canvas, width - lineWidth, y, width, y, mPaint);
                else
                    drawLinePath(canvas, 0, y, lineWidth, y, mPaint);
            }
        }
    }

    private int getQueryStrokeColor() {
        return ColorUtil.getColor(mStrokeColors[0], mAnimTime);
    }

    private void drawQuery(Component component, Canvas canvas) {
        int width = component.getWidth();
        int height = component.getHeight();
        float size = 0f;

        if (mRunState == RUN_STATE_STARTING)
            size = (float) mStrokeSize * Math.min(mInAnimationDuration, (Time.getRealActiveTime() - mLastRunStateTime)) / mInAnimationDuration;
        else if (mRunState == RUN_STATE_STOPPING)
            size = (float) mStrokeSize * Math.max(0, (mOutAnimationDuration - Time.getRealActiveTime() + mLastRunStateTime)) / mOutAnimationDuration;
        else if (mRunState != RUN_STATE_STOPPED)
            size = mStrokeSize;

        if (size > 0) {
            float y = 0;

            switch (mVerticalAlign) {
                case ALIGN_TOP:
                    y = size / 2;
                    break;
                case ALIGN_CENTER:
                    y = height / 2f;
                    break;
                case ALIGN_BOTTOM:
                    y = height - size / 2;
                    break;
            }

            mPaint.setStrokeWidth(size);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);

            if (mProgressPercent != 1f) {
                mPaint.setColor(new Color(mStrokeSecondaryColor));
                canvas.drawLine(new Point(0, y), new Point(width, y), mPaint);

                if (mAnimTime < 1f) {
                    float endLine = Math.max(0, Math.min(width, mStartLine + mLineWidth));
                    mPaint.setColor(new Color(getQueryStrokeColor()));
                    drawLinePath(canvas, mStartLine, y, endLine, y, mPaint);
                }
            }

            if (mProgressPercent != 0f) {
                float lineWidth = width * mProgressPercent;
                mPaint.setColor(new Color(mStrokeColors[0]));

                if (mReverse)
                    drawLinePath(canvas, width - lineWidth, y, width, y, mPaint);
                else
                    drawLinePath(canvas, 0, y, lineWidth, y, mPaint);
            }

        }
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    public int getProgressMode() {
        return mProgressMode;
    }

    public void setProgressMode(int mode) {
        if (mProgressMode != mode) {
            mProgressMode = mode;
            mComponent.invalidate();
        }
    }

    public float getProgress() {
        return mProgressPercent;
    }

    public float getSecondaryProgress() {
        return mSecondaryProgressPercent;
    }

    public void setProgress(float percent) {
        percent = Math.min(1f, Math.max(0f, percent));
        if (mProgressPercent != percent) {
            mProgressPercent = percent;
            if (isRunning())
                mComponent.invalidate();
            else if (mProgressPercent != 0f)
                start();
        }
    }

    public void setSecondaryProgress(float percent) {
        percent = Math.min(1f, Math.max(0f, percent));
        if (mSecondaryProgressPercent != percent) {
            mSecondaryProgressPercent = percent;
            if (isRunning())
                mComponent.invalidate();
            else if (mSecondaryProgressPercent != 0f)
                start();
        }
    }

    //Animation: based on http://cyrilmottier.com/2012/11/27/actionbar-on-the-move/

    private void resetAnimation() {
        mLastUpdateTime = Time.getRealActiveTime();
        mLastProgressStateTime = mLastUpdateTime;
        if (mProgressMode == ProgressView.MODE_INDETERMINATE) {
            mStartLine = mReverse ? mComponent.getWidth() : 0;
            mStrokeColorIndex = 0;
            mLineWidth = mReverse ? -mMinLineWidth : mMinLineWidth;
            mProgressState = PROGRESS_STATE_STRETCH;
        } else if (mProgressMode == ProgressView.MODE_BUFFER) {
            mStartLine = 0;
        } else if (mProgressMode == ProgressView.MODE_QUERY) {
            mStartLine = !mReverse ? 540 : 0;//mComponent.getWidth() : 0;
            mStrokeColorIndex = 0;
            mLineWidth = !mReverse ? -mMaxLineWidth : mMaxLineWidth;
        }
    }

    @Override
    public void start() {
        start(mInAnimationDuration > 0);
    }

    @Override
    public void stop() {
        stop(mOutAnimationDuration > 0);
    }

    private void start(boolean withAnimation) {
        if (isRunning())
            return;

        if (withAnimation) {
            mRunState = RUN_STATE_STARTING;
            mLastRunStateTime = Time.getRealActiveTime();
        }

        resetAnimation();

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
    }

    private void stop(boolean withAnimation) {
        if (!isRunning())
            return;

        if (withAnimation) {
            mLastRunStateTime = Time.getRealActiveTime();

            if (mRunState == RUN_STATE_STARTED) {
                scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
                mComponent.invalidate();
            }
            mRunState = RUN_STATE_STOPPING;
        } else {
            mRunState = RUN_STATE_STOPPED;
            mHandler.removeTask(mUpdater);
            mComponent.invalidate();
        }
    }

    @Override
    public boolean isRunning() {
        return mRunState != RUN_STATE_STOPPED;
    }

    private void scheduleSelf(Runnable what, long delay) {
        if (mRunState == RUN_STATE_STOPPED)
            mRunState = mInAnimationDuration > 0 ? RUN_STATE_STARTING : RUN_STATE_RUNNING;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        switch (mProgressMode) {
            case ProgressView.MODE_DETERMINATE:
                updateDeterminate();
                break;
            case ProgressView.MODE_INDETERMINATE:
                updateIndeterminate();
                break;
            case ProgressView.MODE_BUFFER:
                updateBuffer();
                break;
            case ProgressView.MODE_QUERY:
                updateQuery();
                break;
        }
    }

    private void updateDeterminate() {
        long curTime = Time.getRealActiveTime();

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration) {
                mRunState = RUN_STATE_STARTED;
                return;
            }
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();//invalidateSelf();
    }

    private float offset(float pos, float offset, float max) {
        pos += offset;
        if (pos > max)
            return pos - max;
        if (pos < 0)
            return max + pos;
        return pos;
    }

    private void updateIndeterminate() {
        int width = mComponent.getWidth();

        long curTime = Time.getRealActiveTime();
        float travelOffset = (float) (curTime - mLastUpdateTime) * width / mTravelDuration;
        if (mReverse)
            travelOffset = -travelOffset;
        mLastUpdateTime = curTime;

        switch (mProgressState) {
            case PROGRESS_STATE_STRETCH:
                if (mTransformDuration <= 0) {
                    mLineWidth = mMinLineWidth == 0 ? width * mMinLineWidthPercent : mMinLineWidth;
                    if (mReverse)
                        mLineWidth = -mLineWidth;
                    mStartLine = offset(mStartLine, travelOffset, width);
                    mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                    mLastProgressStateTime = curTime;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    float maxWidth = mMaxLineWidth == 0 ? width * mMaxLineWidthPercent : mMaxLineWidth;
                    float minWidth = mMinLineWidth == 0 ? width * mMinLineWidthPercent : mMinLineWidth;

                    mStartLine = offset(mStartLine, travelOffset, width);
                    mLineWidth = mTransformInterpolator.getInterpolation(value) * (maxWidth - minWidth) + minWidth;
                    if (mReverse)
                        mLineWidth = -mLineWidth;

                    if (value > 1f) {
                        mLineWidth = mReverse ? -maxWidth : maxWidth;
                        mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                        mLastProgressStateTime = curTime;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_STRETCH:
                mStartLine = offset(mStartLine, travelOffset, width);

                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_SHRINK;
                    mLastProgressStateTime = curTime;
                }
                break;
            case PROGRESS_STATE_SHRINK:
                if (mTransformDuration <= 0) {
                    mLineWidth = mMinLineWidth == 0 ? width * mMinLineWidthPercent : mMinLineWidth;
                    if (mReverse)
                        mLineWidth = -mLineWidth;
                    mStartLine = offset(mStartLine, travelOffset, width);
                    mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                    mLastProgressStateTime = curTime;
                    mStrokeColorIndex = (mStrokeColorIndex + 1) % mStrokeColors.length;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    float maxWidth = mMaxLineWidth == 0 ? width * mMaxLineWidthPercent : mMaxLineWidth;
                    float minWidth = mMinLineWidth == 0 ? width * mMinLineWidthPercent : mMinLineWidth;

                    float newLineWidth = (1f - mTransformInterpolator.getInterpolation(value)) * (maxWidth - minWidth) + minWidth;
                    if (mReverse)
                        newLineWidth = -newLineWidth;

                    mStartLine = offset(mStartLine, travelOffset + mLineWidth - newLineWidth, width);
                    mLineWidth = newLineWidth;

                    if (value > 1f) {
                        mLineWidth = mReverse ? -minWidth : minWidth;
                        mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                        mLastProgressStateTime = curTime;
                        mStrokeColorIndex = (mStrokeColorIndex + 1) % mStrokeColors.length;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_SHRINK:
                mStartLine = offset(mStartLine, travelOffset, width);

                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_STRETCH;
                    mLastProgressStateTime = curTime;
                }
                break;
        }

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration)
                mRunState = RUN_STATE_RUNNING;
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();//invalidateSelf();
    }

    private void updateBuffer() {
        long curTime = Time.getRealActiveTime();
        float maxDistance = mStrokeSize * 2;
        mStartLine += maxDistance * (float) (curTime - mLastUpdateTime) / mTravelDuration;
        while (mStartLine > maxDistance)
            mStartLine -= maxDistance;
        mLastUpdateTime = curTime;

        switch (mProgressState) {
            case PROGRESS_STATE_STRETCH:
                if (mTransformDuration <= 0) {
                    mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                    mLastProgressStateTime = curTime;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    mLineWidth = mTransformInterpolator.getInterpolation(value) * mStrokeSize;

                    if (value > 1f) {
                        mLineWidth = mStrokeSize;
                        mProgressState = PROGRESS_STATE_KEEP_STRETCH;
                        mLastProgressStateTime = curTime;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_STRETCH:
                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_SHRINK;
                    mLastProgressStateTime = curTime;
                }
                break;
            case PROGRESS_STATE_SHRINK:
                if (mTransformDuration <= 0) {
                    mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                    mLastProgressStateTime = curTime;
                } else {
                    float value = (curTime - mLastProgressStateTime) / (float) mTransformDuration;
                    mLineWidth = (1f - mTransformInterpolator.getInterpolation(value)) * mStrokeSize;

                    if (value > 1f) {
                        mLineWidth = 0;
                        mProgressState = PROGRESS_STATE_KEEP_SHRINK;
                        mLastProgressStateTime = curTime;
                    }
                }
                break;
            case PROGRESS_STATE_KEEP_SHRINK:
                if (curTime - mLastProgressStateTime > mKeepDuration) {
                    mProgressState = PROGRESS_STATE_STRETCH;
                    mLastProgressStateTime = curTime;
                }
                break;
        }

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration)
                mRunState = RUN_STATE_RUNNING;
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        mComponent.invalidate();//invalidateSelf();
    }

    private void updateQuery() {
        long curTime = Time.getRealActiveTime();
        mAnimTime = (float) (curTime - mLastProgressStateTime) / mTravelDuration;
        boolean requestUpdate = mRunState == RUN_STATE_STOPPING || mProgressPercent == 0 || mAnimTime < 1f;

        if (mAnimTime > 1f) {
            mLastProgressStateTime = Math.round(curTime - (mAnimTime - 1f) * mTravelDuration);
            mAnimTime -= 1f;
        }

        if (requestUpdate && mRunState != RUN_STATE_STOPPING) {
            int width = mComponent.getWidth();

            float maxWidth = mMaxLineWidth == 0 ? width * mMaxLineWidthPercent : mMaxLineWidth;
            float minWidth = mMinLineWidth == 0 ? width * mMinLineWidthPercent : mMinLineWidth;
            mLineWidth = mTransformInterpolator.getInterpolation(mAnimTime) * (minWidth - maxWidth) + maxWidth;
            if (mReverse)
                mLineWidth = -mLineWidth;

            mStartLine = mReverse ? mTransformInterpolator.getInterpolation(mAnimTime) * (width + minWidth) : ((1f - mTransformInterpolator.getInterpolation(mAnimTime)) * (width + minWidth) - minWidth);
        }

        if (mRunState == RUN_STATE_STARTING) {
            if (curTime - mLastRunStateTime > mInAnimationDuration)
                mRunState = RUN_STATE_RUNNING;
        } else if (mRunState == RUN_STATE_STOPPING) {
            if (curTime - mLastRunStateTime > mOutAnimationDuration) {
                stop(false);
                return;
            }
        }

        if (isRunning()) {
            if (requestUpdate) {
                scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
            } else if (mRunState == RUN_STATE_RUNNING) {
                mRunState = RUN_STATE_STARTED;
            }
        }

        mComponent.invalidate();//invalidateSelf();
    }

    public void createNativePtr(Object obj) {

    }

    public static class Builder {
        private float mProgressPercent = 0;
        private float mSecondaryProgressPercent = 0;
        private int mMaxLineWidth;
        private float mMaxLineWidthPercent;
        private int mMinLineWidth;
        private float mMinLineWidthPercent;
        private int mStrokeSize = 8;
        private int mVerticalAlign = LinearProgressDrawable.ALIGN_BOTTOM;
        private int[] mStrokeColors;
        private int mStrokeSecondaryColor;
        private boolean mReverse = false;
        private int mTravelDuration = 1000;
        private int mTransformDuration = 800;
        private int mKeepDuration = 200;
        private Interpolator mTransformInterpolator;
        private int mProgressMode = ProgressView.MODE_INDETERMINATE;
        private int mInAnimationDuration = 400;
        private int mOutAnimationDuration = 400;

        public Builder() {
        }

        public Builder(Context context, int defStyleRes) {
            this(context, null, 0, defStyleRes);
        }

        public Builder(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
            int resId;

            progressPercent(Util.getFloatValue(attrs, "pv_progress", 0));
            secondaryProgressPercent(Util.getFloatValue(attrs, "pv_secondaryProgress", 0));
            maxLineWidth(Util.getFloatValue(attrs, "lpd_maxLineWidth", 0.75f));
            minLineWidth(Util.getFloatValue(attrs, "lpd_minLineWidth", 0.25f));
            strokeSize(Util.getDimensionValue(attrs, "lpd_strokeSize", 12));
            verticalAlign(Util.getIntegerValue(attrs, "lpd_verticalAlign", LinearProgressDrawable.ALIGN_BOTTOM));
            strokeColors(Util.getColorValue(attrs, "lpd_strokeColor", new Color(0xFF000000)).getValue());
            if (Util.getBoolValue(attrs, "lpd_strokeColors", false)) {
                int[] colors = new int[]{0xFF03A9F4, 0xFFD8433C, 0xFFF2AF3A, 0xFF279B5E};
                strokeColors(colors);
            }
            strokeSecondaryColor(Util.getColorValue(attrs, "lpd_strokeSecondaryColor", new Color(0)).getValue());
            reverse(Util.getBoolValue(attrs, "lpd_reverse", false));
            travelDuration(Util.getIntegerValue(attrs, "lpd_travelDuration", 500));
            transformDuration(Util.getIntegerValue(attrs, "lpd_transformDuration", 400));
            keepDuration(Util.getIntegerValue(attrs, "lpd_keepDuration", 200));
//			if((resId = a.getResourceId(R.styleable.LinearProgressDrawable_lpd_transformInterpolator, 0)) != 0)
//				transformInterpolator(AnimationUtils.loadInterpolator(context, resId));
            progressMode(Util.getIntegerValue(attrs, "pv_progressMode", ProgressView.MODE_INDETERMINATE));
            inAnimDuration(Util.getIntegerValue(attrs, "lpd_inAnimDuration", 400));
            outAnimDuration(Util.getIntegerValue(attrs, "lpd_outAnimDuration", 400));

        }

        public LinearProgressDrawable build() {
            if (mStrokeColors == null)
                mStrokeColors = new int[]{0xFF0099FF};

            if (mTransformInterpolator == null)
                mTransformInterpolator = new DecelerateInterpolator();

            return new LinearProgressDrawable(mProgressPercent, mSecondaryProgressPercent, mMaxLineWidth, mMaxLineWidthPercent, mMinLineWidth, mMinLineWidthPercent, mStrokeSize, mVerticalAlign, mStrokeColors, mStrokeSecondaryColor, mReverse, mTravelDuration, mTransformDuration, mKeepDuration, mTransformInterpolator, mProgressMode, mInAnimationDuration, mOutAnimationDuration);
        }

        public Builder secondaryProgressPercent(float percent) {
            mSecondaryProgressPercent = percent;
            return this;
        }

        public Builder progressPercent(float percent) {
            mProgressPercent = percent;
            return this;
        }

        public Builder maxLineWidth(int width) {
            mMaxLineWidth = width;
            return this;
        }

        public Builder maxLineWidth(float percent) {
            mMaxLineWidthPercent = Math.max(0f, Math.min(1f, percent));
            mMaxLineWidth = 0;
            return this;
        }

        public Builder minLineWidth(int width) {
            mMinLineWidth = width;
            return this;
        }

        public Builder minLineWidth(float percent) {
            mMinLineWidthPercent = Math.max(0f, Math.min(1f, percent));
            mMinLineWidth = 0;
            return this;
        }

        public Builder strokeSize(int strokeSize) {
            mStrokeSize = strokeSize;
            return this;
        }

        public Builder verticalAlign(int align) {
            mVerticalAlign = align;
            return this;
        }

        public Builder strokeColors(int... strokeColors) {
            mStrokeColors = strokeColors;
            return this;
        }

        public Builder strokeSecondaryColor(int color) {
            mStrokeSecondaryColor = color;
            return this;
        }

        public Builder reverse(boolean reverse) {
            mReverse = reverse;
            return this;
        }

        public Builder reverse() {
            return reverse(true);
        }

        public Builder travelDuration(int duration) {
            mTravelDuration = duration;
            return this;
        }

        public Builder transformDuration(int duration) {
            mTransformDuration = duration;
            return this;
        }

        public Builder keepDuration(int duration) {
            mKeepDuration = duration;
            return this;
        }

        public Builder transformInterpolator(Interpolator interpolator) {
            mTransformInterpolator = interpolator;
            return this;
        }

        public Builder progressMode(int mode) {
            mProgressMode = mode;
            return this;
        }

        public Builder inAnimDuration(int duration) {
            mInAnimationDuration = duration;
            return this;
        }

        public Builder outAnimDuration(int duration) {
            mOutAnimationDuration = duration;
            return this;
        }
    }
}
