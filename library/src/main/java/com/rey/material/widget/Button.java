package com.rey.material.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ViewUtil;

public class Button extends ohos.agp.components.Button implements Component.TouchEventListener, ThemeManager.OnThemeChangedListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Button");

    private RippleManager mRippleManager;

    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    public Button(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public Button(Context context, AttrSet attrs) {
        super(context, attrs);

        init(context, attrs, 0, 0);
    }


    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        setBindStateChangedListener(mBindStateChangedListener);
        setTouchEventListener(this::onTouchEvent);
        applyStyle(context, attrs, defStyleAttr, defStyleRes);
        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(Button.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            RippleManager.cancelRipple(Button.this);
            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(Button.this::onThemeChanged);
        }
    };

    @Override
    public void setBackground(Element element) {
        Element background = getBackgroundElement();
        if (background instanceof RippleDrawable && !(element instanceof RippleDrawable)) {
            ((RippleDrawable) background).setBackgroundDrawable(element);
        } else {
            super.setBackground(element);
        }
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null)
                    mRippleManager = new RippleManager();
            }
        }

        return mRippleManager;
    }

    @Override
    public void setClickedListener(ClickedListener listener) {
        RippleManager rippleManager = getRippleManager();
        if (listener == rippleManager)
            super.setClickedListener(listener);
        else {
            rippleManager.setOnClickListener(listener);
            setClickedListener(rippleManager);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        return getRippleManager().onTouchEvent(this, event);
    }

}
