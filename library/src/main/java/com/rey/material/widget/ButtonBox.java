/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.rey.material.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.drawable.AccelerateInterpolator;
import com.rey.material.drawable.Animatable;
import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class ButtonBox extends ohos.agp.components.Button implements Animatable, Component.DrawTask, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ButtonBox");

    private boolean mRunning = false;

    private Paint mShaderPaint;
    private Paint mFillPaint;
    private Mask mMask;
    private RadialShader mInShader;
    private RadialShader mOutShader;
    private Matrix mMatrix;
    private int mAlpha = 255;

    private RectFloat mBackgroundBounds;
    private Path mBackground;
    private int mBackgroundAnimDuration;
    private int mBackgroundColor;
    private float mBackgroundAlphaPercent;

    private Point mRipplePoint;
    private float mRippleRadius;
    private int mRippleType;
    private int mMaxRippleRadius;
    private int mRippleAnimDuration;
    private int mRippleColor;
    private float mRippleAlphaPercent;
    private int mDelayClickType;

    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;

    private long mStartTime;

    private long mTouchTime;
    private int mDelayRippleTime;

    private int mState = STATE_OUT;

    public static final int DELAY_CLICK_NONE = 0;
    public static final int DELAY_CLICK_UNTIL_RELEASE = 1;
    public static final int DELAY_CLICK_AFTER_RELEASE = 2;

    private static final int STATE_OUT = 0;
    private static final int STATE_PRESS = 1;
    private static final int STATE_HOVER = 2;
    private static final int STATE_RELEASE_ON_HOLD = 3;
    private static final int STATE_RELEASE = 4;

    private static final int TYPE_TOUCH_MATCH_VIEW = -1;
    private static final int TYPE_TOUCH = 0;
    private static final int TYPE_WAVE = 1;

    private static final float[] GRADIENT_STOPS = new float[]{0f, 0.99f, 1f};
    private static final float GRADIENT_RADIUS = 16;

    private int mMaskType;
    private int mMaskTopLeftCornerRadius;
    private int mMaskTopRightCornerRadius;
    private int mMaskBottomLeftCornerRadius;
    private int mMaskBottomRightCornerRadius;
    private int mMaskLeft;
    private int mMaskTop;
    private int mMaskRight;
    private int mMaskBottom;

    private EventHandler mHandler;

    public ButtonBox(Context context) {
        super(context);
    }

    public ButtonBox(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        mBackgroundColor = Util.getColorValue(attrs, "rd_backgroundColor", new Color(0)).getValue();
        mBackgroundAnimDuration = Util.getIntegerValue(attrs, "rd_backgroundAnimDuration", 400);
        mRippleType = Util.getIntegerValue(attrs, "rd_rippleType", TYPE_TOUCH);
        mDelayClickType = Util.getIntegerValue(attrs, "rd_delayClick", DELAY_CLICK_NONE);
        mDelayRippleTime = Util.getIntegerValue(attrs, "rd_delayRipple", 0);
        mMaxRippleRadius = Util.getDimensionValue(attrs, "rd_maxRippleRadius", 48);
        mRippleColor = Util.getColorValue(attrs, "rd_rippleColor", new Color(0)).getValue();
        mRippleAnimDuration = Util.getIntegerValue(attrs, "rd_rippleAnimDuration", 400);
        mMaskType = Util.getIntegerValue(attrs, "rd_maskType", Mask.TYPE_RECTANGLE);
        cornerRadius(Util.getDimensionValue(attrs, "rd_cornerRadius", 0));
        mMaskTopLeftCornerRadius = Util.getDimensionValue(attrs, "rd_topLeftCornerRadius", mMaskTopLeftCornerRadius);
        mMaskTopRightCornerRadius = Util.getDimensionValue(attrs, "rd_topRightCornerRadius", mMaskTopRightCornerRadius);
        mMaskBottomRightCornerRadius = Util.getDimensionValue(attrs, "rd_bottomRightCornerRadius", mMaskBottomRightCornerRadius);
        mMaskBottomLeftCornerRadius = Util.getDimensionValue(attrs, "rd_bottomLeftCornerRadius", mMaskBottomLeftCornerRadius);
        padding(Util.getDimensionValue(attrs, "rd_padding", 0));
        mMaskLeft = Util.getDimensionValue(attrs, "rd_leftPadding", mMaskLeft);
        mMaskRight = Util.getDimensionValue(attrs, "rd_rightPadding", mMaskRight);
        mMaskTop = Util.getDimensionValue(attrs, "rd_topPadding", mMaskTop);
        mMaskBottom = Util.getDimensionValue(attrs, "rd_bottomPadding", mMaskBottom);

        setMask(mMaskType, mMaskTopLeftCornerRadius, mMaskTopRightCornerRadius, mMaskBottomRightCornerRadius, mMaskBottomLeftCornerRadius,
                mMaskLeft, mMaskTop, mMaskRight, mMaskBottom);

        if (mInInterpolator == null)
            mInInterpolator = new AccelerateInterpolator();

        if (mOutInterpolator == null)
            mOutInterpolator = new DecelerateInterpolator();

        mFillPaint = new Paint();
        mFillPaint.setStyle(Paint.Style.FILL_STYLE);

        mShaderPaint = new Paint();
        mShaderPaint.setStyle(Paint.Style.FILL_STYLE);

        mBackground = new Path();
        mBackgroundBounds = new RectFloat();

        mRipplePoint = new Point();

        mMatrix = new Matrix();

        mInShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                new Color[]{new Color(mRippleColor), new Color(mRippleColor), new Color(0)}, Shader.TileMode.CLAMP_TILEMODE);
        if (mRippleType == TYPE_WAVE)
            mOutShader = new RadialShader(new Point(0, 0), GRADIENT_RADIUS, GRADIENT_STOPS,
                    new Color[]{new Color(0), new Color(ColorUtil.getColor(mRippleColor, 0f)), new Color(mRippleColor)},
                    Shader.TileMode.CLAMP_TILEMODE);

        mHandler = new EventHandler(EventRunner.getMainEventRunner());

        setLayoutRefreshedListener(refreshedListener);
        setTouchEventListener(this::onTouchEvent);
        addDrawTask(this::onDraw);
    }

    public int getDelayClickType() {
        return mDelayClickType;
    }

    public void setDelayClickType(int type) {
        mDelayClickType = type;
    }

    public void setMask(int type, int topLeftCornerRadius, int topRightCornerRadius, int bottomRightCornerRadius, int bottomLeftCornerRadius, int left, int top, int right, int bottom) {
        mMask = new Mask(type, topLeftCornerRadius, topRightCornerRadius, bottomRightCornerRadius, bottomLeftCornerRadius, left, top, right, bottom);
    }

    public void cancel() {
        setRippleState(STATE_OUT);
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
    }

    @Override
    public void start() {
        if (isRunning())
            return;

        resetAnimation();
        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        invalidate();
    }

    @Override
    public void stop() {
        mRunning = false;
        mHandler.removeTask(mUpdater);
        invalidate();
    }

    @Override
    public boolean isRunning() {
        return mState != STATE_OUT && mState != STATE_HOVER && mRunning;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {

        switch (mRippleType) {
            case TYPE_TOUCH:
            case TYPE_TOUCH_MATCH_VIEW:
                drawTouch(canvas);
                break;
            case TYPE_WAVE:
                drawWave(canvas);
                break;
        }
    }

    private void drawTouch(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mBackgroundAlphaPercent > 0) {
                mFillPaint.setColor(new Color(mBackgroundColor));
                mFillPaint.setAlpha(mBackgroundAlphaPercent);
                canvas.drawPath(mBackground, mFillPaint);

            }
            if (mRippleRadius > 0 && mRippleAlphaPercent > 0) {
                mShaderPaint.setAlpha(mRippleAlphaPercent);
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private void drawWave(Canvas canvas) {
        if (mState != STATE_OUT) {
            if (mState == STATE_RELEASE) {
                if (mRippleRadius == 0) {
                    mFillPaint.setColor(new Color(mRippleColor));
                    canvas.drawPath(mBackground, mFillPaint);
                } else {
                    mShaderPaint.setShader(mOutShader, Paint.ShaderType.RADIAL_SHADER);
                    canvas.drawPath(mBackground, mShaderPaint);
                }
            } else if (mRippleRadius > 0) {
                mShaderPaint.setShader(mInShader, Paint.ShaderType.RADIAL_SHADER);
                canvas.drawPath(mBackground, mShaderPaint);
            }
        }
    }

    private int getMaxRippleRadius(float x, float y) {
        float x1 = x < mBackgroundBounds.getHorizontalCenter() ? mBackgroundBounds.right : mBackgroundBounds.left;
        float y1 = y < mBackgroundBounds.getVerticalCenter() ? mBackgroundBounds.bottom : mBackgroundBounds.top;
        int maxRadius = (int) Math.round(Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2)));
        return maxRadius;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
            case TouchEvent.POINT_MOVE:
                if (mState == STATE_OUT || mState == STATE_RELEASE) {
                    long time = Time.getRealActiveTime();
                    if (mTouchTime == 0)
                        mTouchTime = time;

                    setRippleEffect(x, y, 0);

                    if (mTouchTime <= time - mDelayRippleTime) {
                        if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                            mMaxRippleRadius = getMaxRippleRadius(x, y);

                        setRippleState(STATE_PRESS);
                    }
                } else if (mRippleType == TYPE_TOUCH) {
                    if (setRippleEffect(x, y, mRippleRadius)) {
                        invalidate();
                    }
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mTouchTime > 0 && mState == STATE_OUT) {
                    if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                        mMaxRippleRadius = getMaxRippleRadius(x, y);
                    setRippleState(STATE_PRESS);
                }
            case TouchEvent.CANCEL:
                mTouchTime = 0;
                if (mState != STATE_OUT) {
                    if (mState == STATE_HOVER) {
                        if (mRippleType == TYPE_WAVE || mRippleType == TYPE_TOUCH_MATCH_VIEW)
                            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), 0);

                        setRippleState(STATE_RELEASE);
                    } else
                        setRippleState(STATE_RELEASE_ON_HOLD);
                }
                break;
        }
        return true;
    }

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            int width = component.getWidth();
            int height = component.getHeight();

            mBackgroundBounds.modify(mMask.left, mMask.top, width - mMask.right, height - mMask.bottom);
            mBackground.reset();

            switch (mMask.type) {
                case Mask.TYPE_OVAL:
                    mBackground.addOval(mBackgroundBounds, Path.Direction.CLOCK_WISE);
                    break;
                case Mask.TYPE_RECTANGLE:
                    mBackground.addRoundRect(mBackgroundBounds, mMask.cornerRadius, Path.Direction.CLOCK_WISE);
                    break;
            }
        }
    };

    public long getClickDelayTime() {
        switch (mDelayClickType) {
            case DELAY_CLICK_NONE:
                return -1;
            case DELAY_CLICK_UNTIL_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
            case DELAY_CLICK_AFTER_RELEASE:
                if (mState == STATE_RELEASE_ON_HOLD)
                    return 2 * Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                else if (mState == STATE_RELEASE)
                    return Math.max(mBackgroundAnimDuration, mRippleAnimDuration) - (Time.getRealActiveTime() - mStartTime);
                break;
        }

        return -1;
    }

    private void setRippleState(int state) {
        if (mState != state) {
            //fix bug incorrect state switch
            if (mState == STATE_OUT && state != STATE_PRESS)
                return;

            mState = state;

            if (mState == STATE_OUT || mState == STATE_HOVER)
                stop();
            else
                start();
        }
    }

    private boolean setRippleEffect(float x, float y, float radius) {

        if (mRipplePoint.getPointX() != x || mRipplePoint.getPointY() != y || mRippleRadius != radius) {
            mRipplePoint.modify(x, y);
            mRippleRadius = radius;
            mInShader = new RadialShader(new Point(x, y), mRippleRadius / 1.5f, GRADIENT_STOPS,
                    new Color[]{new Color(mRippleColor), new Color(mRippleColor), new Color(0)}, Shader.TileMode.CLAMP_TILEMODE);
            if (mOutShader != null)
                mOutShader = new RadialShader(new Point(x, y), mRippleRadius / 1.5f, GRADIENT_STOPS,
                        new Color[]{new Color(0), new Color(ColorUtil.getColor(mRippleColor, 0f)), new Color(mRippleColor)},
                        Shader.TileMode.CLAMP_TILEMODE);

            return true;
        }

        return false;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            switch (mRippleType) {
                case TYPE_TOUCH:
                case TYPE_TOUCH_MATCH_VIEW:
                    updateTouch();
                    break;
                case TYPE_WAVE:
                    updateWave();
                    break;
            }
        }

    };

    private void updateTouch() {
        if (mState != STATE_RELEASE) {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = mInInterpolator.getInterpolation(backgroundProgress) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = mInInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(touchProgress));

            if (backgroundProgress == 1f && touchProgress == 1f) {
                mStartTime = Time.getRealActiveTime();
                setRippleState(mState == STATE_PRESS ? STATE_HOVER : STATE_RELEASE);
            }
        } else {
            float backgroundProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mBackgroundAnimDuration);
            mBackgroundAlphaPercent = (1f - mOutInterpolator.getInterpolation(backgroundProgress)) * Color.alpha(mBackgroundColor) / 255f;

            float touchProgress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);
            mRippleAlphaPercent = 1f - mOutInterpolator.getInterpolation(touchProgress);

            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * (1f + 0.5f * mOutInterpolator.getInterpolation(touchProgress)));

            if (backgroundProgress == 1f && touchProgress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        invalidate();
    }

    private void updateWave() {
        float progress = Math.min(1f, (float) (Time.getRealActiveTime() - mStartTime) / mRippleAnimDuration);

        if (mState != STATE_RELEASE) {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mInInterpolator.getInterpolation(progress));

            if (progress == 1f) {
                mStartTime = Time.getRealActiveTime();
                if (mState == STATE_PRESS)
                    setRippleState(STATE_HOVER);
                else {
                    setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), 0);
                    setRippleState(STATE_RELEASE);
                }
            }
        } else {
            setRippleEffect(mRipplePoint.getPointX(), mRipplePoint.getPointY(), mMaxRippleRadius * mOutInterpolator.getInterpolation(progress));

            if (progress == 1f)
                setRippleState(STATE_OUT);
        }

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        invalidate();
    }

    public static class Mask {

        public static final int TYPE_RECTANGLE = 0;
        public static final int TYPE_OVAL = 1;

        final int type;

        final float[] cornerRadius = new float[8];

        final int left;
        final int top;
        final int right;
        final int bottom;

        public Mask(int type, int topLeftCornerRadius, int topRightCornerRadius, int bottomRightCornerRadius, int bottomLeftCornerRadius, int left, int top, int right, int bottom) {
            this.type = type;

            cornerRadius[0] = topLeftCornerRadius;
            cornerRadius[1] = topLeftCornerRadius;

            cornerRadius[2] = topRightCornerRadius;
            cornerRadius[3] = topRightCornerRadius;

            cornerRadius[4] = bottomRightCornerRadius;
            cornerRadius[5] = bottomRightCornerRadius;

            cornerRadius[6] = bottomLeftCornerRadius;
            cornerRadius[7] = bottomLeftCornerRadius;

            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

    }

    public void padding(int padding) {
        mMaskLeft = padding;
        mMaskTop = padding;
        mMaskRight = padding;
        mMaskBottom = padding;
    }

    public void cornerRadius(int radius) {
        mMaskTopLeftCornerRadius = radius;
        mMaskTopRightCornerRadius = radius;
        mMaskBottomLeftCornerRadius = radius;
        mMaskBottomRightCornerRadius = radius;
    }


}
