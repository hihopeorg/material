package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.Animatable;
import com.rey.material.drawable.CircularProgressDrawable;
import com.rey.material.drawable.LinearProgressDrawable;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class ProgressView extends Component implements ThemeManager.OnThemeChangedListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "ProgressView");

    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    private boolean mAutostart = false;
    private boolean mCircular = true;
    private int mProgressId;

    public static final int MODE_DETERMINATE = 0;
    public static final int MODE_INDETERMINATE = 1;
    public static final int MODE_BUFFER = 2;
    public static final int MODE_QUERY = 3;


    private DrawTask mProgressDrawable;

    public ProgressView(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public ProgressView(Context context, AttrSet attrs) {
        super(context, attrs);


        init(context, attrs, 0, 0);
    }

    public ProgressView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        applyStyle(context, attrs, defStyleAttr, defStyleRes);
        setBindStateChangedListener(mBindStateChangedListener);
        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);

    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    private boolean needCreateProgress(boolean circular) {
        if (mProgressDrawable == null)
            return true;

        if (circular)
            return !(mProgressDrawable instanceof CircularProgressDrawable);
        else
            return !(mProgressDrawable instanceof LinearProgressDrawable);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

        int progressId = 0;
        int progressMode = 0;
        float progress = 0;
        float secondaryProgress = 0;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("pv_autostart")) {
                mAutostart = Util.getBoolValue(attrs, name, false);
            } else if (name.equals("pv_circular")) {
                mCircular = Util.getBoolValue(attrs, name, true);
            } else if (name.equals("pv_progressStyle")) {
                progressId = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("pv_progressMode")) {
                progressMode = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("pv_progress")) {
                progress = Util.getFloatValue(attrs, name, 0);
            } else if (name.equals("pv_secondaryProgress")) {
                secondaryProgress = Util.getFloatValue(attrs, name, 0);
            }
        }

        boolean needStart = false;

        if (needCreateProgress(mCircular)) {
            mProgressId = progressId;

            needStart = mProgressDrawable != null && ((Animatable) mProgressDrawable).isRunning();
            mProgressDrawable = mCircular ? new CircularProgressDrawable.Builder(context, attrs, 0, mProgressId).build() : new LinearProgressDrawable.Builder(context, attrs, 0, mProgressId).build();
            this.addDrawTask(mProgressDrawable, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);

        }
        if (progressMode >= 0) {
            if (mProgressDrawable instanceof CircularProgressDrawable)
                ((CircularProgressDrawable) mProgressDrawable).setProgressMode(progressMode);
            else
                ((LinearProgressDrawable) mProgressDrawable).setProgressMode(progressMode);
        }

        if (progress >= 0)
            setProgress(progress);

        if (secondaryProgress >= 0)
            setSecondaryProgress(secondaryProgress);

        if (needStart) {
            start();
        }

    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (getVisibility() == Component.VISIBLE && mAutostart)
                start();
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(ProgressView.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            if (mAutostart)
                stop();

            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(ProgressView.this::onThemeChanged);
        }
    };

    public int getProgressMode() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getProgressMode();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getProgressMode();
    }

    /**
     * @return The current progress of this view in [0..1] range.
     */
    public float getProgress() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getProgress();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getProgress();
    }

    /**
     * @return The current secondary progress of this view in [0..1] range.
     */
    public float getSecondaryProgress() {
        if (mCircular)
            return ((CircularProgressDrawable) mProgressDrawable).getSecondaryProgress();
        else
            return ((LinearProgressDrawable) mProgressDrawable).getSecondaryProgress();
    }

    /**
     * Set the current progress of this view.
     *
     * @param percent The progress value in [0..1] range.
     */
    public void setProgress(float percent) {
        if (mCircular)
            ((CircularProgressDrawable) mProgressDrawable).setProgress(percent);
        else
            ((LinearProgressDrawable) mProgressDrawable).setProgress(percent);
    }

    /**
     * Set the current secondary progress of this view.
     *
     * @param percent The progress value in [0..1] range.
     */
    public void setSecondaryProgress(float percent) {
        if (mCircular)
            ((CircularProgressDrawable) mProgressDrawable).setSecondaryProgress(percent);
        else
            ((LinearProgressDrawable) mProgressDrawable).setSecondaryProgress(percent);
    }

    /**
     * Start showing progress.
     */
    public void start() {
        if (mProgressDrawable != null)
            ((Animatable) mProgressDrawable).start();
    }

    /**
     * Stop showing progress.
     */
    public void stop() {
        if (mProgressDrawable != null)
            ((Animatable) mProgressDrawable).stop();
    }

}
