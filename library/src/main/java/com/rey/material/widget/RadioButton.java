package com.rey.material.widget;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.drawable.Animatable;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class RadioButton extends ohos.agp.components.Button implements Animatable, Component.DrawTask, Component.TouchEventListener {

    private boolean mRunning = false;

    private Paint mPaint;

    private long mStartTime;
    private float mAnimProgress;
    private int mAnimDuration;
    private int mStrokeSize;
    private int mRadius;
    private int mInnerRadius;
    private int mStrokeColor;
    private boolean mChecked = false;
    private boolean mAutoStart = false;
    private RectFloat mBoxRect;

    private EventHandler mHandler;

    public RadioButton(Context context) {
        super(context);
    }

    public RadioButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        mStrokeSize = Util.getDimensionValue(attrs, "rbd_strokeSize", AttrHelper.vp2px(2, context));
        mRadius = Util.getDimensionValue(attrs, "rbd_radius", AttrHelper.vp2px(10, context));
        mInnerRadius = Util.getDimensionValue(attrs, "rbd_innerRadius", AttrHelper.vp2px(5, context));
        mAnimDuration = Util.getIntegerValue(attrs, "rbd_animDuration", 400);
        mChecked = Util.getBoolValue(attrs, "rbd_checked", false);

        mBoxRect = new RectFloat();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mHandler = new EventHandler(EventRunner.getMainEventRunner());

        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
        setLayoutRefreshedListener(refreshedListener);

        if (mAutoStart)
            start();

    }

    private void drawChecked(Canvas canvas) {
        float cx = mBoxRect.getHorizontalCenter();
        float cy = mBoxRect.getVerticalCenter();

        if (isRunning()) {
            float halfStrokeSize = mStrokeSize / 2f;
            float inTime = (mRadius - halfStrokeSize) / (mRadius - halfStrokeSize + mRadius - mStrokeSize - mInnerRadius);

            if (mAnimProgress < inTime) {
                float inProgress = mAnimProgress / inTime;
                float outerRadius = mRadius + halfStrokeSize * (1f - inProgress);
                float innerRadius = (mRadius - halfStrokeSize) * (1f - inProgress);

                mPaint.setColor(new Color(mStrokeColor));//ColorUtil.getMiddleColor(mPrevColor, mCurColor, inProgress));
                mPaint.setStrokeWidth(outerRadius - innerRadius);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawCircle(cx, cy, (outerRadius + innerRadius) / 2, mPaint);
            } else {
                float outProgress = (mAnimProgress - inTime) / (1f - inTime);
                float innerRadius = (mRadius - mStrokeSize) * (1 - outProgress) + mInnerRadius * outProgress;

                mPaint.setColor(new Color(mStrokeColor));
                mPaint.setStyle(Paint.Style.FILL_STYLE);
                canvas.drawCircle(cx, cy, innerRadius, mPaint);

                float outerRadius = mRadius + halfStrokeSize * outProgress;
                mPaint.setStrokeWidth(mStrokeSize);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawCircle(cx, cy, outerRadius - halfStrokeSize, mPaint);
            }
        } else {
            mPaint.setColor(new Color(mStrokeColor));
            mPaint.setStrokeWidth(mStrokeSize);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            canvas.drawCircle(cx, cy, mRadius, mPaint);

            mPaint.setStyle(Paint.Style.FILL_STYLE);
            canvas.drawCircle(cx, cy, mInnerRadius, mPaint);
        }
    }

    private void drawUnchecked(Canvas canvas) {
        float cx = mBoxRect.getHorizontalCenter();
        float cy = mBoxRect.getVerticalCenter();

        if (isRunning()) {
            float halfStrokeSize = mStrokeSize / 2f;
            float inTime = (mRadius - mStrokeSize - mInnerRadius) / (mRadius - halfStrokeSize + mRadius - mStrokeSize - mInnerRadius);

            if (mAnimProgress < inTime) {
                float inProgress = mAnimProgress / inTime;
                float innerRadius = (mRadius - mStrokeSize) * inProgress + mInnerRadius * (1f - inProgress);

                mPaint.setColor(new Color(mStrokeColor));
                mPaint.setStyle(Paint.Style.FILL_STYLE);
                canvas.drawCircle(cx, cy, innerRadius, mPaint);

                float outerRadius = mRadius + halfStrokeSize * (1f - inProgress);
                mPaint.setStrokeWidth(mStrokeSize);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawCircle(cx, cy, outerRadius - halfStrokeSize, mPaint);
            } else {
                float outProgress = (mAnimProgress - inTime) / (1f - inTime);
                float outerRadius = mRadius + halfStrokeSize * outProgress;
                float innerRadius = (mRadius - halfStrokeSize) * outProgress;

                mPaint.setColor(new Color(mStrokeColor));
                mPaint.setStrokeWidth(outerRadius - innerRadius);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawCircle(cx, cy, (outerRadius + innerRadius) / 2, mPaint);
            }
        } else {
            mPaint.setColor(new Color(mStrokeColor));
            mPaint.setStrokeWidth(mStrokeSize);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            canvas.drawCircle(cx, cy, mRadius, mPaint);
        }
    }

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            int height = component.getHeight();
            mBoxRect.modify(getPaddingLeft() + mStrokeSize, height / 2 - mRadius, getPaddingLeft() + mRadius * 2, height / 2 + mRadius);
        }
    };

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mAnimProgress = 0f;
    }

    @Override
    public void start() {
        resetAnimation();

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        invalidate();
    }

    @Override
    public void stop() {
        mRunning = false;
        mHandler.removeTask(mUpdater);
        invalidate();
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (mAnimProgress == 1f)
            mRunning = false;

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int height = component.getHeight();
        mBoxRect.modify(getPaddingLeft() + mStrokeSize, height / 2 - mRadius, getPaddingLeft() + mRadius * 2, height / 2 + mRadius);
        if (mChecked) {
            mStrokeColor = 0xFF00E5FF;
            drawChecked(canvas);
        } else {
            mStrokeColor = 0xFF78909C;
            drawUnchecked(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                mChecked = !mChecked;
                start();
                break;
        }
        return true;
    }
}
