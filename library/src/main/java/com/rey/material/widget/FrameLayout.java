package com.rey.material.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ViewUtil;

public class FrameLayout extends StackLayout implements ThemeManager.OnThemeChangedListener,
        Component.BindStateChangedListener, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "FrameLayout");
    private RippleManager mRippleManager;

    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    public FrameLayout(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public FrameLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public FrameLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, 0, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        applyStyle(context, attrs, defStyleAttr, defStyleRes);
        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
        setBindStateChangedListener(this);
        setTouchEventListener(this);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        if (mStyleId != 0) {
            ThemeManager.getInstance().registerOnThemeChangedListener(this);
            onThemeChanged(null);
        }
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        RippleManager.cancelRipple(this);
        if (mStyleId != 0) {
            ThemeManager.getInstance().unregisterOnThemeChangedListener(this);
        }
    }

    @Override
    public void setBackground(Element drawable) {
        Element background = getBackgroundElement();
        if (background instanceof RippleDrawable && !(drawable instanceof RippleDrawable)) {
            ((RippleDrawable) background).setBackgroundDrawable(drawable);
        } else {
            super.setBackground(drawable);
        }
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null) {
                    mRippleManager = new RippleManager();
                }
            }
        }

        return mRippleManager;
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        RippleManager rippleManager = getRippleManager();
        if (l == rippleManager) {
            super.setClickedListener(l);
        } else {
            rippleManager.setOnClickListener(l);
            setClickedListener(rippleManager);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        return getRippleManager().onTouchEvent(this, event);
    }
}
