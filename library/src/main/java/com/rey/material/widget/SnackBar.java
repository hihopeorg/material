package com.rey.material.widget;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ViewUtil;

public class SnackBar extends FrameLayout implements ThemeManager.OnThemeChangedListener,
        Component.EstimateSizeListener, ComponentContainer.ArrangeListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "SnackBar");
    private TextView mText;
    private Button mAction;

    public static final int MATCH_PARENT = ComponentContainer.LayoutConfig.MATCH_PARENT;
    public static final int MATCH_CONTENT = ComponentContainer.LayoutConfig.MATCH_CONTENT;

    private ShapeElement mBackground;
    private int mMarginStart;
    private int mMarginBottom;
    private int mWidth;
    private int mHeight;
    private int mMaxHeight;
    private int mMinHeight;
    private long mDuration;
    private int mActionId;
    private boolean mRemoveOnDismiss;

    private AnimatorProperty mInAnimation;
    private AnimatorProperty mOutAnimation;

    private Runnable mDismissRunnable = new Runnable() {
        @Override
        public void run() {
            dismiss();
        }
    };

    private int mState = STATE_DISMISSED;

    /**
     * Indicate this SnackBar is already dismissed.
     */
    public static final int STATE_DISMISSED = 0;
    /**
     * Indicate this SnackBar is already shown.
     */
    public static final int STATE_SHOWN = 1;
    /**
     * Indicate this SnackBar is being shown.
     */
    public static final int STATE_SHOWING = 2;
    /**
     * Indicate this SnackBar is being dismissed.
     */
    public static final int STATE_DISMISSING = 3;

    private boolean mIsRtl;

    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());


    /**
     * Interface definition for a callback to be invoked when action button is clicked.
     */
    public interface OnActionClickListener {

        /**
         * Called when action button is clicked.
         *
         * @param sb       The SnackBar fire this event.
         * @param actionId The ActionId of this SnackBar.
         */
        void onActionClick(SnackBar sb, int actionId);
    }

    private OnActionClickListener mActionClickListener;

    /**
     * Interface definition for a callback to be invoked when SnackBar's state is changed.
     */
    public interface OnStateChangeListener {

        /**
         * Called when SnackBar's state is changed.
         *
         * @param sb       The SnackBar fire this event.
         * @param oldState The old state of SnackBar.
         * @param newState The new state of SnackBar.
         */
        void onStateChange(SnackBar sb, int oldState, int newState);
    }

    private OnStateChangeListener mStateChangeListener;

    public static SnackBar make(Context context) {
        return new SnackBar(context);
    }

    public SnackBar(Context context) {
        super(context);
    }

    public SnackBar(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public SnackBar(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        mWidth = MATCH_PARENT;
        mHeight = MATCH_CONTENT;
        mDuration = -1;
        mIsRtl = false;
        mText = new TextView(context);
        mText.setMultipleLine(false);
        mText.setTextAlignment(TextAlignment.START | TextAlignment.VERTICAL_CENTER);
        addComponent(mText, new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT));

        mAction = new Button(context);
        mAction.setBackground(null);
        mAction.setTextAlignment(TextAlignment.CENTER);
        mAction.setClickedListener(new ClickedListener() {

            @Override
            public void onClick(Component v) {
                if (mActionClickListener != null) {
                    mActionClickListener.onActionClick(SnackBar.this, mActionId);
                }

                dismiss();
            }

        });
        addComponent(mAction, new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT));

        mBackground = new ShapeElement();
        mBackground.setShape(ShapeElement.RECTANGLE);
        mBackground.setRgbColor(RgbColor.fromArgbInt(0xff323232));
        ViewUtil.setBackground(this, mBackground);
        setClickable(true);

        super.init(context, attrs, defStyleAttr, defStyleRes);
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    @Override
    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        super.applyStyle(context, attrs, defStyleAttr, defStyleRes);
        int horizontalPadding = -1;
        int verticalPadding = -1;
        int textSize = -1;
        int textColor = 0;
        boolean textColorDefined = false;
        int actionTextSize = -1;
        Color actionTextColor = null;
        ViewUtil.TypedArray a = ViewUtil.TypedArray.obtainStyledAttributes(context, attrs, null, null, defStyleRes);
        for (int i = 0, count = a.getIndexCount(); i < count; i++) {
            String attr = a.getIndex(i);
            if ("sb_backgroundColor".equals(attr)) {
                backgroundColor(a.getColorValue(attr, Color.TRANSPARENT).getValue());
            } else if ("sb_backgroundCornerRadius".equals(attr)) {
                backgroundRadius(a.getDimensionValue(attr, 0));
            } else if ("sb_horizontalPadding".equals(attr)) {
                horizontalPadding = a.getDimensionValue(attr, 0);
            } else if ("sb_verticalPadding".equals(attr)) {
                verticalPadding = a.getDimensionValue(attr, 0);
            } else if ("sb_width".equals(attr)) {
                width(a.getDimensionValue(attr, 0));
            } else if ("sb_height".equals(attr)) {
                height(a.getDimensionValue(attr, 0));
            } else if ("sb_minWidth".equals(attr)) {
                minWidth(a.getDimensionValue(attr, 0));
            } else if ("sb_maxWidth".equals(attr)) {
                maxWidth(a.getDimensionValue(attr, 0));
            } else if ("sb_minHeight".equals(attr)) {
                minHeight(a.getDimensionValue(attr, 0));
            } else if ("sb_maxHeight".equals(attr)) {
                maxHeight(a.getDimensionValue(attr, 0));
            } else if ("sb_marginStart".equals(attr)) {
                marginStart(a.getDimensionValue(attr, 0));
            } else if ("sb_marginBottom".equals(attr)) {
                marginBottom(a.getDimensionValue(attr, 0));
            } else if ("sb_textSize".equals(attr)) {
                textSize = a.getDimensionValue(attr, 0);
            } else if ("sb_textColor".equals(attr)) {
                textColor = a.getColorValue(attr, Color.TRANSPARENT).getValue();
                textColorDefined = true;
            } else if ("sb_text".equals(attr)) {
                text(a.getStringValue(attr, ""));
            } else if ("sb_singleLine".equals(attr)) {
                singleLine(a.getBoolValue(attr, true));
            } else if ("sb_maxLines".equals(attr)) {
                maxLines(a.getIntegerValue(attr, 0));
            } else if ("sb_lines".equals(attr)) {
                lines(a.getIntegerValue(attr, 0));
            } else if ("sb_ellipsize".equals(attr)) {
                int ellipsize = a.getIntegerValue(attr, 0);
                switch (ellipsize) {
                    case 1:
                        ellipsize(Text.TruncationMode.ELLIPSIS_AT_START);
                        break;
                    case 2:
                        ellipsize(Text.TruncationMode.ELLIPSIS_AT_MIDDLE);
                        break;
                    case 3:
                        ellipsize(Text.TruncationMode.ELLIPSIS_AT_END);
                        break;
                    case 4:
                        ellipsize(Text.TruncationMode.AUTO_SCROLLING);
                        break;
                    default:
                        ellipsize(Text.TruncationMode.ELLIPSIS_AT_END);
                        break;
                }
            } else if ("sb_actionTextSize".equals(attr)) {
                actionTextSize = a.getDimensionValue(attr, 0);
            } else if ("sb_actionTextColor".equals(attr)) {
                actionTextColor = a.getColorValue(attr, Color.TRANSPARENT);
            } else if ("sb_actionText".equals(attr)) {
                actionText(a.getStringValue(attr, ""));
            } else if ("sb_actionRipple".equals(attr)) {
                actionRipple(a.getIntegerValue(attr, 0));
            } else if ("sb_duration".equals(attr)) {
                duration(a.getIntegerValue(attr, 0));
            } else if ("sb_removeOnDismiss".equals(attr)) {
                removeOnDismiss(a.getBoolValue(attr, true));
            }
        }

        a.recycle();

        if (horizontalPadding >= 0 || verticalPadding >= 0) {
            padding(horizontalPadding >= 0 ? horizontalPadding : mText.getPaddingLeft(),
                    verticalPadding >= 0 ? verticalPadding : mText.getPaddingTop());
        }

        if (textSize >= 0) {
            textSize(textSize);
        }
        if (textColorDefined) {
            textColor(textColor);
        }

        if (actionTextSize >= 0) {
            actionTextSize(actionTextSize);
        }
        if (actionTextColor != null) {
            actionTextColor(actionTextColor);
        }
    }

    @Override
    public void onRtlChanged(LayoutDirection layoutDirection) {
        boolean rtl = layoutDirection == LayoutDirection.RTL;
        if (mIsRtl != rtl) {
            mIsRtl = rtl;
            mText.setLayoutDirection((mIsRtl ? LayoutDirection.RTL : LayoutDirection.LTR));
            mAction.setLayoutDirection((mIsRtl ? LayoutDirection.RTL : LayoutDirection.LTR));

            postLayout();
        }
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int width;
        int height;

        if (mAction.getVisibility() == Component.VISIBLE) {
            mAction.estimateSize(EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT), heightMeasureSpec);
            int padding = mIsRtl ? mText.getPaddingLeft() : mText.getPaddingRight();
            mText.estimateSize(EstimateSpec.getSizeWithMode(
                    widthSize - (mAction.getEstimatedWidth() - padding), widthMode), heightMeasureSpec);
            width = mText.getEstimatedWidth() + mAction.getEstimatedWidth() - padding;
        } else {
            mText.estimateSize(EstimateSpec.getSizeWithMode(widthSize, widthMode), heightMeasureSpec);
            width = mText.getEstimatedWidth();
        }

        height = Math.max(mText.getEstimatedHeight(), mAction.getEstimatedHeight());

        switch (widthMode) {
            case EstimateSpec.NOT_EXCEED:
                width = Math.min(widthSize, width);
                break;
            case EstimateSpec.PRECISE:
                width = widthSize;
                break;
        }

        switch (heightMode) {
            case EstimateSpec.NOT_EXCEED:
                height = Math.min(heightSize, height);
                break;
            case EstimateSpec.PRECISE:
                height = heightSize;
                break;
        }

        if (mMaxHeight > 0) {
            height = Math.min(mMaxHeight, height);
        }

        if (mMinHeight > 0) {
            height = Math.max(mMinHeight, height);
        }

        setEstimatedSize(width, height);
        return true;
    }

    @Override
    public boolean onArrange(int l, int t, int width, int height) {
        int r = l + width;
        int b = t + height;
        int childLeft = getPaddingLeft();
        int childRight = r - l - getPaddingRight();
        int childTop = getPaddingTop();
        int childBottom = b - t - getPaddingBottom();
        if (mAction.getVisibility() == Component.VISIBLE) {
            if (mIsRtl) {
                mAction.arrange(childLeft, childTop, mAction.getEstimatedWidth(), childBottom - childTop);
                childLeft += mAction.getEstimatedWidth() - mText.getPaddingLeft();
            } else {
                mAction.arrange(
                        childRight - mAction.getEstimatedWidth(), childTop, mAction.getEstimatedWidth(),
                        childBottom - childTop);
                childRight -= mAction.getEstimatedWidth() - mText.getPaddingRight();
            }
        }
        mText.arrange(childLeft, childTop, childRight - childLeft, childBottom - childTop);
        return true;
    }

    /**
     * Set the text that this SnackBar is to display.
     *
     * @param text The text is displayed.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar text(String text) {
        mText.setText(text);
        return this;
    }

    /**
     * Set the text that this SnackBar is to display.
     *
     * @param id The resourceId of text is displayed.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar text(int id) {
        return text(getContext().getString(id));
    }

    /**
     * Set the text color.
     *
     * @param color The color of text.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar textColor(int color) {
        mText.setTextColor(new Color(color));
        return this;
    }

    /**
     * Set the text size to the given value, interpreted as "scaled pixel" units.
     *
     * @param size The size of text.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar textSize(float size) {
        mText.setTextSize((int) size, Text.TextSizeType.PX);
        return this;
    }

    /**
     * Causes words in the text that are longer than the view is wide to be ellipsized instead of broken in the middle.
     *
     * @param at
     * @return This SnackBar for chaining methods.
     */
    public SnackBar ellipsize(Text.TruncationMode at) {
        mText.setTruncationMode(at);
        return this;
    }

    /**
     * Sets the text will be single-line or not.
     *
     * @param b
     * @return This SnackBar for chaining methods.
     */
    public SnackBar singleLine(boolean b) {
        mText.setMultipleLine(!b);
        return this;
    }

    /**
     * Makes the text at most this many lines tall.
     *
     * @param lines The maximum line value.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar maxLines(int lines) {
        mText.setMaxTextLines(lines);
        return this;
    }

    /**
     * Makes the text exactly this many lines tall.
     *
     * @param lines The line number.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar lines(int lines) {
        mText.setMaxTextLines(lines);//
        return this;
    }

    /**
     * Set the actionId of this SnackBar. Used to determine the current action of this SnackBar.
     *
     * @param id The actionId value.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionId(int id) {
        mActionId = id;
        return this;
    }

    /**
     * Set the text that the ActionButton is to display.
     *
     * @param text If null, then the ActionButton will be hidden.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionText(String text) {
        if (TextTool.isNullOrEmpty(text)) {
            mAction.setVisibility(Component.INVISIBLE);
        } else {
            mAction.setVisibility(Component.VISIBLE);
            mAction.setText(text);
        }
        return this;
    }

    /**
     * Set the text that the ActionButton is to display.
     *
     * @param id If 0, then the ActionButton will be hidden.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionText(int id) {
        if (id == 0) {
            return actionText(null);
        }

        return actionText(getContext().getString(id));
    }

    /**
     * Set the text color of the ActionButton for all states.
     *
     * @param color The color of text.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionTextColor(int color) {
        mAction.setTextColor(new Color(color));
        return this;
    }

    /**
     * Set the text color of the ActionButton.
     *
     * @param color
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionTextColor(Color color) {
        mAction.setTextColor(color);
        return this;
    }

    /**
     * Set the text size of the ActionButton to the given value, interpreted as "scaled pixel" units.
     *
     * @param size The size of text.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionTextSize(float size) {
        mAction.setTextSize((int) size, Text.TextSizeType.PX);
        return this;
    }

    /**
     * Set the style of RippleEffect of the ActionButton.
     *
     * @param resId The resourceId of RippleEffect.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionRipple(int resId) {
        if (resId != 0) {
            ViewUtil.setBackground(mAction, new RippleDrawable.Builder(getContext(), resId).build());
        }
        return this;
    }

    /**
     * Set the duration this SnackBar will be shown before dismissing.
     *
     * @param duration If 0, then the SnackBar will not be dismissed until {@link #dismiss() dismiss()} is called.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar duration(long duration) {
        mDuration = duration;
        return this;
    }

    /**
     * Set the background color of this SnackBar.
     * <p>
     * param color The color of background.
     *
     * @return This SnackBar for chaining methods.
     */
    public SnackBar backgroundColor(int color) {
        mBackground.setRgbColor(RgbColor.fromArgbInt(color));
        return this;
    }

    /**
     * Set the background's corner radius of this SnackBar.
     *
     * @param radius The corner radius.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar backgroundRadius(int radius) {
        mBackground.setCornerRadius(radius);
        return this;
    }

    /**
     * Set the horizontal padding between this SnackBar and it's text and button.
     *
     * @param padding
     * @return This SnackBar for chaining methods.
     */
    public SnackBar horizontalPadding(int padding) {
        mText.setPadding(padding, mText.getPaddingTop(), padding, mText.getPaddingBottom());
        mAction.setPadding(padding, mAction.getPaddingTop(), padding, mAction.getPaddingBottom());
        return this;
    }

    /**
     * Set the vertical padding between this SnackBar and it's text and button.
     *
     * @param padding
     * @return This SnackBar for chaining methods.
     */
    public SnackBar verticalPadding(int padding) {
        mText.setPadding(mText.getPaddingLeft(), padding, mText.getPaddingRight(), padding);
        mAction.setPadding(mAction.getPaddingLeft(), padding, mAction.getPaddingRight(), padding);
        return this;
    }

    /**
     * Set the padding between this SnackBar and it's text and button.
     *
     * @param horizontalPadding The horizontal padding.
     * @param verticalPadding   The vertical padding.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar padding(int horizontalPadding, int verticalPadding) {
        mText.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
        mAction.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
        return this;
    }

    /**
     * Makes this SnackBar exactly this many pixels wide.
     *
     * @param width The width value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar width(int width) {
        mWidth = width;
        return this;
    }

    /**
     * Makes this SnackBar at least this many pixels wide
     *
     * @param width The minimum width value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar minWidth(int width) {
        mText.setMinWidth(width);
        return this;
    }

    /**
     * Makes this SnackBar at most this many pixels wide
     *
     * @param width The maximum width value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar maxWidth(int width) {
        mText.setMaxTextWidth(width);
        return this;
    }

    /**
     * Makes this SnackBar exactly this many pixels tall.
     *
     * @param height The height value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar height(int height) {
        mHeight = height;
        return this;
    }

    /**
     * Makes this SnackBar at most this many pixels tall
     *
     * @param height The maximum height value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar maxHeight(int height) {
        mMaxHeight = height;
        return this;
    }

    /**
     * Makes this SnackBar at least this many pixels tall
     *
     * @param height The maximum height value in pixels.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar minHeight(int height) {
        mMinHeight = height;
        return this;
    }

    /**
     * Set the start margin between this SnackBar and it's parent.
     *
     * @param size
     * @return This SnackBar for chaining methods.
     */
    public SnackBar marginStart(int size) {
        mMarginStart = size;
        return this;
    }

    /**
     * Set the bottom margin between this SnackBar and it's parent.
     *
     * @param size
     * @return This SnackBar for chaining methods.
     */
    public SnackBar marginBottom(int size) {
        mMarginBottom = size;
        return this;
    }

    /**
     * Set the listener will be called when the ActionButton is clicked.
     *
     * @param listener The {@link OnActionClickListener} will be called.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar actionClickListener(OnActionClickListener listener) {
        mActionClickListener = listener;
        return this;
    }

    /**
     * Set the listener will be called when this SnackBar's state is changed.
     *
     * @param listener The {@link OnStateChangeListener} will be called.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar stateChangeListener(OnStateChangeListener listener) {
        mStateChangeListener = listener;
        return this;
    }

    /**
     * Set the animation will be shown when SnackBar enter screen.
     *
     * @param anim The animation.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar animationIn(AnimatorProperty anim) {
        mInAnimation = anim;
        return this;
    }

    /**
     * Set the animation will be shown when SnackBar exit screen.
     *
     * @param anim The animation.
     * @return This SnackBar for chaining methods.
     */
    public SnackBar animationOut(AnimatorProperty anim) {
        mOutAnimation = anim;
        return this;
    }

    /**
     * Indicate that this SnackBar should remove itself from parent view after being dismissed.
     *
     * @param b
     * @return This SnackBar for chaining methods.
     */
    public SnackBar removeOnDismiss(boolean b) {
        mRemoveOnDismiss = b;
        return this;
    }

    /**
     * Show this SnackBar. It will auto attach to the parent view.
     *
     * @param parent Must be {@linke StackLayout} or {@link DependentLayout}
     */
    public void show(ComponentContainer parent) {
        if (mState == STATE_SHOWING || mState == STATE_DISMISSING) {
            return;
        }

        if (getComponentParent() != parent) {
            if (getComponentParent() != null) {
                getComponentParent().removeComponent(this);
            }

            parent.addComponent(this);
        }

        show();
    }

    /**
     * Show this SnackBar.
     * Make sure it already attached to a parent view or this method will do nothing.
     */
    public void show() {
        ComponentContainer parent = (ComponentContainer) getComponentParent();
        if (parent == null || mState == STATE_SHOWING || mState == STATE_DISMISSING) {
            return;
        }

        if (parent instanceof StackLayout) {
            LayoutConfig params = (LayoutConfig) getLayoutConfig();

            params.width = mWidth;
            params.height = mHeight;
            params.alignment = LayoutAlignment.START | LayoutAlignment.BOTTOM;
            if (mIsRtl) {
                params.setMarginRight(mMarginStart);
            } else {
                params.setMarginLeft(mMarginStart);
            }
            params.setMarginBottom(mMarginBottom);

            setLayoutConfig(params);
        } else if (parent instanceof DependentLayout) {
            DependentLayout.LayoutConfig params = (DependentLayout.LayoutConfig) getLayoutConfig();

            params.width = mWidth;
            params.height = mHeight;
            params.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
            params.addRule(DependentLayout.LayoutConfig.ALIGN_PARENT_START);
            if (mIsRtl) {
                params.setMarginRight(mMarginStart);
            } else {
                params.setMarginLeft(mMarginStart);
            }
            params.setMarginBottom(mMarginBottom);

            setLayoutConfig(params);
        }

        if (mInAnimation != null && mState != STATE_SHOWN) {
            mInAnimation.cancel();
            mInAnimation.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    setState(STATE_SHOWING);
                    setVisibility(Component.VISIBLE);
                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    setState(STATE_SHOWN);
                    startTimer();
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            if (mOutAnimation != null) {
                mOutAnimation.cancel();
            }
            mInAnimation.setTarget(this);
            mInAnimation.start();
        } else {
            setVisibility(Component.VISIBLE);
            setState(STATE_SHOWN);
            startTimer();
        }
    }

    private void startTimer() {
        removeCallbacks(mDismissRunnable);
        if (mDuration > 0) {
            postDelayed(mDismissRunnable, mDuration);
        }
    }

    /**
     * Dismiss this SnackBar. It must be in {@link #STATE_SHOWN} to be dismissed.
     */
    public void dismiss() {
        if (mState != STATE_SHOWN) {
            return;
        }

        removeCallbacks(mDismissRunnable);

        if (mOutAnimation != null) {
            mOutAnimation.cancel();
            mOutAnimation.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                    setState(STATE_DISMISSING);
                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (mRemoveOnDismiss && getComponentParent() != null
                            && getComponentParent() instanceof ComponentContainer) {
                        getComponentParent().removeComponent(SnackBar.this);
                    }

                    setState(STATE_DISMISSED);
                    setVisibility(Component.HIDE);
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            if (mInAnimation != null) {
                mInAnimation.cancel();
            }
            mOutAnimation.setTarget(this);
            mOutAnimation.start();
        } else {
            if (mRemoveOnDismiss && getComponentParent() != null
                    && getComponentParent() instanceof ComponentContainer) {
                getComponentParent().removeComponent(this);
            }

            setState(STATE_DISMISSED);
            setVisibility(Component.HIDE);
        }

    }

    private void postDelayed(Runnable runnable, long delay) {
        handler.postTask(runnable, delay);
    }

    private void removeCallbacks(Runnable runnable) {
        handler.removeTask(runnable);
    }

    /**
     * Get the current state of this SnackBar.
     *
     * @return The current state of this SnackBar. Can be {@link #STATE_DISMISSED}, {@link #STATE_DISMISSING},
     * {@link #STATE_SHOWING} or {@link #STATE_SHOWN}.
     */
    public int getState() {
        return mState;
    }

    private void setState(int state) {
        if (mState != state) {
            int oldState = mState;
            mState = state;
            if (mStateChangeListener != null) {
                mStateChangeListener.onStateChange(this, oldState, mState);
            }
        }
    }
}
