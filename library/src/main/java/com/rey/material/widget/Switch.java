package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.RadialShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class Switch extends Component implements ThemeManager.OnThemeChangedListener, Component.TouchEventListener, Component.DrawTask {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Switch");

    private RippleManager mRippleManager;
    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    private boolean mRunning = false;

    private Paint mPaint;
    private RectFloat mDrawRect;
    private RectFloat mTempRect;
    private Path mTrackPath;

    private int mTrackSize = -1;
    private Color mTrackColors;
    private Paint.StrokeCap mTrackCap = Paint.StrokeCap.ROUND_CAP;
    private int mThumbRadius = -1;
    private Color mThumbColors;
    private float mThumbPosition;
    private int mMaxAnimDuration = -1;
    private Interpolator mInterpolator;
    private int mGravity = LayoutAlignment.VERTICAL_CENTER;

    private boolean mChecked = false;
    private float mMemoX;

    private float mStartX;
    private float mFlingVelocity;

    private long mStartTime;
    private int mAnimDuration;
    private float mStartPosition;

    private int[] mTempStates = new int[2];

    private int mShadowSize = -1;
    private int mShadowOffset = -1;
    private Path mShadowPath;
    private Paint mShadowPaint;

    private static final int COLOR_SHADOW_START = 0x4C000000;
    private static final int COLOR_SHADOW_END = 0x00000000;

    private boolean mIsRtl = false;

    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    /**
     * Interface definition for a callback to be invoked when the checked state is changed.
     */
    public interface OnCheckedChangeListener {
        /**
         * Called when the checked state is changed.
         *
         * @param view    The Switch view.
         * @param checked The checked state.
         */
        void onCheckedChanged(Switch view, boolean checked);
    }

    private OnCheckedChangeListener mOnCheckedChangeListener;

    public Switch(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public Switch(Context context, AttrSet attrs) {
        super(context, attrs);

        init(context, attrs, 0, 0);
    }

    public Switch(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        mPaint = new Paint();

        mDrawRect = new RectFloat();
        mTempRect = new RectFloat();
        mTrackPath = new Path();

        mFlingVelocity = 150.0F;

        applyStyle(context, attrs, defStyleAttr, defStyleRes);

        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
        setTouchEventListener(this::onTouchEvent);
        setLayoutRefreshedListener(refreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        addDrawTask(this::onDraw);
        setBindStateChangedListener(bindStateChangedListener);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("sw_trackSize"))
                mTrackSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sw_trackColor")) {
                //mTrackColors = a.getColorStateList(attr);
            } else if (name.equals("sw_trackCap")) {
                int cap = Util.getIntegerValue(attrs, name, 0);
                if (cap == 0)
                    mTrackCap = Paint.StrokeCap.BUTT_CAP;
                else if (cap == 1)
                    mTrackCap = Paint.StrokeCap.ROUND_CAP;
                else
                    mTrackCap = Paint.StrokeCap.SQUARE_CAP;
            } else if (name.equals("sw_thumbColor")) {
                //mThumbColors = a.getColorStateList(attr);
            } else if (name.equals("sw_thumbRadius"))
                mThumbRadius = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sw_thumbElevation")) {
                mShadowSize = Util.getDimensionValue(attrs, name, 0);
                mShadowOffset = mShadowSize / 2;
            } else if (name.equals("sw_animDuration"))
                mMaxAnimDuration = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("gravity"))
                mGravity = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("checked"))
                setCheckedImmediately(Util.getBoolValue(attrs, name, mChecked));
        }

        if (mTrackSize < 0)
            mTrackSize = AttrHelper.vp2px(2, context);

        if (mThumbRadius < 0)
            mThumbRadius = AttrHelper.vp2px(8, context);

        if (mShadowSize < 0) {
            mShadowSize = AttrHelper.vp2px(2, context);
            mShadowOffset = mShadowSize / 2;
        }

        if (mMaxAnimDuration < 0)
            mMaxAnimDuration = 400;

        if (mInterpolator == null)
            mInterpolator = new DecelerateInterpolator();

        if (mTrackColors == null) {
            int[][] states = new int[][]{
                    new int[]{-ComponentState.COMPONENT_STATE_CHECKED},
                    new int[]{ComponentState.COMPONENT_STATE_CHECKED},
            };
            if (mChecked) {
                mTrackColors = new Color(0x9003A9F4);
            } else {
                mTrackColors = new Color(0x9078909C);
            }
        }

        if (mThumbColors == null) {
            int[][] states = new int[][]{
                    new int[]{-ComponentState.COMPONENT_STATE_CHECKED},
                    new int[]{ComponentState.COMPONENT_STATE_CHECKED},
            };
            if (mChecked) {
                mThumbColors = new Color(0xFF03A9F4);
            } else {
                mThumbColors = new Color(0xFF78909C);
            }
        }

        mPaint.setStrokeCap(mTrackCap);
        buildShadow();
        invalidate();
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener bindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(Switch.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            RippleManager.cancelRipple(Switch.this);
            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(Switch.this::onThemeChanged);
        }
    };

    @Override
    public void setBackground(Element element) {
        Element background = getBackgroundElement();
        if (background instanceof RippleDrawable && !(element instanceof RippleDrawable))
            ((RippleDrawable) background).setBackgroundDrawable(element);
        else
            super.setBackground(element);
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null)
                    mRippleManager = new RippleManager();
            }
        }

        return mRippleManager;
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        RippleManager rippleManager = getRippleManager();
        if (l == rippleManager)
            super.setClickedListener(l);
        else {
            rippleManager.setOnClickListener(l);
            setClickedListener(rippleManager);
        }
    }

    /**
     * Set a listener will be called when the checked state is changed.
     *
     * @param listener The {@link OnCheckedChangeListener} will be called.
     */
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    public void setChecked(boolean checked) {
        if (mChecked != checked) {
            mChecked = checked;
            if (mOnCheckedChangeListener != null)
                mOnCheckedChangeListener.onCheckedChanged(this, mChecked);
        }

        float desPos = mChecked ? 1f : 0f;

        if (Math.abs(mThumbPosition - desPos) > .0000001)
            startAnimation();
    }

    /**
     * Change the checked state of this Switch immediately without showing animation.
     *
     * @param checked The checked state.
     */
    public void setCheckedImmediately(boolean checked) {
        if (mChecked != checked) {
            mChecked = checked;
            if (mOnCheckedChangeListener != null)
                mOnCheckedChangeListener.onCheckedChanged(this, mChecked);
        }
        mThumbPosition = mChecked ? 1f : 0f;
        invalidate();
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void toggle() {
        if (isEnabled())
            setChecked(!mChecked);
    }

    @Override
    protected void onRtlChanged(LayoutDirection layoutDirection) {
        super.onRtlChanged(layoutDirection);
        boolean rtl = layoutDirection == LayoutDirection.RTL;
        if (mIsRtl != rtl) {
            mIsRtl = rtl;
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        getRippleManager().onTouchEvent(this, event);

        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        if (mIsRtl)
            x = 2 * mDrawRect.getHorizontalCenter() - x;

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mMemoX = x;
                mStartX = mMemoX;
                mStartTime = Time.getRealActiveTime();
                break;
            case TouchEvent.POINT_MOVE:
                float offset = (x - mMemoX) / (mDrawRect.getWidth() - mThumbRadius * 2);
                mThumbPosition = Math.min(1f, Math.max(0f, mThumbPosition + offset));
                mMemoX = x;
                invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_UP:

                float velocity = (x - mStartX) / (Time.getRealActiveTime() - mStartTime) * 1000;
                if (Math.abs(velocity) >= mFlingVelocity)
                    setChecked(velocity > 0);
                else if ((!mChecked && mThumbPosition < 0.1f) || (mChecked && mThumbPosition > 0.9f)) {
                    toggle();
                } else
                    setChecked(mThumbPosition > 0.5f);
                break;
            case TouchEvent.CANCEL:
                setChecked(mThumbPosition > 0.5f);
                break;
        }

        return true;
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {

            int widthSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);

            int heightSize = EstimateSpec.getSize(heightMeasureSpec);
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);

            switch (widthMode) {
                case EstimateSpec.UNCONSTRAINT:
                    widthSize = getMinWidth();
                    break;
                case EstimateSpec.NOT_EXCEED:
                    widthSize = Math.min(widthSize, getMinWidth());
                    break;
            }

            switch (heightMode) {
                case EstimateSpec.UNCONSTRAINT:
                    heightSize = getMinHeight();
                    break;
                case EstimateSpec.NOT_EXCEED:
                    heightSize = Math.min(heightSize, getMinHeight());
                    break;
            }

            setEstimatedSize(widthSize, heightSize);
            return false;
        }
    };

    @Override
    public int getMinWidth() {
        return mThumbRadius * 4 + Math.max(mShadowSize, getPaddingLeft()) + Math.max(mShadowSize, getPaddingRight());
    }

    @Override
    public int getMinHeight() {
        return mThumbRadius * 2 + Math.max(mShadowSize - mShadowOffset, getPaddingTop()) + Math.max(mShadowSize + mShadowOffset, getPaddingBottom());
    }

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            measureComponent(component);
        }
    };

    private void measureComponent(Component component) {
        mDrawRect.left = Math.max(mShadowSize, getPaddingLeft());
        mDrawRect.right = component.getWidth() - Math.max(mShadowSize, getPaddingRight());

        int height = mThumbRadius * 2;
        int align = mGravity;//& Gravity.VERTICAL_GRAVITY_MASK;

        switch (align) {
            case LayoutAlignment.TOP:
                mDrawRect.top = Math.max(mShadowSize - mShadowOffset, getPaddingTop());
                mDrawRect.bottom = mDrawRect.top + height;
                break;
            case LayoutAlignment.BOTTOM:
                mDrawRect.bottom = component.getHeight() - Math.max(mShadowSize + mShadowOffset, getPaddingBottom());
                mDrawRect.top = mDrawRect.bottom - height;
                break;
            default:
                mDrawRect.top = (component.getHeight() - height) / 2f;
                mDrawRect.bottom = mDrawRect.top + height;
                break;
        }
    }

    private int getTrackColor(boolean checked) {
        if (checked) {
            return 0xFF03A9F4;
        } else {
            return 0xFF78909C;
        }
    }

    private int getThumbColor(boolean checked) {
        if (checked) {
            return 0x9003A9F4;
        } else {
            return 0x9078909C;
        }
    }

    private void buildShadow() {
        if (mShadowSize <= 0)
            return;

        if (mShadowPaint == null) {
            mShadowPaint = new Paint();
            mShadowPaint.setStyle(Paint.Style.FILL_STYLE);
            mShadowPaint.setDither(true);
        }
        float startRatio = (float) mThumbRadius / (mThumbRadius + mShadowSize + mShadowOffset);
        mShadowPaint.setShader(new RadialShader(new Point(0, 0), mThumbRadius + mShadowSize,
                new float[]{0f, startRatio, 1f},
                new Color[]{new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_START), new Color(COLOR_SHADOW_END)},
                Shader.TileMode.CLAMP_TILEMODE), Paint.ShaderType.RADIAL_SHADER);

        if (mShadowPath == null) {
            mShadowPath = new Path();
            mShadowPath.setFillType(Path.FillType.EVEN_ODD);
        } else
            mShadowPath.reset();
        float radius = mThumbRadius + mShadowSize;
        mTempRect.modify(-radius, -radius, radius, radius);
        mShadowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);
        radius = mThumbRadius - 1;
        mTempRect.modify(-radius, -radius - mShadowOffset, radius, radius - mShadowOffset);
        mShadowPath.addOval(mTempRect, Path.Direction.CLOCK_WISE);
    }

    private void getTrackPath(float x, float y, float radius) {
        float halfStroke = mTrackSize / 2f;

        mTrackPath.reset();

        if (mTrackCap != Paint.StrokeCap.ROUND_CAP) {
            mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
            float angle = (float) (Math.asin(halfStroke / (radius - 1f)) / Math.PI * 180);

            if (x - radius > mDrawRect.left) {
                mTrackPath.moveTo(mDrawRect.left, y - halfStroke);
                mTrackPath.arcTo(mTempRect, 180 + angle, -angle * 2);
                mTrackPath.lineTo(mDrawRect.left, y + halfStroke);
                mTrackPath.close();
            }

            if (x + radius < mDrawRect.right) {
                mTrackPath.moveTo(mDrawRect.right, y - halfStroke);
                mTrackPath.arcTo(mTempRect, -angle, angle * 2);
                mTrackPath.lineTo(mDrawRect.right, y + halfStroke);
                mTrackPath.close();
            }
        } else {
            float angle = (float) (Math.asin(halfStroke / (radius - 1f)) / Math.PI * 180);

            if (x - radius > mDrawRect.left) {
                float angle2 = (float) (Math.acos(Math.max(0f, (mDrawRect.left + halfStroke - x + radius) / halfStroke)) / Math.PI * 180);

                mTempRect.modify(mDrawRect.left, y - halfStroke, mDrawRect.left + mTrackSize, y + halfStroke);
                mTrackPath.arcTo(mTempRect, 180 - angle2, angle2 * 2);

                mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
                mTrackPath.arcTo(mTempRect, 180 + angle, -angle * 2);
                mTrackPath.close();
            }

            if (x + radius < mDrawRect.right) {
                float angle2 = (float) Math.acos(Math.max(0f, (x + radius - mDrawRect.right + halfStroke) / halfStroke));
                mTrackPath.moveTo((float) (mDrawRect.right - halfStroke + Math.cos(angle2) * halfStroke), (float) (y + Math.sin(angle2) * halfStroke));

                angle2 = (float) (angle2 / Math.PI * 180);
                mTempRect.modify(mDrawRect.right - mTrackSize, y - halfStroke, mDrawRect.right, y + halfStroke);
                mTrackPath.arcTo(mTempRect, angle2, -angle2 * 2);

                mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
                mTrackPath.arcTo(mTempRect, -angle, angle * 2);
                mTrackPath.close();
            }
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        measureComponent(component);
        float x = (mDrawRect.getWidth() - mThumbRadius * 2) * mThumbPosition + mDrawRect.left + mThumbRadius;
        if (mIsRtl)
            x = 2 * mDrawRect.getHorizontalCenter() - x;
        float y = mDrawRect.getVerticalCenter();

        getTrackPath(x, y, mThumbRadius);
        mPaint.setColor(new Color(getTrackColor(mChecked)));
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawPath(mTrackPath, mPaint);

        mPaint.setColor(new Color(getThumbColor(mChecked)));
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(x, y, mThumbRadius, mPaint);

        if (mShadowSize > 0) {
            int saveCount = canvas.save();
            canvas.translate(x, y + mShadowOffset);
            canvas.drawPath(mShadowPath, mShadowPaint);
            canvas.restoreToCount(saveCount);
        }

    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mStartPosition = mThumbPosition;
        mAnimDuration = (int) (mMaxAnimDuration * (mChecked ? (1f - mStartPosition) : mStartPosition));
    }

    private void startAnimation() {
        if (getHandler() != null) {
            resetAnimation();
            mRunning = true;
            getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
        } else
            mThumbPosition = mChecked ? 1f : 0f;
        invalidate();
    }

    private void stopAnimation() {
        mRunning = false;
        mThumbPosition = mChecked ? 1f : 0f;
        if (getHandler() != null)
            getHandler().removeTask(mUpdater);
        invalidate();
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        float progress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);
        float value = mInterpolator.getInterpolation(progress);

        mThumbPosition = mChecked ? (mStartPosition * (1 - value) + value) : (mStartPosition * (1 - value));

        if (progress == 1f)
            stopAnimation();

        if (mRunning) {
            if (getHandler() != null)
                getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
            else
                stopAnimation();
        }

        invalidate();
    }

    private EventHandler getHandler() {
        return mHandler;
    }

}
