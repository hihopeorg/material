package com.rey.material.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ViewUtil;

public class TextView extends Text implements ThemeManager.OnThemeChangedListener, Component.TouchEventListener {

    private RippleManager mRippleManager;
    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;


    public interface OnSelectionChangedListener {
        void onSelectionChanged(Component v, int selStart, int selEnd);
    }

    private OnSelectionChangedListener mOnSelectionChangedListener;

    public TextView(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public TextView(Context context, AttrSet attrs) {
        super(context, attrs);

        init(context, attrs, 0, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        applyStyle(context, attrs, defStyleAttr, defStyleRes);
        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
        setTouchEventListener(this::onTouchEvent);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(TextView.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            RippleManager.cancelRipple(TextView.this);
            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(TextView.this::onThemeChanged);
        }
    };

    @Override
    public void setBackground(Element element) {
        Element background = getBackgroundElement();
        if (background instanceof RippleDrawable && !(element instanceof RippleDrawable))
            ((RippleDrawable) background).setBackgroundDrawable(element);
        else
            super.setBackground(element);
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null)
                    mRippleManager = new RippleManager();
            }
        }

        return mRippleManager;
    }


    @Override
    public void setClickedListener(ClickedListener l) {
        RippleManager rippleManager = getRippleManager();
        if (l == rippleManager)
            super.setClickedListener(l);
        else {
            rippleManager.setOnClickListener(l);
            setClickedListener(rippleManager);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return getRippleManager().onTouchEvent(this, touchEvent);
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener listener) {
        mOnSelectionChangedListener = listener;
    }
}
