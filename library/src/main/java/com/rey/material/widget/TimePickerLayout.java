/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.ResourceTable;
import com.rey.material.util.Util;

import java.text.DateFormat;
import java.util.Calendar;

public class TimePickerLayout extends ComponentContainer implements Component.ClickedListener, TimePicker.OnTimeChangedListener, Component.DrawTask, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "TimePickerLayout");
    private int mHeaderHeight;
    private int mTextTimeColor = 0xFF000000;
    private int mTextTimeSize;
    private boolean mIsLeadingZero = false;

    private boolean mIsAm = true;
    private int mCheckBoxSize;

    private int mHeaderRealWidth;
    private int mHeaderRealHeight;

    private Text mAmView;
    private Text mPmView;
    private TimePicker mTimePicker;

    private Paint mPaint;
    private Path mHeaderBackground;
    private RectFloat mRect;

    private static final String TIME_DIVIDER = ":";
    private static final String BASE_TEXT = "0";
    private static final String FORMAT = "%02d";
    private static final String FORMAT_NO_LEADING_ZERO = "%d";

    private boolean mLocationDirty = true;
    private float mBaseY;
    private float mHourX;
    private float mDividerX;
    private float mMinuteX;
    private float mMiddayX;
    private float mHourWidth;
    private float mMinuteWidth;
    private float mBaseHeight;

    private String mHour;
    private String mMinute;
    private String mMidday;

    protected int mContentPadding;
    protected int mActionPadding;
    private float mCornerRadius;
    ShapeElement element;

    public interface OnTimeChangedListener {

        /**
         * Called when the selected time is changed.
         *
         * @param oldHour   The hour value of old time.
         * @param oldMinute The minute value of old time.
         * @param newHour   The hour value of new time.
         * @param newMinute The minute value of new time.
         */
        void onTimeChanged(int oldHour, int oldMinute, int newHour, int newMinute);

    }

    private OnTimeChangedListener mOnTimeChangedListener;

    public TimePickerLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);

        mContentPadding = AttrHelper.vp2px(24, context);
        mActionPadding = AttrHelper.vp2px(8, context);
        mPaint = new Paint();
        mPaint.setTextAlign(TextAlignment.LEFT);
        mHeaderBackground = new Path();
        mRect = new RectFloat();

        mAmView = new Text(context);
        mPmView = new Text(context);
        mTimePicker = new TimePicker(context, attrSet);

        mTimePicker.setPadding(mContentPadding, mContentPadding, mContentPadding, mContentPadding);
        mTimePicker.setOnTimeChangedListener(this);
        mAmView.setTextAlignment(TextAlignment.CENTER);
        mPmView.setTextAlignment(TextAlignment.CENTER);
        mAmView.setClickedListener(this);
        mPmView.setClickedListener(this);

        addComponent(mTimePicker);
        addComponent(mAmView);
        addComponent(mPmView);

        mHeaderHeight = AttrHelper.vp2px(120, context);
        mCheckBoxSize = AttrHelper.vp2px(70, context);
        mTextTimeSize = AttrHelper.fp2px(24, context);

        applyStyle(context, attrSet);

        setTouchEventListener(this::onTouchEvent);
        setLayoutRefreshedListener(refreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        setArrangeListener(arrangeListener);
        addDrawTask(this::onDraw);
    }

    public void applyStyle(Context context, AttrSet attrs) {
        mTimePicker.applyStyle(context, attrs, 0, 0);
        String am = null;
        String pm = null;

        element = new ShapeElement(context, ResourceTable.Graphic_text_shape);

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("tp_headerHeight"))
                mHeaderHeight = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_textTimeColor"))
                mTextTimeColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("tp_textTimeSize"))
                mTextTimeSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_leadingZero"))
                mIsLeadingZero = Util.getBoolValue(attrs, name, false);
            else if (name.equals("tp_checkBox"))
                mCheckBoxSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_am"))
                am = Util.getStringValue(attrs, name, "");
            else if (name.equals("tp_pm"))
                pm = Util.getStringValue(attrs, name, "");
        }

        if (am == null)
            am = "上午";

        if (pm == null)
            pm = "下午";

        mAmView.setTextSize(22, Text.TextSizeType.FP);
        mAmView.setTextColor(Color.WHITE);
        mAmView.setBackground(element);
        mAmView.setText(am);

        mPmView.setTextSize(22, Text.TextSizeType.FP);
        mPmView.setTextColor(Color.BLACK);
        mPmView.setBackground(null);
        mPmView.setText(pm);

        mHour = String.format(mIsLeadingZero ? FORMAT : FORMAT_NO_LEADING_ZERO, !mTimePicker.is24Hour() && mTimePicker.getHour() == 0 ? 12 : mTimePicker.getHour());
        mMinute = String.format(FORMAT, mTimePicker.getMinute());

        if (!mTimePicker.is24Hour())
            mMidday = mIsAm ? mAmView.getText() : mPmView.getText();

        mLocationDirty = true;
        invalidate();
    }

    public void setHour(int hour) {
        if (!mTimePicker.is24Hour()) {
            if (hour > 11 && hour < 24)
                setAm(false, false);
            else
                setAm(true, false);
        }
        mTimePicker.setHour(hour);
    }

    public int getHour() {
        return mTimePicker.is24Hour() || mIsAm ? mTimePicker.getHour() : mTimePicker.getHour() + 12;
    }

    public void setMinute(int minute) {
        mTimePicker.setMinute(minute);
    }

    public int getMinute() {
        return mTimePicker.getMinute();
    }

    private void setAm(boolean am, boolean animation) {
        if (mTimePicker.is24Hour())
            return;

        if (mIsAm != am) {
            int oldHour = getHour();

            mIsAm = am;
            if (mIsAm) {
                mAmView.setBackground(element);
                mAmView.setTextColor(Color.WHITE);
                mPmView.setBackground(null);
                mPmView.setTextColor(Color.BLACK);
            } else {
                mPmView.setBackground(element);
                mPmView.setTextColor(Color.WHITE);
                mAmView.setBackground(null);
                mAmView.setTextColor(Color.BLACK);
            }
            mMidday = mIsAm ? mAmView.getText() : mPmView.getText();
            invalidate();

            if (mOnTimeChangedListener != null)
                mOnTimeChangedListener.onTimeChanged(oldHour, getMinute(), getHour(), getMinute());
        }
    }

    public String getFormattedTime(DateFormat formatter) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, getHour());
        cal.set(Calendar.MINUTE, getMinute());
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return formatter.format(cal.getTime());
    }

    public void setOnTimeChangedListener(OnTimeChangedListener listener) {
        mOnTimeChangedListener = listener;
    }

    @Override
    public void onClick(Component component) {
        setAm(component == mAmView, true);
    }

    @Override
    public void onModeChanged(int mode) {
        invalidate();
    }

    @Override
    public void onHourChanged(int oldValue, int newValue) {
        int oldHour = mTimePicker.is24Hour() || mIsAm ? oldValue : oldValue + 12;

        mHour = String.format(mIsLeadingZero ? FORMAT : FORMAT_NO_LEADING_ZERO, !mTimePicker.is24Hour() && newValue == 0 ? 12 : newValue);
        mLocationDirty = true;
        invalidate();

        if (mOnTimeChangedListener != null)
            mOnTimeChangedListener.onTimeChanged(oldHour, getMinute(), getHour(), getMinute());
    }

    @Override
    public void onMinuteChanged(int oldValue, int newValue) {
        mMinute = String.format(FORMAT, newValue);
        mLocationDirty = true;
        invalidate();

        if (mOnTimeChangedListener != null)
            mOnTimeChangedListener.onTimeChanged(getHour(), oldValue, getHour(), newValue);
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            int widthSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);
            int heightSize = EstimateSpec.getSize(heightMeasureSpec);
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);

            boolean isPortrait = true;

            int checkboxSize = mTimePicker.is24Hour() ? 0 : mCheckBoxSize;

            if (isPortrait) {
                if (heightMode == EstimateSpec.NOT_EXCEED)
                    heightSize = Math.min(heightSize, checkboxSize + widthSize + mHeaderHeight);

                if (checkboxSize > 0) {
                    int spec = EstimateSpec.getSizeWithMode(mCheckBoxSize, EstimateSpec.PRECISE);
                    mAmView.setVisibility(Component.VISIBLE);
                    mPmView.setVisibility(Component.VISIBLE);
                    mAmView.estimateSize(spec, spec);
                    mPmView.estimateSize(spec, spec);
                } else {
                    mAmView.setVisibility(Component.HIDE);
                    mPmView.setVisibility(Component.HIDE);
                }

                int spec = EstimateSpec.getSizeWithMode(widthSize, EstimateSpec.PRECISE);
                mTimePicker.estimateSize(spec, spec);

                setEstimatedSize(widthSize, heightSize);
            } else {
                int halfWidth = widthSize / 2;

                if (heightMode == EstimateSpec.NOT_EXCEED)
                    heightSize = Math.min(heightSize, Math.max(checkboxSize > 0 ? checkboxSize + mHeaderHeight + mContentPadding : mHeaderHeight, halfWidth));

                if (checkboxSize > 0) {
                    int spec = EstimateSpec.getSizeWithMode(checkboxSize, EstimateSpec.PRECISE);
                    mAmView.setVisibility(Component.VISIBLE);
                    mPmView.setVisibility(Component.VISIBLE);
                    mAmView.estimateSize(spec, spec);
                    mPmView.estimateSize(spec, spec);
                } else {
                    mAmView.setVisibility(Component.HIDE);
                    mPmView.setVisibility(Component.HIDE);
                }

                int spec = EstimateSpec.getSizeWithMode(Math.min(halfWidth, heightSize), EstimateSpec.PRECISE);
                mTimePicker.estimateSize(spec, spec);

                setEstimatedSize(widthSize, heightSize);
            }
            return false;
        }
    };

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            measureComponent(component);
        }
    };

    private void measureComponent(Component component) {
        boolean isPortrait = true;
        int w = component.getWidth();
        int h = component.getHeight();

        mLocationDirty = true;
        int checkboxSize = mTimePicker.is24Hour() ? 0 : mCheckBoxSize;

        if (isPortrait) {
            mHeaderRealWidth = component.getWidth();
            mHeaderRealHeight = h - checkboxSize - w;
            mHeaderBackground.reset();
            if (mCornerRadius == 0) {
                mHeaderBackground.addRect(0, 0, mHeaderRealWidth, mHeaderRealHeight, Path.Direction.CLOCK_WISE);
            } else {
                mHeaderBackground.moveTo(0, mHeaderRealHeight);
                mHeaderBackground.lineTo(0, mCornerRadius);
                mRect.modify(0, 0, mCornerRadius * 2, mCornerRadius * 2);
                mHeaderBackground.arcTo(mRect, 180f, 90f, false);
                mHeaderBackground.lineTo(mHeaderRealWidth - mCornerRadius, 0);
                mRect.modify(mHeaderRealWidth - mCornerRadius * 2, 0, mHeaderRealWidth, mCornerRadius * 2);
                mHeaderBackground.arcTo(mRect, 270f, 90f, false);
                mHeaderBackground.lineTo(mHeaderRealWidth, mHeaderRealHeight);
                mHeaderBackground.close();
            }
        } else {
            mHeaderRealWidth = w / 2;
            mHeaderRealHeight = checkboxSize > 0 ? h - checkboxSize - mContentPadding : h;
            mHeaderBackground.reset();
            if (mCornerRadius == 0)
                mHeaderBackground.addRect(0, 0, mHeaderRealWidth, mHeaderRealHeight, Path.Direction.CLOCK_WISE);
            else {
                mHeaderBackground.moveTo(0, mHeaderRealHeight);
                mHeaderBackground.lineTo(0, mCornerRadius);
                mRect.modify(0, 0, mCornerRadius * 2, mCornerRadius * 2);
                mHeaderBackground.arcTo(mRect, 180f, 90f, false);
                mHeaderBackground.lineTo(mHeaderRealWidth, 0);
                mHeaderBackground.lineTo(mHeaderRealWidth, mHeaderRealHeight);
                mHeaderBackground.close();
            }
        }
    }

    ArrangeListener arrangeListener = new ArrangeListener() {
        @Override
        public boolean onArrange(int left, int top, int width, int height) {
            int childLeft = 0;
            int childTop = 0;
            int childRight = width;
            int childBottom = height;

            boolean isPortrait = true;
            int checkboxSize = mTimePicker.is24Hour() ? 0 : mCheckBoxSize;

            if (isPortrait) {
                int paddingHorizontal = mContentPadding + mActionPadding;
                int paddingVertical = mContentPadding - mActionPadding;

                if (checkboxSize > 0) {
                    mAmView.arrange(childLeft + paddingHorizontal, childBottom - paddingVertical - checkboxSize, checkboxSize, checkboxSize);
                    mPmView.arrange(childRight - paddingHorizontal - checkboxSize, childBottom - paddingVertical - checkboxSize, checkboxSize, checkboxSize);
                }

                childTop += height - mCheckBoxSize - width;
                childBottom -= checkboxSize;
                mTimePicker.arrange(childLeft, childTop, childRight - childLeft, childBottom - childTop);
            } else {
                int paddingHorizontal = (childRight / 2 - mTimePicker.getEstimatedWidth()) / 2;
                int paddingVertical = (childBottom - mTimePicker.getEstimatedHeight()) / 2;
                mTimePicker.arrange(childRight - paddingHorizontal - mTimePicker.getEstimatedWidth(), childTop + paddingVertical, childRight - paddingHorizontal, childTop + paddingVertical + mTimePicker.getEstimatedHeight());

                if (checkboxSize > 0) {
                    childRight = childRight / 2;

                    paddingHorizontal = mContentPadding + mActionPadding;
                    paddingVertical = mContentPadding - mActionPadding;
                    mAmView.arrange(childLeft + paddingHorizontal, childBottom - paddingVertical - checkboxSize, checkboxSize, checkboxSize);
                    mPmView.arrange(childRight - paddingHorizontal - checkboxSize, childBottom - paddingVertical - checkboxSize, checkboxSize, checkboxSize);
                }
            }
            return false;
        }
    };

    private void measureTimeText() {
        if (!mLocationDirty)
            return;

        mPaint.setTextSize(mTextTimeSize);

        Rect bounds = mPaint.getTextBounds(BASE_TEXT);
        mBaseHeight = bounds.getHeight();

        mBaseY = (mHeaderRealHeight + mBaseHeight) / 2f;

        float dividerWidth = mPaint.measureText(TIME_DIVIDER);
        mHourWidth = mPaint.measureText(mHour);
        mMinuteWidth = mPaint.measureText(mMinute);

        mDividerX = (mHeaderRealWidth - dividerWidth) / 2f;
        mHourX = mDividerX - mHourWidth;
        mMinuteX = mDividerX + dividerWidth;
        mMiddayX = mMinuteX + mMinuteWidth;

        mLocationDirty = false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        measureComponent(component);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(new Color(mTimePicker.getSelectionColor()));
        canvas.drawPath(mHeaderBackground, mPaint);

        measureTimeText();

        mPaint.setTextSize(mTextTimeSize);
        mPaint.setColor(new Color(mTimePicker.getMode() == TimePicker.MODE_HOUR ? mTimePicker.getTextHighlightColor() : mTextTimeColor));
        canvas.drawText(mPaint, mHour, mHourX, mBaseY);

        mPaint.setColor(new Color(mTextTimeColor));
        canvas.drawText(mPaint, TIME_DIVIDER, mDividerX, mBaseY);

        mPaint.setColor(new Color(mTimePicker.getMode() == TimePicker.MODE_MINUTE ? mTimePicker.getTextHighlightColor() : mTextTimeColor));
        canvas.drawText(mPaint, mMinute, mMinuteX, mBaseY);

        if (!mTimePicker.is24Hour()) {
            mPaint.setTextSize(mTimePicker.getTextSize());
            mPaint.setColor(new Color(mTextTimeColor));
            canvas.drawText(mPaint, mMidday, mMiddayX, mBaseY);
        }
    }

    private boolean isTouched(float left, float top, float right, float bottom, float x, float y) {
        return x >= left && x <= right && y >= top && y <= bottom;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (isTouched(mHourX, mBaseY - mBaseHeight, mHourX + mHourWidth, mBaseY, x, y))
                    return mTimePicker.getMode() == TimePicker.MODE_MINUTE;

                if (isTouched(mMinuteX, mBaseY - mBaseHeight, mMinuteX + mMinuteWidth, mBaseY, x, y))
                    return mTimePicker.getMode() == TimePicker.MODE_HOUR;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (isTouched(mHourX, mBaseY - mBaseHeight, mHourX + mHourWidth, mBaseY, x, y))
                    mTimePicker.setMode(TimePicker.MODE_HOUR, true);

                if (isTouched(mMinuteX, mBaseY - mBaseHeight, mMinuteX + mMinuteWidth, mBaseY, x, y))
                    mTimePicker.setMode(TimePicker.MODE_MINUTE, true);
                break;
        }
        return false;
    }

}
