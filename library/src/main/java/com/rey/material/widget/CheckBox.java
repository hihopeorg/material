package com.rey.material.widget;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.drawable.Animatable;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class CheckBox extends ohos.agp.components.Button implements Animatable, Component.DrawTask, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "CheckBoxDrawable");
    private boolean mRunning = false;

    private Paint mPaint;

    private long mStartTime;
    private float mAnimProgress;
    private int mAnimDuration;
    private int mStrokeSize;
    private int mWidth;
    private int mHeight;
    private int mCornerRadius;
    private int mBoxSize;
    private int mTickColor = 0xFFFFFFFF;
    private int mStrokeColor;
    private RectFloat mBoxRect;
    private Path mTickPath;
    private float mTickPathProgress = -1f;
    private boolean mChecked = false;

    private boolean mAnimEnable = true;
    private boolean mAutoStart = false;

    private static final float[] TICK_DATA = new float[]{0f, 0.473f, 0.367f, 0.839f, 1f, 0.207f};
    private static final float FILL_TIME = 0.4f;

    private EventHandler mHandler;

    public CheckBox(Context context) {
        super(context);
    }

    public CheckBox(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        mWidth = Util.getDimensionValue(attrs, "cbd_width", AttrHelper.vp2px(32, context));
        mHeight = Util.getDimensionValue(attrs, "cbd_height", AttrHelper.vp2px(32, context));
        mBoxSize = Util.getDimensionValue(attrs, "cbd_boxSize", AttrHelper.vp2px(18, context));
        mCornerRadius = Util.getDimensionValue(attrs, "cbd_cornerRadius", AttrHelper.vp2px(2, context));
        mStrokeSize = Util.getDimensionValue(attrs, "cbd_strokeSize", AttrHelper.vp2px(2, context));
        mTickColor = Util.getColorValue(attrs, "cbd_tickColor", new Color(0xFFFFFFFF)).getValue();
        mAnimDuration = Util.getIntegerValue(attrs, "cbd_animDuration", 400);
        mChecked = Util.getBoolValue(attrs, "cbd_checked", false);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);

        mBoxRect = new RectFloat();
        mTickPath = new Path();

        mHandler = new EventHandler(EventRunner.getMainEventRunner());

        setLayoutRefreshedListener(refreshedListener);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);

        setTextAlignment(TextAlignment.CENTER);

    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int height = component.getHeight();
        mBoxRect.modify(getPaddingLeft() + mStrokeSize, height / 2 - mBoxSize / 2, getPaddingLeft() + mBoxSize, height / 2 + mBoxSize / 2);
        if (mChecked) {
            mStrokeColor = 0xFF00E5FF;
            drawChecked(canvas);
        } else {
            mStrokeColor = 0xFF78909C;
            drawUnchecked(canvas);
        }
    }

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            int height = component.getHeight();
            mBoxRect.modify(getPaddingLeft() + mStrokeSize, height / 2 - mBoxSize / 2, getPaddingLeft() + mBoxSize, height / 2 + mBoxSize / 2);
        }
    };

    private Path getTickPath(Path path, float x, float y, float size, float progress, boolean in) {
        if (mTickPathProgress == progress)
            return path;

        mTickPathProgress = progress;

        float x1 = x + size * TICK_DATA[0];
        float y1 = y + size * TICK_DATA[1];
        float x2 = x + size * TICK_DATA[2];
        float y2 = y + size * TICK_DATA[3];
        float x3 = x + size * TICK_DATA[4];
        float y3 = y + size * TICK_DATA[5];

        float d1 = (float) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        float d2 = (float) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        float midProgress = d1 / (d1 + d2);

        path.reset();

        if (in) {
            path.moveTo(x1, y1);

            if (progress < midProgress) {
                progress = progress / midProgress;
                path.lineTo(x1 * (1 - progress) + x2 * progress, y1 * (1 - progress) + y2 * progress);
            } else {
                progress = (progress - midProgress) / (1f - midProgress);
                path.lineTo(x2, y2);
                path.lineTo(x2 * (1 - progress) + x3 * progress, y2 * (1 - progress) + y3 * progress);
            }
        } else {
            path.moveTo(x3, y3);

            if (progress < midProgress) {
                progress = progress / midProgress;
                path.lineTo(x2, y2);
                path.lineTo(x1 * (1 - progress) + x2 * progress, y1 * (1 - progress) + y2 * progress);
            } else {
                progress = (progress - midProgress) / (1f - midProgress);
                path.lineTo(x2 * (1 - progress) + x3 * progress, y2 * (1 - progress) + y3 * progress);
            }
        }

        return path;
    }

    private void drawChecked(Canvas canvas) {
        float size = mBoxSize - mStrokeSize * 2;
        float x = mBoxRect.left + mStrokeSize;
        float y = mBoxRect.top + mStrokeSize;

        if (isRunning()) {
            if (mAnimProgress < FILL_TIME) {
                float progress = mAnimProgress / FILL_TIME;
                float fillWidth = (mBoxSize - mStrokeSize) / 2f * progress;
                float padding = mStrokeSize / 2f + fillWidth / 2f - 0.5f;

                mPaint.setColor(new Color(mStrokeColor));//new Color(ColorUtil.getMiddleColor(mPrevColor, mCurColor, progress)));
                mPaint.setStrokeWidth(fillWidth);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawRect(mBoxRect.left + padding, mBoxRect.top + padding, mBoxRect.right - padding, mBoxRect.bottom - padding, mPaint);

                mPaint.setStrokeWidth(mStrokeSize);
                canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);
            } else {
                float progress = (mAnimProgress - FILL_TIME) / (1f - FILL_TIME);

                mPaint.setColor(new Color(mStrokeColor));//new Color(mCurColor));
                mPaint.setStrokeWidth(mStrokeSize);
                mPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
                canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);

                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                mPaint.setStrokeJoin(Paint.Join.MITER_JOIN);
                mPaint.setStrokeCap(Paint.StrokeCap.BUTT_CAP);
                mPaint.setColor(new Color(mTickColor));

                canvas.drawPath(getTickPath(mTickPath, x, y, size, progress, true), mPaint);
            }
        } else {
            mPaint.setColor(new Color(mStrokeColor));//new Color(mCurColor));
            mPaint.setStrokeWidth(mStrokeSize);
            mPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
            canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);

            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeJoin(Paint.Join.MITER_JOIN);
            mPaint.setStrokeCap(Paint.StrokeCap.BUTT_CAP);
            mPaint.setColor(new Color(mTickColor));

            canvas.drawPath(getTickPath(mTickPath, x, y, size, 1f, true), mPaint);
        }
    }

    private void drawUnchecked(Canvas canvas) {
        if (isRunning()) {
            if (mAnimProgress < 1f - FILL_TIME) {
                float size = mBoxSize - mStrokeSize * 2;
                float x = mBoxRect.left + mStrokeSize;
                float y = mBoxRect.top + mStrokeSize;
                float progress = mAnimProgress / (1f - FILL_TIME);

                mPaint.setColor(new Color(mStrokeColor));//new Color(mPrevColor));
                mPaint.setStrokeWidth(mStrokeSize);
                mPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
                canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);

                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                mPaint.setStrokeJoin(Paint.Join.MITER_JOIN);
                mPaint.setStrokeCap(Paint.StrokeCap.BUTT_CAP);
                mPaint.setColor(new Color(mTickColor));

                canvas.drawPath(getTickPath(mTickPath, x, y, size, progress, false), mPaint);
            } else {
                float progress = (mAnimProgress + FILL_TIME - 1f) / FILL_TIME;
                float fillWidth = (mBoxSize - mStrokeSize) / 2f * (1f - progress);
                float padding = mStrokeSize / 2f + fillWidth / 2f - 0.5f;

                mPaint.setColor(new Color(mStrokeColor));//new Color(ColorUtil.getMiddleColor(mPrevColor, mCurColor, progress)));
                mPaint.setStrokeWidth(fillWidth);
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                canvas.drawRect(mBoxRect.left + padding, mBoxRect.top + padding, mBoxRect.right - padding, mBoxRect.bottom - padding, mPaint);

                mPaint.setStrokeWidth(mStrokeSize);
                canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);
            }
        } else {
            mPaint.setColor(new Color(mStrokeColor));//new Color(mCurColor));
            mPaint.setStrokeWidth(mStrokeSize);
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            canvas.drawRoundRect(mBoxRect, mCornerRadius, mCornerRadius, mPaint);
        }
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mAnimProgress = 0f;
    }

    @Override
    public void start() {
        resetAnimation();

        scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);
        invalidate();
    }

    @Override
    public void stop() {
        mRunning = false;
        mHandler.removeTask(mUpdater);
        invalidate();
    }

    @Override
    public boolean isRunning() {
        return mRunning;
    }

    private void scheduleSelf(Runnable what, long delay) {
        mRunning = true;
        mHandler.postTask(what, delay);
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (mAnimProgress == 1f)
            mRunning = false;

        if (isRunning())
            scheduleSelf(mUpdater, ViewUtil.FRAME_DURATION);

        invalidate();
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
                mChecked = !mChecked;
                start();
                break;
        }
        return true;
    }
}
