/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by Rey on 12/31/2014.
 */
public class DatePicker extends ListContainer implements Component.DrawTask {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "DatePicker");

    private int mTextSize;
    private int mTextColor;
    private int mTextLabelColor;
    private int mTextHighlightColor;
    private int mTextDisableColor;
    private int mSelectionColor;
    private int mAnimDuration;
    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;

    private Paint mPaint;
    private float mDayBaseWidth;
    private float mDayBaseHeight;
    private float mDayHeight;
    private float mDayWidth;
    private int mDayPadding;
    private float mSelectionRadius;
    private int mMonthRealWidth;
    private int mMonthRealHeight;

    private Calendar mCalendar;
    private int mFirstDayOfWeek;
    private String[] mLabels;
    private static String[] mDayTexts;

    private MonthAdapter mAdapter;

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mDayWidth = (getEstimatedWidth() - mPaddingLeft - mPaddingRight) / 7f;
        mDayHeight = (getEstimatedHeight() - mDayBaseHeight - mDayPadding * 2 - mPaddingTop - mPaddingBottom) / 7f;
        mSelectionRadius = Math.min(mDayWidth, mDayHeight) / 2f;
    }

    /**
     * Interface definition for a callback to be invoked when the selected date is changed.
     */
    public interface OnDateChangedListener {

        /**
         * Called when the selected date is changed.
         *
         * @param oldDay   The day value of old date.
         * @param oldMonth The month value of old date.
         * @param oldYear  The year value of old date.
         * @param newDay   The day value of new date.
         * @param newMonth The month value of new date.
         * @param newYear  The year value of new date.
         */
        void onDateChanged(int oldDay, int oldMonth, int oldYear, int newDay, int newMonth, int newYear);
    }

    private OnDateChangedListener mOnDateChangedListener;

    protected static final int SCROLL_DURATION = 250;
    protected static final int SCROLL_CHANGE_DELAY = 40;
    protected static final int LIST_TOP_OFFSET = -1;

    protected EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    protected int mCurrentScrollState = 0;
    protected long mPreviousScrollPosition;
    protected int mPreviousScrollState = 0;
    protected float mFriction = 1.0F;
    protected ScrollStateRunnable mScrollStateChangedRunnable = new ScrollStateRunnable();

    private int mPaddingLeft;
    private int mPaddingTop;
    private int mPaddingRight;
    private int mPaddingBottom;

    private static final String DAY_FORMAT = "%2d";
    private static final String YEAR_FORMAT = "%4d";

    public DatePicker(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public DatePicker(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        //mTypeface = Typeface.DEFAULT;
        mTextSize = -1;
        mTextColor = 0xFF000000;
        mTextLabelColor = 0xFF767676;
        mTextHighlightColor = 0xFFFFFFFF;
        mAnimDuration = -1;
        mLabels = new String[7];
        mFriction = 1.0F;

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setTextAlign(TextAlignment.CENTER);

        mDayPadding = AttrHelper.vp2px(4, context);

        mSelectionColor = 0xFF000000;

        mCalendar = Calendar.getInstance();
        mFirstDayOfWeek = mCalendar.getFirstDayOfWeek();

        int index = mCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        DateFormat format = new SimpleDateFormat("EEEEE");
        for (int i = 0; i < 7; i++) {
            mLabels[index] = format.format(mCalendar.getTime());
            index = (index + 1) % 7;
            mCalendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        setEstimateSizeListener(estimateSizeListener);
        //setLayoutRefreshedListener(refreshedListener);

        mAdapter = new MonthAdapter();
        setItemProvider(mAdapter);
        applyStyle(context, attrs, defStyleAttr, defStyleRes);

        addDrawTask(this::onDraw);

    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

        String familyName = null;
        int style = -1;

        int padding = -1;
        int paddingLeft = -1;
        int paddingRight = -1;
        int paddingTop = -1;
        int paddingBottom = -1;
        boolean paddingDefined = false;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("dp_dayTextSize"))
                mTextSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("dp_textColor"))
                mTextColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_textHighlightColor"))
                mTextHighlightColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_textLabelColor"))
                mTextLabelColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_textDisableColor"))
                mTextDisableColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_selectionColor"))
                mSelectionColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_animDuration"))
                mAnimDuration = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("dp_fontFamily"))
                familyName = Util.getStringValue(attrs, name, "");
            else if (name.equals("dp_textStyle"))
                style = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("dp_padding")) {
                padding = Util.getDimensionValue(attrs, name, 0);
                paddingDefined = true;
            } else if (name.equals("dp_leftPadding")) {
                paddingLeft = Util.getDimensionValue(attrs, name, 0);
                paddingDefined = true;
            } else if (name.equals("dp_topPadding")) {
                paddingTop = Util.getDimensionValue(attrs, name, 0);
                paddingDefined = true;
            } else if (name.equals("dp_rightPadding")) {
                paddingRight = Util.getDimensionValue(attrs, name, 0);
                paddingDefined = true;
            } else if (name.equals("dp_bottomPadding")) {
                paddingBottom = Util.getDimensionValue(attrs, name, 0);
                paddingDefined = true;
            }
        }
        if (mTextSize < 0)
            mTextSize = AttrHelper.fp2px(12, context);

        if (mAnimDuration < 0)
            mAnimDuration = 400;

        if (mInInterpolator == null)
            mInInterpolator = new DecelerateInterpolator();

        if (mOutInterpolator == null)
            mOutInterpolator = new DecelerateInterpolator();

        if (paddingDefined) {
            if (padding >= 0)
                setContentPadding(padding, padding, padding, padding);

            if (paddingLeft >= 0)
                mPaddingLeft = paddingLeft;

            if (paddingTop >= 0)
                mPaddingTop = paddingTop;

            if (paddingRight >= 0)
                mPaddingRight = paddingRight;

            if (paddingBottom >= 0)
                mPaddingBottom = paddingBottom;
        }

        postLayout();
        mAdapter.notifyDataChanged();

    }

    private void measureBaseSize() {
        mPaint.setTextSize(mTextSize);
        mDayBaseWidth = mPaint.measureText("88") + mDayPadding * 2;

        Rect bounds = mPaint.getTextBounds("88");
        mDayBaseHeight = bounds.getHeight();
    }

    private void measureMonthView(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = Component.EstimateSpec.getMode(widthMeasureSpec);
        int widthSize = Component.EstimateSpec.getSize(widthMeasureSpec);
        int heightMode = Component.EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = Component.EstimateSpec.getSize(heightMeasureSpec);

        measureBaseSize();

        int size = Math.round(Math.max(mDayBaseWidth, mDayBaseHeight));

        int width = size * 7 + mPaddingLeft + mPaddingRight;
        int height = Math.round(size * 7 + mDayBaseHeight + mDayPadding * 2 + mPaddingTop + mPaddingBottom);

        switch (widthMode) {
            case EstimateSpec.NOT_EXCEED:
                width = Math.min(width, widthSize);
                break;
            case EstimateSpec.PRECISE:
                width = widthSize;
                break;
        }

        switch (heightMode) {
            case Component.EstimateSpec.NOT_EXCEED:
                height = Math.min(height, heightSize);
                break;
            case EstimateSpec.PRECISE:
                height = heightSize;
                break;
        }

        mMonthRealWidth = width;
        mMonthRealHeight = height;
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            measureMonthView(widthMeasureSpec, heightMeasureSpec);
            return false;
        }
    };

//    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
//        @Override
//        public void onRefreshed(Component component) {
//            mDayWidth = (component.getEstimatedWidth() - mPaddingLeft - mPaddingRight) / 7f;
//            mDayHeight = (component.getEstimatedHeight() - mDayBaseHeight - mDayPadding * 2 - mPaddingTop - mPaddingBottom) / 7f;
//            mSelectionRadius = Math.min(mDayWidth, mDayHeight) / 2f;
//        }
//    };

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(0, 0, 0, 0);
    }

    public void setContentPadding(int left, int top, int right, int bottom) {
        mPaddingLeft = left;
        mPaddingTop = top;
        mPaddingRight = right;
        mPaddingBottom = bottom;
    }

    private String getDayText(int day) {
        if (mDayTexts == null) {
            synchronized (DatePicker.class) {
                if (mDayTexts == null)
                    mDayTexts = new String[31];
            }
        }

        if (mDayTexts[day - 1] == null)
            mDayTexts[day - 1] = String.format(DAY_FORMAT, day);

        return mDayTexts[day - 1];
    }

    /**
     * Set the range of selectable dates.
     *
     * @param minDay   The day value of minimum date.
     * @param minMonth The month value of minimum date.
     * @param minYear  The year value of minimum date.
     * @param maxDay   The day value of maximum date.
     * @param maxMonth The month value of maximum date.
     * @param maxYear  The year value of maximum date.
     */
    public void setDateRange(int minDay, int minMonth, int minYear, int maxDay, int maxMonth, int maxYear) {
        mAdapter.setDateRange(minDay, minMonth, minYear, maxDay, maxMonth, maxYear);
    }

    /**
     * Jump to the view of a specific month.
     *
     * @param month
     * @param year
     */
    public void goTo(int month, int year) {
        int position = mAdapter.positionOfMonth(month, year);
        postSetSelectionFromTop(position, 0);
    }

    public void postSetSelectionFromTop(final int position, final int offset) {
        mHandler.postTask(new Runnable() {
            @Override
            public void run() {
                scrollTo(position);
                postLayout();
            }
        });
    }

    /**
     * Set the selected date of this DatePicker.
     *
     * @param day   The day value of selected date.
     * @param month The month value of selected date.
     * @param year  The year value of selected date.
     */
    public void setDate(int day, int month, int year) {
        if (mAdapter.getYear() == year && mAdapter.getMonth() == month && mAdapter.getDay() == day)
            return;

        mAdapter.setDate(day, month, year, false);
        goTo(month, year);
    }

    /**
     * Set the listener will be called when the selected date is changed.
     *
     * @param listener The {@link OnDateChangedListener} will be called.
     */
    public void setOnDateChangedListener(OnDateChangedListener listener) {
        mOnDateChangedListener = listener;
    }

    /**
     * @return The day value of selected date.
     */
    public int getDay() {
        return mAdapter.getDay();
    }

    /**
     * @return The month value of selected date.
     */
    public int getMonth() {
        return mAdapter.getMonth();
    }

    /**
     * @return The year value of selected date.
     */
    public int getYear() {
        return mAdapter.getYear();
    }

    /**
     * Get the formatted string of selected date.
     *
     * @param formatter The DateFormat used to format the date.
     * @return
     */
    public String getFormattedDate(DateFormat formatter) {
        mCalendar.set(Calendar.YEAR, mAdapter.getYear());
        mCalendar.set(Calendar.MONTH, mAdapter.getMonth());
        mCalendar.set(Calendar.DAY_OF_MONTH, mAdapter.getDay());
        return formatter.format(mCalendar.getTime());
    }

    public int getSelectionColor() {
        return mSelectionColor;
    }

    public int getTextSize() {
        return mTextSize;
    }

//    public Typeface getTypeface(){
//        return mTypeface;
//    }

    public int getTextColor() {
        return mTextColor;
    }

    public int getTextLabelColor() {
        return mTextLabelColor;
    }

    public int getTextHighlightColor() {
        return mTextHighlightColor;
    }

    public int getTextDisableColor() {
        return mTextDisableColor;
    }

    public Calendar getCalendar() {
        return mCalendar;
    }

    private class ScrollStateRunnable implements Runnable {
        private int mNewState;

        /**
         * Sets up the runnable with a short delay in case the scroll state
         * immediately changes again.
         *
         * @param view        The list view that changed state
         * @param scrollState The new state it changed to
         */
        public void doScrollStateChange(ListContainer view, int scrollState) {
            mHandler.removeTask(this);
            mNewState = scrollState;
            mHandler.postTask(this, SCROLL_CHANGE_DELAY);
        }

        @Override
        public void run() {
            mCurrentScrollState = mNewState;

            int i = 0;
            Component child = getComponentAt(i);
            while (child != null && child.getBottom() <= 0)
                child = getComponentAt(++i);
            if (child == null)
                return;

            int firstPosition = getFirstVisibleItemPosition();
            int lastPosition = getLastVisibleItemPosition();
            boolean scroll = firstPosition != 0 && lastPosition != getChildCount() - 1;
            final int top = child.getTop();
            final int bottom = child.getBottom();
            final int midpoint = getHeight() / 2;
            if (scroll && top < LIST_TOP_OFFSET) {
                if (bottom > midpoint)
                    scrollBy(top, SCROLL_DURATION);
                else
                    scrollBy(bottom, SCROLL_DURATION);
            }
        }
    }

    private class MonthView extends Component implements DrawTask, TouchEventListener {

        private long mStartTime;
        private float mAnimProgress;
        private boolean mRunning;

        private int mTouchedDay = -1;

        private int mMonth;
        private int mYear;
        private int mMaxDay;
        private int mFirstDayCol;
        private int mMinAvailDay = -1;
        private int mMaxAvailDay = -1;
        private int mToday = -1;
        private int mSelectedDay = -1;
        private int mPreviousSelectedDay = -1;
        private String mMonthText;

        public MonthView(Context context) {
            super(context);
            setEstimateSizeListener(estimateSizeListener);
            addDrawTask(this::onDraw);
            setTouchEventListener(this::onTouchEvent);
        }

        public void setMonth(int month, int year) {
            if (mMonth != month || mYear != year) {
                mMonth = month;
                mYear = year;
                calculateMonthView();
                invalidate();
            }
        }

        public void setSelectedDay(int day, boolean animation) {
            if (mSelectedDay != day) {
                mPreviousSelectedDay = mSelectedDay;
                mSelectedDay = day;

                if (animation)
                    startAnimation();
                else
                    invalidate();
            }
        }

        public void setToday(int day) {
            if (mToday != day) {
                mToday = day;
                invalidate();
            }
        }

        public void setAvailableDay(int min, int max) {
            if (mMinAvailDay != min || mMaxAvailDay != max) {
                mMinAvailDay = min;
                mMaxAvailDay = max;
                invalidate();
            }
        }

        private void calculateMonthView() {
            mCalendar.set(Calendar.DAY_OF_MONTH, 1);
            mCalendar.set(Calendar.MONTH, mMonth);
            mCalendar.set(Calendar.YEAR, mYear);

            mMaxDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
            mFirstDayCol = dayOfWeek < mFirstDayOfWeek ? dayOfWeek + 7 - mFirstDayOfWeek : dayOfWeek - mFirstDayOfWeek;
            mMonthText = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()) + " " + String.format(YEAR_FORMAT, mYear);
        }

        EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
            @Override
            public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
                setEstimatedSize(EstimateSpec.getSizeWithMode(mMonthRealWidth, EstimateSpec.PRECISE), EstimateSpec.getSizeWithMode(mMonthRealHeight, EstimateSpec.PRECISE));
                return true;
            }
        };

        @Override
        public void onDraw(Component component, Canvas canvas) {
            //draw month text
            mPaint.setTextSize(mTextSize);
            float x = 3.5f * mDayWidth + getPaddingLeft();
            float y = mDayPadding * 2 + mDayBaseHeight + getPaddingTop();
            mPaint.setFakeBoldText(true);
            mPaint.setColor(new Color(mTextColor));
            canvas.drawText(mPaint, mMonthText, x, y);

            //draw selection
            float paddingLeft = getPaddingLeft();
            float paddingTop = mDayPadding * 2 + mDayBaseHeight + getPaddingTop();
            if (mSelectedDay > 0) {
                int col = (mFirstDayCol + mSelectedDay - 1) % 7;
                int row = (mFirstDayCol + mSelectedDay - 1) / 7 + 1;

                x = (col + 0.5f) * mDayWidth + paddingLeft;
                y = (row + 0.5f) * mDayHeight + paddingTop;
                float radius = mRunning ? mInInterpolator.getInterpolation(mAnimProgress) * mSelectionRadius : mSelectionRadius;
                mPaint.setColor(new Color(mSelectionColor));
                canvas.drawCircle(x, y, radius, mPaint);
            }

            if (mRunning && mPreviousSelectedDay > 0) {
                int col = (mFirstDayCol + mPreviousSelectedDay - 1) % 7;
                int row = (mFirstDayCol + mPreviousSelectedDay - 1) / 7 + 1;

                x = (col + 0.5f) * mDayWidth + paddingLeft;
                y = (row + 0.5f) * mDayHeight + paddingTop;
                float radius = (1f - mOutInterpolator.getInterpolation(mAnimProgress)) * mSelectionRadius;
                mPaint.setColor(new Color(mSelectionColor));
                canvas.drawCircle(x, y, radius, mPaint);
            }

            //draw label
            mPaint.setFakeBoldText(false);
            mPaint.setColor(new Color(mTextLabelColor));
            paddingTop += (mDayHeight + mDayBaseHeight) / 2f;
            for (int i = 0; i < 7; i++) {
                x = (i + 0.5f) * mDayWidth + paddingLeft;
                y = paddingTop;
                int index = (i + mFirstDayOfWeek - 1) % 7;
                canvas.drawText(mPaint, mLabels[index], x, y);
            }

            //draw date text
            int col = mFirstDayCol;
            int row = 1;
            int maxDay = mMaxAvailDay > 0 ? Math.min(mMaxAvailDay, mMaxDay) : mMaxDay;
            for (int day = 1; day <= mMaxDay; day++) {
                if (day == mSelectedDay)
                    mPaint.setColor(new Color(mTextHighlightColor));
                else if (day < mMinAvailDay || day > maxDay)
                    mPaint.setColor(new Color(mTextDisableColor));
                else if (day == mToday)
                    mPaint.setColor(new Color(mSelectionColor));
                else
                    mPaint.setColor(new Color(mTextColor));

                x = (col + 0.5f) * mDayWidth + paddingLeft;
                y = row * mDayHeight + paddingTop;

                canvas.drawText(mPaint, getDayText(day), x, y);
                col++;
                if (col == 7) {
                    col = 0;
                    row++;
                }
            }
        }

        private int getTouchedDay(float x, float y) {
            float paddingTop = mDayPadding * 2 + mDayBaseHeight + getPaddingTop() + mDayHeight;
            if (x < getPaddingLeft() || x > getWidth() - getPaddingRight() || y < paddingTop || y > getHeight() - getPaddingBottom())
                return -1;

            int col = (int) Math.floor((x - getPaddingLeft()) / mDayWidth);
            int row = (int) Math.floor((y - paddingTop) / mDayHeight);
            int maxDay = mMaxAvailDay > 0 ? Math.min(mMaxAvailDay, mMaxDay) : mMaxDay;

            int day = row * 7 + col - mFirstDayCol + 1;
            if (day < 0 || day < mMinAvailDay || day > maxDay)
                return -1;

            return day;
        }

        @Override
        public boolean onTouchEvent(Component component, TouchEvent event) {
            MmiPoint point = event.getPointerScreenPosition(event.getIndex());
            int[] location = component.getLocationOnScreen();
            float x = point.getX() - location[0];
            float y = point.getY() - location[1];
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    mTouchedDay = getTouchedDay(x, y);
                    return true;
                case TouchEvent.PRIMARY_POINT_UP:
                    if (getTouchedDay(x, y) == mTouchedDay && mTouchedDay > 0) {
                        mAdapter.setDate(mTouchedDay, mMonth, mYear, true);
                        mTouchedDay = -1;
                    }
                    return true;
                case TouchEvent.CANCEL:
                    mTouchedDay = -1;
                    return true;
            }
            return true;
        }

        private void resetAnimation() {
            mStartTime = Time.getRealActiveTime();
            mAnimProgress = 0f;
        }

        private void startAnimation() {
            if (getHandler() != null) {
                resetAnimation();
                mRunning = true;
                getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
            }

            invalidate();
        }

        private void stopAnimation() {
            mRunning = false;
            mAnimProgress = 1f;
            if (getHandler() != null)
                getHandler().removeTask(mUpdater);
            invalidate();
        }

        private final Runnable mUpdater = new Runnable() {

            @Override
            public void run() {
                update();
            }

        };

        private void update() {
            long curTime = Time.getRealActiveTime();
            mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

            if (mAnimProgress == 1f)
                stopAnimation();

            if (mRunning) {
                if (getHandler() != null)
                    getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
                else
                    stopAnimation();
            }

            invalidate();
        }

        private EventHandler getHandler() {
            return mHandler;
        }

    }

    private class MonthAdapter extends BaseItemProvider {
        private int mDay = -1;
        private int mMonth = -1;
        private int mYear = -1;
        private int mMinDay = -1;
        private int mMinMonth = -1;
        private int mMinYear = -1;
        private int mMaxDay = -1;
        private int mMaxMonth = -1;
        private int mMaxYear = -1;
        private int mToday;
        private int mTodayMonth;
        private int mTodayYear;
        private int mMinMonthValue;
        private int mMaxMonthValue;

        public void setDateRange(int minDay, int minMonth, int minYear, int maxDay, int maxMonth, int maxYear) {
            int minMonthValue = minDay < 0 || minMonth < 0 || minYear < 0 ? 0 : minYear * 12 + minMonth;
            int maxMonthValue = maxDay < 0 || maxMonth < 0 || maxYear < 0 ? Integer.MAX_VALUE - 1 : maxYear * 12 + maxMonth;

            if (minDay != mMinDay || mMinMonthValue != minMonthValue || maxDay != mMaxDay || mMaxMonthValue != maxMonthValue) {
                mMinDay = minDay;
                mMinMonth = minMonth;
                mMinYear = minYear;

                mMaxDay = maxDay;
                mMaxMonth = maxMonth;
                mMaxYear = maxYear;

                mMinMonthValue = minMonthValue;
                mMaxMonthValue = maxMonthValue;
                notifyDataChanged();
            }
        }

        public void setDate(int day, int month, int year, boolean animation) {
            MonthView v;
            if (mMonth != month || mYear != year) {
                int oldDay = mDay;
                int oldMonth = mMonth;
                int oldYear = mYear;

                mDay = day;
                mMonth = month;
                mYear = year;
                v = (MonthView) getComponentAt(positionOfMonth(mMonth, mYear));
                if (v != null)
                    v.setSelectedDay(mDay, animation);

                if (mOnDateChangedListener != null)
                    mOnDateChangedListener.onDateChanged(oldDay, oldMonth, oldYear, mDay, mMonth, mYear);
            } else if (day != mDay) {
                int oldDay = mDay;

                mDay = day;
                v = (MonthView) getComponentAt(positionOfMonth(mMonth, mYear));
                if (v != null)
                    v.setSelectedDay(mDay, animation);

                if (mOnDateChangedListener != null)
                    mOnDateChangedListener.onDateChanged(oldDay, mMonth, mYear, mDay, mMonth, mYear);
            }
        }

        public int positionOfMonth(int month, int year) {
            return year * 12 + month - mMinMonthValue;
        }

        public int getDay() {
            return mDay;
        }

        public int getMonth() {
            return mMonth;
        }

        public int getYear() {
            return mYear;
        }

        private void calToday() {
            mCalendar.setTimeInMillis(System.currentTimeMillis());
            mToday = mCalendar.get(Calendar.DAY_OF_MONTH);
            mTodayMonth = mCalendar.get(Calendar.MONTH);
            mTodayYear = mCalendar.get(Calendar.YEAR);
        }

        @Override
        public int getCount() {
            return mMaxMonthValue - mMinMonthValue + 1;
        }

        @Override
        public Object getItem(int position) {
            return position + mMinMonthValue;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer parent) {
            MonthView v = (MonthView) component;
            if (v == null) {
                v = new MonthView(parent.getContext());
                v.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
            }

            calToday();
            int monthValue = (Integer) getItem(position);
            int year = monthValue / 12;
            int month = monthValue % 12;
            int minDay = month == mMinMonth && year == mMinYear ? mMinDay : -1;
            int maxDay = month == mMaxMonth && year == mMaxYear ? mMaxDay : -1;
            int today = mTodayMonth == month && mTodayYear == year ? mToday : -1;
            int day = month == mMonth && year == mYear ? mDay : -1;

            v.setMonth(month, year);
            v.setToday(today);
            v.setAvailableDay(minDay, maxDay);
            v.setSelectedDay(day, false);

            return v;
        }
    }
}
