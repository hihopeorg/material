package com.rey.material.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.drawable.RippleDrawable;
import com.rey.material.drawable.ToolbarRippleDrawable;
import com.rey.material.util.Util;

public final class RippleManager implements Component.ClickedListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "RippleManager");

    private Component.ClickedListener mClickListener;
    private boolean mClickScheduled = false;

    private Element rippleDrawable;

    public RippleManager() {
    }

    /**
     * Should be called in the construction method of view to create a RippleDrawable.
     *
     * @param v
     * @param context
     * @param attrs
     * @param defStyleAttr
     * @param defStyleRes
     */
    public void onCreate(Component v, Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

        int rippleStyle = Util.getIntegerValue(attrs, "rd_style", 0);
        Component.DrawTask drawable = null;

        if (rippleStyle != 0)
            drawable = new RippleDrawable.Builder(context, rippleStyle).backgroundDrawable(getBackground(v)).build();
        else {
            boolean rippleEnable = Util.getBoolValue(attrs, "rd_enable", false);
            if (rippleEnable)
                drawable = new RippleDrawable.Builder(context, attrs, defStyleAttr, defStyleRes).backgroundDrawable(getBackground(v)).build();
        }

        if (drawable != null) {
            v.addDrawTask(drawable, Component.DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
            rippleDrawable = (Element) drawable;
        }
    }

    private Element getBackground(Component v) {
        Element background = v.getBackgroundElement();
        if (background == null)
            return null;

        if (background instanceof RippleDrawable)
            return ((RippleDrawable) background).getBackgroundDrawable();

        return background;
    }

    public void setOnClickListener(Component.ClickedListener l) {
        mClickListener = l;
    }

    public boolean onTouchEvent(Component v, TouchEvent event) {
        if (rippleDrawable != null) {
            ((RippleDrawable) rippleDrawable).onTouchEvent(v, event);
        }
        return false;
    }

    @Override
    public void onClick(Component component) {
        Element background = component.getBackgroundElement();
        long delay = 0;

        if (background != null) {
            if (background instanceof RippleDrawable)
                delay = ((RippleDrawable) background).getClickDelayTime();
            else if (background instanceof ToolbarRippleDrawable)
                delay = ((ToolbarRippleDrawable) background).getClickDelayTime();
        }

        if (delay > 0) {
            if (!mClickScheduled) {
                mClickScheduled = true;
                component.getContext().getUITaskDispatcher().delayDispatch(new ClickRunnable(component), delay);
            }
        } else
            dispatchClickEvent(component);
    }

    private void dispatchClickEvent(Component v) {
        if (mClickListener != null)
            mClickListener.onClick(v);
    }

    /**
     * Cancel the ripple effect of this view and all of it's children.
     *
     * @param v
     */
    public static void cancelRipple(Component v) {
        Element background = v.getBackgroundElement();
        if (background instanceof RippleDrawable)
            ((RippleDrawable) background).cancel();
        else if (background instanceof ToolbarRippleDrawable)
            ((ToolbarRippleDrawable) background).cancel();

        if (v instanceof ComponentContainer) {
            ComponentContainer vg = (ComponentContainer) v;
            for (int i = 0, count = vg.getChildCount(); i < count; i++)
                RippleManager.cancelRipple(vg.getComponentAt(i));
        }
    }

    class ClickRunnable implements Runnable {
        Component mView;

        public ClickRunnable(Component v) {
            mView = v;
        }

        @Override
        public void run() {
            mClickScheduled = false;
            dispatchClickEvent(mView);
        }
    }

}
