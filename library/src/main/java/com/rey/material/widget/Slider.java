package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.app.dispatcher.task.Revocable;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.drawable.RippleDrawable;
import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

/**
 * Created by Ret on 3/18/2015.
 */
public class Slider extends Component implements ThemeManager.OnThemeChangedListener, Component.DrawTask, Component.TouchEventListener {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "Slider");
    private RippleManager mRippleManager;
    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    private Paint mPaint;
    private RectFloat mDrawRect;
    private RectFloat mTempRect;
    private Path mLeftTrackPath;
    private Path mRightTrackPath;
    private Path mMarkPath;

    private int mMinValue = 0;
    private int mMaxValue = 100;
    private int mStepValue = 1;

    private boolean mDiscreteMode = false;

    private int mPrimaryColor;
    private int mSecondaryColor;
    private int mTrackSize = -1;
    private Paint.StrokeCap mTrackCap = Paint.StrokeCap.BUTT_CAP;
    private int mThumbBorderSize = -1;
    private int mThumbRadius = -1;
    private int mThumbFocusRadius = -1;
    private int mThumbTouchRadius = -1;
    private float mThumbPosition = -1;
    private int mTextSize = -1;
    private int mTextColor = 0xFFFFFFFF;
    private int mGravity = LayoutAlignment.CENTER;
    private int mTravelAnimationDuration = -1;
    private int mTransformAnimationDuration = -1;
    private Interpolator mInterpolator;
    private int mBaselineOffset;

    private int mTouchSlop;
    private Point mMemoPoint;
    private boolean mIsDragging;
    private float mThumbCurrentRadius;
    private float mThumbFillPercent;
    private boolean mAlwaysFillThumb = false;
    private int mTextHeight;
    private int mMemoValue;
    private String mValueText;

    private ThumbRadiusAnimator mThumbRadiusAnimator;
    private ThumbStrokeAnimator mThumbStrokeAnimator;
    private ThumbMoveAnimator mThumbMoveAnimator;

    private boolean mIsRtl = false;

    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    /**
     * Interface definition for a callback to be invoked when thumb's position changed.
     */
    public interface OnPositionChangeListener {
        /**
         * Called when thumb's position changed.
         *
         * @param view     The view fire this event.
         * @param fromUser Indicate the change is from user touch event or not.
         * @param oldPos   The old position of thumb.
         * @param newPos   The new position of thumb.
         * @param oldValue The old value.
         * @param newValue The new value.
         */
        void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue);
    }

    private OnPositionChangeListener mOnPositionChangeListener;

    public interface ValueDescriptionProvider {

        String getDescription(int value);

    }

    private ValueDescriptionProvider mValueDescriptionProvider;

    public Slider(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public Slider(Context context, AttrSet attrs) {
        super(context, attrs);

        init(context, attrs, 0, 0);
    }

    public Slider(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        mPaint = new Paint();

        //default color
        mPrimaryColor = 0xFF000000;
        mSecondaryColor = 0xFF000000;

        mDrawRect = new RectFloat();
        mTempRect = new RectFloat();
        mLeftTrackPath = new Path();
        mRightTrackPath = new Path();

        mThumbRadiusAnimator = new ThumbRadiusAnimator();
        mThumbStrokeAnimator = new ThumbStrokeAnimator();
        mThumbMoveAnimator = new ThumbMoveAnimator();

        mTouchSlop = 24;
        mMemoPoint = new Point();

        applyStyle(context, attrs, defStyleAttr, defStyleRes);

        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);

        setBindStateChangedListener(mBindStateChangedListener);
        setLayoutRefreshedListener(mRefreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        getRippleManager().onCreate(this, context, attrs, defStyleAttr, defStyleRes);

        int minValue = getMinValue();
        int maxValue = getMaxValue();
        boolean valueRangeDefined = false;
        int value = -1;
        boolean valueDefined = false;
        String familyName = null;
        boolean textStyleDefined = false;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("sl_discreteMode"))
                mDiscreteMode = Util.getBoolValue(attrs, name, false);
            else if (name.equals("sl_primaryColor"))
                mPrimaryColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("sl_secondaryColor"))
                mSecondaryColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("sl_trackSize"))
                mTrackSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sl_trackCap")) {
                int cap = Util.getIntegerValue(attrs, name, 0);
                if (cap == 0)
                    mTrackCap = Paint.StrokeCap.BUTT_CAP;
                else if (cap == 1)
                    mTrackCap = Paint.StrokeCap.ROUND_CAP;
                else
                    mTrackCap = Paint.StrokeCap.SQUARE_CAP;
            } else if (name.equals("sl_thumbBorderSize"))
                mThumbBorderSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sl_thumbRadius"))
                mThumbRadius = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sl_thumbFocusRadius"))
                mThumbFocusRadius = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sl_thumbTouchRadius"))
                mThumbTouchRadius = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("sl_travelAnimDuration")) {
                mTravelAnimationDuration = Util.getIntegerValue(attrs, name, 0);
                mTransformAnimationDuration = mTravelAnimationDuration;
            } else if (name.equals("sl_alwaysFillThumb")) {
                mAlwaysFillThumb = Util.getBoolValue(attrs, name, false);
            } else if (name.equals("gravity"))
                mGravity = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("sl_minValue")) {
                minValue = Util.getIntegerValue(attrs, name, 0);
                valueRangeDefined = true;
            } else if (name.equals("sl_maxValue")) {
                maxValue = Util.getIntegerValue(attrs, name, 0);
                valueRangeDefined = true;
            } else if (name.equals("sl_stepValue"))
                mStepValue = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("sl_value")) {
                value = Util.getIntegerValue(attrs, name, 0);
                valueDefined = true;
            } else if (name.equals("sl_fontFamily")) {
                familyName = Util.getStringValue(attrs, name, "");
                textStyleDefined = true;
            } else if (name.equals("sl_textStyle")) {
                //style = Util.getIntegerValue(attrs,name,0);
                textStyleDefined = true;
            } else if (name.equals("sl_textColor"))
                mTextColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("sl_textSize"))
                mTextSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("enabled"))
                setEnabled(Util.getBoolValue(attrs, name, true));
            else if (name.equals("sl_baselineOffset"))
                mBaselineOffset = Util.getDimensionValue(attrs, name, 0);
        }

        if (mTrackSize < 0)
            mTrackSize = AttrHelper.vp2px(2, context);

        if (mThumbBorderSize < 0)
            mThumbBorderSize = AttrHelper.vp2px(2, context);

        if (mThumbRadius < 0)
            mThumbRadius = AttrHelper.vp2px(10, context);

        if (mThumbFocusRadius < 0)
            mThumbFocusRadius = AttrHelper.vp2px(14, context);

        if (mTravelAnimationDuration < 0) {
            mTravelAnimationDuration = 400;
            mTransformAnimationDuration = mTravelAnimationDuration;
        }

        if (mInterpolator == null)
            mInterpolator = new DecelerateInterpolator();

        if (valueRangeDefined)
            setValueRange(minValue, maxValue, false);

        if (valueDefined)
            setValue(value, false);
        else if (mThumbPosition < 0)
            setValue(mMinValue, false);

        if (mTextSize < 0)
            mTextSize = 14;

        mPaint.setTextSize(mTextSize);
        mPaint.setTextAlign(TextAlignment.CENTER);

        measureText();
        invalidate();
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(Slider.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            RippleManager.cancelRipple(Slider.this);
            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(Slider.this::onThemeChanged);
        }
    };

    private void measureText() {
        if (mValueText == null)
            return;

        mPaint.setTextSize(mTextSize);
        float width = mPaint.measureText(mValueText);
        float maxWidth = (float) (mThumbRadius * Math.sqrt(2) * 2 - AttrHelper.vp2px(8, getContext()));
        if (width > maxWidth) {
            float textSize = mTextSize * maxWidth / width;
            mPaint.setTextSize((int) textSize);
        }

        Rect temp = mPaint.getTextBounds(mValueText);
        mTextHeight = temp.getHeight();
    }

    private String getValueText() {
        int value = getValue();
        if (mValueText == null || mMemoValue != value) {
            mMemoValue = value;
            mValueText = mValueDescriptionProvider == null ? String.valueOf(mMemoValue) : mValueDescriptionProvider.getDescription(mMemoValue);
            measureText();
        }

        return mValueText;
    }

    /**
     * @return The minimum selectable value.
     */
    public int getMinValue() {
        return mMinValue;
    }

    /**
     * @return The maximum selectable value.
     */
    public int getMaxValue() {
        return mMaxValue;
    }

    /**
     * @return The step value.
     */
    public int getStepValue() {
        return mStepValue;
    }

    /**
     * Set the randge of selectable value.
     *
     * @param min       The minimum selectable value.
     * @param max       The maximum selectable value.
     * @param animation Indicate that should show animation when thumb's current position changed.
     */
    public void setValueRange(int min, int max, boolean animation) {
        if (max < min || (min == mMinValue && max == mMaxValue))
            return;

        float oldValue = getExactValue();
        float oldPosition = getPosition();
        mMinValue = min;
        mMaxValue = max;

        setValue(oldValue, animation);
        if (mOnPositionChangeListener != null && oldPosition == getPosition() && oldValue != getExactValue())
            mOnPositionChangeListener.onPositionChanged(this, false, oldPosition, oldPosition, Math.round(oldValue), getValue());
    }

    /**
     * @return The selected value.
     */
    public int getValue() {
        return Math.round(getExactValue());
    }

    /**
     * @return The exact selected value.
     */
    public float getExactValue() {
        return (mMaxValue - mMinValue) * getPosition() + mMinValue;
    }

    /**
     * @return The current position of thumb in [0..1] range.
     */
    public float getPosition() {
        return mThumbMoveAnimator.isRunning() ? mThumbMoveAnimator.getPosition() : mThumbPosition;
    }

    /**
     * Set current position of thumb.
     *
     * @param pos       The position in [0..1] range.
     * @param animation Indicate that should show animation when change thumb's position.
     */
    public void setPosition(float pos, boolean animation) {
        setPosition(pos, animation, animation, false);
    }

    private void setPosition(float pos, boolean moveAnimation, boolean transformAnimation, boolean fromUser) {
        boolean change = getPosition() != pos;
        int oldValue = getValue();
        float oldPos = getPosition();

        if (!moveAnimation || !mThumbMoveAnimator.startAnimation(pos)) {
            mThumbPosition = pos;

            if (transformAnimation) {
                if (!mIsDragging)
                    mThumbRadiusAnimator.startAnimation(mThumbRadius);
                mThumbStrokeAnimator.startAnimation(pos == 0 ? 0 : 1);
            } else {
                mThumbCurrentRadius = mThumbRadius;
                mThumbFillPercent = (mAlwaysFillThumb || mThumbPosition != 0) ? 1 : 0;
                invalidate();
            }
        }

        int newValue = getValue();
        float newPos = getPosition();

        if (change && mOnPositionChangeListener != null)
            mOnPositionChangeListener.onPositionChanged(this, fromUser, oldPos, newPos, oldValue, newValue);
    }

    /**
     * Changes the primary color and invalidates the view to force a redraw.
     *
     * @param color New color to assign to mPrimaryColor.
     */
    public void setPrimaryColor(int color) {
        mPrimaryColor = color;
        invalidate();
    }

    /**
     * Changes the secondary color and invalidates the view to force a redraw.
     *
     * @param color New color to assign to mSecondaryColor.
     */
    public void setSecondaryColor(int color) {
        mSecondaryColor = color;
        invalidate();
    }

    /**
     * Set if we want the thumb to always be filled.
     *
     * @param alwaysFillThumb Do we want it to always be filled.
     */
    public void setAlwaysFillThumb(boolean alwaysFillThumb) {
        mAlwaysFillThumb = alwaysFillThumb;
    }

    /**
     * Set the selected value of this Slider.
     *
     * @param value     The selected value.
     * @param animation Indicate that should show animation when change thumb's position.
     */
    public void setValue(float value, boolean animation) {
        value = Math.min(mMaxValue, Math.max(value, mMinValue));
        setPosition((value - mMinValue) / (mMaxValue - mMinValue), animation);
    }

    /**
     * Set a listener will be called when thumb's position changed.
     *
     * @param listener The {@link OnPositionChangeListener} will be called.
     */
    public void setOnPositionChangeListener(OnPositionChangeListener listener) {
        mOnPositionChangeListener = listener;
    }

    public void setValueDescriptionProvider(ValueDescriptionProvider provider) {
        mValueDescriptionProvider = provider;
    }

    @Override
    public void setBackground(Element element) {
        Element background = getBackgroundElement();
        if (background instanceof RippleDrawable && !(element instanceof RippleDrawable))
            ((RippleDrawable) background).setBackgroundDrawable(element);
        else
            super.setBackground(element);
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null)
                    mRippleManager = new RippleManager();
            }
        }

        return mRippleManager;
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        RippleManager rippleManager = getRippleManager();
        if (l == rippleManager)
            super.setClickedListener(l);
        else {
            rippleManager.setOnClickListener(l);
            setClickedListener(rippleManager);
        }
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            int widthSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);

            int heightSize = EstimateSpec.getSize(heightMeasureSpec);
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);

            switch (widthMode) {
                case EstimateSpec.UNCONSTRAINT:
                    widthSize = getMinWidth();
                    break;
                case EstimateSpec.NOT_EXCEED:
                    widthSize = Math.min(widthSize, getMinWidth());
                    break;
            }

            switch (heightMode) {
                case EstimateSpec.UNCONSTRAINT:
                    heightSize = getMinHeight();
                    break;
                case EstimateSpec.NOT_EXCEED:
                    heightSize = Math.min(heightSize, getMinHeight());
                    break;
            }

            setEstimatedSize(widthSize, heightSize);
            return false;
        }
    };

    @Override
    public int getMinWidth() {
        return (mDiscreteMode ? (int) (mThumbRadius * Math.sqrt(2)) : mThumbFocusRadius) * 4 + getPaddingLeft() + getPaddingRight();
    }

    @Override
    public int getMinHeight() {
        return (mDiscreteMode ? (int) (mThumbRadius * (4 + Math.sqrt(2))) : mThumbFocusRadius * 2) + getPaddingTop() + getPaddingBottom();
    }

    @Override
    protected void onRtlChanged(LayoutDirection layoutDirection) {
        super.onRtlChanged(layoutDirection);
        boolean rtl = layoutDirection == LayoutDirection.RTL;
        if (mIsRtl != rtl) {
            mIsRtl = rtl;
            invalidate();
        }
    }

    LayoutRefreshedListener mRefreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            measureComponent(component);
        }
    };

    private void measureComponent(Component component) {
        mDrawRect.left = getPaddingLeft() + mThumbRadius;
        mDrawRect.right = component.getWidth() - getPaddingRight() - mThumbRadius;

        int align = mGravity;

        if (mDiscreteMode) {
            int fullHeight = (int) (mThumbRadius * (4 + Math.sqrt(2)));
            int height = mThumbRadius * 2;
            switch (align) {
                case LayoutAlignment.TOP:
                    mDrawRect.top = Math.max(getPaddingTop(), fullHeight - height);
                    mDrawRect.bottom = mDrawRect.top + height;
                    break;
                case LayoutAlignment.BOTTOM:
                    mDrawRect.bottom = component.getHeight() - getPaddingBottom();
                    mDrawRect.top = mDrawRect.bottom - height;
                    break;
                default:
                    mDrawRect.top = Math.max((component.getHeight() - height) / 2f, fullHeight - height);
                    mDrawRect.bottom = mDrawRect.top + height;
                    break;
            }
        } else {
            int height = mThumbFocusRadius * 2;
            switch (align) {
                case LayoutAlignment.TOP:
                    mDrawRect.top = getPaddingTop();
                    mDrawRect.bottom = mDrawRect.top + height;
                    break;
                case LayoutAlignment.BOTTOM:
                    mDrawRect.bottom = component.getHeight() - getPaddingBottom();
                    mDrawRect.top = mDrawRect.bottom - height;
                    break;
                default:
                    mDrawRect.top = (component.getHeight() - height) / 2f;
                    mDrawRect.bottom = mDrawRect.top + height;
                    break;
            }
        }
    }

    private boolean isThumbHit(float x, float y, float radius) {
        float cx = mDrawRect.getWidth() * mThumbPosition + mDrawRect.left;
        float cy = mDrawRect.getVerticalCenter();


        return x >= cx - radius && x <= cx + radius && y >= cy - radius && y < cy + radius;
    }

    private double distance(float x1, float y1, float x2, float y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    private float correctPosition(float position) {
        if (!mDiscreteMode)
            return position;

        int totalOffset = mMaxValue - mMinValue;
        int valueOffset = Math.round(totalOffset * position);
        int stepOffset = valueOffset / mStepValue;
        int lowerValue = stepOffset * mStepValue;
        int higherValue = Math.min(totalOffset, (stepOffset + 1) * mStepValue);

        if (valueOffset - lowerValue < higherValue - valueOffset)
            position = lowerValue / (float) totalOffset;
        else
            position = higherValue / (float) totalOffset;

        return position;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        getRippleManager().onTouchEvent(this, event);

        if (!isEnabled())
            return false;

        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        if (mIsRtl)
            x = 2 * mDrawRect.getHorizontalCenter() - x;

        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mIsDragging = isThumbHit(x, y, mThumbTouchRadius > 0 ? mThumbTouchRadius : mThumbRadius) && !mThumbMoveAnimator.isRunning();
                mMemoPoint.modify(x, y);
                if (mIsDragging) {
                    mThumbRadiusAnimator.startAnimation(mDiscreteMode ? 0 : mThumbFocusRadius);
                }
                break;
            case TouchEvent.POINT_MOVE:
                if (mIsDragging) {
                    if (mDiscreteMode) {
                        float position = correctPosition(Math.min(1f, Math.max(0f, (x - mDrawRect.left) / mDrawRect.getWidth())));
                        setPosition(position, true, true, true);
                    } else {
                        float offset = (x - mMemoPoint.getPointX()) / mDrawRect.getWidth();
                        float position = Math.min(1f, Math.max(0f, mThumbPosition + offset));
                        setPosition(position, false, true, true);
                        mMemoPoint.position[0] = x;
                        invalidate();
                    }
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mIsDragging) {
                    mIsDragging = false;
                    setPosition(getPosition(), true, true, true);
                } else if (distance(mMemoPoint.getPointX(), mMemoPoint.getPointY(), x, y) <= mTouchSlop) {
                    float position = correctPosition(Math.min(1f, Math.max(0f, (x - mDrawRect.left) / mDrawRect.getWidth())));
                    setPosition(position, true, true, true);
                }
                break;
            case TouchEvent.CANCEL:
                if (mIsDragging) {
                    mIsDragging = false;
                    setPosition(getPosition(), true, true, true);
                }
                break;
        }

        return true;
    }

    private void getTrackPath(float x, float y, float radius) {
        float halfStroke = mTrackSize / 2f;

        mLeftTrackPath.reset();
        mRightTrackPath.reset();

        if (radius - 1f < halfStroke) {
            if (mTrackCap != Paint.StrokeCap.ROUND_CAP) {
                if (x > mDrawRect.left) {
                    mLeftTrackPath.moveTo(mDrawRect.left, y - halfStroke);
                    mLeftTrackPath.lineTo(x, y - halfStroke);
                    mLeftTrackPath.lineTo(x, y + halfStroke);
                    mLeftTrackPath.lineTo(mDrawRect.left, y + halfStroke);
                    mLeftTrackPath.close();
                }

                if (x < mDrawRect.right) {
                    mRightTrackPath.moveTo(mDrawRect.right, y + halfStroke);
                    mRightTrackPath.lineTo(x, y + halfStroke);
                    mRightTrackPath.lineTo(x, y - halfStroke);
                    mRightTrackPath.lineTo(mDrawRect.right, y - halfStroke);
                    mRightTrackPath.close();
                }
            } else {
                if (x > mDrawRect.left) {
                    mTempRect.modify(mDrawRect.left, y - halfStroke, mDrawRect.left + mTrackSize, y + halfStroke);
                    mLeftTrackPath.arcTo(mTempRect, 90, 180);
                    mLeftTrackPath.lineTo(x, y - halfStroke);
                    mLeftTrackPath.lineTo(x, y + halfStroke);
                    mLeftTrackPath.close();
                }

                if (x < mDrawRect.right) {
                    mTempRect.modify(mDrawRect.right - mTrackSize, y - halfStroke, mDrawRect.right, y + halfStroke);
                    mRightTrackPath.arcTo(mTempRect, 270, 180);
                    mRightTrackPath.lineTo(x, y + halfStroke);
                    mRightTrackPath.lineTo(x, y - halfStroke);
                    mRightTrackPath.close();
                }
            }
        } else {
            if (mTrackCap != Paint.StrokeCap.ROUND_CAP) {
                mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
                float angle = (float) (Math.asin(halfStroke / (radius - 1f)) / Math.PI * 180);

                if (x - radius > mDrawRect.left) {
                    mLeftTrackPath.moveTo(mDrawRect.left, y - halfStroke);
                    mLeftTrackPath.arcTo(mTempRect, 180 + angle, -angle * 2);
                    mLeftTrackPath.lineTo(mDrawRect.left, y + halfStroke);
                    mLeftTrackPath.close();
                }

                if (x + radius < mDrawRect.right) {
                    mRightTrackPath.moveTo(mDrawRect.right, y - halfStroke);
                    mRightTrackPath.arcTo(mTempRect, -angle, angle * 2);
                    mRightTrackPath.lineTo(mDrawRect.right, y + halfStroke);
                    mRightTrackPath.close();
                }
            } else {
                float angle = (float) (Math.asin(halfStroke / (radius - 1f)) / Math.PI * 180);

                if (x - radius > mDrawRect.left) {
                    float angle2 = (float) (Math.acos(Math.max(0f, (mDrawRect.left + halfStroke - x + radius) / halfStroke)) / Math.PI * 180);

                    mTempRect.modify(mDrawRect.left, y - halfStroke, mDrawRect.left + mTrackSize, y + halfStroke);
                    mLeftTrackPath.arcTo(mTempRect, 180 - angle2, angle2 * 2);

                    mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
                    mLeftTrackPath.arcTo(mTempRect, 180 + angle, -angle * 2);
                    mLeftTrackPath.close();
                }

                if (x + radius < mDrawRect.right) {
                    float angle2 = (float) Math.acos(Math.max(0f, (x + radius - mDrawRect.right + halfStroke) / halfStroke));
                    mRightTrackPath.moveTo((float) (mDrawRect.right - halfStroke + Math.cos(angle2) * halfStroke), (float) (y + Math.sin(angle2) * halfStroke));

                    angle2 = (float) (angle2 / Math.PI * 180);
                    mTempRect.modify(mDrawRect.right - mTrackSize, y - halfStroke, mDrawRect.right, y + halfStroke);
                    mRightTrackPath.arcTo(mTempRect, angle2, -angle2 * 2);

                    mTempRect.modify(x - radius + 1f, y - radius + 1f, x + radius - 1f, y + radius - 1f);
                    mRightTrackPath.arcTo(mTempRect, -angle, angle * 2);
                    mRightTrackPath.close();
                }
            }
        }
    }

    private Path getMarkPath(Path path, float cx, float cy, float radius, float factor) {
        if (path == null)
            path = new Path();
        else
            path.reset();

        float x1 = cx - radius;
        float y1 = cy;
        float x2 = cx + radius;
        float y2 = cy;
        float x3 = cx;
        float y3 = cy + radius;

        float nCx = cx;
        float nCy = cy - radius * factor;

        // calculate first arc
        float angle = (float) (Math.atan2(y2 - nCy, x2 - nCx) * 180 / Math.PI);
        float nRadius = (float) distance(nCx, nCy, x1, y1);
        mTempRect.modify(nCx - nRadius, nCy - nRadius, nCx + nRadius, nCy + nRadius);
        path.moveTo(x1, y1);
        path.arcTo(mTempRect, 180 - angle, 180 + angle * 2);

        if (factor > 0.9f)
            path.lineTo(x3, y3);
        else {
            // find center point for second arc
            float x4 = (x2 + x3) / 2;
            float y4 = (y2 + y3) / 2;

            double d1 = distance(x2, y2, x4, y4);
            double d2 = d1 / Math.tan(Math.PI * (1f - factor) / 4);

            nCx = (float) (x4 - Math.cos(Math.PI / 4) * d2);
            nCy = (float) (y4 - Math.sin(Math.PI / 4) * d2);

            // calculate second arc
            angle = (float) (Math.atan2(y2 - nCy, x2 - nCx) * 180 / Math.PI);
            float angle2 = (float) (Math.atan2(y3 - nCy, x3 - nCx) * 180 / Math.PI);
            nRadius = (float) distance(nCx, nCy, x2, y2);
            mTempRect.modify(nCx - nRadius, nCy - nRadius, nCx + nRadius, nCy + nRadius);
            path.arcTo(mTempRect, angle, angle2 - angle);

            // calculate third arc
            nCx = cx * 2 - nCx;
            angle = (float) (Math.atan2(y3 - nCy, x3 - nCx) * 180 / Math.PI);
            angle2 = (float) (Math.atan2(y1 - nCy, x1 - nCx) * 180 / Math.PI);
            mTempRect.modify(nCx - nRadius, nCy - nRadius, nCx + nRadius, nCy + nRadius);
            path.arcTo(mTempRect, angle + (float) Math.PI / 4, angle2 - angle);
        }

        path.close();

        return path;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        measureComponent(component);
        float x = mDrawRect.getWidth() * mThumbPosition + mDrawRect.left;
        if (mIsRtl)
            x = 2 * mDrawRect.getHorizontalCenter() - x;
        float y = mDrawRect.getVerticalCenter();
        int filledPrimaryColor = ColorUtil.getMiddleColor(mSecondaryColor, isEnabled() ? mPrimaryColor : mSecondaryColor, mThumbFillPercent);

        getTrackPath(x, y, mThumbCurrentRadius);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(new Color(mIsRtl ? filledPrimaryColor : mSecondaryColor));
        canvas.drawPath(mRightTrackPath, mPaint);
        mPaint.setColor(new Color(mIsRtl ? mSecondaryColor : filledPrimaryColor));
        canvas.drawPath(mLeftTrackPath, mPaint);

        mPaint.setColor(new Color(filledPrimaryColor));
        if (mDiscreteMode) {
            float factor = 1f - mThumbCurrentRadius / mThumbRadius;

            if (factor > 0) {
                mMarkPath = getMarkPath(mMarkPath, x, y, mThumbRadius, factor);
                mPaint.setStyle(Paint.Style.FILL_STYLE);
                int saveCount = canvas.save();
                canvas.translate(0, -mThumbRadius * 2 * factor);
                canvas.drawPath(mMarkPath, mPaint);
                mPaint.setColor(new Color(ColorUtil.getColor(mTextColor, factor)));
                canvas.drawText(mPaint, getValueText(), x, y + mTextHeight / 2f - mThumbRadius * factor);
                canvas.restoreToCount(saveCount);
            }

            float radius = isEnabled() ? mThumbCurrentRadius : mThumbCurrentRadius - mThumbBorderSize;
            if (radius > 0) {
                mPaint.setColor(new Color(filledPrimaryColor));
                canvas.drawCircle(x, y, radius, mPaint);
            }
        } else {
            float radius = isEnabled() ? mThumbCurrentRadius : mThumbCurrentRadius - mThumbBorderSize;
            if (mThumbFillPercent == 1)
                mPaint.setStyle(Paint.Style.FILL_STYLE);
            else {
                float strokeWidth = (radius - mThumbBorderSize) * mThumbFillPercent + mThumbBorderSize;
                radius = radius - strokeWidth / 2f;
                mPaint.setStyle(Paint.Style.STROKE_STYLE);
                mPaint.setStrokeWidth(strokeWidth);
            }
            canvas.drawCircle(x, y, radius, mPaint);
        }
    }

    class ThumbRadiusAnimator implements Runnable {

        boolean mRunning = false;
        long mStartTime;
        float mStartRadius;
        int mRadius;
        Revocable revocable;

        public void resetAnimation() {
            mStartTime = Time.getRealActiveTime();
            mStartRadius = mThumbCurrentRadius;
        }

        public boolean startAnimation(int radius) {
            if (mThumbCurrentRadius == radius)
                return false;

            mRadius = radius;

            if (getHandler() != null) {
                resetAnimation();
                mRunning = true;
                getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                invalidate();
                return true;
            } else {
                mThumbCurrentRadius = mRadius;
                invalidate();
                return false;
            }
        }

        public void stopAnimation() {
            mRunning = false;
            mThumbCurrentRadius = mRadius;
            if (getHandler() != null)
                getHandler().removeTask(this);

            invalidate();
        }

        @Override
        public void run() {
            long curTime = Time.getRealActiveTime();
            float progress = Math.min(1f, (float) (curTime - mStartTime) / mTransformAnimationDuration);
            float value = mInterpolator.getInterpolation(progress);

            mThumbCurrentRadius = (mRadius - mStartRadius) * value + mStartRadius;

            if (progress == 1f)
                stopAnimation();

            if (mRunning) {
                if (getHandler() != null)
                    getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                else
                    stopAnimation();
            }

            invalidate();
        }

    }

    class ThumbStrokeAnimator implements Runnable {

        boolean mRunning = false;
        long mStartTime;
        float mStartFillPercent;
        int mFillPercent;

        public void resetAnimation() {
            mStartTime = Time.getRealActiveTime();
            mStartFillPercent = mThumbFillPercent;
        }

        public boolean startAnimation(int fillPercent) {
            if (mThumbFillPercent == fillPercent)
                return false;

            mFillPercent = fillPercent;

            if (getHandler() != null) {
                resetAnimation();
                mRunning = true;
                getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                invalidate();
                return true;
            } else {
                mThumbFillPercent = mAlwaysFillThumb ? 1 : mFillPercent;
                invalidate();
                return false;
            }
        }

        public void stopAnimation() {
            mRunning = false;
            mThumbFillPercent = mAlwaysFillThumb ? 1 : mFillPercent;
            if (getHandler() != null)
                getHandler().removeTask(this);
            invalidate();
        }

        @Override
        public void run() {
            long curTime = Time.getRealActiveTime();
            float progress = Math.min(1f, (float) (curTime - mStartTime) / mTransformAnimationDuration);
            float value = mInterpolator.getInterpolation(progress);

            mThumbFillPercent = mAlwaysFillThumb ? 1 : ((mFillPercent - mStartFillPercent) * value + mStartFillPercent);

            if (progress == 1f)
                stopAnimation();

            if (mRunning) {
                if (getHandler() != null)
                    getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                else
                    stopAnimation();
            }

            invalidate();
        }

    }

    class ThumbMoveAnimator implements Runnable {

        boolean mRunning = false;
        long mStartTime;
        float mStartFillPercent;
        float mStartRadius;
        float mStartPosition;
        float mPosition;
        float mFillPercent;
        int mDuration;

        public boolean isRunning() {
            return mRunning;
        }

        public float getPosition() {
            return mPosition;
        }

        public void resetAnimation() {
            mStartTime = Time.getRealActiveTime();
            mStartPosition = mThumbPosition;
            mStartFillPercent = mThumbFillPercent;
            mStartRadius = mThumbCurrentRadius;
            mFillPercent = mPosition == 0 ? 0 : 1;
            mDuration = mDiscreteMode && !mIsDragging ? mTransformAnimationDuration * 2 + mTravelAnimationDuration : mTravelAnimationDuration;
        }

        public boolean startAnimation(float position) {
            if (mThumbPosition == position)
                return false;

            mPosition = position;

            if (getHandler() != null) {
                resetAnimation();
                mRunning = true;
                getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                invalidate();
                return true;
            } else {
                mThumbPosition = position;
                invalidate();
                return false;
            }
        }

        public void stopAnimation() {
            mRunning = false;
            mThumbCurrentRadius = mDiscreteMode && mIsDragging ? 0 : mThumbRadius;
            mThumbFillPercent = mAlwaysFillThumb ? 1 : mFillPercent;
            mThumbPosition = mPosition;
            if (getHandler() != null)
                getHandler().removeTask(this);
            invalidate();
        }

        @Override
        public void run() {
            long curTime = Time.getRealActiveTime();
            float progress = Math.min(1f, (float) (curTime - mStartTime) / mDuration);
            float value = mInterpolator.getInterpolation(progress);

            if (mDiscreteMode) {
                if (mIsDragging) {
                    mThumbPosition = (mPosition - mStartPosition) * value + mStartPosition;
                    mThumbFillPercent = mAlwaysFillThumb ? 1 : ((mFillPercent - mStartFillPercent) * value + mStartFillPercent);
                } else {
                    float p1 = (float) mTravelAnimationDuration / mDuration;
                    float p2 = (float) (mTravelAnimationDuration + mTransformAnimationDuration) / mDuration;
                    if (progress < p1) {
                        value = mInterpolator.getInterpolation(progress / p1);
                        mThumbCurrentRadius = mStartRadius * (1f - value);
                        mThumbPosition = (mPosition - mStartPosition) * value + mStartPosition;
                        mThumbFillPercent = mAlwaysFillThumb ? 1 : ((mFillPercent - mStartFillPercent) * value + mStartFillPercent);
                    } else if (progress > p2) {
                        mThumbCurrentRadius = mThumbRadius * (progress - p2) / (1 - p2);
                    }
                }
            } else {
                mThumbPosition = (mPosition - mStartPosition) * value + mStartPosition;
                mThumbFillPercent = mAlwaysFillThumb ? 1 : ((mFillPercent - mStartFillPercent) * value + mStartFillPercent);

                if (progress < 0.2)
                    mThumbCurrentRadius = Math.max(mThumbRadius + mThumbBorderSize * progress * 5, mThumbCurrentRadius);
                else if (progress >= 0.8)
                    mThumbCurrentRadius = mThumbRadius + mThumbBorderSize * (5f - progress * 5);
            }


            if (progress == 1f)
                stopAnimation();

            if (mRunning) {
                if (getHandler() != null)
                    getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                else
                    stopAnimation();
            }

            invalidate();
        }

    }

    private EventHandler getHandler() {
        return mHandler;
    }
}
