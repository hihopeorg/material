/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rey.material.widget;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.util.ColorUtil;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

/**
 * Created by Rey on 12/19/2014.
 */
public class TimePicker extends Component implements ThemeManager.OnThemeChangedListener, Component.TouchEventListener, Component.DrawTask {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "TimePicker");

    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    private int mBackgroundColor;
    private int mSelectionColor;
    private int mSelectionRadius = -1;
    private int mTickSize = -1;
    private int mTextSize = -1;
    private int mTextColor = 0xFF000000;
    private int mTextHighlightColor = 0xFFFFFFFF;
    private boolean m24Hour = true;

    private int mAnimDuration = -1;
    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;
    private long mStartTime;
    private float mAnimProgress;
    private boolean mRunning;

    private Paint mPaint;

    private Point mCenterPoint;
    private float mOuterRadius;
    private float mInnerRadius;
    private float mSecondInnerRadius;

    private float[] mLocations = new float[72];
    private Rect mRect;
    private String[] mTicks;

    private int mMode = MODE_HOUR;

    public static final int MODE_HOUR = 0;
    public static final int MODE_MINUTE = 1;

    private int mHour = 0;
    private int mMinute = 0;

    private boolean mEdited = false;

    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    /**
     * Interface definition for a callback to be invoked when the selected time is changed.
     */
    public interface OnTimeChangedListener {

        /**
         * Called when the select mode is changed
         *
         * @param mode The current mode. Can be {@link #MODE_HOUR} or {@link #MODE_MINUTE}.
         */
        void onModeChanged(int mode);

        /**
         * Called then the selected hour is changed.
         *
         * @param oldValue The old hour value.
         * @param newValue The new hour value.
         */
        void onHourChanged(int oldValue, int newValue);

        /**
         * Called then the selected minute is changed.
         *
         * @param oldValue The old minute value.
         * @param newValue The new minute value.
         */
        void onMinuteChanged(int oldValue, int newValue);
    }

    private OnTimeChangedListener mOnTimeChangedListener;

    public TimePicker(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public TimePicker(Context context, AttrSet attrs) {
        super(context, attrs);

        init(context, attrs, 0, 0);
    }

    public TimePicker(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        mPaint = new Paint();
        mRect = new Rect();

        mBackgroundColor = ColorUtil.getColor(0xFF000000, 0.25f);
        mSelectionColor = 0xFF000000;

        initTickLabels();

        applyStyle(context, attrs, defStyleAttr, defStyleRes);

        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
        setTouchEventListener(this::onTouchEvent);
        setEstimateSizeListener(estimateSizeListener);
        addDrawTask(this::onDraw);
        setBindStateChangedListener(bindStateChangedListener);
        setLayoutRefreshedListener(refreshedListener);
    }

    /**
     * Init the localized label of ticks. The value of ticks in order:
     * 1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
     * "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "0",
     * "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "0"
     */
    private void initTickLabels() {
        String format = "%2d";
        mTicks = new String[36];
        for (int i = 0; i < 23; i++)
            mTicks[i] = String.format(format, i + 1);
        mTicks[23] = String.format(format, 0);
        mTicks[35] = mTicks[23];
        for (int i = 24; i < 35; i++)
            mTicks[i] = String.format(format, (i - 23) * 5);
    }

    public void applyStyle(int styleId) {
        ViewUtil.applyStyle(this, styleId);
        applyStyle(getContext(), null, 0, styleId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

        boolean hourDefined = false;
        String familyName = null;
        int style = -1;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("tp_backgroundColor"))
                mBackgroundColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("tp_selectionColor"))
                mSelectionColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("tp_selectionRadius"))
                mSelectionRadius = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_tickSize"))
                mTickSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_textSize"))
                mTextSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("tp_textColor"))
                mTextColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("tp_textHighlightColor"))
                mTextHighlightColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("tp_animDuration"))
                mAnimDuration = Util.getIntegerValue(attrs, name, 0);
            else if (name.equals("tp_mode"))
                setMode(Util.getIntegerValue(attrs, name, 0), false);
            else if (name.equals("tp_24Hour")) {
                set24Hour(Util.getBoolValue(attrs, name, false));
                hourDefined = true;
            } else if (name.equals("tp_hour"))
                setHour(Util.getIntegerValue(attrs, name, 0));
            else if (name.equals("tp_minute"))
                setMinute(Util.getIntegerValue(attrs, name, 0));
            else if (name.equals("tp_fontFamily"))
                familyName = Util.getStringValue(attrs, name, "");
            else if (name.equals("tp_textStyle"))
                style = Util.getIntegerValue(attrs, name, 0);
        }

        if (mSelectionRadius < 0)
            mSecondInnerRadius = AttrHelper.vp2px(8, context);

        if (mTickSize < 0)
            mTickSize = AttrHelper.vp2px(1, context);

        if (mTextSize < 0)
            mTextSize = 12;

        if (mAnimDuration < 0)
            mAnimDuration = 400;

        if (mInInterpolator == null)
            mInInterpolator = new DecelerateInterpolator();

        if (mOutInterpolator == null)
            mOutInterpolator = new DecelerateInterpolator();

        if (!hourDefined)
            set24Hour(false);
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener bindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(TimePicker.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(TimePicker.this::onThemeChanged);
                onThemeChanged(null);
            }
        }
    };

    public int getBackgroundColor() {
        return mBackgroundColor;
    }

    public int getSelectionColor() {
        return mSelectionColor;
    }

    public int getTextSize() {
        return mTextSize;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public int getTextHighlightColor() {
        return mTextHighlightColor;
    }

    public int getAnimDuration() {
        return mAnimDuration;
    }

    public Interpolator getInInterpolator() {
        return mInInterpolator;
    }

    public Interpolator getOutInterpolator() {
        return mOutInterpolator;
    }

    /**
     * @return The current select mode. Can be {@link #MODE_HOUR} or {@link #MODE_MINUTE}.
     */
    public int getMode() {
        return mMode;
    }

    /**
     * @return The selected hour value.
     */
    public int getHour() {
        return mHour;
    }

    /**
     * @return The selected minute value.
     */
    public int getMinute() {
        return mMinute;
    }

    /**
     * @return this TimePicker use 24-hour format or not.
     */
    public boolean is24Hour() {
        return m24Hour;
    }

    /**
     * Set the select mode of this TimePicker.
     *
     * @param mode      The select mode. Can be {@link #MODE_HOUR} or {@link #MODE_MINUTE}.
     * @param animation Indicate that should show animation when switch select mode or not.
     */
    public void setMode(int mode, boolean animation) {
        if (mMode != mode) {
            mMode = mode;

            if (mOnTimeChangedListener != null)
                mOnTimeChangedListener.onModeChanged(mMode);

            if (animation)
                startAnimation();
            else
                invalidate();
        }
    }

    /**
     * Set the selected hour value.
     *
     * @param hour The selected hour value.
     */
    public void setHour(int hour) {
        if (m24Hour)
            hour = Math.max(hour, 0) % 24;
        else
            hour = Math.max(hour, 0) % 12;

        if (mHour != hour) {
            int old = mHour;
            mHour = hour;

            if (mOnTimeChangedListener != null)
                mOnTimeChangedListener.onHourChanged(old, mHour);

            if (mMode == MODE_HOUR)
                invalidate();
        }
    }

    /**
     * Set the selected minute value.
     *
     * @param minute The selected minute value.
     */
    public void setMinute(int minute) {
        minute = Math.min(Math.max(minute, 0), 59);

        if (mMinute != minute) {
            int old = mMinute;
            mMinute = minute;

            if (mOnTimeChangedListener != null)
                mOnTimeChangedListener.onMinuteChanged(old, mMinute);

            if (mMode == MODE_MINUTE)
                invalidate();
        }
    }

    /**
     * Set a listener will be called when the selected time is changed.
     *
     * @param listener The {@link OnTimeChangedListener} will be called.
     */
    public void setOnTimeChangedListener(OnTimeChangedListener listener) {
        mOnTimeChangedListener = listener;
    }

    /**
     * Set this TimePicker use 24-hour format or not.
     *
     * @param b
     */
    public void set24Hour(boolean b) {
        if (m24Hour != b) {
            m24Hour = b;
            if (!m24Hour && mHour > 11)
                setHour(mHour - 12);
            calculateTextLocation();
        }
    }

    private float getAngle(int value, int mode) {
        switch (mode) {
            case MODE_HOUR:
                return (float) (-Math.PI / 2 + Math.PI / 6 * value);
            case MODE_MINUTE:
                return (float) (-Math.PI / 2 + Math.PI / 30 * value);
            default:
                return 0f;
        }
    }

    private int getSelectedTick(int value, int mode) {
        switch (mode) {
            case MODE_HOUR:
                return value == 0 ? (m24Hour ? 23 : 11) : value - 1;
            case MODE_MINUTE:
                if (value % 5 == 0)
                    return (value == 0) ? 35 : (value / 5 + 23);
            default:
                return -1;
        }
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);
            int widthSize = (widthMode == EstimateSpec.UNCONSTRAINT) ? mSelectionRadius * 12 : EstimateSpec.getSize(widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);
            int heightSize = (heightMode == EstimateSpec.UNCONSTRAINT) ? mSelectionRadius * 12 : EstimateSpec.getSize(heightMeasureSpec) - getPaddingTop() - getPaddingBottom();

            int size = Math.min(widthSize, heightSize);

            int width = (widthMode == EstimateSpec.PRECISE) ? widthSize : size;
            int height = (heightMode == EstimateSpec.PRECISE) ? heightSize : size;

            setEstimatedSize(width + getPaddingLeft() + getPaddingRight(), height + getPaddingTop() + getPaddingBottom());
            return false;
        }
    };

    private void calculateTextLocation() {
        if (mCenterPoint == null)
            return;

        double step = Math.PI / 6;
        double angle = -Math.PI / 3;
        float x, y;

        mPaint.setTextSize(mTextSize);
        mPaint.setTextAlign(TextAlignment.CENTER);

        if (m24Hour) {
            for (int i = 0; i < 12; i++) {
                mPaint.getTextBounds(mTicks[i]);
                if (i == 0)
                    mSecondInnerRadius = mInnerRadius - mSelectionRadius - mRect.getHeight();

                x = mCenterPoint.getPointX() + (float) Math.cos(angle) * mSecondInnerRadius;
                y = mCenterPoint.getPointY() + (float) Math.sin(angle) * mSecondInnerRadius;

                mLocations[i * 2] = x;
                mLocations[i * 2 + 1] = y + mRect.getHeight() / 2f;

                angle += step;
            }

            for (int i = 12; i < mTicks.length; i++) {
                x = mCenterPoint.getPointX() + (float) Math.cos(angle) * mInnerRadius;
                y = mCenterPoint.getPointY() + (float) Math.sin(angle) * mInnerRadius;

                mPaint.getTextBounds(mTicks[i]);
                mLocations[i * 2] = x;
                mLocations[i * 2 + 1] = y + mRect.getHeight() / 2f;

                angle += step;
            }
        } else {
            for (int i = 0; i < 12; i++) {
                x = mCenterPoint.getPointX() + (float) Math.cos(angle) * mInnerRadius;
                y = mCenterPoint.getPointY() + (float) Math.sin(angle) * mInnerRadius;

                mPaint.getTextBounds(mTicks[i]);
                mLocations[i * 2] = x;
                mLocations[i * 2 + 1] = y + mRect.getHeight() / 2f;

                angle += step;
            }

            for (int i = 24; i < mTicks.length; i++) {
                x = mCenterPoint.getPointX() + (float) Math.cos(angle) * mInnerRadius;
                y = mCenterPoint.getPointY() + (float) Math.sin(angle) * mInnerRadius;

                mPaint.getTextBounds(mTicks[i]);
                mLocations[i * 2] = x;
                mLocations[i * 2 + 1] = y + mRect.getHeight() / 2f;

                angle += step;
            }
        }

    }

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            measureComponent(component);
        }
    };

    private void measureComponent(Component component) {
        int left = getPaddingLeft();
        int top = getPaddingTop();
        int size = Math.min(component.getWidth() - getPaddingLeft() - getPaddingRight(), component.getHeight() - getPaddingTop() - getPaddingBottom());

        if (mCenterPoint == null)
            mCenterPoint = new Point();

        mOuterRadius = size / 2f;
        mCenterPoint.modify(left + mOuterRadius, top + mOuterRadius);
        mInnerRadius = mOuterRadius - mSelectionRadius - AttrHelper.vp2px(4, getContext());

        calculateTextLocation();
    }

    private int getPointedValue(float x, float y, boolean isDown) {
        float radius = (float) Math.sqrt(Math.pow(x - mCenterPoint.getPointX(), 2) + Math.pow(y - mCenterPoint.getPointY(), 2));
        if (isDown) {
            if (mMode == MODE_HOUR && m24Hour) {
                if (radius > mInnerRadius + mSelectionRadius || radius < mSecondInnerRadius - mSelectionRadius)
                    return -1;
            } else if (radius > mInnerRadius + mSelectionRadius || radius < mInnerRadius - mSelectionRadius)
                return -1;
        }

        float angle = (float) Math.atan2(y - mCenterPoint.getPointY(), x - mCenterPoint.getPointX());
        if (angle < 0)
            angle += Math.PI * 2;

        if (mMode == MODE_HOUR) {
            if (m24Hour) {
                if (radius > mSecondInnerRadius + mSelectionRadius / 2) {
                    int value = (int) Math.round(angle * 6 / Math.PI) + 15;
                    if (value == 24)
                        return 0;
                    else if (value > 24)
                        return value - 12;
                    else
                        return value;
                } else {
                    int value = (int) Math.round(angle * 6 / Math.PI) + 3;
                    return value > 12 ? value - 12 : value;
                }
            } else {
                int value = (int) Math.round(angle * 6 / Math.PI) + 3;
                return value > 11 ? value - 12 : value;
            }
        } else if (mMode == MODE_MINUTE) {
            int value = (int) Math.round(angle * 30 / Math.PI) + 15;
            return value > 59 ? value - 60 : value;
        }

        return -1;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                int value = getPointedValue(x, y, true);
                if (value < 0)
                    return false;
                else if (mMode == MODE_HOUR)
                    setHour(value);
                else if (mMode == MODE_MINUTE)
                    setMinute(value);
                mEdited = true;
                return true;
            case TouchEvent.POINT_MOVE:
                value = getPointedValue(x, y, false);
                if (value < 0)
                    return true;
                else if (mMode == MODE_HOUR)
                    setHour(value);
                else if (mMode == MODE_MINUTE)
                    setMinute(value);
                mEdited = true;
                return true;
            case TouchEvent.PRIMARY_POINT_UP:
                if (mEdited && mMode == MODE_HOUR) {
                    setMode(MODE_MINUTE, true);
                    mEdited = false;
                    return true;
                }
                break;
            case TouchEvent.CANCEL:
                mEdited = false;
                break;
        }

        return false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        measureComponent(component);
        mPaint.setColor(new Color(mBackgroundColor));
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(mCenterPoint.getPointX(), mCenterPoint.getPointY(), mOuterRadius, mPaint);

        if (!mRunning) {
            float angle;
            int selectedTick;
            int start;
            int length;
            float radius;

            if (mMode == MODE_HOUR) {
                angle = getAngle(mHour, MODE_HOUR);
                selectedTick = getSelectedTick(mHour, MODE_HOUR);
                start = 0;
                length = m24Hour ? 24 : 12;
                radius = m24Hour && selectedTick < 12 ? mSecondInnerRadius : mInnerRadius;
            } else {
                angle = getAngle(mMinute, MODE_MINUTE);
                selectedTick = getSelectedTick(mMinute, MODE_MINUTE);
                start = 24;
                length = 12;
                radius = mInnerRadius;
            }

            mPaint.setColor(new Color(mSelectionColor));
            float x = mCenterPoint.getPointX() + (float) Math.cos(angle) * radius;
            float y = mCenterPoint.getPointY() + (float) Math.sin(angle) * radius;
            canvas.drawCircle(x, y, mSelectionRadius, mPaint);

            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeWidth(mTickSize);
            x -= (float) Math.cos(angle) * mSelectionRadius;
            y -= (float) Math.sin(angle) * mSelectionRadius;
            canvas.drawLine(new Point(mCenterPoint.getPointX(), mCenterPoint.getPointY()), new Point(x, y), mPaint);

            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setColor(new Color(mTextColor));
            canvas.drawCircle(mCenterPoint.getPointX(), mCenterPoint.getPointY(), mTickSize * 2, mPaint);

            mPaint.setTextSize(mTextSize);
            mPaint.setTextAlign(TextAlignment.CENTER);

            int index;
            for (int i = 0; i < length; i++) {
                index = start + i;
                mPaint.setColor(new Color(index == selectedTick ? mTextHighlightColor : mTextColor));
                canvas.drawText(mPaint, mTicks[index], mLocations[index * 2], mLocations[index * 2 + 1]);
            }
        } else {
            float maxOffset = mOuterRadius - mInnerRadius + mTextSize / 2;
            int textOutColor = ColorUtil.getColor(mTextColor, 1f - mAnimProgress);
            int textHighlightOutColor = ColorUtil.getColor(mTextHighlightColor, 1f - mAnimProgress);
            int textInColor = ColorUtil.getColor(mTextColor, mAnimProgress);
            int textHighlightInColor = ColorUtil.getColor(mTextHighlightColor, mAnimProgress);
            float outOffset;
            float inOffset;
            float outAngle;
            float inAngle;
            int outStart;
            int inStart;
            int outLength;
            int inLength;
            int outSelectedTick;
            int inSelectedTick;
            float outRadius;
            float inRadius;

            if (mMode == MODE_MINUTE) {
                outAngle = getAngle(mHour, MODE_HOUR);
                inAngle = getAngle(mMinute, MODE_MINUTE);
                outOffset = mOutInterpolator.getInterpolation(mAnimProgress) * maxOffset;
                inOffset = (1f - mInInterpolator.getInterpolation(mAnimProgress)) * -maxOffset;
                outSelectedTick = getSelectedTick(mHour, MODE_HOUR);
                inSelectedTick = getSelectedTick(mMinute, MODE_MINUTE);
                outStart = 0;
                outLength = m24Hour ? 24 : 12;
                outRadius = m24Hour && outSelectedTick < 12 ? mSecondInnerRadius : mInnerRadius;
                inStart = 24;
                inLength = 12;
                inRadius = mInnerRadius;
            } else {
                outAngle = getAngle(mMinute, MODE_MINUTE);
                inAngle = getAngle(mHour, MODE_HOUR);
                outOffset = mOutInterpolator.getInterpolation(mAnimProgress) * -maxOffset;
                inOffset = (1f - mInInterpolator.getInterpolation(mAnimProgress)) * maxOffset;
                outSelectedTick = getSelectedTick(mMinute, MODE_MINUTE);
                inSelectedTick = getSelectedTick(mHour, MODE_HOUR);
                outStart = 24;
                outLength = 12;
                outRadius = mInnerRadius;
                inStart = 0;
                inLength = m24Hour ? 24 : 12;
                inRadius = m24Hour && inSelectedTick < 12 ? mSecondInnerRadius : mInnerRadius;
            }

            mPaint.setColor(new Color(ColorUtil.getColor(mSelectionColor, 1f - mAnimProgress)));
            float x = mCenterPoint.getPointX() + (float) Math.cos(outAngle) * (outRadius + outOffset);
            float y = mCenterPoint.getPointY() + (float) Math.sin(outAngle) * (outRadius + outOffset);
            canvas.drawCircle(x, y, mSelectionRadius, mPaint);

            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeWidth(mTickSize);
            x -= (float) Math.cos(outAngle) * mSelectionRadius;
            y -= (float) Math.sin(outAngle) * mSelectionRadius;
            canvas.drawLine(new Point(mCenterPoint.getPointX(), mCenterPoint.getPointY()), new Point(x, y), mPaint);

            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setColor(new Color(ColorUtil.getColor(mSelectionColor, mAnimProgress)));
            x = mCenterPoint.getPointX() + (float) Math.cos(inAngle) * (inRadius + inOffset);
            y = mCenterPoint.getPointY() + (float) Math.sin(inAngle) * (inRadius + inOffset);
            canvas.drawCircle(x, y, mSelectionRadius, mPaint);

            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeWidth(mTickSize);
            x -= (float) Math.cos(inAngle) * mSelectionRadius;
            y -= (float) Math.sin(inAngle) * mSelectionRadius;
            canvas.drawLine(new Point(mCenterPoint.getPointX(), mCenterPoint.getPointY()), new Point(x, y), mPaint);

            mPaint.setStyle(Paint.Style.FILL_STYLE);
            mPaint.setColor(new Color(mTextColor));
            canvas.drawCircle(mCenterPoint.getPointX(), mCenterPoint.getPointY(), mTickSize * 2, mPaint);

            mPaint.setTextSize(mTextSize);
            mPaint.setTextAlign(TextAlignment.CENTER);

            double step = Math.PI / 6;
            double angle = -Math.PI / 3;
            int index;

            for (int i = 0; i < outLength; i++) {
                index = i + outStart;
                x = mLocations[index * 2] + (float) Math.cos(angle) * outOffset;
                y = mLocations[index * 2 + 1] + (float) Math.sin(angle) * outOffset;
                mPaint.setColor(new Color(index == outSelectedTick ? textHighlightOutColor : textOutColor));
                canvas.drawText(mPaint, mTicks[index], x, y);
                angle += step;
            }

            for (int i = 0; i < inLength; i++) {
                index = i + inStart;
                x = mLocations[index * 2] + (float) Math.cos(angle) * inOffset;
                y = mLocations[index * 2 + 1] + (float) Math.sin(angle) * inOffset;
                mPaint.setColor(new Color(index == inSelectedTick ? textHighlightInColor : textInColor));
                canvas.drawText(mPaint, mTicks[index], x, y);
                angle += step;
            }
        }
    }

    private void resetAnimation() {
        mStartTime = Time.getRealActiveTime();
        mAnimProgress = 0f;
    }

    private void startAnimation() {
        if (getHandler() != null) {
            resetAnimation();
            mRunning = true;
            getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
        }

        invalidate();
    }

    private void stopAnimation() {
        mRunning = false;
        mAnimProgress = 1f;
        if (getHandler() != null)
            getHandler().removeTask(mUpdater);
        invalidate();
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = Time.getRealActiveTime();
        mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (mAnimProgress == 1f)
            stopAnimation();

        if (mRunning) {
            if (getHandler() != null)
                getHandler().postTask(mUpdater, ViewUtil.FRAME_DURATION);
            else
                stopAnimation();
        }

        invalidate();
    }

    private EventHandler getHandler() {
        return mHandler;
    }
}
