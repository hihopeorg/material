/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rey.material.widget;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.util.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DatePickerLayout extends FrameLayout implements DatePicker.OnDateChangedListener, Component.DrawTask {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "DatePickerLayout");
    private DatePicker mDatePicker;

    private int mHeaderPrimaryHeight;
    private int mHeaderPrimaryColor;
    private int mHeaderSecondaryHeight;
    private int mHeaderSecondaryColor;
    private int mHeaderPrimaryTextSize;
    private int mHeaderSecondaryTextSize;
    private int mTextHeaderColor = 0xFF000000;

    private Paint mPaint;
    private int mHeaderPrimaryRealHeight;
    private int mHeaderRealWidth;
    private RectFloat mRect;
    private Path mHeaderSecondaryBackground;

    private int mPadding;

    private boolean mDaySelectMode = true;

    private boolean mMonthFirst = true;
    private boolean mLocationDirty = true;

    private String mWeekDay;
    private String mMonth;
    private String mDay;
    private String mYear;

    private float mBaseX;
    private float mWeekDayY;
    private float mMonthY;
    private float mDayY;
    private float mYearY;
    private float mFirstWidth;
    private float mCenterY;
    private float mSecondWidth;

    private static final String BASE_TEXT = "0";
    private static final String DAY_FORMAT = "%2d";
    private static final String YEAR_FORMAT = "%4d";

    protected int mMinDay;
    protected int mMinMonth;
    protected int mMinYear;
    protected int mMaxDay;
    protected int mMaxMonth;
    protected int mMaxYear;
    protected int mToDay;
    protected int mToMonth;
    protected int mToYear;

    private float mCornerRadius;

    public interface OnDateChangedListener {

        /**
         * Called when the selected date is changed.
         *
         * @param oldDay   The day value of old date.
         * @param oldMonth The month value of old date.
         * @param oldYear  The year value of old date.
         * @param newDay   The day value of new date.
         * @param newMonth The month value of new date.
         * @param newYear  The year value of new date.
         */
        void onDateChanged(int oldDay, int oldMonth, int oldYear, int newDay, int newMonth, int newYear);
    }

    private OnDateChangedListener mOnDateChangedListener;

    public DatePickerLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);

        Calendar cal = Calendar.getInstance();
        mToDay = cal.get(Calendar.DAY_OF_MONTH);
        mToMonth = cal.get(Calendar.MONTH);
        mToYear = cal.get(Calendar.YEAR);
        mMinDay = mToDay;
        mMinMonth = mToMonth;
        mMinYear = mToYear - 12;
        mMaxDay = mToDay;
        mMaxMonth = mToMonth;
        mMaxYear = mToYear + 12;

        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setTextAlign(TextAlignment.CENTER);
        mRect = new RectFloat();
        mHeaderSecondaryBackground = new Path();
        mPadding = AttrHelper.vp2px(8, context);

        mDatePicker = new DatePicker(context, attrSet);
        mDatePicker.setContentPadding(mPadding, mPadding, mPadding, mPadding);
        mDatePicker.setOnDateChangedListener(this);

        addComponent(mDatePicker);

        mDatePicker.setVisibility(mDaySelectMode ? Component.VISIBLE : Component.HIDE);

        mMonthFirst = isMonthFirst();

        mHeaderPrimaryHeight = AttrHelper.vp2px(144, context);
        mHeaderSecondaryHeight = AttrHelper.vp2px(32, context);
        mHeaderPrimaryTextSize = AttrHelper.fp2px(45, context);
        mHeaderSecondaryTextSize = AttrHelper.fp2px(24, context);

        applyStyle(context, attrSet);
        setLayoutRefreshedListener(refreshedListener);
        setEstimateSizeListener(estimateSizeListener);
        setArrangeListener(arrangeListener);
        addDrawTask(this::onDraw);

        setDateRange(mMinDay, mMinMonth, mMinYear, mMaxDay, mMaxMonth, mMaxYear);
        setDate(mToDay, mToMonth, mToYear);

    }

    private boolean isMonthFirst() {
        SimpleDateFormat format = (SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.FULL);
        String pattern = format.toLocalizedPattern();

        return pattern.indexOf("M") < pattern.indexOf("d");
    }

    public void setDateSelectMode(boolean enable) {
        if (mDaySelectMode != enable) {
            mDaySelectMode = enable;

            if (mDaySelectMode) {
                mDatePicker.goTo(mDatePicker.getMonth(), mDatePicker.getYear());
                animIn(mDatePicker);
            } else {
                animOut(mDatePicker);
            }

            invalidate();
        }
    }

    private void animOut(final Component v) {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.alphaFrom(1f);
        animatorProperty.alpha(0f);
        animatorProperty.setDuration(400);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                v.setVisibility(Component.HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorProperty.start();
    }

    private void animIn(final Component v) {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.alphaFrom(0f);
        animatorProperty.alpha(1f);
        animatorProperty.setDuration(400);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                v.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                v.setVisibility(Component.HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorProperty.start();
    }

    public void applyStyle(Context context, AttrSet attrs) {
        mDatePicker.applyStyle(context, attrs, 0, 0);

        mHeaderPrimaryColor = mDatePicker.getSelectionColor();
        mHeaderSecondaryColor = mHeaderPrimaryColor;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("dp_headerPrimaryHeight"))
                mHeaderPrimaryHeight = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("dp_headerSecondaryHeight"))
                mHeaderSecondaryHeight = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("dp_headerPrimaryColor"))
                mHeaderPrimaryColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_headerSecondaryColor"))
                mHeaderSecondaryColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
            else if (name.equals("dp_headerPrimaryTextSize"))
                mHeaderPrimaryTextSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("dp_headerSecondaryTextSize"))
                mHeaderSecondaryTextSize = Util.getDimensionValue(attrs, name, 0);
            else if (name.equals("dp_textHeaderColor"))
                mTextHeaderColor = Util.getColorValue(attrs, name, new Color(0)).getValue();
        }
    }

    public void setDateRange(int minDay, int minMonth, int minYear, int maxDay, int maxMonth, int maxYear) {
        mDatePicker.setDateRange(minDay, minMonth, minYear, maxDay, maxMonth, maxYear);
    }

    public void setDateRange(long minTime, long maxTime) {
        Calendar cal = mDatePicker.getCalendar();
        cal.setTimeInMillis(minTime);
        int minDay = cal.get(Calendar.DAY_OF_MONTH);
        int minMonth = cal.get(Calendar.MONTH);
        int minYear = cal.get(Calendar.YEAR);
        cal.setTimeInMillis(maxTime);
        int maxDay = cal.get(Calendar.DAY_OF_MONTH);
        int maxMonth = cal.get(Calendar.MONTH);
        int maxYear = cal.get(Calendar.YEAR);

        setDateRange(minDay, minMonth, minYear, maxDay, maxMonth, maxYear);
    }

    public void setDate(int day, int month, int year) {
        mDatePicker.setDate(day, month, year);
    }

    public void setDate(long time) {
        Calendar cal = mDatePicker.getCalendar();
        cal.setTimeInMillis(time);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        mDatePicker.setDate(day, month, year);
    }

    public int getDay() {
        return mDatePicker.getDay();
    }

    public int getMonth() {
        return mDatePicker.getMonth();
    }

    public int getYear() {
        return mDatePicker.getYear();
    }

    public String getFormattedDate(DateFormat formatter) {
        return mDatePicker.getFormattedDate(formatter);
    }

    public Calendar getCalendar() {
        return mDatePicker.getCalendar();
    }

    @Override
    public void onDateChanged(int oldDay, int oldMonth, int oldYear, int newDay, int newMonth, int newYear) {
        if (newDay < 0 || newMonth < 0 || newYear < 0) {
            mWeekDay = null;
            mMonth = null;
            mDay = null;
            mYear = null;
        } else {
            Calendar cal = mDatePicker.getCalendar();
            cal.set(Calendar.YEAR, newYear);
            cal.set(Calendar.MONTH, newMonth);
            cal.set(Calendar.DAY_OF_MONTH, newDay);

            mWeekDay = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
            mMonth = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
            mDay = String.format(DAY_FORMAT, newDay);
            mYear = String.format(YEAR_FORMAT, newYear);

            if (oldMonth != newMonth || oldYear != newYear) {
                mDatePicker.goTo(newMonth, newYear);
            }
        }

        mLocationDirty = true;
        invalidate();

        if (mOnDateChangedListener != null)
            mOnDateChangedListener.onDateChanged(oldDay, oldMonth, oldYear, newDay, newMonth, newYear);
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {

            int widthSize = EstimateSpec.getSize(widthMeasureSpec);
            int widthMode = EstimateSpec.getMode(widthMeasureSpec);
            int heightSize = EstimateSpec.getSize(heightMeasureSpec);
            int heightMode = EstimateSpec.getMode(heightMeasureSpec);

            boolean isPortrait = true;
            if (isPortrait) {
                if (heightMode == EstimateSpec.NOT_EXCEED) {
                    int ws = EstimateSpec.getSizeWithMode(widthSize, EstimateSpec.PRECISE);
                    int hs = EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT);
                    mDatePicker.estimateSize(ws, hs);
                } else {
                    int height = Math.max(heightSize - mHeaderSecondaryHeight - mHeaderPrimaryHeight, mDatePicker.getEstimatedHeight());
                    int ws = EstimateSpec.getSizeWithMode(widthSize, EstimateSpec.PRECISE);
                    mDatePicker.estimateSize(ws, EstimateSpec.getSizeWithMode(height, EstimateSpec.PRECISE));
                }

                setEstimatedSize(widthSize, heightSize);
            } else {
                if (heightMode == EstimateSpec.NOT_EXCEED) {
                    int ws = EstimateSpec.getSizeWithMode(widthSize / 2, EstimateSpec.PRECISE);
                    int hs = EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT);
                    mDatePicker.estimateSize(ws, hs);
                } else {
                    int height = Math.max(heightSize, mDatePicker.getEstimatedHeight());
                    int ws = EstimateSpec.getSizeWithMode(widthSize / 2, EstimateSpec.PRECISE);
                    mDatePicker.estimateSize(ws, EstimateSpec.getSizeWithMode(height, EstimateSpec.PRECISE));
                }

                setEstimatedSize(widthSize, heightSize);
            }
            return true;
        }
    };

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            measureComponent(component);
        }
    };

    private void measureComponent(Component component) {
        boolean isPortrait = true;
        if (isPortrait) {
            mHeaderRealWidth = component.getWidth();
            mHeaderPrimaryRealHeight = component.getHeight() - mHeaderSecondaryHeight - mDatePicker.getEstimatedHeight();
            mHeaderSecondaryBackground.reset();
            if (mCornerRadius == 0)
                mHeaderSecondaryBackground.addRect(0, 0, mHeaderRealWidth, mHeaderSecondaryHeight, Path.Direction.CLOCK_WISE);
            else {
                mHeaderSecondaryBackground.moveTo(0, mHeaderSecondaryHeight);
                mHeaderSecondaryBackground.lineTo(0, mCornerRadius);
                mRect.modify(0, 0, mCornerRadius * 2, mCornerRadius * 2);
                mHeaderSecondaryBackground.arcTo(mRect, 180f, 90f, false);
                mHeaderSecondaryBackground.lineTo(mHeaderRealWidth - mCornerRadius, 0);
                mRect.modify(mHeaderRealWidth - mCornerRadius * 2, 0, mHeaderRealWidth, mCornerRadius * 2);
                mHeaderSecondaryBackground.arcTo(mRect, 270f, 90f, false);
                mHeaderSecondaryBackground.lineTo(mHeaderRealWidth, mHeaderSecondaryHeight);
                mHeaderSecondaryBackground.close();
            }
        } else {
            mHeaderRealWidth = component.getWidth() - mDatePicker.getEstimatedWidth();
            mHeaderPrimaryRealHeight = component.getHeight() - mHeaderSecondaryHeight;
            mHeaderSecondaryBackground.reset();
            if (mCornerRadius == 0)
                mHeaderSecondaryBackground.addRect(0, 0, mHeaderRealWidth, mHeaderSecondaryHeight, Path.Direction.CLOCK_WISE);
            else {
                mHeaderSecondaryBackground.moveTo(0, mHeaderSecondaryHeight);
                mHeaderSecondaryBackground.lineTo(0, mCornerRadius);
                mRect.modify(0, 0, mCornerRadius * 2, mCornerRadius * 2);
                mHeaderSecondaryBackground.arcTo(mRect, 180f, 90f, false);
                mHeaderSecondaryBackground.lineTo(mHeaderRealWidth, 0);
                mHeaderSecondaryBackground.lineTo(mHeaderRealWidth, mHeaderSecondaryHeight);
                mHeaderSecondaryBackground.close();
            }
        }
    }

    ArrangeListener arrangeListener = new ArrangeListener() {
        @Override
        public boolean onArrange(int left, int top, int width, int height) {
            int right = left + width;
            int bottom = top + height;

            int childLeft = 0;
            int childTop = 0;
            int childRight = right - left;
            int childBottom = bottom - top;

            boolean isPortrait = true;

            if (isPortrait)
                childTop += mHeaderPrimaryRealHeight + mHeaderSecondaryHeight;
            else
                childLeft += mHeaderRealWidth;

            mDatePicker.arrange(childLeft, childTop, childRight - childLeft, childBottom - childTop);

            return true;
        }
    };

    private void measureHeaderText() {
        if (!mLocationDirty)
            return;

        if (mWeekDay == null) {
            mLocationDirty = false;
            return;
        }

        mBaseX = mHeaderRealWidth / 2f;

        mPaint.setTextSize(mDatePicker.getTextSize());
        Rect weekBounds = mPaint.getTextBounds(mWeekDay);
        int height = weekBounds.getHeight();
        mWeekDayY = (mHeaderSecondaryHeight + height) / 2f;

        mPaint.setTextSize(mHeaderPrimaryTextSize);
        Rect dayBounds = mPaint.getTextBounds(mDay);
        int primaryTextHeight = dayBounds.getHeight();
        if (mMonthFirst)
            mFirstWidth = mPaint.measureText(mDay);
        else
            mFirstWidth = mPaint.measureText(mMonth);

        mPaint.setTextSize(mHeaderSecondaryTextSize);
        Rect monthBounds = mPaint.getTextBounds(mMonth);
        int secondaryTextHeight = monthBounds.getHeight();
        if (mMonthFirst)
            mFirstWidth = Math.max(mFirstWidth, mPaint.measureText(mMonth));
        else
            mFirstWidth = Math.max(mFirstWidth, mPaint.measureText(mDay));
        mSecondWidth = mPaint.measureText(mYear);

        mCenterY = mHeaderSecondaryHeight + (mHeaderPrimaryRealHeight + primaryTextHeight) / 2f;
        float y = ((mHeaderPrimaryRealHeight - primaryTextHeight) / 2f + secondaryTextHeight) / 2f;
        float aboveY = mHeaderSecondaryHeight + y;
        float belowY = mCenterY + y;

        if (mMonthFirst) {
            mDayY = mCenterY;
            mMonthY = aboveY;
        } else {
            mMonthY = mCenterY;
            mDayY = aboveY;
        }

        mYearY = belowY;

        mLocationDirty = false;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        measureComponent(component);
        mPaint.setColor(new Color(mHeaderSecondaryColor));
        canvas.drawPath(mHeaderSecondaryBackground, mPaint);
        mPaint.setColor(new Color(mHeaderPrimaryColor));
        canvas.drawRect(0, mHeaderSecondaryHeight, mHeaderRealWidth, mHeaderPrimaryRealHeight + mHeaderSecondaryHeight, mPaint);

        measureHeaderText();

        if (mWeekDay == null)
            return;

        mPaint.setTextSize(mDatePicker.getTextSize());
        mPaint.setColor(new Color(mDatePicker.getTextHighlightColor()));

        canvas.drawText(mPaint, mWeekDay, mBaseX, mWeekDayY);

        mPaint.setColor(new Color(mDaySelectMode ? mDatePicker.getTextHighlightColor() : mTextHeaderColor));
        mPaint.setTextSize(mHeaderPrimaryTextSize);
        if (mMonthFirst)
            canvas.drawText(mPaint, mDay, mBaseX, mDayY);
        else
            canvas.drawText(mPaint, mMonth, mBaseX, mMonthY);

        mPaint.setTextSize(mHeaderSecondaryTextSize);
        if (mMonthFirst)
            canvas.drawText(mPaint, mMonth, mBaseX, mMonthY);
        else
            canvas.drawText(mPaint, mDay, mBaseX, mDayY);

        mPaint.setColor(new Color(mDaySelectMode ? mTextHeaderColor : mDatePicker.getTextHighlightColor()));
        canvas.drawText(mPaint, mYear, mBaseX, mYearY);
    }

    private boolean isTouched(float left, float top, float right, float bottom, float x, float y) {
        return x >= left && x <= right && y >= top && y <= bottom;
    }

}
