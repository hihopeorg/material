package com.rey.material.widget;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import com.rey.material.app.ThemeManager;
import com.rey.material.drawable.DecelerateInterpolator;
import com.rey.material.drawable.Interpolator;
import com.rey.material.drawable.LineMorphingDrawable;
import com.rey.material.drawable.OvalShadowDrawable;
import com.rey.material.util.Util;
import com.rey.material.util.ViewUtil;

public class FloatingActionButton extends Component implements ThemeManager.OnThemeChangedListener, Component.TouchEventListener, Component.DrawTask {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00101, "FloatingActionButton");
    private OvalShadowDrawable mBackground;
    private Element mIcon;
    private Element mPrevIcon;
    private int mAnimDuration = -1;
    private Interpolator mInterpolator;
    private SwitchIconAnimator mSwitchIconAnimator;
    private int mIconSize = -1;

    private RippleManager mRippleManager;
    protected int mStyleId;
    protected int mCurrentStyle = ThemeManager.THEME_UNDEFINED;

    private EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    public FloatingActionButton(Context context) {
        super(context);

        init(context, null, 0, 0);
    }

    public FloatingActionButton(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public FloatingActionButton(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    protected void init(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {
        setClickable(true);
        mSwitchIconAnimator = new SwitchIconAnimator();
        applyStyle(context, attrs, defStyleAttr, defStyleRes);

        mStyleId = ThemeManager.getStyleId(context, attrs, defStyleAttr, defStyleRes);
        setEstimateSizeListener(estimateSizeListener);
        addDrawTask(this::onDraw);
        setTouchEventListener(this::onTouchEvent);
        setLayoutRefreshedListener(refreshedListener);
    }

    public void applyStyle(int resId) {
        ViewUtil.applyStyle(this, resId);
        applyStyle(getContext(), null, 0, resId);
    }

    protected void applyStyle(Context context, AttrSet attrs, int defStyleAttr, int defStyleRes) {

        int radius = -1;
        int elevation = -1;
        Color bgColor = null;
        int bgAnimDuration = -1;
        Element iconSrc = null;
        int iconLineMorphing = 0;

        int count = attrs != null ? attrs.getLength() : 0;
        for (int i = 0; i < count; i++) {
            Attr attr = attrs.getAttr(i).get();
            String name = attr.getName();
            if (name.equals("fab_radius")) {
                radius = Util.getDimensionValue(attrs, name, 0);
            } else if (name.equals("fab_elevation")) {
                elevation = Util.getDimensionValue(attrs, name, 0);
            } else if (name.equals("fab_backgroundColor")) {
                bgColor = Util.getColorValue(attrs, name, new Color(0));
            } else if (name.equals("fab_backgroundAnimDuration")) {
                bgAnimDuration = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("fab_iconSrc")) {
                iconSrc = Util.getElement(attrs, name);
            } else if (name.equals("fab_iconLineMorphing")) {
                iconLineMorphing = Util.getIntegerValue(attrs, name, 0);
            } else if (name.equals("fab_iconSize")) {
                mIconSize = Util.getDimensionValue(attrs, name, 0);
            } else if (name.equals("fab_animDuration")) {
                mAnimDuration = Util.getIntegerValue(attrs, name, 0);
            }
        }

        if (mIconSize < 0)
            mIconSize = AttrHelper.vp2px(24, context);

        if (mAnimDuration < 0)
            mAnimDuration = 400;

        if (mInterpolator == null)
            mInterpolator = new DecelerateInterpolator();

        if (mBackground == null) {
            if (radius < 0)
                radius = AttrHelper.vp2px(28, context);

            if (elevation < 0)
                elevation = AttrHelper.vp2px(4, context);

            if (bgColor == null)
                bgColor = new Color(0);

            if (bgAnimDuration < 0)
                bgAnimDuration = 0;

            mBackground = new OvalShadowDrawable(this, radius, bgColor, elevation, elevation, bgAnimDuration);
            mBackground.setBounds(0, 0, getWidth(), getHeight());
        } else {
            if (radius >= 0)
                mBackground.setRadius(radius);

            if (bgColor != null)
                mBackground.setColor(bgColor);

            if (elevation >= 0)
                mBackground.setShadow(elevation, elevation);

            if (bgAnimDuration >= 0)
                mBackground.setAnimationDuration(bgAnimDuration);
        }
        if (iconLineMorphing != 0) {
            Element element = new LineMorphingDrawable.Builder(this, context, attrs, iconLineMorphing).build();
            setIcon(element, false);
        } else if (iconSrc != null) {
            setIcon(iconSrc, false);
        }
    }

    @Override
    public void onThemeChanged(ThemeManager.OnThemeChangedEvent event) {
        int style = ThemeManager.getInstance().getCurrentStyle(mStyleId);
        if (mCurrentStyle != style) {
            mCurrentStyle = style;
            applyStyle(mCurrentStyle);
        }
    }

    BindStateChangedListener mBindStateChangedListener = new BindStateChangedListener() {
        @Override
        public void onComponentBoundToWindow(Component component) {
            if (mStyleId != 0) {
                ThemeManager.getInstance().registerOnThemeChangedListener(FloatingActionButton.this::onThemeChanged);
                onThemeChanged(null);
            }
        }

        @Override
        public void onComponentUnboundFromWindow(Component component) {
            RippleManager.cancelRipple(FloatingActionButton.this);
            if (mStyleId != 0)
                ThemeManager.getInstance().unregisterOnThemeChangedListener(FloatingActionButton.this::onThemeChanged);
        }
    };


    /**
     * @return The radius of the button.
     */
    public int getRadius() {
        return mBackground.getRadius();
    }

    /**
     * Set radius of the button.
     *
     * @param radius The radius in pixel.
     */
    public void setRadius(int radius) {
        if (mBackground.setRadius(radius))
            postLayout();
    }

    /**
     * @return The line state of LineMorphingDrawable that is used as this button's icon.
     */
    public int getLineMorphingState() {
        if (mIcon != null && mIcon instanceof LineMorphingDrawable)
            return ((LineMorphingDrawable) mIcon).getLineState();

        return -1;
    }

    /**
     * Set the line state of LineMorphingDrawable that is used as this button's icon.
     *
     * @param state     The line state.
     * @param animation Indicate should show animation when switch line state or not.
     */
    public void setLineMorphingState(int state, boolean animation) {
        if (mIcon != null && mIcon instanceof LineMorphingDrawable)
            ((LineMorphingDrawable) mIcon).switchLineState(state, animation);
    }

    /**
     * @return The drawable is used as this button's icon.
     */
    public Element getIcon() {
        return mIcon;
    }

    /**
     * Set the drawable that is used as this button's icon.
     *
     * @param icon      The drawable.
     * @param animation Indicate should show animation when switch drawable or not.
     */
    public void setIcon(Element icon, boolean animation) {
        if (icon == null)
            return;

        if (animation) {
            mSwitchIconAnimator.startAnimation(icon);
            invalidate();
        } else {
            if (mIcon != null) {
                mIcon.setCallback(null);
            }

            mIcon = icon;
            float half = mIconSize / 2f;
            mIcon.setBounds((int) (getWidth() / 2 - half), (int) (getHeight() / 2 - half), (int) (getWidth() / 2 + half), (int) (getHeight() / 2 + half));
            invalidate();
        }
    }

    /**
     * Show this button at the specific location. If this button isn't attached to any parent view yet,
     * it will be add to activity's root view. If not, it will just update the location.
     *
     * @param ability The activity that this button will be attached to.
     * @param x       The x value of anchor point.
     * @param y       The y value of anchor point.
     * @param gravity The gravity apply with this button.
     * @see LayoutAlignment
     */
    public void show(Ability ability, int x, int y, int gravity) {
        if (getComponentParent() == null) {
            StackLayout.LayoutConfig params = new StackLayout.LayoutConfig(mBackground.getIntrinsicWidth(), mBackground.getIntrinsicHeight());
            updateParams(x, y, gravity, params);

        } else
            updateLocation(x, y, gravity);
    }

    /**
     * Show this button at the specific location. If this button isn't attached to any parent view yet,
     * it will be add to activity's root view. If not, it will just update the location.
     *
     * @param parent  The parent view. Should be {@link StackLayout} or {@link DependentLayout}
     * @param x       The x value of anchor point.
     * @param y       The y value of anchor point.
     * @param gravity The gravity apply with this button.
     * @see LayoutAlignment
     */
    public void show(ComponentContainer parent, int x, int y, int gravity) {
        if (getComponentParent() == null) {
            ComponentContainer.LayoutConfig params = parent.createLayoutConfig((Context) this, null);
            params.width = mBackground.getIntrinsicWidth();
            params.height = mBackground.getIntrinsicHeight();
            updateParams(x, y, gravity, params);

            parent.addComponent(this, params);
        } else
            updateLocation(x, y, gravity);
    }

    /**
     * Update the location of this button. This method only work if it's already attached to a parent view.
     *
     * @param x       The x value of anchor point.
     * @param y       The y value of anchor point.
     * @param gravity The gravity apply with this button.
     * @see LayoutAlignment
     */
    public void updateLocation(int x, int y, int gravity) {
        if (getComponentParent() != null)
            updateParams(x, y, gravity, getLayoutConfig());
        else
            HiLog.warn(LABEL, "updateLocation() is called without parent");

    }

    private void updateParams(int x, int y, int gravity, ComponentContainer.LayoutConfig params) {
        int horizontalGravity = gravity & LayoutAlignment.HORIZONTAL_CENTER;

        switch (horizontalGravity) {
            case LayoutAlignment.LEFT:
                setLeftMargin(params, (int) (x - mBackground.getPaddingLeft()));
                break;
            case LayoutAlignment.HORIZONTAL_CENTER:
                setLeftMargin(params, (int) (x - mBackground.getCenterX()));
                break;
            case LayoutAlignment.RIGHT:
                setLeftMargin(params, (int) (x - mBackground.getPaddingLeft() - mBackground.getRadius() * 2));
                break;
            default:
                setLeftMargin(params, (int) (x - mBackground.getPaddingLeft()));
                break;
        }

        int verticalGravity = gravity & LayoutAlignment.VERTICAL_CENTER;

        switch (verticalGravity) {
            case LayoutAlignment.TOP:
                setTopMargin(params, (int) (y - mBackground.getPaddingTop()));
                break;
            case LayoutAlignment.VERTICAL_CENTER:
                setTopMargin(params, (int) (y - mBackground.getCenterY()));
                break;
            case LayoutAlignment.BOTTOM:
                setTopMargin(params, (int) (y - mBackground.getPaddingTop() - mBackground.getRadius() * 2));
                break;
            default:
                setTopMargin(params, (int) (y - mBackground.getPaddingTop()));
                break;
        }
        setLayoutConfig(params);
    }

    private void setLeftMargin(ComponentContainer.LayoutConfig params, int value) {
        params.setMarginLeft(value);
    }

    private void setTopMargin(ComponentContainer.LayoutConfig params, int value) {
        params.setMarginTop(value);
    }

    /**
     * Remove this button from parent view.
     */
    public void dismiss() {
        if (getComponentParent() != null)
            getComponentParent().removeComponent(this);
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int i, int i1) {
            setEstimatedSize(mBackground.getIntrinsicWidth(), mBackground.getIntrinsicHeight());
            return false;
        }
    };

    LayoutRefreshedListener refreshedListener = new LayoutRefreshedListener() {
        @Override
        public void onRefreshed(Component component) {
            mBackground.setBounds(0, 0, component.getWidth(), component.getHeight());

            if (mIcon != null) {
                float half = mIconSize / 2f;
                mIcon.setBounds((int) (getWidth() / 2 - half), (int) (getHeight() / 2 - half), (int) (getWidth() / 2 + half), (int) (getHeight() / 2 + half));
            }

            if (mPrevIcon != null) {
                float half = mIconSize / 2f;
                mPrevIcon.setBounds((int) (getWidth() / 2 - half), (int) (getHeight() / 2 - half), (int) (getWidth() / 2 + half), (int) (getHeight() / 2 + half));
            }
        }
    };

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mBackground.drawToCanvas(canvas);
        canvas.restore();

        if (mPrevIcon != null) {
            mPrevIcon.drawToCanvas(canvas);
        }
        if (mIcon != null) {
            mIcon.drawToCanvas(canvas);
        }
    }

    protected RippleManager getRippleManager() {
        if (mRippleManager == null) {
            synchronized (RippleManager.class) {
                if (mRippleManager == null)
                    mRippleManager = new RippleManager();
            }
        }

        return mRippleManager;
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        RippleManager rippleManager = getRippleManager();
        if (l == rippleManager) {
            super.setClickedListener(l);
        } else {
            rippleManager.setOnClickListener(l);
            setClickedListener(rippleManager);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int[] location = component.getLocationOnScreen();
        float x = point.getX() - location[0];
        float y = point.getY() - location[1];
        if (action == TouchEvent.PRIMARY_POINT_DOWN && !mBackground.isPointerOver(x, y))
            return false;

        return getRippleManager().onTouchEvent(this, event);
    }

    class SwitchIconAnimator implements Runnable {

        boolean mRunning = false;
        long mStartTime;

        public void resetAnimation() {
            mStartTime = Time.getRealActiveTime();
            mIcon.setAlpha(0);
            mPrevIcon.setAlpha(255);
        }

        public boolean startAnimation(Element icon) {
            if (mIcon == icon)
                return false;

            mPrevIcon = mIcon;
            mIcon = icon;
            float half = mIconSize / 2f;
            mIcon.setBounds((int) (getWidth() / 2 - half), (int) (getHeight() / 2 - half), (int) (getWidth() / 2 + half), (int) (getHeight() / 2 + half));

            if (getHandler() != null) {
                resetAnimation();
                mRunning = true;
                getHandler().postTask(this, ViewUtil.FRAME_DURATION);
            } else {
                mPrevIcon.setCallback(null);
                mPrevIcon = null;
            }

            invalidate();
            return true;
        }

        public void stopAnimation() {
            mRunning = false;
            mPrevIcon.setCallback(null);
            mPrevIcon = null;
            mIcon.setAlpha(255);
            if (getHandler() != null)
                getHandler().removeTask(this);
            invalidate();
        }

        @Override
        public void run() {
            long curTime = Time.getRealActiveTime();
            float progress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);
            float value = mInterpolator.getInterpolation(progress);

            mIcon.setAlpha(Math.round(255 * value));
            mPrevIcon.setAlpha(Math.round(255 * (1f - value)));

            if (progress == 1f)
                stopAnimation();

            if (mRunning) {
                if (getHandler() != null)
                    getHandler().postTask(this, ViewUtil.FRAME_DURATION);
                else
                    stopAnimation();
            }

            invalidate();
        }

    }

    private EventHandler getHandler() {
        return mHandler;
    }
}
